class RescueFrom
  attr_reader :code_proc

  def initialize(code_proc)
    @code_proc = code_proc
  end

  def call(obj, args, ctx)
    code_proc.call(obj, args, ctx)
  rescue FormattedError, ValidationError => e
    GraphQL::ExecutionError.new(e.message, options: e.options)
  rescue StandardError, CustomError, UserReadableError => e
    #Utility.trigger_rollbar(e)
    GraphQL::ExecutionError.new(e.message)
  end
end
