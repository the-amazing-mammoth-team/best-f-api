# frozen_string_literal: true

source 'https://rubygems.org'

git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.7.5'

gem 'activeadmin'
gem 'active_material' # clean this after not being used
gem 'active_bootstrap_skin'
gem "aws-sdk-s3", require: false
gem 'bootsnap', '>= 1.4.2', require: false
gem 'bootstrap', '~> 4.4.1'
gem 'cancancan'
gem 'candy_check'
gem 'devise', '~> 4.8.1'
gem 'graphql', '~> 1.13.10'
gem 'graphiql-rails'
gem 'haml'
gem 'hirb'
gem 'jbuilder', '~> 2.7'
gem 'jquery-rails'
gem 'jwt'
gem 'mixpanel-ruby'
gem 'omniauth-facebook'
gem 'omniauth-google-oauth2'
gem 'omniauth-rails_csrf_protection'
gem 'paper_trail'
gem 'pg', '>= 0.18', '< 2.0'
gem 'puma', '~> 4.1'
gem 'pry', '~> 0.12.2'
gem 'pry-byebug'
gem 'rails', '~> 6.0.2', '>= 6.0.2.1'
gem 'rubocop'
gem 'sass-rails', '>= 6'
gem 'sentry-raven'
gem 'sidekiq'
gem 'stripe'
gem 'traco'
gem 'turbolinks', '~> 5'
gem 'webpacker'
# Use Active Model has_secure_password
# gem 'bcrypt', '~> 3.1.7'
# Reduces boot times through caching; required in config/boot.rb

group :development, :test do
  gem 'annotate'
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem "capistrano", "~> 3.14", require: false
  gem 'factory_bot'
  gem 'factory_bot_rails'
  gem 'faker'  
  gem 'rspec-rails'
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'web-console', '>= 3.3.0'
end

group :test do
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '>= 2.15'
  gem 'selenium-webdriver'
  gem 'shoulda-matchers' # very useful for validations in models
  # Easy installation and use of web drivers to run system tests with browsers
  gem 'webdrivers'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
