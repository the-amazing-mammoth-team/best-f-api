Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  Raven.configure do |config|
    config.dsn = 'https://a51bb02130a8440fb6b0c4e06c97d9c2@o335134.ingest.sentry.io/5354846'#ENV.fetch('SENTRY_DSN')
  end

  config.active_job.queue_adapter = :sidekiq
  config.hosts << "backend.mhunters.com"

  Sidekiq.configure_server do |config|
    config.redis = { url: "redis://bfapi_bfapi-redis_1:6379" }
  end

  Sidekiq.configure_client do |config|
    config.redis = { url: "redis://bfapi_bfapi-redis_1:6379" }
  end

  # Do not eager load code on boot.
  config.eager_load = false

  # Show full error reports.
  config.consider_all_requests_local = true

  # Enable/disable caching. By default caching is disabled.
  # Run rails dev:cache to toggle caching.
  if Rails.root.join('tmp', 'caching-dev.txt').exist?
    config.action_controller.perform_caching = true
    config.action_controller.enable_fragment_cache_logging = true

    config.cache_store = :memory_store
    config.public_file_server.headers = {
      'Cache-Control' => "public, max-age=#{2.days.to_i}"
    }
  else
    config.action_controller.perform_caching = false

    config.cache_store = :null_store
  end

  # Store uploaded files on the local file system (see config/storage.yml for options).
  config.active_storage.service = :amazon_dev

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = true

  # config.action_mailer.smtp_settings = {
  #   address: "smtp.mailtrap.io",
  #   port: "465",
  #   domain: "mailtrap.io",
  #   authentication: "login",
  #   user_name: "b2366c7d1989cf",
  #   password: "2858cc25cfa89c",
  #   enable_starttls_auto: true
  # }
  config.action_mailer.smtp_settings = {
    :address              => "smtp.gmail.com",
    :port                 => 587,
    :user_name            => 'pablo@mhunters.com',
    :password             => 'esmagnifico1313',
    :authentication       => "plain",
    :enable_starttls_auto => true
    }

  # Send email in development mode.
  config.action_mailer.perform_deliveries    = true

  config.action_mailer.perform_caching = false

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise an error on page load if there are pending migrations.
  config.active_record.migration_error = :page_load

  # Highlight code that triggered database queries in logs.
  config.active_record.verbose_query_logs = true

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.debug = true

  # Suppress logger output for asset requests.
  config.assets.quiet = true

  # Raises error for missing translations.
  # config.action_view.raise_on_missing_translations = true

  # Use an evented file watcher to asynchronously detect changes in source code,
  # routes, locales, etc. This feature depends on the listen gem.
  config.file_watcher = ActiveSupport::EventedFileUpdateChecker

  config.action_mailer.default_url_options = { :host => 'localhost:3000' }

end
