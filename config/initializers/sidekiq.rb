Sidekiq.configure_server do |config|
  config.redis = { url: "redis://bfapi-redis:6379" }
end

Sidekiq.configure_client do |config|
  config.redis = { url: "redis://bfapi-redis:6379" }
end

# Sidekiq::Extensions.enable_delay!

# REDIS_URL  crm-assistant_crm-assistant-redis_1