Rails.application.routes.draw do
  require 'sidekiq/web'

  root to: 'onboarding#diagnostic'

  namespace :onboarding do
    get 'diagnostic', action: 'diagnostic'
    post 'sign_up_options'
    post 'new_sign_up'  #it has this posts as it needs to pass the user info
    post 'sign_up'
    get 'diagnostic_results'
    get 'suggested_programs'
    get 'program_detail/:program_id', action: 'program_detail'
    get 'choose_subscription/:program_id', action: 'choose_subscription'
    get 'pay_selected_plan', action: 'pay_selected_plan'
    post 'subscribe_stripe', action: 'subscribe_stripe'
  end

  namespace :buy_subscription do
    get 'offer_login'
    get 'suggested_programs'
    get 'more_programs'
    get 'program_detail'
    get 'choose_subscription'
    get 'payment_page'
    post 'subscribe_stripe'
    get 'lite_option'
    get 'thank_you_page'
    get 'upsale'
  end

 
  if Rails.env.development?
    mount GraphiQL::Rails::Engine, at: '/graphiql', graphql_path: '/graphql'
  end
  post '/graphql', to: 'graphql#execute'
  # devise_for :users, ActiveAdmin::Devise.config
  # ActiveAdmin.routes(self)
  devise_for :users, controllers: {
    sessions: 'users/sessions',
    omniauth_callbacks: 'users/omniauth'
  }

  authenticate :user, -> (u) { u.is_admin? } do
    mount Sidekiq::Web => '/admin/sidekiq'
  end
  devise_for :admin_users, {class_name: 'User'}.merge(ActiveAdmin::Devise.config)
  ActiveAdmin.routes(self)

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  get 'unauthorized', to: 'errors#unauthorized'
  # onboarding
  
end
