#!/bin/bash

set -e

cd /mhunters-api

bundle exec rails db:create db:schema:load

exec "$@"
