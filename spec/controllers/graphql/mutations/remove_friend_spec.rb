require 'rails_helper'

RSpec.describe 'remove_friend Mutation', type: :request do
  
  def remove_friend_mutation(friend_id:)
    <<~GQL
       mutation {
        removeFriend(input: { friendId: #{friend_id} }) 
      }
    GQL
  end

  context '#remove_friend' do

    let!(:user) { FactoryBot.create(:user, email: 'pablo@mhunters.com', password: '123password', sign_in_count: 1) } 
    let!(:user2) { FactoryBot.create(:user, email: 'pablo23@mhunters.com', password: '123password', sign_in_count: 1) } 
    let!(:friendship) { FactoryBot.create(:friendship, user_id: user.id, friend_id: user2.id) }
    let!(:jwt) { User.authenticate({ email: user.email, password: '123password' }).jwt }

    context 'with a correct friend_id' do
      it 'deletes the friendship between both' do
        post('/graphql', params: { query: remove_friend_mutation(friend_id: user2.id ) }, headers: { 'Authorization' => jwt })
        remove = JSON.parse(response.body)["data"]["removeFriend"]
        expect(remove).to be_truthy
        expect(Friendship.count).to eq 0
      end
    end

    context 'with an incorrect friend_id' do
      it 'doesnt delete a friendship' do
        post('/graphql', params: { query: remove_friend_mutation(friend_id: user2.id + 6) }, headers: { 'Authorization' => jwt })
        remove = JSON.parse(response.body)["data"]["removeFriend"]
        expect(remove).to be_falsey
        expect(Friendship.count).to eq 1
      end
    end
  end

end