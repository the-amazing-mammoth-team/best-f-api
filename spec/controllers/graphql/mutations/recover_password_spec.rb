require 'rails_helper'

RSpec.describe 'recover password Mutation', type: :request do
  
  def recover_password_mutation(email:)
    <<~GQL
       mutation {
        recoverPassword(input: { email: "#{email}" })
      }
    GQL
  end

  context '#recoverPassword' do

    let!(:user) { FactoryBot.create(:user, email: 'pablo@mhunters.com', password: '123password', sign_in_count: 1)} 
  
    context 'with correct email' do
      it 'returns true if succesful' do
        post('/graphql', params: { query: recover_password_mutation(email: user.email ) })
      end
    end
  
  end

end