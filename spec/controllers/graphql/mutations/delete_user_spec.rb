require 'rails_helper'

RSpec.describe 'delete user Mutation', type: :request do
  
  def delete_user_mutation(user_id:)
    <<~GQL
      mutation {
        deleteUser(input: {userId: #{user_id}})
      }
    GQL
  end

  let!(:user) { FactoryBot.create(:user, email: 'pablo@mhunters.com', password: '123password', sign_in_count: 1) } 
  let!(:user2) { FactoryBot.create(:user, email: 'pablo23@mhunters.com', password: '123password', sign_in_count: 1) } 
  let!(:friendship) { FactoryBot.create(:friendship, user_id: user.id, friend_id: user2.id) }
  let!(:jwt) { User.authenticate({ email: user.email, password: '123password' }).jwt }

  context 'correct id and correct user' do
    it 'deletes the user sending true' do
      post('/graphql', params: { query: delete_user_mutation(user_id: user.id ) }, headers: { 'Authorization' => jwt })
      res = JSON.parse(response.body)['data']['deleteUser']
      expect(res).to be_truthy
      expect(User.count).to eq 1
    end
  end

  context 'correct id and correct user' do
    it 'doesnt delete the user sending false' do
      post('/graphql', params: { query: delete_user_mutation(user_id: user2.id ) }, headers: { 'Authorization' => jwt })
      res = JSON.parse(response.body)['data']['deleteUser']
      expect(res).to be_falsey
      expect(User.count).to eq 2
    end
  end

end
