require 'rails_helper'

RSpec.describe 'program feedback', type: :request do
  
  def program_feedback_mutation(user_program_id:, enjoyment_notes:, enjoyment:)
    <<~GQL
       mutation {
        programFeedback(input: { userProgramId: #{user_program_id}, enjoymentNotes: "#{enjoyment_notes}", enjoyment: "#{enjoyment}" }) {
          id
          enjoymentNotes
          enjoyment
        }
      }
    GQL
  end
  
  context '#call' do
    let!(:user) { FactoryBot.create(:user, password: '123password') }
    let!(:program) { FactoryBot.create(:program, user: user) }
    let!(:program2) { FactoryBot.create(:program, user: user) }
    let!(:jwt) { User.authenticate({ email: user.email, password: '123password' }).jwt }
    let!(:user_program) {FactoryBot.create(:user_program, user: user, program: program)}

    it 'saves the data sent by the user' do
      post('/graphql', params: { query: program_feedback_mutation(user_program_id: user_program.id, enjoyment_notes: 'nice', enjoyment: 'i hate it') }, headers: { 'Authorization' => jwt })
      user_program = JSON.parse(response.body)['data']['programFeedback']
      expect(user_program['enjoymentNotes']).to eq 'nice'
      expect(user_program['enjoyment']).to eq 'i hate it'
    end


  end

end