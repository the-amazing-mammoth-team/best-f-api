require 'rails_helper'

RSpec.describe 'login Mutation', type: :request do
  
  def login_mutation(email:, password:, facebook_uid:, google_uid:, apple_id_token:)
    <<~GQL
       mutation {
        login(input: { email: "#{email}", password: "#{password}", facebookUid: "#{facebook_uid}", googleUid: "#{google_uid}", appleIdToken: "#{apple_id_token}" }) {
          id
          jwt
        }
      }
    GQL
  end

  context '#login' do

    let!(:facebook_uid) { '12345uid' }
    let!(:google_uid) { '333332222kkkk' }
    let!(:apple_id_token) { 'appleidtoken' }
    let!(:user) { FactoryBot.create(:user, email: 'pablo@mhunters.com', password: '123password', sign_in_count: 1, facebook_uid: facebook_uid, google_uid: nil)} 
    let!(:user2) { FactoryBot.create(:user, email: 'pablo23@mhunters.com', password: '123password', sign_in_count: 1, google_uid: google_uid, facebook_uid: nil)} 
    let!(:user3) { FactoryBot.create(:user, email: 'pablo234@mhunters.com', password: '123password', sign_in_count: 1, google_uid: nil, facebook_uid: nil, apple_id_token: apple_id_token)} 

    context 'with correct credentials' do
      it 'returns a user with jwt' do
        post('/graphql', params: { query: login_mutation(email: user.email, password: '123password', facebook_uid: nil, google_uid: nil, apple_id_token: nil ) })
        logged_user  =  JSON.parse(response.body)["data"]["login"]
        expect(logged_user['id'].to_i).to eq user.id
        expect(logged_user['jwt'].present?).to be_truthy
      end
    end

    context 'with incorrect password credentials' do
      it 'returns graphql errors' do
        post('/graphql', params: { query: login_mutation(email: user.email, password: 'asdsda', facebook_uid: nil, google_uid: nil, apple_id_token: nil ) })
        errors =  JSON.parse(response.body)["errors"]
        expect(errors.present?).to be_truthy
        expect(errors.first['message']).to eq 'User wrong password'
      end
    end

    context 'with incorrect email credentials' do
      it 'returns graphql errors' do
        post('/graphql', params: { query: login_mutation(email: 'dsadsa@gmail.com', password: 'asdsda', facebook_uid: nil, google_uid: nil, apple_id_token: nil ) })
        errors =  JSON.parse(response.body)["errors"]
        expect(errors.present?).to be_truthy
        expect(errors.first['message']).to eq 'user not in the db'
      end
    end

    context 'with correct facebook uid' do
      it 'returns the user with jwt' do
        post('/graphql', params: { query: login_mutation(email: nil, password: nil, facebook_uid: facebook_uid, google_uid: nil, apple_id_token: nil ) })
        logged_user  =  JSON.parse(response.body)["data"]["login"]
        expect(logged_user['id'].to_i).to eq user.id
        expect(logged_user['jwt'].present?).to be_truthy
      end
    end

    context 'with correct google uid' do
      it 'returns the user with jwt' do
        post('/graphql', params: { query: login_mutation(email: nil, password: nil, facebook_uid: nil, google_uid: google_uid , apple_id_token: nil) })
        logged_user  =  JSON.parse(response.body)["data"]["login"]
        expect(logged_user['id'].to_i).to eq user2.id
        expect(logged_user['jwt'].present?).to be_truthy
      end
    end

    context 'with correct apple id' do
      it 'returns the user with jwt' do
        post('/graphql', params: { query: login_mutation(email: nil, password: nil, facebook_uid: nil, google_uid: nil, apple_id_token: apple_id_token ) })
        logged_user  =  JSON.parse(response.body)["data"]["login"]
        expect(logged_user['id'].to_i).to eq user3.id
        expect(logged_user['jwt'].present?).to be_truthy
      end
    end

    context 'with incorrect facebook uid' do
      it 'returns graphql errors' do
        post('/graphql', params: { query: login_mutation(email: nil, password: nil, facebook_uid: 'bliblibl12', google_uid: nil , apple_id_token: nil) })
        errors =  JSON.parse(response.body)["errors"]
        expect(errors.present?).to be_truthy
        expect(errors.first['message']).to eq 'user not in the db'
      end
    end

    context 'with incorrect google uid' do
      it 'returns graphql errors' do
        post('/graphql', params: { query: login_mutation(email: nil, password: nil, facebook_uid: nil, google_uid: 'bliblibl12', apple_id_token: nil ) })
        errors =  JSON.parse(response.body)["errors"]
        expect(errors.present?).to be_truthy
        expect(errors.first['message']).to eq 'user not in the db'
      end
    end

  end
end
