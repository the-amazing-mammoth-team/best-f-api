require 'rails_helper'

RSpec.describe 'update password for user Mutation', type: :request do
  
  def update_password_mutation(email:, code:, password:, old_password: nil)
    if code.present?
      <<~GQL
        mutation {
          updatePassword(input: { email: "#{email}", code: #{code}, password: "#{password}", oldPassword: "#{old_password}" }){
            id
            jwt
          }
        }
      GQL
    else
      <<~GQL
       mutation {
        updatePassword(input: { email: "#{email}", password: "#{password}", oldPassword: "#{old_password}" }){
          id
          jwt
          }
        }
      GQL
    end
  end

  context '#updatePassword' do

    let!(:user) { FactoryBot.create(:user, email: 'pablo@mhunters.com', password: '123password', sign_in_count: 1, recover_password_code: 123456)} 
  
    context 'with correct email' do
      it 'returns an authenticated user if succesful' do
        post('/graphql', params: { query: update_password_mutation(email: user.email, code: 123456, password: 'esmagnifico1313') })
        user = JSON.parse(response.body)["data"]["updatePassword"]
        expect(user['id'].present?).to be_truthy
        expect(user['jwt'].present?).to be_truthy
      end
    end

    context 'with incorrect email' do
      it 'returns an error' do
        post('/graphql', params: { query: update_password_mutation(email: 'nada@email.com', code: 123456, password: 'esmagnifico1313') })
        errors = JSON.parse(response.body)["errors"]
        expect(errors.first['message']).to eq 'user not found'
      end
    end

    context 'with correct old_passwword' do
      it 'updates the password' do
        post('/graphql', params: { query: update_password_mutation(email: user.email, code: nil, password: 'esmagnifico1313', old_password: '123password') })
        user = JSON.parse(response.body)["data"]["updatePassword"]
        expect(user['id'].present?).to be_truthy
        expect(user['jwt'].present?).to be_truthy
      end
    end

    context 'with incorrect old_passwword' do
      it 'updates the password' do
        post('/graphql', params: { query: update_password_mutation(email: user.email, code: nil, password: 'esmagnifico1313', old_password: '123passwordsdsd') })
        errors = JSON.parse(response.body)["errors"]
        expect(errors.first['message']).to eq 'User wrong password'
      end
    end
  
  end

end