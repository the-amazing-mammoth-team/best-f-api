require 'rails_helper'

RSpec.describe 'add_friend Mutation', type: :request do
  
  def add_friend_mutation(friend_id:)
    <<~GQL
       mutation {
        addFriend(input: { friendId: #{friend_id} }) {
          id
          names
          email
        }
      }
    GQL
  end

  context '#add_friend' do

    let!(:user) { FactoryBot.create(:user, email: 'pablo@mhunters.com', password: '123password', sign_in_count: 1) } 
    let!(:user2) { FactoryBot.create(:user, email: 'pablo23@mhunters.com', password: '123password', sign_in_count: 1) } 
    let!(:jwt) { User.authenticate({ email: user.email, password: '123password' }).jwt }

    context 'with a correct friend_id' do
      it 'creates a friendship between both' do
        post('/graphql', params: { query: add_friend_mutation(friend_id: user2.id ) }, headers: { 'Authorization' => jwt })
        friend = JSON.parse(response.body)["data"]["addFriend"]
        expect(Friendship.count).to eq 1
        expect(friend['id'].to_i).to eq user2.id
      end
    end

    context 'with an incorrect friend_id' do
      it 'doesnt create a friendship' do
        post('/graphql', params: { query: add_friend_mutation(friend_id: (user2.id + 2 ) ) }, headers: { 'Authorization' => jwt })
        expect(Friendship.count).to eq 0
      end
    end
  end

end