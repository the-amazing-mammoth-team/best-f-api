require 'rails_helper'

RSpec.describe 'save workout Mutation', type: :request do
  
  def save_session_execution_mutation(session_id:, program_id:, exercises:)
    <<~GQL
      mutation {
        saveSessionExecution(input: 
          { sessionExecution: 
            { frontEndId: "5a63c546-d2ef-11ea-87d0-0242ac130003", sessionId: #{session_id}, programId: #{program_id},
              sessionBlockExecutions: [
                { order: 1, sessionSetExecutions: 
                  [ { order: 1, exerciseExecutions: [{exerciseId: #{exercises.first.id}, order: 1, repsExecuted: 30, executionTime: 40}]}, 
                    { order: 2, exerciseExecutions: [{exerciseId: #{exercises.second.id}, order: 1, repsExecuted: 20, executionTime: 30}]}] 
                }, 
                { order: 2, sessionSetExecutions: 
                  [ { order: 1, exerciseExecutions: [{exerciseId: #{exercises.first.id}, order: 1, repsExecuted: 30, executionTime: 40}]}, 
                    { order: 2, exerciseExecutions: [{exerciseId: #{exercises.second.id}, order: 1, repsExecuted: 20, executionTime: 30}]}]
                }
              ]
            }
          }
        ){
          id
        }
      }
    GQL
  end

  context '#saveSessionExecution' do

    let!(:user) { FactoryBot.create(:user, email: 'pablo@mhunters.com', password: '123password', sign_in_count: 1)} 
    let!(:jwt) { User.authenticate({ email: user.email, password: '123password' }).jwt }
    let!(:programs) { FactoryBot.create_list(:program, 2, user: user, available: true) }
    let!(:sessions) { FactoryBot.create_list(:session, 2) }
    let!(:session_block) { FactoryBot.create(:session_block, session: sessions.first)}
    let!(:program_session) { FactoryBot.create(:program_session, program: programs.first, session: sessions.first) }
    let!(:program_session2) { FactoryBot.create(:program_session, program: programs.first, session: sessions.second) }
    let!(:session_sets) { FactoryBot.create_list(:session_set, 2, session_block: session_block) }
    let!(:exercises) { FactoryBot.create_list(:exercise, 2) }
    let!(:exercise_set_1) { FactoryBot.create(:exercise_set, exercise: exercises.first, session_set: session_sets.first) }
    let!(:exercise_set_2) { FactoryBot.create(:exercise_set, exercise: exercises.second, session_set: session_sets.second) }
    # set user program
    let!(:user_program) { FactoryBot.create(:user_program, user_id: user.id, program_id: programs.first.id)} 
    let!(:crunch) {FactoryBot.create(:exercise, name: 'crunch')}
    let!(:pull_up) {FactoryBot.create(:exercise, name: 'pull up')}

    context 'authenticated user with saving a session with 2 sets and 1 exercises for each set' do
      it 'saves the execution of the session with every set and exercise' do
        post('/graphql', params: { query: save_session_execution_mutation(session_id: sessions.first.id, program_id: programs.first.id, exercises: exercises) }, headers: { 'Authorization' => jwt }) 
        session_execution = JSON.parse(response.body)["data"]["saveSessionExecution"]
        expect(session_execution['id'].present?).to be_truthy
        expect(SessionExecution.count).to eq 1
        expect(SessionBlockExecution.count).to eq 2
        expect(SessionSetExecution.count).to eq 4
        expect(ExerciseExecution.count).to eq 4
      end
    end

    context 'with wrong data should raise error' do
      it 'saves the execution of the session with every set and exercise' do
        post('/graphql', params: { query: save_session_execution_mutation(session_id: sessions.first.id, program_id: programs.last.id, exercises: exercises) }, headers: { 'Authorization' => jwt }) 
        errors = JSON.parse(response.body)["errors"]
        expect(errors.present?).to be_truthy
      end

    end

  end

end

