require 'rails_helper'

RSpec.describe 'Restart Program Mutation', type: :request do

   def restart_program_mutation(user_id: )
    <<~GQL
      mutation {
        restartProgram(input: {userId: #{user_id}}) {
          id
          currentSession {
            id
          }
          userProgram {
            id
            programSessionsCount
            progress
          }
        }
      }
    GQL
  end

  let!(:admin_user) { FactoryBot.create(:user, email: 'pablo@mhunters.com', password: '123password', sign_in_count: 1)} 
  let!(:programs) {FactoryBot.create_list(:program, 5, user: user, available: true)}
  let!(:sessions) { FactoryBot.create_list(:session, 2) }
  let!(:program_session) { FactoryBot.create(:program_session, program: programs.first, session: sessions.first) }
  let!(:program_session2) { FactoryBot.create(:program_session, program: programs.first, session: sessions.second) }
  let!(:apple_3_months) {FactoryBot.create(:product, store: :apple, price: 33)}
  let!(:apple_1_year) {FactoryBot.create(:product, store: :apple, price: 100)}
  let!(:google_1_year) {FactoryBot.create(:product, store: :google_play, price: 80)}
  let!(:program_pro) { FactoryBot.create(:program, code_name: 'pro_automatic', pro: true, available:  true) }
  let!(:user) { FactoryBot.create(:user, email: 'pablo2@mhunters.com', password: '123password', sign_in_count: 1)} 
  let!(:jwt) { User.authenticate({ email: user.email, password: '123password' }).jwt }
  let!(:exercise_names) { [:push_up, :squat, :burpee, :plank_pivot, :rest, :semi_burpee, :knee_push_up, :pull_up, :run]}

  context 'restart program' do

    before(:each) do
      exercise_names.each { |exercise_name| FactoryBot.create(:exercise, exercise_name) }
      program = programs.first
      session = FactoryBot.create(:session)
      program.sessions << session
      # subscribe user to the current program
      user_program = SetUserProgram.new(user: user, program_id: program.id).call
      # add session executions to the user
      session_execution = SessionExecution.create(user_program: user_program, session: sessions.first)
    end

    it 'restarts the current user program' do
      post('/graphql', params: { query: restart_program_mutation(user_id: user.id) }, headers: { 'Authorization' => jwt })
      user_data = JSON.parse(response.body)['data']['restartProgram']
      current_session = user_data['currentSession']
      user_program = user_data['userProgram']
      expect(current_session['id'].to_i).to eq programs.first.sessions.first.id
      expect(user_program['programSessionsCount']).to eq 3
      expect(user_program['progress']).to eq 0
    end

  end
end