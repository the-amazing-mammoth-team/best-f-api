require 'rails_helper'

RSpec.describe 'SetCurrentUserProgram', type: :request do

  def set_current_user_program_mutation(program_id:)
    <<~GQL
      mutation {
        setCurrentUserProgram(input: {programId: #{program_id}})
      }
    GQL
  end

  context '#call' do
    let!(:user) { FactoryBot.create(:user, password: '123password') }
    let!(:program) { FactoryBot.create(:program, user: user) }
    let!(:program2) { FactoryBot.create(:program, user: user) }
    let!(:program3) { FactoryBot.create(:program, user: user, code_name: 'MH_UNN') }

    let!(:jwt) { User.authenticate({ email: user.email, password: '123password' }).jwt }

    before(:each) do
      setup_exercises
    end

    context 'setting unnegotiable' do
      it 'sets the current program as the pro_auto' do
        post('/graphql', params: { query: set_current_user_program_mutation(program_id: program3.id) }, headers: { 'Authorization' => jwt }) 
        expect(user.current_program.code_name).to eq 'pro_auto'
      end
    end

    context '#setting a first user program' do
      it 'sets the current program of the user' do
        post('/graphql', params: { query: set_current_user_program_mutation(program_id: program2.id) }, headers: { 'Authorization' => jwt }) 
        expect(user.current_program).to eq program2
      end
    end

     context '#choosing a new program' do
      it 'sets a new one as the current program of the user' do
        FactoryBot.create(:user_program, active: true, user: user, program: program)
        post('/graphql', params: { query: set_current_user_program_mutation(program_id: program2.id) }, headers: { 'Authorization' => jwt }) 
        expect(user.current_program).to eq program2
        expect(user.user_programs.count).to eq 2
      end
    end

  end
end