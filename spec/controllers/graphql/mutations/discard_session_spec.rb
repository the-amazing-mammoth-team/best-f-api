require 'rails_helper'

RSpec.describe 'discard session Mutation', type: :request do
  
  def discard_session_mutation(session_id:, program_id:, discard_reason:)
    <<~GQL
      mutation {
        discardSession(input: {sessionId: #{session_id}, programId: #{program_id}, discardReason: "#{discard_reason}"}) {
          id
          discarded
          discardReason
        }
      }
    GQL
  end

  let!(:user) { FactoryBot.create(:user, email: 'pablo@mhunters.com', password: '123password', sign_in_count: 1)} 
  let!(:jwt) { User.authenticate({ email: user.email, password: '123password' }).jwt }
  let!(:programs) { FactoryBot.create_list(:program, 2, user: user, available: true) }
  let!(:sessions) { FactoryBot.create_list(:session, 2) }
  let!(:session_block) { FactoryBot.create(:session_block, session: sessions.first)}
  let!(:program_session) { FactoryBot.create(:program_session, program: programs.first, session: sessions.first) }
  let!(:program_session2) { FactoryBot.create(:program_session, program: programs.first, session: sessions.second) }
  let!(:session_sets) { FactoryBot.create_list(:session_set, 2, session_block: session_block) }
  let!(:exercises) { FactoryBot.create_list(:exercise, 2) }
  let!(:exercise_set_1) { FactoryBot.create(:exercise_set, exercise: exercises.first, session_set: session_sets.first) }
  let!(:exercise_set_2) { FactoryBot.create(:exercise_set, exercise: exercises.second, session_set: session_sets.second) }
  # set user program
  let!(:user_program) { FactoryBot.create(:user_program, user_id: user.id, program_id: programs.first.id)} 
  let!(:crunch) {FactoryBot.create(:exercise, name: 'crunch')}
  let!(:pull_up) {FactoryBot.create(:exercise, name: 'pull up')}

  context 'discard' do
    it 'sets the session execution as discarded' do
      post('/graphql', params: { query: discard_session_mutation(session_id: sessions.first.id, program_id: programs.first.id, discard_reason: 'other') }, headers: { 'Authorization' => jwt })
      session_execution = JSON.parse(response.body)["data"]["discardSession"]
      expect(session_execution['discarded']).to be_truthy
      expect(session_execution['discardReason']).to eq 'other'
    end
  end



end
  