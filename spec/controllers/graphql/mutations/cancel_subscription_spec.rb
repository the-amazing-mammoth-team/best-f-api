require 'rails_helper'

RSpec.describe 'cancel subscription mutation', type: :request do
  
  def cancel_subscription_mutation
    <<~GQL
       mutation {
        cancelSubscription(input: {cancellationReason: "something"}) {
          id
          cancelled
          cancellationReason
        }
      }
    GQL
  end

  let!(:admin) { FactoryBot.create(:user, is_admin: true) }
  let!(:program) { FactoryBot.create(:program, user: admin) }
  let!(:product) { FactoryBot.create(:product) }
  let!(:user) { FactoryBot.create(:user) }
  let!(:user2) { FactoryBot.create(:user) }
  let!(:user_subscription) { FactoryBot.create(:subscription, user: user, program: program, product: product, status: :active) }
  let!(:jwt) { User.authenticate({ email: user.email, password: 'mammothhunter1234' }).jwt }

  context '#call' do

    it 'cancels the current subscription of the user' do
      post('/graphql', params: { query: cancel_subscription_mutation }, headers: { 'Authorization' => jwt }) 
      cancelled_subscription = JSON.parse(response.body)["data"]['cancelSubscription']
      expect(cancelled_subscription['cancelled']).to be_truthy
      expect(cancelled_subscription['cancellationReason']).to eq 'something'
    end

  end
end
