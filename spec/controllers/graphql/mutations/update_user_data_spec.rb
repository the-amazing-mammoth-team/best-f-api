require 'rails_helper'

RSpec.describe 'update data for user Mutation', type: :request do
  
  def update_user_data_mutation(args: , implement_ids:)
    <<~GQL
       mutation {
        updateUserData(input: { userData:{ email: "#{args[:email]}", scientificDataUsage: #{args[:scientific_data_usage]} }, implementIds: #{implement_ids}}){
          id
          jwt
          email
          scientificDataUsage
          implements {
            id
          }
        }
      }
    GQL
  end

  context '#updatePassword' do

    let!(:user) { FactoryBot.create(:user, email: 'pablo@mhunters.com', password: '123password', sign_in_count: 1, recover_password_code: 123456)}
    let!(:jwt) { User.authenticate({ email: user.email, password: '123password' }).jwt }
    let!(:implements) { FactoryBot.create_list(:implement, 2)}

    let(:args) { { email: 'newpablito@gmail.com', scientific_data_usage: true }}
  
    context 'with correct email' do
      it 'returns an authenticated user if succesful' do
        post('/graphql', params: { query: update_user_data_mutation(args: args, implement_ids: implements.pluck(:id)) }, headers: { 'Authorization' => jwt })
        user = JSON.parse(response.body)["data"]["updateUserData"]
        expect(user['id'].present?).to be_truthy
        expect(user['email']).to eq 'newpablito@gmail.com'
        expect(user['implements'].count).to eq 2
        expect(user['scientificDataUsage']).to eq true 
      end
    end

     context 'with deleted implements' do
      it 'updates the elements deleted' do
        post('/graphql', params: { query: update_user_data_mutation(args: args, implement_ids: implements.pluck(:id)) }, headers: { 'Authorization' => jwt })
        sleep 1
        post('/graphql', params: { query: update_user_data_mutation(args: args, implement_ids: [implements.first.id] ) }, headers: { 'Authorization' => jwt })
        user = JSON.parse(response.body)["data"]["updateUserData"]
        expect(user['id'].present?).to be_truthy
        expect(user['email']).to eq 'newpablito@gmail.com'
        expect(user['implements'].count).to eq 1
        expect(user['scientificDataUsage']).to eq true 
      end
    end
  
  end

end