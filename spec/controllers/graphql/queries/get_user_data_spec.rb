require 'rails_helper'

RSpec.describe 'user data query', type: :request do
  
  def get_user_data_query(locale: :es)
    <<~GQL
       query {
        getUserData(locale: "#{locale}" ) {
          id
          names
          lastName
          gender
          points
          totalSessions                
          totalTime                    
          kcalPerSession
          repsPerSession              
          achievments {
            id
          }
          level {
            id
          }
          nextLevel
          {
            id
          }
          trainingDaysSetting
          bestWeeklyStreak
          currentWeeklyStreak
          notificationsSetting
          isPro
          warmupSession {
            id
          }
          weeklyExecutions {
            id
          } 
          userPrograms {
            id
          }
          userProgram {
            id
            progress
            programSessionsCount
            executedSessionsCount
            active 
            completed
            executedSessions {
              id
            }
            sessionExecutions {
              id
            }
          }
          currentProgram {
            id
            name
            warmupExercises{
              id
            }
            exercises{
              id
            }
            sessions{
              id
            }
          }
          currentSession {
            id
          }
          challenges {
            id
            sessions {
              id
            }
          }
          activeRests {
            id
            sessions {
              id
            }
          }

        }
      }
    GQL
  end

  context 'call' do

    let!(:user) { FactoryBot.create(:user, email: 'pablo@mhunters.com', password: '123password', sign_in_count: 1, 
                                    best_weekly_streak: 5, current_weekly_streak: 4, points: 200, training_days_setting: 4,
                                    total_sessions: 10, total_time: 3300, reps_per_session: 100, kcal_per_session: 120) } 
    let!(:user2) { FactoryBot.create(:user, email: 'pablo23@mhunters.com', password: '123password', sign_in_count: 1) } 
    let!(:jwt) { User.authenticate({ email: user.email, password: '123password' }).jwt }
    let!(:level1) { FactoryBot.create(:achievment, points: 100, achievment_type: 'level') }
    let!(:level2) { FactoryBot.create(:achievment, points: 500, achievment_type: 'level') }
    let!(:user_achievment) { FactoryBot.create(:user_achievment, user: user, achievment: level1) }

    before(:each) do
      setup_exercises
      setup_programs_and_user_programs(user)
      post('/graphql', params: { query: get_user_data_query }, headers: { 'Authorization' => jwt })
      @user_data = JSON.parse(response.body)["data"]["getUserData"]
    end
    
    it 'should get the user info' do
      expect(@user_data.present?)
      expect(@user_data['id'].to_i).to eq user.id
      expect(@user_data['names']).to eq user.names
      expect(@user_data['lastName']).to eq user.last_name
      expect(@user_data['gender']).to eq user.gender
      expect(@user_data['points'].to_i).to eq user.points
      expect(@user_data['weeklyExecutions']).to eq []
      expect(@user_data['trainingDaysSetting']).to eq 4
      expect(@user_data['bestWeeklyStreak']).to eq 5
      expect(@user_data['currentWeeklyStreak']).to eq 4
      expect(@user_data['notificationsSetting']).to be_truthy
      expect(@user_data['isPro']).to be_falsey
      expect(@user_data['level']['id'].to_i).to eq level1.id
      expect(@user_data['nextLevel']['id'].to_i).to eq level2.id
      expect(@user_data['achievments'].count).to eq 1
      expect(@user_data['achievments'].first['id'].to_i).to eq level1.id
      expect(@user_data['totalTime']).to eq 3300
      expect(@user_data['totalSessions']).to eq 10
      expect(@user_data['kcalPerSession']).to eq 120
      expect(@user_data['repsPerSession']).to eq 100
    end

    it 'should get the associated programs' do
      expect(@user_data['currentProgram']['id'].to_i).to eq user.current_program.id
      expect(@user_data['currentProgram']['name']).to eq user.current_program.name_es
      expect(@user_data['currentProgram']['exercises'].count).to eq 1
      expect(@user_data['currentProgram']['warmupExercises'].count).to eq 1
      expect(@user_data['currentProgram']['sessions'].count).to eq user.current_program.sessions.count
      expect(@user_data['currentSession']['id'].to_i).to eq user.current_session.id
      expect(@user_data['challenges']['id'].to_i).to eq user.challenges.id
      expect(@user_data['activeRests']['id'].to_i).to eq user.active_rests.id
      expect(@user_data['userPrograms'].count > 0).to be_truthy
    end

    it 'should translate the data' do
      post('/graphql', params: { query: get_user_data_query(locale: 'en') }, headers: { 'Authorization' => jwt })
      @user_data = JSON.parse(response.body)["data"]["getUserData"]
      expect(@user_data['currentProgram']['name']).to eq user.current_program.name_en
    end

    it 'should translate using the locale' do
      post('/graphql', params: { query: get_user_data_query(locale: :en) }, headers: { 'Authorization' => jwt })
      @user_data = JSON.parse(response.body)["data"]["getUserData"]
      expect(@user_data['currentProgram']['name']).to eq user.current_program.name_en
    end

    it 'should get the program progress info' do
      expect(@user_data['userProgram']['id'].to_i).to eq user.user_program.id
      expect(@user_data['userProgram']['progress'].to_f).to eq user.user_program.progress
      expect(@user_data['userProgram']['programSessionsCount'].to_i).to eq user.user_program.program_sessions_count
      expect(@user_data['userProgram']['executedSessionsCount'].to_i).to eq user.user_program.executed_sessions_count
      expect(@user_data['userProgram']['executedSessions']).to eq []
      expect(@user_data['userProgram']['sessionExecutions']).to eq []
      expect(@user_data['userProgram']['active']).to eq user.user_program.active
      expect(@user_data['userProgram']['completed']).to eq user.user_program.completed
      expect(@user_data['warmupSession']).to eq nil
    end
  end

end