require 'rails_helper'

RSpec.describe 'get session execution summary', type: :request do

  let!(:user) { FactoryBot.create(:user, email: 'pablo@mhunters.com', password: '123password', sign_in_count: 1)} 
  let!(:jwt) { User.authenticate({ email: user.email, password: '123password' }).jwt }
  let!(:programs) { FactoryBot.create_list(:program, 2, user: user, available: true, name_en: 'name en', name_es: 'name es') }
  let!(:sessions) { FactoryBot.create_list(:session, 2) }
  let!(:session_block) { FactoryBot.create(:session_block, session: sessions.first)}
  let!(:program_sessions) { FactoryBot.create_list(:program_session, 2, program: programs.first, session: sessions.first) }
  # program sessions is not necesary but it can be helpfull for a helper in the future
  let!(:crunch) {FactoryBot.create(:exercise, name: 'crunch', met_multiplier: 3, execution_time: 3)}
  let!(:pull_up) {FactoryBot.create(:exercise, name: 'pull up', met_multiplier: 5, execution_time: 4)}
  let!(:session_sets) { FactoryBot.create_list(:session_set, 2, session_block: session_block) }
  let!(:exercises) { FactoryBot.create_list(:exercise, 2) }
  let!(:exercise_set_1) { FactoryBot.create(:exercise_set, exercise: crunch, session_set: session_sets.first) }
  let!(:exercise_set_2) { FactoryBot.create(:exercise_set, exercise: pull_up, session_set: session_sets.second) }
  # set user program
  let!(:user_program) { FactoryBot.create(:user_program, user_id: user.id, program_id: programs.first.id)} 
  #set levels and achievments
  let!(:level1) { FactoryBot.create(:achievment, points: 100, achievment_type: 'level') }
  let!(:level2) { FactoryBot.create(:achievment, points: 500, achievment_type: 'level') }

  def save_session_execution_mutation(session_id:, program_id:, exercises:)
    <<~GQL
      mutation {
        saveSessionExecution(input: 
          { sessionExecution: 
            { frontEndId: "5a63c546-d2ef-11ea-87d0-0242ac130003", sessionId: #{session_id}, programId: #{program_id}, difficultyFeedback: 5, enjoymentFeedback: 6
              sessionBlockExecutions: [
                { order: 1, sessionSetExecutions: 
                  [ { order: 1, exerciseExecutions: [{exerciseId: #{exercises.first.id}, order: 1, repsExecuted: 30, executionTime: 40}]}, 
                    { order: 2, exerciseExecutions: [{exerciseId: #{exercises.second.id}, order: 1, repsExecuted: 20, executionTime: 30}]}] 
                }, 
                { order: 2, sessionSetExecutions: 
                  [ { order: 1, exerciseExecutions: [{exerciseId: #{exercises.first.id}, order: 1, repsExecuted: 30, executionTime: 40}]}, 
                    { order: 2, exerciseExecutions: [{exerciseId: #{exercises.second.id}, order: 1, repsExecuted: 20, executionTime: 30}]}]
                }
              ]
            }
          }
        ){
          id
        }
      }
    GQL
  end

  def get_session_execution_summary_query(session_execution_id:, locale: 'es')
     <<~GQL
      query {
        getSessionExecutionSummary(sessionExecutionId: "#{session_execution_id}", locale: "#{locale}") {
          id
          points
          effort
          valueOfSession
          totalTime
          totalKcal
          totalReps
          sessionExecution {
            id
            sessionId
            difficultyFeedback
            enjoymentFeedback
            session {
              id
              bodyPartsFocused
            }
            userProgram {
              id
              program {
                id
                name
                user {
                  id
                  names
                }
              }
            }
          }
        }
      }
    GQL
  end
  
  context 'find session execution' do    

    before(:each) do 
      post('/graphql', params: { query: save_session_execution_mutation(session_id: Session.last.id, program_id: programs.first.id, exercises: exercises) }, headers: { 'Authorization' => jwt }) 
      ses = SessionExecutionSummary.new(session_execution: SessionExecution.last)
      ses.set_values
      ses.save!
    end

    context 'with a valid session_execution_id' do
      before(:each) do
        session_execution_id = SessionExecution.last.id
        post('/graphql', params: { query: get_session_execution_summary_query(session_execution_id: session_execution_id ) })
      end

      it 'should return a summary with their data' do
        summary = JSON.parse(response.body)["data"]["getSessionExecutionSummary"]
        expect(summary['id'].present?).to be_truthy
        expect(summary['points'].to_i).to eq 100
        expect(summary['effort'].to_i).to eq 5
        expect(summary['valueOfSession'].to_i).to eq 6
        expect(summary['sessionExecution']['difficultyFeedback']).to eq 5
        expect(summary['sessionExecution']['enjoymentFeedback']).to eq 6
      end

      it 'should translate names of the program to spanish default' do
        post('/graphql', params: { query: get_session_execution_summary_query(session_execution_id: SessionExecution.last.id ) })
        program = JSON.parse(response.body)["data"]["getSessionExecutionSummary"]["sessionExecution"]["userProgram"]["program"]
        expect(program['name']).to eq 'name es'
      end

      it 'should translate names of the program to english if locale' do
        post('/graphql', params: { query: get_session_execution_summary_query(session_execution_id: SessionExecution.last.id, locale: 'en' ) })
        program = JSON.parse(response.body)["data"]["getSessionExecutionSummary"]["sessionExecution"]["userProgram"]["program"]
        expect(program['name']).to eq 'name en'
      end

      it 'should increase the number of points and global data of the user' do 
        summary = JSON.parse(response.body)["data"]["getSessionExecutionSummary"]
        user.reload
        expect(user.points).to eq 100
        expect(user.total_sessions).to eq 1
        expect(user.current_weekly_streak).to eq 1
        expect(user.best_weekly_streak).to eq 1
        expect(user.total_time).to eq summary['totalTime']
        expect(user.kcal_per_session).to eq summary['totalKcal']
        expect(user.reps_per_session).to eq summary['totalReps']
      end

      it 'should update the user level and achievments' do
        expect(user.reload.achievments.count).to eq 1
      end
    end

    context 'with a non existent in the db email' do
      it 'should return a friend data with their email and names' do
        session_execution_id = SessionExecution.last.id
        post('/graphql', params: { query: get_session_execution_summary_query(session_execution_id: session_execution_id + 1) })
        summary = JSON.parse(response.body)["data"]["getSessionExecutionSummary"]
        expect(summary.present?).to be_falsey
      end
    end

  end
end
