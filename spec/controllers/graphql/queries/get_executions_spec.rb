require 'rails_helper'

RSpec.describe 'get exercise data', type: :request do

  def get_executions_query(from:, to:)
     <<~GQL
      query {
        getExecutions(from: "#{from}", to: "#{to}") {
          id
          imported
        }
      }
    GQL
  end

  let!(:user) { FactoryBot.create(:user, email: 'pablo@mhunters.com', password: '123password', sign_in_count: 1)} 
  let!(:jwt) { User.authenticate({ email: user.email, password: '123password' }).jwt }
  let!(:programs) { FactoryBot.create_list(:program, 2, user: user, available: true) }
  let!(:sessions) { FactoryBot.create_list(:session, 2) }
  let!(:session_block) { FactoryBot.create(:session_block, session: sessions.first)}
  let!(:program_sessions) { FactoryBot.create_list(:program_session, 2, program: programs.first, session: sessions.first) }
  # program sessions is not necesary but it can be helpfull for a helper in the future
  let!(:session_sets) { FactoryBot.create_list(:session_set, 2, session_block: session_block) }
  let!(:exercises) { FactoryBot.create_list(:exercise, 2) }
  let!(:exercise_set_1) { FactoryBot.create(:exercise_set, exercise: exercises.first, session_set: session_sets.first) }
  let!(:exercise_set_2) { FactoryBot.create(:exercise_set, exercise: exercises.second, session_set: session_sets.second) }
  # set user program
  let!(:user_program) { FactoryBot.create(:user_program, user_id: user.id, program_id: programs.first.id)} 
  let!(:crunch) {FactoryBot.create(:exercise, name: 'crunch')}
  let!(:pull_up) {FactoryBot.create(:exercise, name: 'pull up')}

  context 'a user with executions' do

    before(:each) do
      sessions.first.update(order: 1)
      sessions.second.update(order: 2)
      session_execution  = SessionExecution.create(user_program: user_program, session: sessions.first, imported: true)
    end

    it 'returns the session executions in the time period asked(this month) ' do
      post('/graphql', params: { query: get_executions_query(from: Date.today.beginning_of_month.iso8601, to: Date.today.end_of_month.iso8601) }, headers: { 'Authorization' => jwt }) 
      session_executions = JSON.parse(response.body)['data']['getExecutions']
      expect(session_executions.count).to eq 1
      expect(session_executions.first['imported']).to be_truthy
    end

    it 'returns the session executions in the time period asked(no session this month) ' do
      SessionExecution.destroy_all
      post('/graphql', params: { query: get_executions_query(from: Date.today.beginning_of_month.iso8601, to: Date.today.end_of_month.iso8601) }, headers: { 'Authorization' => jwt }) 
      session_executions = JSON.parse(response.body)['data']['getExecutions']
      expect(session_executions.count).to eq 0
    end

    it 'returns the session executions in the time period asked(other months sessions) ' do
      to = (Date.today.beginning_of_month - 1.day).iso8601
      from = (Date.today.beginning_of_month - 1.day).beginning_of_month.iso8601 
      session_execution  = SessionExecution.create(user_program: user_program, session: sessions.first, created_at: (Date.parse(from) + 1.day))
      session_execution2  = SessionExecution.create(user_program: user_program, session: sessions.second, created_at: (Date.parse(from) + 2.days))
      post('/graphql', params: { query: get_executions_query(from: from, to: to) }, headers: { 'Authorization' => jwt }) 
      session_executions = JSON.parse(response.body)['data']['getExecutions']
      expect(session_executions.count).to eq 2
    end

  end

end