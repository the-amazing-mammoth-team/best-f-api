require 'rails_helper'

RSpec.describe 'get exercise data', type: :request do

  def get_exercise_data_query(id:, locale: )
     <<~GQL
      query {
        getExerciseData(id: #{id}, locale: "#{locale}") {
          id
          video
          thumbnail
          thumbnail400
          deprecated
          name
          metMultiplier
          bodyPartsFocused
        }
      }
    GQL
  end

  let!(:exercise)  { FactoryBot.create(:exercise, video: 'video', video_male: 'video_male', video_female: 'video_female', thumbnail_400: 't400',
                                        thumbnail: 'thumbnail', thumbnail_female: 'thumbnail_female', thumbnail_male: 'thumbnail_male' ) }
  let!(:exercise_without_video)  { FactoryBot.create(:exercise, video: nil, video_male: 'video_male', video_female: nil, thumbnail_400: 't400',
                                                      thumbnail: nil, thumbnail_female: nil, thumbnail_male: 'thumbnail_male') }
  let!(:exercise_without_video_and_video_male)  { FactoryBot.create(:exercise, video: nil, video_male: nil, video_female: 'video_female', thumbnail_400: 't400',
                                                                  thumbnail: nil, thumbnail_female: 'thumbnail_female', thumbnail_male: nil) }


  let!(:female_user) { FactoryBot.create(:user, gender: :female)}
  let!(:male_user) { FactoryBot.create(:user, gender: :male)}
  let!(:jwt_female) { User.authenticate({ email: female_user.email, password: 'mammothhunter1234' }).jwt }
  let!(:jwt_male)  { User.authenticate({ email: male_user.email, password: 'mammothhunter1234' }).jwt}

  context 'call with a correct id and without a user' do
    it 'should give the default exercise data' do
      post('/graphql', params: { query: get_exercise_data_query(id: exercise.id, locale: 'en') } )
      exercise_data = JSON.parse(response.body)['data']['getExerciseData']
      expect(exercise_data['id'].to_i).to eq exercise.id
      expect(exercise_data['video']).to eq 'video'
      expect(exercise_data['thumbnail']).to eq 'thumbnail'
      expect(exercise_data['thumbnail400']).to eq 't400'
      expect(exercise_data['name']).to eq 'name en'
      expect(exercise_data['metMultiplier']).to eq 2.3
      expect(exercise_data['bodyPartsFocused'].present?).to be_truthy
    end

     it 'should give the video_male if no video exercise data' do
      post('/graphql', params: { query: get_exercise_data_query(id: exercise_without_video.id, locale: 'es') } )
      exercise_data = JSON.parse(response.body)['data']['getExerciseData']
      expect(exercise_data['id'].to_i).to eq exercise_without_video.id
      expect(exercise_data['video']).to eq 'video_male'
      expect(exercise_data['thumbnail']).to eq 'thumbnail_male'
      expect(exercise_data['thumbnail400']).to eq 't400'
      expect(exercise_data['name']).to eq 'name es'
    end

    it 'should give the video_female if no video or video_male exercise data' do
      post('/graphql', params: { query: get_exercise_data_query(id: exercise_without_video_and_video_male.id, locale: 'es') } )
      exercise_data = JSON.parse(response.body)['data']['getExerciseData']
      expect(exercise_data['id'].to_i).to eq exercise_without_video_and_video_male.id
      expect(exercise_data['video']).to eq 'video_female'
      expect(exercise_data['thumbnail']).to eq 'thumbnail_female'
      expect(exercise_data['thumbnail400']).to eq 't400'
    end
  end

  context 'call with a correct id with a female user' do
    it 'should give the female video for a exercise with female video' do
      post('/graphql', params: { query: get_exercise_data_query(id: exercise.id, locale: 'es') },
                                headers: { 'Authorization' => jwt_female } )
      exercise_data = JSON.parse(response.body)['data']['getExerciseData']
      expect(exercise_data['id'].to_i).to eq exercise.id
      expect(exercise_data['video']).to eq 'video_female'
      expect(exercise_data['thumbnail']).to eq 'thumbnail_female'
      expect(exercise_data['thumbnail400']).to eq 't400'
    end

    it 'should give the male video if there is no female video' do
      post('/graphql', params: { query: get_exercise_data_query(id: exercise_without_video.id, locale: 'es') },
                                headers: { 'Authorization' => jwt_female } )
      exercise_data = JSON.parse(response.body)['data']['getExerciseData']
      expect(exercise_data['id'].to_i).to eq exercise_without_video.id
      expect(exercise_data['video']).to eq 'video_male'
      expect(exercise_data['thumbnail']).to eq 'thumbnail_male'
      expect(exercise_data['thumbnail400']).to eq 't400'
    end
  end

  context 'call with a correct id with a male user' do
    it 'should give the male video for a exercise with male video available' do
      post('/graphql', params: { query: get_exercise_data_query(id: exercise.id, locale: 'es') },
                                headers: { 'Authorization' => jwt_male } )
      exercise_data = JSON.parse(response.body)['data']['getExerciseData']                         
      expect(exercise_data['id'].to_i).to eq exercise.id
      expect(exercise_data['video']).to eq 'video_male'
      expect(exercise_data['thumbnail']).to eq 'thumbnail_male'
      expect(exercise_data['thumbnail400']).to eq 't400'
    end

    it 'should give the female video if there is no male video' do
      post('/graphql', params: { query: get_exercise_data_query(id: exercise_without_video_and_video_male.id, locale: 'es') },
                                headers: { 'Authorization' => jwt_male } )
      exercise_data = JSON.parse(response.body)['data']['getExerciseData']
      expect(exercise_data['id'].to_i).to eq exercise_without_video_and_video_male.id
      expect(exercise_data['video']).to eq 'video_female'
      expect(exercise_data['thumbnail']).to eq 'thumbnail_female'
      expect(exercise_data['thumbnail400']).to eq 't400'
    end
  end

end