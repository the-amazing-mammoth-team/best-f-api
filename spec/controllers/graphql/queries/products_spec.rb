require 'rails_helper'

RSpec.describe 'products', type: :request do

  def products_query(offer_codes:, device:)
     <<~GQL
      query {
        products(offerCodes: #{offer_codes}, device: "#{device}") {
          id
          storeReference
          hasTrial
          trialDays
        }
      }
    GQL
  end
  
  context 'products' do

    before(:each) do
      # for tests we will use 1y_5999t, 1y_7999t, 3m_4999t, 1y_7999_ft_14t
      @product = FactoryBot.create(:product, store_reference: '1.y', store: 'google_play', has_trial: true, trial_days: 14)
      @product2 = FactoryBot.create(:product, store_reference: '1.y', store: 'stripe')
      @product3 = FactoryBot.create(:product, store_reference: '1.y', store: 'apple')
      @product4 = FactoryBot.create(:product, store_reference: '3.m', store: 'google_play')
      @product5 = FactoryBot.create(:product, store_reference: '3.m', store: 'stripe')
      @product6 = FactoryBot.create(:product, store_reference: '3.m', store: 'apple')
      offer = FactoryBot.create(:offer, offer_code: '1y_7999t')
      offer2 = FactoryBot.create(:offer, offer_code: '3m_4999t')
      ProductOffer.create!(product: @product, offer: offer)
      ProductOffer.create!(product: @product2, offer: offer)
      ProductOffer.create!(product: @product3, offer: offer)
      ProductOffer.create!(product: @product4, offer: offer2)
      ProductOffer.create!(product: @product5, offer: offer2)
      ProductOffer.create!(product: @product6, offer: offer2)
    end


    context 'with existing offers and products' do
      it 'should return the products associated to the offer' do
        post('/graphql', params: { query: products_query(offer_codes: ['1y_7999t', '3m_4999t'], device: 'google_play' ) })
        products = JSON.parse(response.body)["data"]["products"]
        expect(products.first['hasTrial']).to be_truthy
        expect(products.first['trialDays']).to eq 14
        expect(products.count).to eq 2
      end
    end
  end
end