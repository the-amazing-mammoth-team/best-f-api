require 'rails_helper'

RSpec.describe 'Get session query', type: :request do
  
  def get_session_query(session_id:, locale:)
    <<~GQL
       query {
        getSession(sessionId: #{session_id}, locale: "#{locale}") {
          id
          name
          calories
          timeDuration
          reps
          sessionType
          bodyPartsFocused
          thumbnail
          thumbnail400
          sessionBlocks {
            id
            blockType
          }
        }
      }
    GQL
  end

  context '#get_session' do
    
    let!(:user) { FactoryBot.create(:user, email: 'pablo@mhunters.com', password: '123password', sign_in_count: 1)} 
    let!(:programs) {FactoryBot.create_list(:program, 2, user: user, available: true)}
    let!(:sessions) {FactoryBot.create_list(:session, 2, time_duration: 20, reps: 300)}
    let!(:program_sessions) {FactoryBot.create_list(:program_session, 2, program: programs.first, session: sessions.first)}
    # program sessions is not necesary but it can be helpfull for a helper in the future
    let!(:session_block) { FactoryBot.create(:session_block, session: sessions.first, block_type: 'to_the_one') }
    let!(:session_sets) {FactoryBot.create_list(:session_set, 2, session_block: session_block)}
    let!(:exercises) { FactoryBot.create_list(:exercise, 2, name_en: Faker::Name.name, thumbnail: Faker::Name.name, thumbnail_400: Faker::Name.name, 
                                              met_multiplier: 4, execution_time: 2) }
    let!(:exercise_set_1) {FactoryBot.create(:exercise_set, exercise: exercises.first, session_set: session_sets.first, reps: 10, time_duration: nil)}
    let!(:exercise_set_2) {FactoryBot.create(:exercise_set, exercise: exercises.second, session_set: session_sets.second, time_duration: 60, reps: nil)}

    before(:each) do
      exercises.first.update(body_parts_focused: ['core'])
      exercises.last.update(body_parts_focused: ['back_body', 'upper_body'])
      session =  sessions.first
      session.session_type = :challenge
      session.calories = session.set_calories
      session.time_duration = session.set_time
      session.save!
    end

    context 'a session with 2 session sets, each one with 1 exercise response' do
      it 'returns the session/session_set/exercises data' do
        post('/graphql', params: { query: get_session_query(session_id: sessions.first.id, locale: :en ) })
        session  = JSON.parse(response.body)["data"]["getSession"]
        expect(session['calories']).to eq 1
        expect(session['timeDuration']).to eq 80
        expect(session['reps']).to eq 300
        expect(session['bodyPartsFocused'].count).to eq 8
        expect(session['thumbnail']).to eq exercises.first.thumbnail
        expect(session['thumbnail400']).to eq exercises.first.thumbnail_400
        expect(session['sessionBlocks'].first['blockType']).to eq 'To the one'
        expect(session['sessionType']).to eq 'challenge'
      end
    end
  end

end
