require 'rails_helper'

RSpec.describe 'suggested programs', type: :request do

  def suggested_programs_query(user_id:, locale:)
     <<~GQL
      query {
        suggestedPrograms(userId: "#{user_id}", locale: "#{locale}") {
          id
          name
          codeName
          priorityOrder
        }
      }
    GQL
  end
  
  context 'suggestedPrograms' do
    
    let(:password) { '123password' }

    let!(:admin) {FactoryBot.create(:user)}
    let!(:fat_female_user) { FactoryBot.create(:user, body_fat: 34, gender: :female, activity_level: :sedentary, goal: :loss_weight)} 
    let!(:fat_male_user) { FactoryBot.create(:user, body_fat: 27, gender: :male, activity_level: :sedentary, goal: :loss_weight)} 
    let!(:muscle_male_user) { FactoryBot.create(:user, body_fat: 20, gender: :male, activity_level: :medium_active, goal: 1)} 
    let!(:muscle_female_user) { FactoryBot.create(:user, body_fat: 25, gender: :female, activity_level: :medium_active, goal: 1)} 
    let!(:antiage_female_user) { FactoryBot.create(:user, body_fat: 26, gender: :female, activity_level: :medium_active, goal: :antiaging)} 
    let!(:antiage_male_user) { FactoryBot.create(:user, body_fat: 26, gender: :male, activity_level: :medium_active, goal: :antiaging)}
    let!(:unprofiled_user) { FactoryBot.create(:user, body_fat: 0, gender: :male, activity_level: :medium_active, goal: :antiaging)} 
    # programs
    let!(:default_program) { FactoryBot.create(:program, user: admin, available: true, code_name: 'MH_UNN', priority_order: 2) }
    let!(:fat_ppl_program) { FactoryBot.create(:program, user: admin, available: true, name: 'fat_ppl_program', priority_order: 1) }
    let!(:fat_ppl_program_non_available) { FactoryBot.create(:program, user: admin, available: false, name: 'fat_ppl_program_non_available') }
    let!(:muscle_ppl_program) { FactoryBot.create(:program, user: admin, available: true, name: 'muscle_ppl_program') }
    let!(:antiage_ppl_program) { FactoryBot.create(:program, user: admin, available: true, name: 'antiage_ppl_program') }
    # profiles
    let!(:fat_female_profile) { FactoryBot.create(:profile, gender: :female, activity_level: :sedentary, min_fat_level: 30, max_fat_level: 100,
                                                  goal: :loss_weight) }
    let!(:fat_male_profile) { FactoryBot.create(:profile, gender: :male, activity_level: :sedentary, min_fat_level: 26, max_fat_level: 100,
                                                  goal: :loss_weight) }
    # check those with max 0 fat level
    let!(:muscle_female_profile) { FactoryBot.create(:profile, gender: :female, activity_level: :medium_active, min_fat_level: 0, max_fat_level: 30,
                                                     goal: :gain_muscle) }
    let!(:muscle_male_profile) { FactoryBot.create(:profile, gender: :male, activity_level: :medium_active, min_fat_level: 0, max_fat_level: 25,
                                                    goal: :gain_muscle) }
    let!(:antiage_female_profile) { FactoryBot.create(:profile, gender: :female, activity_level: :medium_active, min_fat_level: 25, 
                                                      max_fat_level: 100, goal: :antiaging) }
    let!(:antiage_male_profile) { FactoryBot.create(:profile, gender: :male, activity_level: :medium_active, min_fat_level: 25, 
                                                    max_fat_level: 100, goal: :antiaging) }
    # connect profiles to programs
    let!(:fat_profile_program_f) { FactoryBot.create(:program_profile, profile: fat_female_profile, program: fat_ppl_program) }
    let!(:fat_profile_program_m) { FactoryBot.create(:program_profile, profile: fat_male_profile, program: fat_ppl_program) }
    let!(:fat_profile_program_f_non) { FactoryBot.create(:program_profile, profile: fat_female_profile, program: fat_ppl_program_non_available) }
    let!(:fat_profile_program_m_non) { FactoryBot.create(:program_profile, profile: fat_male_profile, program: fat_ppl_program_non_available) }
    let!(:muscle_profile_program_f) { FactoryBot.create(:program_profile, profile: muscle_female_profile, program: muscle_ppl_program) }
    let!(:muscle_profile_program_m) { FactoryBot.create(:program_profile, profile: muscle_male_profile, program: muscle_ppl_program) }
    let!(:antiage_profile_program_f) { FactoryBot.create(:program_profile, profile: antiage_female_profile, program: antiage_ppl_program) }
    let!(:antiage_profile_program_m) { FactoryBot.create(:program_profile, profile: antiage_male_profile, program: antiage_ppl_program) }
    
    context 'with affiliate_code' do
      #setup affiliate user with their affiliate_code and program
      let!(:affiliate) {FactoryBot.create(:user, affiliate_code: 'afil1')}
      let!(:affiliate_program) { FactoryBot.create(:program, user: affiliate, available: true, code_name: 'MH PRO', name_es: 'afil', name_en: 'afil') }
      let!(:affiliate_program2) { FactoryBot.create(:program, user: affiliate, available: true, code_name: 'MH PRO', name_es: 'afil2', name_en: 'afil2') }


      it 'gives you the program associated to the affiliate or the program id' do
        suggested = SuggestedProgramsService.call(user_id: fat_female_user.id, locale: 'es', onboarding_params: {affiliate_code: 'afil1'})
        expect(suggested.first.id).to eq affiliate_program.id
      end

      it 'it gives you the program from the program_id' do
        suggested = SuggestedProgramsService.call(user_id: fat_female_user.id, locale: 'es', onboarding_params: {program_id: affiliate_program.id})
        expect(suggested.first.id).to eq affiliate_program.id
      end

      it 'gives you the program associated to the affiliate and the program id' do
        suggested = SuggestedProgramsService.call(user_id: fat_female_user.id, locale: 'es', onboarding_params: {affiliate_code: 'afil1', program_id: affiliate_program2.id})
        expect(suggested.first.id).to eq affiliate_program2.id
      end
    end

    context 'with a fat profile' do
      it 'should show fat available programs to the fat female ' do
        post('/graphql', params: { query: suggested_programs_query(user_id: fat_female_user.id, locale: 'es' ) })
        programs = JSON.parse(response.body)["data"]["suggestedPrograms"]
        expect(programs.map {|program| program['id'].to_i }.first).to eq fat_ppl_program.id
        expect(programs.map {|program| program['id'].to_i }.include?(fat_ppl_program.id)).to be_truthy
        expect(programs.map {|program| program['id'].to_i }.include?(fat_ppl_program_non_available.id)).to be_falsey
      end

       it 'should show fat available programs to the fat male ' do
        post('/graphql', params: { query: suggested_programs_query(user_id: fat_male_user.id, locale: 'es' ) })
        programs = JSON.parse(response.body)["data"]["suggestedPrograms"]
        expect(programs.map {|program| program['id'].to_i }.include?(fat_ppl_program.id)).to be_truthy
        expect(programs.map {|program| program['id'].to_i }.include?(fat_ppl_program_non_available.id)).to be_falsey
      end
    end

    context 'with a muscle gain profile' do
      it 'should show fat available programs to the muscle gain female ' do
        post('/graphql', params: { query: suggested_programs_query(user_id: muscle_female_user.id, locale: 'es' ) })
        programs = JSON.parse(response.body)["data"]["suggestedPrograms"]
        expect(programs.map {|program| program['id'].to_i }.include?(muscle_ppl_program.id)).to be_truthy
      end

       it 'should show fat available programs to the muscle gain male ' do
        post('/graphql', params: { query: suggested_programs_query(user_id: muscle_male_user.id, locale: 'es' ) })
        programs = JSON.parse(response.body)["data"]["suggestedPrograms"]
        expect(programs.map {|program| program['id'].to_i }.include?(muscle_ppl_program.id)).to be_truthy
      end
    end

    context 'with a antiaging  profile' do
      it 'should show antiage available programs to the antiaging female ' do
        post('/graphql', params: { query: suggested_programs_query(user_id: antiage_female_user.id, locale: 'es' ) })
        programs = JSON.parse(response.body)["data"]["suggestedPrograms"]
        expect(programs.map {|program| program['id'].to_i }.include?(antiage_ppl_program.id)).to be_truthy
      end

      it 'should show antiage available programs to the antiaging male ' do
        post('/graphql', params: { query: suggested_programs_query(user_id: antiage_male_user.id, locale: 'es' ) })
        programs = JSON.parse(response.body)["data"]["suggestedPrograms"]
        expect(programs.map {|program| program['id'].to_i }.include?(antiage_ppl_program.id)).to be_truthy
      end
    end

    context 'with no  profile found' do
      it 'should show the default available programs to the non profile user' do
        post('/graphql', params: { query: suggested_programs_query(user_id: unprofiled_user.id, locale: 'es' ) })
        programs = JSON.parse(response.body)["data"]["suggestedPrograms"]
        expect(programs.map {|program| program['id'].to_i }.include?(default_program.id)).to be_truthy
      end
    end
  end
end