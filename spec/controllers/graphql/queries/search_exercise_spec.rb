require 'rails_helper'

RSpec.describe 'search exercise', type: :request do

  def search_exercise_query(locale:, name:)
     <<~GQL
      query {
        searchExercise(locale: "#{locale}", name: "#{name}") {
          id
          name
          video
          harderVariation{
            id
          }
          easierVariation {
            id
          }
        }
      }
    GQL
  end

  let!(:exercise)  { FactoryBot.create(:exercise, video: 'video', name_en: 'Ex_en', name_es: 'Ex_es', deprecated: nil ) }
  let!(:exercise2)  { FactoryBot.create(:exercise, video: 'video2',  name_en: 'Ex_en2', name_es: 'Ex_es2', deprecated: false ) }
  let!(:exercise3)  { FactoryBot.create(:exercise, video: 'video', name_en: 'Ex_en', name_es: 'deprecated', deprecated: true ) }


  context 'calling with a deprecated exercise name' do
    it 'should not send any data about deprecated exercises' do
      post('/graphql', params: { query: search_exercise_query(locale: 'es', name: 'deprecated') } )
      exercise_data = JSON.parse(response.body)['data']['searchExercise']
      expect(exercise_data.count).to eq 0
    end
  end
 
  context '#search with correct data' do
    it 'should return the exercises with the name translated to the locale that presents' do
      post('/graphql', params: { query: search_exercise_query(locale: 'es', name: 'ex') } )
      exercise_data = JSON.parse(response.body)['data']['searchExercise']
      expect(exercise_data.count).to eq 2
      expect(exercise_data.first['name']).to eq exercise.name_es
      expect(exercise_data.second['name']).to eq exercise2.name_es
    end
  end

  context 'search with incorrect name' do
    it 'should return nil' do
      post('/graphql', params: { query: search_exercise_query(locale: 'es', name: 'asd') } )
      exercise_data = JSON.parse(response.body)['data']['searchExercise']
      expect(exercise_data.count).to eq 0
    end
  end

  context 'search with incorrect locale' do
    it 'should return raise error' do
      expect { post('/graphql', params: { query: search_exercise_query(locale: 'esss', name: 'ex') } )}.to raise_error
    end
  end

  context 'with easier harder variations' do
    let!(:easier_and_harder_variation) { FactoryBot.create(:exercise, harder_variation: exercise, easier_variation: exercise2)}

    it 'returns easier and harder variations' do
      post('/graphql', params: { query: search_exercise_query(name: easier_and_harder_variation.name_en, locale: :en ) })
      exercise_search  = JSON.parse(response.body)["data"]["searchExercise"].first
      expect(exercise_search['harderVariation']['id'].to_i).to eq exercise.id
      expect(exercise_search['easierVariation']['id'].to_i).to eq exercise2.id
    end
  end

end
