require 'rails_helper'

RSpec.describe 'get session execution summary', type: :request do

  def get_program_data_query(id:)
     <<~GQL
      query {
        getProgramData(id: #{id}) {
          id
          implements {
            id
            name
            imageUrl
          }
        }
      }
    GQL
  end

  let!(:user) { FactoryBot.create(:user, email: 'pablo@mhunters.com', password: '123password', sign_in_count: 1)} 
  let!(:program) { FactoryBot.create(:program,  user: user, available: true) }

  before(:each) do 
    setup_exercises
    setup_programs_and_user_programs(user)
    user.current_program.store_implements
  end

  context 'calling with a valid id' do
    it 'returns a program with their data' do
      post('/graphql', params: { query: get_program_data_query(id: user.current_program.id) } )
      program_data = JSON.parse(response.body)['data']['getProgramData']
      expect(program_data['id'].to_i).to eq user.current_program.id
      expect(program_data['implements'].count).to eq user.current_program.implements_query.count
    end

    it 'returns a nil with their data' do
      post('/graphql', params: { query: get_program_data_query(id: Program.last.id + 1) } )
      program_data = JSON.parse(response.body)['data']['getProgramData']
      expect(program_data.nil?).to be_truthy
    end
  end
end
  