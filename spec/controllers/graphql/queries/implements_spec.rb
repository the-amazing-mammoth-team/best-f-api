require 'rails_helper'

RSpec.describe 'Implements', type: :request do

  def implements_query(locale:)
     <<~GQL
      query {
        implements(locale: "#{locale}") {
          id
          name
        }
      }
    GQL
  end

  context 'implements' do
    let!(:implements) {FactoryBot.create_list(:implement, 2)}

    it 'returns the list of all implements at the platform' do
      post('/graphql', params: { query: implements_query(locale: 'es' ) })
      implements = JSON.parse(response.body)["data"]["implements"]
      expect(implements.count).to eq 2
      expect(implements.first['name']).to eq 'spanish'
    end
  end

end