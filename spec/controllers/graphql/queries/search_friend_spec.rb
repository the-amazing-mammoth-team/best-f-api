require 'rails_helper'

RSpec.describe 'search friend', type: :request do

  def search_friend_query(email:)
     <<~GQL
      query {
        searchFriend(email: "#{email}") {
          id
          names
          email
        }
      }
    GQL
  end
  
  context 'find friend' do

    let!(:user) { FactoryBot.create(:user, email: 'pablo@mhunters.com', password: '123password', sign_in_count: 1, names: 'pablito pedros')} 

    context 'with a valid email' do
      it 'should return a friend data with their email and names' do
        post('/graphql', params: { query: search_friend_query(email: user.email ) })
        friend = JSON.parse(response.body)["data"]["searchFriend"]
        expect(friend['id'].present?).to be_truthy
        expect(friend['names'].present?).to be_truthy
        expect(friend['email'].present?).to be_truthy
      end
    end

    context 'with a non existent in the db email' do
      it 'should return a friend data with their email and names' do
        post('/graphql', params: { query: search_friend_query(email: 'lala@gmail.com' ) })
        friend = JSON.parse(response.body)["data"]["searchFriend"]
        expect(friend.present?).to be_falsey
      end
    end

  end
end