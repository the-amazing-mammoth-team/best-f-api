require 'rails_helper'

RSpec.describe 'onboarding Queries', type: :request do
  
  def suggested_programs_query(user_id:, locale:)
    <<~GQL
       query {
        suggestedPrograms(userId: #{user_id}, locale: "#{locale}") {
          id
          name
        }
      }
    GQL
  end

  def available_programs(locale: )
    <<~GQL
      query {
        availablePrograms(locale: "#{locale}") {
          id
          name
        }
      }
    GQL
  end

  context 'Onboarding queries' do

    let!(:user) { FactoryBot.create(:user, email: 'pablo@mhunters.com', password: '123password', sign_in_count: 1)} 
    let!(:programs) {FactoryBot.create_list(:program, 5, user: user, available: true)}
    let!(:not_available_program) {FactoryBot.create(:program, user: user)}

    context '#available_programs' do
      it 'returns all available programs' do
        programs  = post('/graphql', params: { query: available_programs(locale: :en )})
        programs = JSON.parse(response.body)["data"]["availablePrograms"]
        expect(programs.count).to eq Program.where(available: true).count
      end
    end

  end
end
