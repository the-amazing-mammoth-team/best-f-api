# == Schema Information
#
# Table name: sessions
#
#  id             :bigint           not null, primary key
#  level          :integer
#  order          :integer
#  session_type   :integer
#  time_duration  :integer
#  reps           :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  strength       :integer          default(0)
#  endurance      :integer          default(0)
#  technique      :integer          default(0)
#  flexibility    :integer          default(0)
#  intensity      :integer          default(0)
#  code_name      :string
#  name_en        :string
#  name_es        :string
#  description_en :string
#  description_es :string
#  calories       :integer
#  warmup_id      :bigint
#  cooldown_id    :bigint
#
require 'rails_helper'

RSpec.describe Session, type: :model do
  let!(:user) { FactoryBot.create(:user, email: 'pablo@mhunters.com', password: '123password', sign_in_count: 1)}
  let!(:program) { FactoryBot.create(:program, user: user)}
  let(:session_attributes) { {name: 'ola', description: 'ola ola desc'} }
  let!(:crunch) {FactoryBot.create(:exercise, name: 'crunch')}
  let!(:pull_up) {FactoryBot.create(:exercise, name: 'pull up')}

  before(:each) do
    crunch.update_column(:id, 1)
    pull_up.update_column(:id, 2)
    csv_file =  File.read(Rails.public_path + 'session_example_with_ex_id.csv')
    ImportSession.new(csv_file: csv_file, user: user, session_attributes: session_attributes, program_id: program.id).call
  end

  context 'getting the data of the session' do
    it 'should deliver everything in order' do
      session = Session.first
      session_blocks = session.session_blocks
      expect(session_blocks.pluck(:order)).to eq [1]
      expect(session_blocks.first.session_sets.pluck(:order)).to eq [1,2]
      expect(session_blocks.first.session_sets.first.exercise_sets.pluck(:order)).to eq [1,2]
    end
  end
end
