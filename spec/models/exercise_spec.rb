# == Schema Information
#
# Table name: exercises
#
#  id                     :bigint           not null, primary key
#  video                  :string
#  reps                   :integer
#  time                   :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  legacy_id              :string
#  deprecated             :boolean
#  replacement_legacy_id  :integer
#  family                 :integer
#  sub_family             :integer
#  body_parts_focused     :string           is an Array
#  muscles                :string           is an Array
#  joints                 :string           is an Array
#  met_multiplier         :decimal(, )
#  video_female           :string
#  video_male             :string
#  harder_variation_id    :bigint
#  easier_variation_id    :bigint
#  name_en                :string
#  name_es                :string
#  description_en         :string
#  description_es         :string
#  implement_variation_id :bigint
#  test_correction        :decimal(, )
#  thumbnail              :string
#  thumbnail_male         :string
#  thumbnail_female       :string
#  notes_en               :string
#  notes_es               :string
#  execution_time         :decimal(, )
#  thumbnail_400          :string
#  thumbnail_400_male     :string
#  thumbnail_400_female   :string
#  coach_id               :bigint
#  test_equivalent_id     :bigint
#  t1_min                 :integer          default(0)
#  t1_max                 :integer          default(1000)
#  excluded               :boolean          default(FALSE)
#
require 'rails_helper'

RSpec.describe Exercise, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
