# == Schema Information
#
# Table name: exercise_sets
#
#  id                    :bigint           not null, primary key
#  session_set_id        :bigint           not null
#  exercise_id           :bigint           not null
#  order                 :integer
#  intensity_modificator :float
#  time_duration         :integer
#  reps                  :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  track_reps            :boolean          default(FALSE)
#
require 'rails_helper'

RSpec.describe ExerciseSet, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
