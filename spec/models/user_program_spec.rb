# == Schema Information
#
# Table name: user_programs
#
#  id                 :bigint           not null, primary key
#  user_id            :bigint           not null
#  program_id         :bigint           not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  active             :boolean          default(TRUE)
#  current_session_id :bigint
#  completed          :boolean          default(FALSE)
#  enjoyment          :string
#  enjoyment_notes    :string
#
require 'rails_helper'

RSpec.describe UserProgram, type: :model do
end
