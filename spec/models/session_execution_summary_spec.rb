# == Schema Information
#
# Table name: session_execution_summaries
#
#  id                             :bigint           not null, primary key
#  session_execution_id           :bigint           not null
#  total_reps                     :integer
#  total_time                     :integer
#  reps_per_min                   :integer
#  total_kcal                     :integer
#  reps_per_exercise              :jsonb
#  mins_per_exercise              :jsonb
#  reps_per_min_per_exercise      :jsonb
#  created_at                     :datetime         not null
#  updated_at                     :datetime         not null
#  reps_set_per_block             :jsonb
#  time_set_per_block             :jsonb
#  average_reps_min_set_per_block :jsonb
#  reps_min_set_block             :jsonb
#  effort                         :integer
#  points                         :integer
#  value_of_session               :integer
#  body_parts_spider              :jsonb
#  name                           :string
#
require 'rails_helper'

RSpec.describe SessionExecutionSummary, type: :request do

  let!(:user) { FactoryBot.create(:user, email: 'pablo@mhunters.com', password: '123password', sign_in_count: 1)} 
  let!(:jwt) { User.authenticate({ email: user.email, password: '123password' }).jwt }
  let!(:programs) { FactoryBot.create_list(:program, 2, user: user, available: true) }
  let!(:sessions) { FactoryBot.create_list(:session, 2) }
  let!(:session_block) { FactoryBot.create(:session_block, session: sessions.first, block_type: :to_the_one, order: 1)}
  let!(:session_block2) { FactoryBot.create(:session_block, session: sessions.first, block_type: :to_the_one, order: 2)}

  let!(:program_sessions) { FactoryBot.create_list(:program_session, 2, program: programs.first, session: sessions.first) }
  # program sessions is not necesary but it can be helpfull for a helper in the future
  let!(:session_sets) { FactoryBot.create_list(:session_set, 2, session_block: session_block) }
  let!(:exercises) { FactoryBot.create_list(:exercise, 2) }
  let!(:exercise_set_1) { FactoryBot.create(:exercise_set, exercise: exercises.first, session_set: session_sets.first) }
  let!(:exercise_set_2) { FactoryBot.create(:exercise_set, exercise: exercises.second, session_set: session_sets.second) }
  # set user program
  let!(:user_program) { FactoryBot.create(:user_program, user_id: user.id, program_id: programs.first.id)} 
  let!(:crunch) {FactoryBot.create(:exercise, name_es: 'crunch')}
  let!(:pull_up) {FactoryBot.create(:exercise, name_es: 'pull up', name_en: 'asd')}
  let!(:rest) {FactoryBot.create(:exercise, name_en: 'rest', name_es: 'descanso')}
  let!(:reps_per_exercise) { {"#{exercises.first.id}"=>{"name_en"=> exercises.first.name_en, "name_es"=> exercises.first.name_es, "reps"=>87}, "#{exercises.second.id}"=>{"name_en"=>exercises.second.name_en, "name_es"=>exercises.second.name_es, "reps"=>62}} }
  let!(:mins_per_exercise) { {"#{exercises.first.id}"=>{"name_en"=> exercises.first.name_en, "name_es"=> exercises.first.name_es, "execution_time"=>125}, "#{exercises.second.id}"=>{"name_en"=>exercises.second.name_en, "name_es"=>exercises.second.name_es, "execution_time"=>120}} } 
  let!(:reps_per_min_per_exercise) { {"#{exercises.first.id}"=>{"name_en"=>exercises.first.name_en, "name_es"=>exercises.first.name_es, "value"=>42.0}, "#{exercises.second.id}"=>{"name_en"=>exercises.second.name_en, "name_es"=>exercises.second.name_es, "value"=>31.0}} }
  let!(:reps_set_per_block) { {"1"=>{"1"=>{"block_name"=>"Tto", "reps"=>50}, "2"=>{"block_name"=>"Tto", "reps"=>60}}, "2"=>{"1"=>{"block_name"=>"Tto", "reps"=>17}, "2"=>{"block_name"=>"Tto", "reps"=>22}}} }
  let!(:time_set_per_block) { {"1"=>{"1"=>{"block_name"=>"Tto", "execution_time"=>85}, "2"=>{"block_name"=>"Tto", "execution_time"=>80}}, "2"=>{"1"=>{"block_name"=>"Tto", "execution_time"=>50}, "2"=>{"block_name"=>"Tto", "execution_time"=>30}}} }
  let!(:average_reps_min_set_per_block) { {"1"=>{"1"=>35.29411764705882, "2"=>45.0}, "2"=>{"1"=>20.4, "2"=>44.0}} }

  def save_session_execution_mutation(session_id:, program_id:, exercises:)
    <<~GQL
      mutation {
        saveSessionExecution(input: 
          { sessionExecution: 
            { frontEndId: "5a63c546-d2ef-11ea-87d0-0242ac130003", sessionId: #{session_id}, programId: #{program_id},
              sessionBlockExecutions: [
                { order: 1, sessionSetExecutions: 
                  [ { order: 1, exerciseExecutions: [{exerciseId: #{exercises.first.id}, order: 1, repsExecuted: 35, executionTime: 40},
                                                     {exerciseId: #{rest.id}, order: 2, repsExecuted: 0, executionTime: 40},
                                                     {exerciseId: #{exercises.second.id}, order: 2, repsExecuted: 15, executionTime: 45}                          
                  ]}, 
                    { order: 2, exerciseExecutions: [{exerciseId: #{exercises.second.id}, order: 1, repsExecuted: 25, executionTime: 45},
                                                     {exerciseId: #{exercises.first.id}, order: 2, repsExecuted: 35, executionTime: 35}
                  ]}] 
                }, 
                { order: 2, sessionSetExecutions: 
                  [ { order: 1, exerciseExecutions: [{exerciseId: #{exercises.first.id}, order: 1, repsExecuted: 17, executionTime: 50}
                                                     {exerciseId: #{rest.id}, order: 2, repsExecuted: 0, executionTime: 40}]}, 
                    { order: 2, exerciseExecutions: [{exerciseId: #{exercises.second.id}, order: 1, repsExecuted: 22, executionTime: 30}]}]
                }
              ]
            }
          }
        ){
          id
        }
      }
    GQL
  end

  before(:each) do 
    post('/graphql', params: { query: save_session_execution_mutation(session_id: Session.first.id, program_id: programs.first.id, exercises: exercises) }, headers: { 'Authorization' => jwt }) 
    se = SessionExecution.first
    ses = SessionExecutionSummary.new session_execution: se
    ses.set_values
    ses.save!
    @session_execution_sumary = ses
  end

  context 'rest_block_or_set' do
    it 'ignores rest set or block' do
      #binding.pry
      se = SessionExecution.first
    end
  end

  context '#set_reps_per_exercise' do
    it 'should record and array with reps per exercise, including the exercise name in es or en' do
      expect(@session_execution_sumary.reps_per_exercise).to eq reps_per_exercise
    end
  end

  context '#mins_per_exercise' do
    it 'should record and array with reps per exercise, including the exercise name in es or en' do
      expect(@session_execution_sumary.mins_per_exercise).to eq mins_per_exercise
    end
  end

  context '#reps_per_min_per_exercise' do
    it 'should record and array with reps per exercise, including the exercise name in es or en' do
      expect(@session_execution_sumary.reps_per_min_per_exercise).to eq reps_per_min_per_exercise
    end
  end

  context '#setting up totals' do
    it 'should set the correct values based on the session execution' do
      expect(@session_execution_sumary.total_reps).to eq 149
      expect(@session_execution_sumary.total_time).to eq 325
      expect(@session_execution_sumary.reps_per_min).to eq 27 #
      #expect(@session_execution_sumary.total_kcal).to eq 322
    end
  end

  context '#setting up block summaries' do
    it 'should set the correct values based on the session execution' do
      expect(@session_execution_sumary.reps_set_per_block).to eq reps_set_per_block
      expect(@session_execution_sumary.time_set_per_block).to eq time_set_per_block
      expect(@session_execution_sumary.average_reps_min_set_per_block).to eq average_reps_min_set_per_block
      expect(@session_execution_sumary.reps_min_set_block).to eq nil
    end
  end

  context '#setting_up historic values' do

    # before(:each) do
    #   post('/graphql', params: { query: save_session_execution_mutation(session_id: Session.last.id, program_id: programs.first.id, exercises: exercises) }, headers: { 'Authorization' => jwt }) 
    #   se = SessionExecution.last
    #   ses = SessionExecutionSummary.new(session_execution: se)
    #   ses.set_values
    #   ses.save!
    # end

    # it 'sets up a reps_per_min comparation with similar sessions' do
    #   session_execution_summary = SessionExecutionSummary.last
    #   expect(session_execution_summary.historic.reps_per_min).to eq nil
    # end

  end

end
