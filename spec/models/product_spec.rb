# == Schema Information
#
# Table name: products
#
#  id                  :bigint           not null, primary key
#  price               :decimal(, )
#  name                :string
#  store_reference     :string
#  currency            :integer
#  local               :string
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  store               :integer
#  available           :boolean          default(FALSE)
#  has_trial           :boolean          default(FALSE)
#  trial_days          :integer          default(0)
#  discount_percentage :decimal(, )
#  discount_forever    :boolean
#  period              :integer          default("yearly")
#
require 'rails_helper'

RSpec.describe Product, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
