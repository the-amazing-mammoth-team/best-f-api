# == Schema Information
#
# Table name: session_executions
#
#  id                  :bigint           not null, primary key
#  scheduled_at        :date
#  user_program_id     :bigint           not null
#  difficulty_feedback :integer
#  enjoyment_feedback  :integer
#  feedback_comment    :string
#  reps_executed       :integer
#  execution_time      :integer
#  order               :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  front_end_id        :uuid
#  session_id          :bigint           not null
#  discarded           :boolean          default(FALSE)
#  discard_reason      :integer
#  imported            :boolean          default(FALSE)
#
require 'rails_helper'

RSpec.describe SessionExecution, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
