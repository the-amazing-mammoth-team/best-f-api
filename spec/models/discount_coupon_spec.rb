# == Schema Information
#
# Table name: discount_coupons
#
#  id                  :bigint           not null, primary key
#  stripe_id           :string
#  product_id          :bigint           not null
#  stripe_body         :jsonb
#  discount_percentage :decimal(, )
#  discount_forever    :boolean
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#
require 'rails_helper'

RSpec.describe DiscountCoupon, type: :model do

  after(:each) do
    DiscountCoupon.all.each {|c| c.delete_on_stripe }
  end

  context 'before create' do
    let!(:product) { FactoryBot.create(:product) }
    it 'creates itself at stripe' do
      coupon = DiscountCoupon.new(stripe_id: 'test-coupon', discount_percentage: 25.0, discount_forever: false, product: product)
      coupon.save
      expect(coupon.persisted?).to be_truthy
      expect(coupon.stripe_body.present?).to be_truthy
    end
  end
end
