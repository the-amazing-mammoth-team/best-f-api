# == Schema Information
#
# Table name: subscriptions
#
#  id                  :bigint           not null, primary key
#  user_id             :bigint           not null
#  product_id          :bigint           not null
#  program_id          :bigint           not null
#  platform            :integer
#  transaction_body    :text
#  status              :integer
#  start_date          :datetime
#  end_date            :datetime
#  subscription_type   :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  cancelled_at        :date
#  cancelled           :boolean          default(FALSE)
#  store_metadata      :jsonb
#  affiliate_code      :string
#  offer_code          :string
#  cancellation_reason :string
#  receipt_data        :jsonb
#
require 'rails_helper'

RSpec.describe Subscription, type: :model do
  let!(:admin) { FactoryBot.create(:user, is_admin: true) }
  let!(:user) { FactoryBot.create(:user) }
  let!(:program) { FactoryBot.create(:program, user: admin, code_name: 'not like others') }
  let!(:product) { FactoryBot.create(:product, store: :apple, price: 33) }
  let!(:product2) { FactoryBot.create(:product, store: :apple, price: 100) }
  let!(:product3) { FactoryBot.create(:product, store: :stripe, price: 100) }

  context 'validations' do
    context 'only 1 active subscription per user' do
      it 'creates without problems when there is no other active' do
        expect(Subscription.create!(program: program, user: user, product: product, status: :active))
      end

      it 'fails with other subscription already active' do
        Subscription.create!(program: program, user: user, product: product, status: :active)
        expect {Subscription.create!(program: program, user: user, product: product, status: :active)}.to raise_error
      end
    end
  end

  context 'upgrade_options' do
    context 'for a product with a higher option in the same store' do
      it 'offers options' do
        subscription = Subscription.create!(program: program, user: user, product: product, status: :active)
        expect(subscription.upgrade_options.include?(product2)).to be_truthy
      end
    end

    context 'for a product without a higher option in the same store' do
      it ' doesnt offers options' do
        subscription = Subscription.create!(program: program, user: user, product: product2, status: :active)
        expect(subscription.upgrade_options.empty?).to be_truthy
      end

      it 'no options if no more products' do
        other_subscription = Subscription.create!(program: program, user: user, product: product3, status: :active)
        expect(other_subscription.upgrade_options.empty?).to be_truthy
      end
    end
  end
end
