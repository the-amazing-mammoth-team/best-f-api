# == Schema Information
#
# Table name: exercises
#
#  id                     :bigint           not null, primary key
#  video                  :string
#  reps                   :integer
#  time                   :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  legacy_id              :string
#  deprecated             :boolean
#  replacement_legacy_id  :integer
#  family                 :integer
#  sub_family             :integer
#  body_parts_focused     :string           is an Array
#  muscles                :string           is an Array
#  joints                 :string           is an Array
#  met_multiplier         :decimal(, )
#  video_female           :string
#  video_male             :string
#  harder_variation_id    :bigint
#  easier_variation_id    :bigint
#  name_en                :string
#  name_es                :string
#  description_en         :string
#  description_es         :string
#  implement_variation_id :bigint
#  test_correction        :decimal(, )
#  thumbnail              :string
#  thumbnail_male         :string
#  thumbnail_female       :string
#  notes_en               :string
#  notes_es               :string
#  execution_time         :decimal(, )
#  thumbnail_400          :string
#  thumbnail_400_male     :string
#  thumbnail_400_female   :string
#  coach_id               :bigint
#  test_equivalent_id     :bigint
#  t1_min                 :integer          default(0)
#  t1_max                 :integer          default(1000)
#  excluded               :boolean          default(FALSE)
#
FactoryBot.define do
  factory :exercise do
    name { "MyString" }
    name_en { "name en" }
    name_es { "name es" }
    description { "MyString" }
    video { "MyString" }
    reps { 1 }
    time { 1 }
    t1_min { 0 }
    t1_max { 1000 }
    met_multiplier { 2.3 }
    test_correction { 1 }
    family {['core', 'lower_body', 'upper_body'].sample}
    body_parts_focused {['Core']}

    trait :push_up do
      name_en {'Push up'}
      family {:upper_body}
      test_correction { 1 }
    end

    trait :squat do
      name_en {'Squat'}
      family {:lower_body}
      test_correction { 1 }
    end

    trait :burpee do
      name_en {'Burpee'}
      family {:full_body}
      test_correction { 1 }
    end

    trait :plank_pivot do
      name_en {'Plank pivot'}
      family {:core}
      test_correction { 1 }
    end

    trait :plank_balance do
      name_en {'Plank balance'}
      family {:core}
      test_correction { 1 }
    end

    trait :rest do
      name_en {'rest'}
      family {:full_body}
    end

    trait :run do
      name_en {'run'}
      family {:metabolic}
      test_correction { 1 }
    end

    trait :semi_burpee do
      name_en {'Semi burpee'}
      family {:full_body}
      test_correction { 2 }
    end

    trait :knee_push_up do
      name_en {'Knee push up'}
      family {:upper_body }
    end
    
    trait :pull_up do
      name_en {'Pull up'}
      family {:back_body}
    end

    trait :assisted_pull_up do
      name_en {'Assisted pull up'}
      family {:back_body}
      test_correction { 1.2 }
    end
  end

end
