# == Schema Information
#
# Table name: achievments
#
#  id                 :bigint           not null, primary key
#  name_en            :string
#  name_es            :string
#  icon_url           :string
#  congratulations_en :string
#  congratulations_es :string
#  description_en     :string
#  description_es     :string
#  achievment_type    :integer
#  points             :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
FactoryBot.define do
  factory :achievment do
    name_en { "MyString" }
    name_es { "MyString" }
    icon_url { "MyString" }
    congratulations_en { "MyString" }
    congratulations_es { "MyString" }
    description_en { "MyString" }
    description_es { "MyString" }
    achievment_type { 1 }
    points { 1 }
  end
end
