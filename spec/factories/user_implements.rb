# == Schema Information
#
# Table name: user_implements
#
#  id           :bigint           not null, primary key
#  implement_id :bigint           not null
#  user_id      :bigint           not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
FactoryBot.define do
  factory :user_implement do
    implement { nil }
    user { nil }
  end
end
