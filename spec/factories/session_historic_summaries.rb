# == Schema Information
#
# Table name: session_historic_summaries
#
#  id                   :bigint           not null, primary key
#  user_id              :bigint           not null
#  session_id           :bigint           not null
#  reps_per_min         :jsonb
#  friends_reps_per_min :jsonb
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#
FactoryBot.define do
  factory :session_historic_summary do
    user { nil }
    session { nil }
    reps_per_min { "" }
    friends_reps_per_min { "" }
  end
end
