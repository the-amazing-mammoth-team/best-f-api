# == Schema Information
#
# Table name: session_execution_summaries
#
#  id                             :bigint           not null, primary key
#  session_execution_id           :bigint           not null
#  total_reps                     :integer
#  total_time                     :integer
#  reps_per_min                   :integer
#  total_kcal                     :integer
#  reps_per_exercise              :jsonb
#  mins_per_exercise              :jsonb
#  reps_per_min_per_exercise      :jsonb
#  created_at                     :datetime         not null
#  updated_at                     :datetime         not null
#  reps_set_per_block             :jsonb
#  time_set_per_block             :jsonb
#  average_reps_min_set_per_block :jsonb
#  reps_min_set_block             :jsonb
#  effort                         :integer
#  points                         :integer
#  value_of_session               :integer
#  body_parts_spider              :jsonb
#  name                           :string
#
FactoryBot.define do
  factory :session_execution_summary do
    session_execution { nil }
    total_reps { 1 }
    total_time { 1 }
    total_kcal { "" }
    integer { "MyString" }
    reps_per_exercise { "" }
    mins_per_exercise { "" }
    reps_per_min_per_exercise { "" }
  end
end
