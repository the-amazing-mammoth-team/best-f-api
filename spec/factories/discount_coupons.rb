# == Schema Information
#
# Table name: discount_coupons
#
#  id                  :bigint           not null, primary key
#  stripe_id           :string
#  product_id          :bigint           not null
#  stripe_body         :jsonb
#  discount_percentage :decimal(, )
#  discount_forever    :boolean
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#
FactoryBot.define do
  factory :discount_coupon do
    stripe_id { "MyString" }
    product { nil }
    stripe_body { "" }
  end
end
