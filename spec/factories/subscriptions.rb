# == Schema Information
#
# Table name: subscriptions
#
#  id                  :bigint           not null, primary key
#  user_id             :bigint           not null
#  product_id          :bigint           not null
#  program_id          :bigint           not null
#  platform            :integer
#  transaction_body    :text
#  status              :integer
#  start_date          :datetime
#  end_date            :datetime
#  subscription_type   :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  cancelled_at        :date
#  cancelled           :boolean          default(FALSE)
#  store_metadata      :jsonb
#  affiliate_code      :string
#  offer_code          :string
#  cancellation_reason :string
#  receipt_data        :jsonb
#
FactoryBot.define do
  factory :subscription do
    user { nil }
    product { nil }
    program { nil }
    platform { 1 }
    transaction_body { "MyText" }
    status { 1 }
    start_date { "2020-05-28 10:29:48" }
    end_date { "2020-05-28 10:29:48" }
    subscription_type { 1 }
  end
end
