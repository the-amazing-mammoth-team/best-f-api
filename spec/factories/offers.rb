# == Schema Information
#
# Table name: offers
#
#  id         :bigint           not null, primary key
#  offer_code :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
FactoryBot.define do
  factory :offer do
    offer_code { "MyString" }
  end
end
