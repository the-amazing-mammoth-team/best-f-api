# == Schema Information
#
# Table name: exercise_executions
#
#  id                       :bigint           not null, primary key
#  exercise_id              :bigint           not null
#  session_set_execution_id :bigint           not null
#  reps_executed            :integer
#  execution_time           :integer
#  order                    :integer
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#
FactoryBot.define do
  factory :exercise_execution do
    exercise { nil }
    reps_executed { 1 }
    execution_time { 1 }
    order { 1 }
  end
end
