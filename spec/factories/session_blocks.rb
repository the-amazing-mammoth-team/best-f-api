# == Schema Information
#
# Table name: session_blocks
#
#  id            :bigint           not null, primary key
#  session_id    :bigint           not null
#  time_duration :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  order         :integer
#  block_type    :integer
#  loop          :boolean          default(FALSE)
#
FactoryBot.define do
  factory :session_block do
    session { nil }
    time_duration { 1 }
  end
end
