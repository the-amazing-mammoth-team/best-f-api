# == Schema Information
#
# Table name: program_characteristics
#
#  id         :bigint           not null, primary key
#  program_id :bigint           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  objective  :boolean          default(FALSE)
#  value_en   :string
#  value_es   :string
#
FactoryBot.define do
  factory :program_characteristic do
    value { "MyString" }
  end
end
