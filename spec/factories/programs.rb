# == Schema Information
#
# Table name: programs
#
#  id              :bigint           not null, primary key
#  user_id         :bigint
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  pro             :boolean          default(TRUE), not null
#  available       :boolean          default(FALSE)
#  strength        :integer          default(0)
#  endurance       :integer          default(0)
#  technique       :integer          default(0)
#  flexibility     :integer          default(0)
#  intensity       :integer          default(0)
#  code_name       :string
#  name_es         :string
#  name_en         :string
#  description_en  :string
#  description_es  :string
#  auto_generated  :boolean          default(FALSE)
#  priority_order  :integer
#  next_program_id :integer
#
FactoryBot.define do
  factory :program do
    name_en { Faker::App.name }
    name_es { Faker::App.name }
    priority_order { 1 }
    user { nil }
  end
end
