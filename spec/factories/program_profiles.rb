# == Schema Information
#
# Table name: program_profiles
#
#  id         :bigint           not null, primary key
#  program_id :bigint           not null
#  profile_id :bigint           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
FactoryBot.define do
  factory :program_profile do
    program { nil }
    profile { nil }
  end
end
