# == Schema Information
#
# Table name: profiles
#
#  id             :bigint           not null, primary key
#  gender         :integer
#  activity_level :integer
#  goal           :integer
#  fat_level      :float
#  name           :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  max_fat_level  :float
#  min_fat_level  :float
#
FactoryBot.define do
  factory :profile do
    gender { 1 }
    activity_level { 1 }
    goal { 1 }
  end
end
