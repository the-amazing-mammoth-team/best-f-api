# == Schema Information
#
# Table name: user_achievments
#
#  id            :bigint           not null, primary key
#  user_id       :bigint           not null
#  achievment_id :bigint           not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
FactoryBot.define do
  factory :user_achievment do
    user { nil }
    achievment { nil }
  end
end
