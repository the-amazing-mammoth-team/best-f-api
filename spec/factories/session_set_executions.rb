# == Schema Information
#
# Table name: session_set_executions
#
#  id                         :bigint           not null, primary key
#  reps_executed              :integer
#  execution_time             :integer
#  order                      :integer
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  session_block_execution_id :bigint           not null
#
FactoryBot.define do
  factory :session_set_execution do
    session_execution { nil }
    reps_executed { 1 }
    execution_time { 1 }
  end
end
