# == Schema Information
#
# Table name: product_offers
#
#  id         :bigint           not null, primary key
#  offer_id   :bigint           not null
#  product_id :bigint           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  code_name  :integer
#
FactoryBot.define do
  factory :product_offer do
    offer { nil }
    product { nil }
  end
end
