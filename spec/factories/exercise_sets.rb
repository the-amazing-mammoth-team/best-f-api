# == Schema Information
#
# Table name: exercise_sets
#
#  id                    :bigint           not null, primary key
#  session_set_id        :bigint           not null
#  exercise_id           :bigint           not null
#  order                 :integer
#  intensity_modificator :float
#  time_duration         :integer
#  reps                  :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  track_reps            :boolean          default(FALSE)
#
FactoryBot.define do
  factory :exercise_set do
    session_set { nil }
    exercise { nil }
    sequence(:order) { |n| n }
    intensity_modificator { 1.5 }
    time_duration { 1 }
    reps { 1 }
  end
end
