# == Schema Information
#
# Table name: exercise_implements
#
#  id           :bigint           not null, primary key
#  exercise_id  :bigint           not null
#  implement_id :bigint           not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
FactoryBot.define do
  factory :exercise_implement do
    exercise { nil }
    implement { nil }
  end
end
