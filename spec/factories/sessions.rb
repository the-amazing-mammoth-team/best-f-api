# == Schema Information
#
# Table name: sessions
#
#  id             :bigint           not null, primary key
#  level          :integer
#  order          :integer
#  session_type   :integer
#  time_duration  :integer
#  reps           :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  strength       :integer          default(0)
#  endurance      :integer          default(0)
#  technique      :integer          default(0)
#  flexibility    :integer          default(0)
#  intensity      :integer          default(0)
#  code_name      :string
#  name_en        :string
#  name_es        :string
#  description_en :string
#  description_es :string
#  calories       :integer
#  warmup_id      :bigint
#  cooldown_id    :bigint
#
FactoryBot.define do
  factory :session do
    name { "MyString" }
    level { 1 }
    sequence(:order) { |n| n }
    session_type { 'normal' }
    time_duration { 1 }
    reps { 1 }
  end
end
