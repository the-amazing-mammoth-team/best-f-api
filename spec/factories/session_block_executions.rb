# == Schema Information
#
# Table name: session_block_executions
#
#  id                   :bigint           not null, primary key
#  session_execution_id :bigint           not null
#  block_type           :integer
#  reps_executed        :integer
#  order                :integer
#  execution_time       :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#
FactoryBot.define do
  factory :session_block_execution do
    session_execution { nil }
    block_type { 1 }
    reps_executed { 1 }
    order { 1 }
    execution_time { 1 }
  end
end
