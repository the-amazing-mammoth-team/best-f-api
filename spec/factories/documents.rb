# == Schema Information
#
# Table name: documents
#
#  id              :bigint           not null, primary key
#  name            :string
#  description     :string
#  associated_mode :string
#  user_id         :bigint
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
FactoryBot.define do
  factory :document do
    name { "MyString" }
    description { "MyString" }
    associated_mode { "MyString" }
  end
end
