# == Schema Information
#
# Table name: users
#
#  id                            :bigint           not null, primary key
#  email                         :string           default(""), not null
#  encrypted_password            :string           default(""), not null
#  reset_password_token          :string
#  reset_password_sent_at        :datetime
#  remember_created_at           :datetime
#  created_at                    :datetime         not null
#  updated_at                    :datetime         not null
#  gender                        :integer          not null
#  date_of_birth                 :date             not null
#  height                        :float            not null
#  weight                        :float            not null
#  activity_level                :integer          not null
#  goal                          :integer          not null
#  body_type                     :integer          not null
#  body_fat                      :float            not null
#  newsletter_subscription       :boolean          default(FALSE), not null
#  is_admin                      :boolean
#  names                         :string
#  last_name                     :string
#  sign_in_count                 :integer          default(0), not null
#  current_sign_in_at            :datetime
#  last_sign_in_at               :datetime
#  current_sign_in_ip            :inet
#  last_sign_in_ip               :inet
#  recover_password_code         :integer
#  recover_password_attempts     :integer          default(0)
#  facebook_uid                  :string
#  workout_setting_voice_coach   :boolean          default(TRUE)
#  workout_setting_sound         :boolean          default(TRUE)
#  workout_setting_vibration     :boolean          default(TRUE)
#  workout_setting_mobility      :boolean          default(TRUE)
#  workout_setting_cardio_warmup :boolean          default(TRUE)
#  workout_setting_countdown     :boolean          default(TRUE)
#  notifications_setting         :boolean          default(TRUE)
#  training_days_setting         :integer          default(1)
#  google_uid                    :string
#  language                      :string           default("en")
#  country                       :string
#  points                        :integer          default(0)
#  scientific_data_usage         :boolean          default(FALSE)
#  t1_push                       :integer          default(0)
#  t1_core                       :integer          default(0)
#  t1_legs                       :integer          default(0)
#  t1_full                       :integer          default(0)
#  t1_push_exercise              :integer          default(0)
#  t1_pull_up                    :integer          default(0)
#  t2_reps                       :integer          default(0)
#  t2_steps                      :integer          default(0)
#  t2_reps_push                  :integer          default(0)
#  t2_reps_core                  :integer          default(0)
#  t2_reps_legs                  :integer          default(0)
#  t2_reps_full                  :integer          default(0)
#  t2_time_push                  :integer          default(0)
#  t2_time_core                  :integer          default(0)
#  t2_time_legs                  :integer          default(0)
#  t2_time_full                  :integer          default(0)
#  t1_full_exercise              :integer          default(0)
#  t1_pull_up_exercise           :integer
#  warmup_setting                :boolean
#  warmup_session_id             :integer
#  stripe_id                     :string
#  provider                      :string           default(""), not null
#  uid                           :string           default(""), not null
#  best_weekly_streak            :integer          default(0)
#  current_weekly_streak         :integer          default(0)
#  affiliate_code                :string
#  affiliate_code_signup         :string
#  total_sessions                :integer
#  total_time                    :integer
#  kcal_per_session              :decimal(, )
#  reps_per_session              :integer
#  moengage_id                   :string
#  mix_panel_id                  :string
#  apple_id_token                :string
#  imported                      :boolean          default(FALSE)
#  platform                      :string
#  login_token                   :uuid
#  login_token_generated_at      :datetime
#
FactoryBot.define do
  factory :user do
    email { Faker::Internet.email }
    names {  Faker::Internet.name } 
    last_name { 'hi' }
    gender { 'male' }  
    date_of_birth { '22-02-1988' }
    height { 170 }
    weight { 70 }
    activity_level { 'very_active' }
    body_fat { 18 }
    body_type { 'lean' }
    goal { 'gain_muscle' }
    password { 'mammothhunter1234' }
    newsletter_subscription { 'false' }
    affiliate_code {nil}
  end
end
