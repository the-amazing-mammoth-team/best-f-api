# == Schema Information
#
# Table name: implements
#
#  id         :bigint           not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  name_en    :string
#  name_es    :string
#
FactoryBot.define do
  factory :implement do
    name_en { "english" }
    name_es { "spanish" }
    image { nil }
  end
end
