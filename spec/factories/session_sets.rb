# == Schema Information
#
# Table name: session_sets
#
#  id               :bigint           not null, primary key
#  order            :integer
#  level            :integer
#  time_duration    :integer
#  reps             :integer
#  session_set_type :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  session_block_id :bigint           not null
#  loop             :boolean          default(FALSE)
#
FactoryBot.define do
  factory :session_set do
    session_block { nil }
    order { 1 }
    level { 1 }
    time_duration { 1 }
    reps { 1 }
    session_set_type { 1 }
  end
end
