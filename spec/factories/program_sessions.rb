# == Schema Information
#
# Table name: program_sessions
#
#  id         :bigint           not null, primary key
#  program_id :bigint           not null
#  session_id :bigint           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
FactoryBot.define do
  factory :program_session do
    program { nil }
    session { nil }
  end
end
