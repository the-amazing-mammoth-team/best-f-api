# == Schema Information
#
# Table name: product_programs
#
#  id         :bigint           not null, primary key
#  product_id :bigint           not null
#  program_id :bigint           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
FactoryBot.define do
  factory :product_program do
    product { nil }
    program { nil }
  end
end
