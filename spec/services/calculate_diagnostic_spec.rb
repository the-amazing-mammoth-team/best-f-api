require 'rails_helper'

RSpec.describe CalculateDiagnostic, type: :model do

  context '#call' do
    let!(:user) { FactoryBot.create(:user, email: 'pablo@mhunters.com', password: '123password', gender: 'male', goal: 'gain_muscle', activity_level: 'medium_active', height: 170, weight: 60, body_type: 'medium', body_fat: 10)}

    it 'gives the correct diagnostic values' do
      service = CalculateDiagnostic.new user: user
      expect(service.bmi).to eq(20.8)
      
    end

  end
end