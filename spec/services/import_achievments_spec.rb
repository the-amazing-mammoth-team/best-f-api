require 'rails_helper'

RSpec.describe ImportAchievments, type: :model do

  context '#call' do
    let!(:user) { FactoryBot.create(:user, email: 'pablo@mhunters.com', password: '123password', sign_in_count: 1)}

    it 'loads the achievments' do
      csv_file =  File.read(Rails.public_path + 'achievments.csv')
      ImportAchievments.new(csv_file: csv_file, user: user).call
      expect(Achievment.count).to eq 2
      achievment = Achievment.first
      expect(achievment.icon_url).to eq 'google.cl'
      expect(achievment.name_es).to eq 'nivel 1'
      expect(achievment.name_en).to eq 'level 1'
      expect(achievment.description_es).to eq 'description es'
      expect(achievment.description_en).to eq 'description en'
      expect(achievment.congratulations_en).to eq 'congrats'
      expect(achievment.congratulations_es).to eq 'felicidades'
      expect(achievment.achievment_type).to eq 'level'
      expect(achievment.points).to eq 100
    end
    
    # it 'overwrites updates if same name found' do
    #   csv_file =  File.read(Rails.public_path + 'exercises_updated.csv')
    #   ImportExercises.new(csv_file: csv_file, user: user).call
    #   expect(Exercise.count).to eq 2
    #   deprecated = Exercise.where(deprecated: true).first
    #   expect(deprecated.test_correction).to eq 1.3
    # end

  end

end