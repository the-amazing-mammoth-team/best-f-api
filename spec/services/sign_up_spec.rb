require 'rails_helper'

RSpec.describe SignUpService, type: :model do
  context '#call' do

    let!(:admin) { FactoryBot.create(:user, email: 'admin@mhunters.com') }
    let!(:affiliate) { FactoryBot.create(:user, email: 'affiliate@mhunters.com', affiliate_code: '12345') }

    let!(:challenges) { FactoryBot.create(:program, code_name: 'challenges', user: admin) }
    let!(:active_rest) { FactoryBot.create(:program, code_name: 'active rests', user: admin)}

    context 'with correct params' do
      let(:user_params) { { email: 'email@email.com', password: 'esmag12345', gender: 'female', date_of_birth: '10-10-1980',
                       height: 170.0, weight: 70.0, activity_level: 'very_active', goal: 'gain_muscle', body_type: 'lean',
                       body_fat: 18.0, newsletter_subscription: false, names: 'ola ola', last_name: 'oli', scientific_data_usage: true,
                       affiliate_code_signup: affiliate.affiliate_code, mix_panel_id: 'asddsad12', moengage_id: '321321a'} }
      
      it 'saves a new user with the params' do
        user = SignUpService.call(user_params: user_params)
        expect(user.jwt.present?).to be_truthy
        expect(user.scientific_data_usage).to be_truthy
        expect(user.affiliate_code_signup).to eq affiliate.affiliate_code
        expect(user.moengage_id).to eq '321321a'
        expect(user.mix_panel_id).to eq 'asddsad12'
        expect(user.login_token.present?).to be_truthy
        expect(User.count).to eq 3
      end

      it 'sets challenges and active rests' do
        user = SignUpService.call(user_params: user_params)
        expect(user.programs.include?(challenges))
        expect(user.programs.include?(active_rest))
        expect(user.active_rests.present?).to be_truthy
        expect(user.challenges.present?).to be_truthy
      end

      skip 'sends the email confirmation ' do
      end
    end

    context 'with incorrect params' do
      # it doesnt have the gender required param
      let(:user_params) { { email: 'email@email.com', password: 'esmag12345', date_of_birth: '10-10-1980',
                       height: 170.0, weight: 70.0, activity_level: 'very_active', goal: 'gain_muscle', body_type: 'lean',
                       body_fat: 18.0, newsletter_subscription: false} }

      it 'returns a user with errors' do
        expect { SignUpService.call(user_params: user_params) }.to raise_error
      end

      skip 'doesnt send the email confirmation' do
      end
    end

    context 'apple signup ' do
      let(:user_params) { { apple_id_token: 'someuido123450', email: 'email@email.com', password: 'esmag12345', gender: 'female', date_of_birth: '10-10-1980',
                        height: 170.0, weight: 70.0, activity_level: 'very_active', goal: 'gain_muscle', body_type: 'lean',
                        body_fat: 18.0, newsletter_subscription: false, names: 'ola ola', last_name: 'oli'} }

      it 'saves a new user with the facebook params' do
        user = SignUpService.call(user_params: user_params)
        expect(user.jwt.present?).to be_truthy
        expect(User.count).to eq 3
        expect(user.apple_id_token).to eq 'someuido123450'
      end

      it 'sets challenges and active rests' do
        user = SignUpService.call(user_params: user_params)
        expect(user.programs.include?(challenges))
        expect(user.programs.include?(active_rest))
      end
    end

    context 'facebook subscription ' do
      let(:user_params) { { facebook_uid: 'someuido123450', email: 'email@email.com', password: 'esmag12345', gender: 'female', date_of_birth: '10-10-1980',
                        height: 170.0, weight: 70.0, activity_level: 'very_active', goal: 'gain_muscle', body_type: 'lean',
                        body_fat: 18.0, newsletter_subscription: false, names: 'ola ola', last_name: 'oli'} }

      it 'saves a new user with the facebook params' do
        user = SignUpService.call(user_params: user_params)
        expect(user.jwt.present?).to be_truthy
        expect(User.count).to eq 3
        expect(user.facebook_uid.present?).to be_truthy
      end

      it 'sets challenges and active rests' do
        user = SignUpService.call(user_params: user_params)
        expect(user.programs.include?(challenges))
        expect(user.programs.include?(active_rest))
      end
    end
  end

end
