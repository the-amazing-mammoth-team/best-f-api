require 'rails_helper'

RSpec.describe CancelSubscriptionService, type: :model do
  context '#call' do

    let!(:admin) { FactoryBot.create(:user, is_admin: true) }
    let!(:program) { FactoryBot.create(:program, user: admin) }
    let!(:product) { FactoryBot.create(:product) }
    let!(:user) { FactoryBot.create(:user) }
    let!(:user2) { FactoryBot.create(:user) }
    let!(:subscription) { FactoryBot.create(:subscription, user: user, program: program, product: product, status: :active) }

    context 'a pro user with their pro account' do
      it 'cancels the account setting up their subscription as cancelled on the day' do
        expect(user.is_pro).to be_truthy
        CancelSubscriptionService.new(user: user, cancellation_reason: 'dsa').call
        expect(user.current_subscription.cancelled).to be_truthy
        expect(user.current_subscription.cancelled_at).to eq Date.today
      end
    end

    context 'a non pro user' do
      it 'raises an error as there is nothing to upgrade' do
        expect { CancelSubscriptionService.new(user: user2).call }.to raise_error
      end
    end

  end

end