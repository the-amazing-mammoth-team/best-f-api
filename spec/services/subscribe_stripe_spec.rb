require 'rails_helper'

RSpec.describe SubscribeStripe, type: :model do

  let!(:product) {FactoryBot.create(:product, store: :stripe, price: 59.99, store_reference: 'v12-1y')}
  let!(:user) { FactoryBot.create(:user, email: 'pablo2@mhunters.com', password: '123password', sign_in_count: 1)} 
  
  context '#call' do
    it 'creates a stripe customer and saves it in the user, also creates the subscription' do
      substripe = SubscribeStripe.new(stripe_token: 'tok_visa', product: product , user: user).call
      expect(user.stripe_id.present?).to be_truthy
      expect(substripe.id.present?).to be_truthy
    end
  end
end