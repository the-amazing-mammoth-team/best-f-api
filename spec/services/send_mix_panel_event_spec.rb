require 'rails_helper'

RSpec.describe Analytics::SendMixPanelEvent, type: :model do

  context '#call' do
    let!(:user) { FactoryBot.create(:user, email: 'pablo@mhunters.com', password: '123password', sign_in_count: 1)}
    let!(:programs) {FactoryBot.create_list(:program, 1, user: user, available: true)}
    let!(:google_1_year) {FactoryBot.create(:product, store: :google_play, price: 80)}

    before(:each) do
      subscription = SubscribeUser.new(user: user, program_id: programs.first.id, product_id: google_1_year.id, 
        transaction_body: 'body of transaction', extra_params: {'affiliate_code'=> 'affiliate_code', 'offer_code' => 'offer_code'}).call
    end

    after(:each) do
      Analytics::DeleteMixPanelUser.new(user: user).call
    end

    it 'creates a mix panel event for sign up completed' do
      expect(Analytics::SendMixPanelEvent.new(user: user, event: 'Signup completed').call)
    end

    it 'creates a mix panel event for subscribe' do
      expect(Analytics::SendMixPanelEvent.new(user: user, event: 'Subscribe').call)
    end
  end

end