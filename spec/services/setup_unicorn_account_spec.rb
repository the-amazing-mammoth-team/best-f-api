require 'rails_helper'

RSpec.describe SetupUnicornAccount, type: :model do

  let!(:admin) { FactoryBot.create(:user, is_admin: true)}
  let!(:user) { FactoryBot.create(:user)}
  let!(:product) { FactoryBot.create(:product, store: :unicorn) }
  let!(:program) { FactoryBot.create(:program, user: admin) }

  context '#call' do
    it 'setups a unicorn account' do
      SetupUnicornAccount.new(user: user).call
      user.reload
      expect(user.is_pro).to be_truthy
    end
  end
end