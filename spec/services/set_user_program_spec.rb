require 'rails_helper'

RSpec.describe SetUserProgram, type: :model do
  context '#disable_other_actives' do
    let!(:user) { FactoryBot.create(:user) }
    let!(:program) { FactoryBot.create(:program, user: user) }
    let!(:program2) { FactoryBot.create(:program, user: user) }
    let!(:user_program) { FactoryBot.create(:user_program, active: true, user: user, program: program) }

    context 'with one other user program active' do
      it 'creates a new user program and sets active the new one' do
        new_user_program =  SetUserProgram.new(user: user, program_id: program2.id).call
        expect(UserProgram.where(user: user, active: true).count).to eq 1
        expect(new_user_program.active).to be_truthy
      end
    end

    context 'with no other user program active' do
      it 'creates a new user program and sets active the new one' do
        UserProgram.destroy_all
        new_user_program = SetUserProgram.new(user: user, program_id: program2.id).call
        expect(UserProgram.where(user: user, active: true).count).to eq 1
        expect(new_user_program.active).to be_truthy
      end
    end

    context 'changing between programs already chosen' do
      it 'sets active the chosen one' do
        SetUserProgram.new(user: user, program_id: program2.id).call
        new_user_program = SetUserProgram.new(user: user, program_id: program.id).call
        expect(UserProgram.where(user: user, active: true).count).to eq 1
        expect(new_user_program.active).to be_truthy
      end
    end
  end
end
