require 'rails_helper'

RSpec.describe Analytics::CreateMixPanelUser, type: :model do

  context '#call' do
    let!(:user) { FactoryBot.create(:user, email: 'pablo@mhunters.com', password: '123password', sign_in_count: 1)}

    after(:each) do
      Analytics::DeleteMixPanelUser.new(user: user).call
    end

    it 'creates a mix panel user' do
      expect(Analytics::CreateMixPanelUser.new(user: user).call)
    end
  end


  #tracker.alias('email', 'distinct_id') distinct id es user_id en caso de mixpanel o es alias como se diria, le daremos alias de emial

end