require 'rails_helper'

RSpec.describe UpdatePasswordService, type: :model do

  context '#call' do
    let!(:user) { FactoryBot.create(:user, email: 'pablo@mhunters.com', password: '123password', sign_in_count: 1)}

    before(:each) do
      RecoverPasswordService.call(email: user.email)
    end

    context 'with the correct password sent to the email' do

      it 'sets a new password and sets the recover code to nil ' do
        user.reload
        UpdatePasswordService.new(email: user.email, code: user.recover_password_code, password: 'newpass123').call
        user.reload
        expect(user.recover_password_code.nil?).to be_truthy
        expect(user.valid_password?('newpass123')).to be_truthy
      end
    end

    context 'with incorrect password recover code' do
      it 'raises a standard error with user not found in the db' do
        # we will use 3 numbers code wich will always be wrong as codes are 6 numbers
        expect { UpdatePasswordService.new(email: user.email, code: 123, password: 'newpass123').call }.to raise_error('wrong recover password code')
      end
    end
  end

  context 'with non existent email' do
    it 'raises error user not found' do
      expect { UpdatePasswordService.new(email: 'caca', code: 123, password: 'newpass123').call }.to raise_error('user not found')
    end
  end

end