require 'rails_helper'

RSpec.describe ImportExercises, type: :model do

  context '#call' do
    let!(:user) { FactoryBot.create(:user, email: 'pablo@mhunters.com', password: '123password', sign_in_count: 1)}

    before(:each) do
      user.update_column(:id, 1)
    end

    it 'loads the exercises' do
      csv_file =  File.read(Rails.public_path + 'exercises.csv')
      ImportExercises.new(csv_file: csv_file, user: user).call
      expect(Exercise.count).to eq 2
      deprecated = Exercise.where(deprecated: true).first
      expect(deprecated.name_en.present?).to be_truthy
      expect(deprecated.legacy_id.present?).to be_truthy
      expect(deprecated.name_es.present?).to be_truthy
      expect(deprecated.description_en.present?).to be_truthy
      expect(deprecated.description_es.present?).to be_truthy
      expect(deprecated.replacement_legacy_id).to eq 63
      expect(deprecated.family.present?).to be_truthy 
      expect(deprecated.sub_family.present?).to be_truthy
      expect(deprecated.body_parts_focused.present?).to be_truthy
      expect(deprecated.muscles.present?).to be_truthy
      expect(deprecated.joints.present?).to be_truthy
      expect(deprecated.met_multiplier).to eq 0.12
      expect(deprecated.test_correction).to eq 1.5
      expect(deprecated.notes_es).to eq 'notes es'
      expect(deprecated.notes_en).to eq 'notes en'
      expect(deprecated.execution_time).to eq 2.3
      expect(deprecated.coach_id).to eq 1
      expect(deprecated.implement_variation).to eq nil
    end
    
    it 'overwrites updates if same name found' do
      csv_file =  File.read(Rails.public_path + 'exercises_updated.csv')
      ImportExercises.new(csv_file: csv_file, user: user).call
      expect(Exercise.count).to eq 2
      deprecated = Exercise.where(deprecated: true).first
      expect(deprecated.test_correction).to eq 1.3

    end

  end

end