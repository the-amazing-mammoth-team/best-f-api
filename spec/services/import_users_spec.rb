require 'rails_helper'

RSpec.describe ImportUsers, type: :model do

    let!(:user) { FactoryBot.create(:user, email: 'pablo@mhunters.com', password: '123password', sign_in_count: 1) }
    let!(:program) { FactoryBot.create(:program, user: user, pro: true) }
    let!(:admin) {FactoryBot.create(:user)}
    # programs
    let!(:default_program) { FactoryBot.create(:program, user: admin, available: true, code_name: 'MH_UNN', priority_order: 2) }
    let!(:fat_ppl_program) { FactoryBot.create(:program, user: admin, available: true, name: 'fat_ppl_program', priority_order: 1) }
    let!(:fat_ppl_program_non_available) { FactoryBot.create(:program, user: admin, available: false, name: 'fat_ppl_program_non_available') }
    let!(:muscle_ppl_program) { FactoryBot.create(:program, user: admin, available: true, name: 'muscle_ppl_program') }
    let!(:antiage_ppl_program) { FactoryBot.create(:program, user: admin, available: true, name: 'antiage_ppl_program') }
    # profiles
    let!(:fat_female_profile) { FactoryBot.create(:profile, gender: :female, activity_level: :sedentary, min_fat_level: 30, max_fat_level: 100,
                                                  goal: :loss_weight) }
    let!(:fat_male_profile) { FactoryBot.create(:profile, gender: :male, activity_level: :sedentary, min_fat_level: 26, max_fat_level: 100,
                                                  goal: :loss_weight) }
    # check those with max 0 fat level
    let!(:muscle_female_profile) { FactoryBot.create(:profile, gender: :female, activity_level: :medium_active, min_fat_level: 0, max_fat_level: 30,
                                                     goal: :gain_muscle) }
    let!(:muscle_male_profile) { FactoryBot.create(:profile, gender: :male, activity_level: :medium_active, min_fat_level: 0, max_fat_level: 25,
                                                    goal: :gain_muscle) }
    let!(:antiage_female_profile) { FactoryBot.create(:profile, gender: :female, activity_level: :medium_active, min_fat_level: 25, 
                                                      max_fat_level: 100, goal: :antiaging) }
    let!(:antiage_male_profile) { FactoryBot.create(:profile, gender: :male, activity_level: :medium_active, min_fat_level: 25, 
                                                    max_fat_level: 100, goal: :antiaging) }
    # connect profiles to programs
    let!(:fat_profile_program_f) { FactoryBot.create(:program_profile, profile: fat_female_profile, program: fat_ppl_program) }
    let!(:fat_profile_program_m) { FactoryBot.create(:program_profile, profile: fat_male_profile, program: fat_ppl_program) }
    let!(:fat_profile_program_f_non) { FactoryBot.create(:program_profile, profile: fat_female_profile, program: fat_ppl_program_non_available) }
    let!(:fat_profile_program_m_non) { FactoryBot.create(:program_profile, profile: fat_male_profile, program: fat_ppl_program_non_available) }
    let!(:muscle_profile_program_f) { FactoryBot.create(:program_profile, profile: muscle_female_profile, program: muscle_ppl_program) }
    let!(:muscle_profile_program_m) { FactoryBot.create(:program_profile, profile: muscle_male_profile, program: muscle_ppl_program) }
    let!(:antiage_profile_program_f) { FactoryBot.create(:program_profile, profile: antiage_female_profile, program: antiage_ppl_program) }
    let!(:antiage_profile_program_m) { FactoryBot.create(:program_profile, profile: antiage_male_profile, program: antiage_ppl_program) }
    #unicorn
    let!(:unicorn) { FactoryBot.create(:product, store: :unicorn, store_reference: 'unicorn', price: 10)}
    let!(:unbreakable) { FactoryBot.create(:program, user: user, id: 29) }
    let!(:ringmaster) { FactoryBot.create(:program, user: user, id: 30) }
    let!(:rea) { FactoryBot.create(:program, user: user, id: 31) }
    #challenges and rests
    let!(:challenges) { FactoryBot.create(:program, user: user, code_name: 'challenges') }
    let!(:active_rests) { FactoryBot.create(:program, user: user, code_name: 'active rests') }

  context '#call' do
    
    before(:each) do
      setup_exercises
      csv_file =  'pro_users.csv'
      ImportUsers.new(csv_file: csv_file, user_type: 'pros').call
    end

    it 'imports the 4 users with their subscription data' do
      expect(User.count).to eq 6 # its 6 counting the owner of the program and admin
      user = User.third
      expect(user.email).to eq "yoli@lineaalba.com"
      expect(user.gender).to eq 'female'
      expect(user.date_of_birth).to be_truthy
      expect(user.height).to eq 154
      expect(user.weight).to eq 42.5
      expect(user.activity_level).to eq 'sedentary'
      expect(user.names).to eq 'Yolanda'
      expect(user.body_type).to eq 'lean'
      expect(user.goal).to eq 'antiaging'
      expect(user.body_fat).to eq 20
      expect(user.newsletter_subscription).to be_truthy
      expect(user.country).to eq 'ES'
      expect(user.training_days_setting).to eq 3
      expect(user.t1_push > 0)
      expect(user.t1_core > 0)                       
      expect(user.t1_legs > 0)                       
      expect(user.t1_full > 0)    
      expect(user.t1_push_exercise > 0)              
      expect(user.t1_pull_up > 0)                    
      expect(user.t2_reps > 0)                       
      expect(user.t2_steps > 0)                      
      expect(user.t2_reps_push > 0)                  
      expect(user.t2_reps_core > 0)                  
      expect(user.t2_reps_legs > 0)                  
      expect(user.t2_reps_full > 0)                  
      expect(user.t2_time_push > 0)                  
      expect(user.t2_time_core > 0)                  
      expect(user.t2_time_legs > 0)                  
      expect(user.t2_time_full > 0)                  
      expect(user.t1_full_exercise > 0)              
      expect(user.t1_pull_up_exercise > 0)
      expect(user.imported).to be_truthy
    end

    it 'sets the users subscriptions with their correct data' do
      expect(Subscription.count).to eq 4
    end

    it 'creates 4 products with their data' do
      expect(Product.count).to eq 5 #plus the unicorn product
    end

    it 'sets the subscription data for apple' do
      subscription = Subscription.where(product: Product.where(store: :apple).first).first
      expect(subscription.transaction_body.present?).to be_truthy #'itunes_receipt_raw']
    end

    it 'sets the subscription data for google play' do
      subscription = Subscription.where(product: Product.where(store: :google_play).first).first
      expect(subscription.transaction_body.present?).to be_truthy
    end

    it 'sets the subscription data for stripe' do
      subscription = Subscription.where(product: Product.where(store: :stripe).first).first
      expect(subscription.store_metadata['stripe_data'].present?).to be_truthy
      expect(subscription.store_metadata['stripe_customer_id'].present?).to be_truthy
    end

  end

  context '#call unicorns' do
    before(:each) do
      setup_exercises
      csv_file =  'pro_users.csv'
      ImportUsers.new(csv_file: csv_file, user_type: 'unicorn').call
    end

    it 'setups all unicorn subscriptions' do
      expect(Subscription.all.pluck(:product_id).uniq.first).to eq unicorn.id
    end

    it 'doesnt create 4 products with their data' do
      expect(Product.count).to eq 1 #plus the unicorn product
    end

    it 'imports the 4 users with their subscription data' do
      expect(User.count).to eq 6 # its 6 counting the owner of the program and admin
      user = User.third
      expect(user.email).to eq "yoli@lineaalba.com"
      expect(user.gender).to eq 'female'
      expect(user.date_of_birth).to be_truthy
      expect(user.height).to eq 154
      expect(user.weight).to eq 42.5
      expect(user.activity_level).to eq 'sedentary'
      expect(user.names).to eq 'Yolanda'
      expect(user.body_type).to eq 'lean'
      expect(user.goal).to eq 'antiaging'
      expect(user.body_fat).to eq 20
      expect(user.newsletter_subscription).to be_truthy
      expect(user.country).to eq 'ES'
      expect(user.training_days_setting).to eq 3
      expect(user.t1_push > 0)
      expect(user.t1_core > 0)                       
      expect(user.t1_legs > 0)                       
      expect(user.t1_full > 0)    
      expect(user.t1_push_exercise > 0)              
      expect(user.t1_pull_up > 0)                    
      expect(user.t2_reps > 0)                       
      expect(user.t2_steps > 0)                      
      expect(user.t2_reps_push > 0)                  
      expect(user.t2_reps_core > 0)                  
      expect(user.t2_reps_legs > 0)                  
      expect(user.t2_reps_full > 0)                  
      expect(user.t2_time_push > 0)                  
      expect(user.t2_time_core > 0)                  
      expect(user.t2_time_legs > 0)                  
      expect(user.t2_time_full > 0)                  
      expect(user.t1_full_exercise > 0)              
      expect(user.t1_pull_up_exercise > 0)
      expect(user.imported).to be_truthy
    end

  end

  context '#call programs' do
    before(:each) do
      setup_exercises
      csv_file =  'pro_users.csv'
      ImportUsers.new(csv_file: csv_file, user_type: 'programs').call
    end

    it 'doesnt create 4 products with their data' do
      expect(Product.count).to eq 1 #plus the unicorn product
    end

    it 'imports the only users with permited programs data' do
      expect(User.count).to eq 3 # its 6 counting the owner of the program and admin
    end

    it 'setups the user data' do
      user = User.third
      expect(user.email).to eq "yoli@lineaalba.com"
      expect(user.gender).to eq 'female'
      expect(user.date_of_birth).to be_truthy
      expect(user.height).to eq 154
      expect(user.weight).to eq 42.5
      expect(user.activity_level).to eq 'sedentary'
      expect(user.names).to eq 'Yolanda'
      expect(user.body_type).to eq 'lean'
      expect(user.goal).to eq 'antiaging'
      expect(user.body_fat).to eq 20
      expect(user.newsletter_subscription).to be_truthy
      expect(user.country).to eq 'ES'
      expect(user.training_days_setting).to eq 3
      expect(user.t1_push > 0)
      expect(user.t1_core > 0)                       
      expect(user.t1_legs > 0)                       
      expect(user.t1_full > 0)    
      expect(user.t1_push_exercise > 0)              
      expect(user.t1_pull_up > 0)                    
      expect(user.t2_reps > 0)                       
      expect(user.t2_steps > 0)                      
      expect(user.t2_reps_push > 0)                  
      expect(user.t2_reps_core > 0)                  
      expect(user.t2_reps_legs > 0)                  
      expect(user.t2_reps_full > 0)                  
      expect(user.t2_time_push > 0)                  
      expect(user.t2_time_core > 0)                  
      expect(user.t2_time_legs > 0)                  
      expect(user.t2_time_full > 0)                  
      expect(user.t1_full_exercise > 0)              
      expect(user.t1_pull_up_exercise > 0)
      expect(user.imported).to be_truthy
    end

    it 'setups the user programs' do
      user = User.third
      expect([rea.id, ringmaster.id, unbreakable.id, challenges.id, active_rests.id] - user.user_programs.pluck(:program_id)).to eq []
      expect(user.current_program.id).to eq rea.id
    end
  end
end