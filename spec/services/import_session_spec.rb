require 'rails_helper'

RSpec.describe ImportSession, type: :model do

  context '#call' do
    let!(:user) { FactoryBot.create(:user, email: 'pablo@mhunters.com', password: '123password', sign_in_count: 1)}
    let!(:program) { FactoryBot.create(:program, user: user)}
    let(:session_attributes) { {name: 'ola', description: 'ola ola desc'} }
    let!(:crunch) {FactoryBot.create(:exercise, name: 'crunch')}
    let!(:pull_up) {FactoryBot.create(:exercise, name: 'pull up')}

    before(:each) do
      crunch.update_column(:id, 1)
      pull_up.update_column(:id, 2)
    end

    it 'imports the session with their data' do
      csv_file =  File.read(Rails.public_path + 'session_example_with_ex_id.csv')
      ImportSession.new(csv_file: csv_file, user: user, session_attributes: session_attributes, program_id: program.id).call
      session = Session.first
      blocks = session.session_blocks
      expect(session.present?).to be_truthy
      expect(ProgramSession.count).to eq 1
      expect(blocks.count).to eq 1
      expect(blocks.first.session_sets.count).to eq 2
      expect(blocks.first.session_sets.first.session_set_type).to eq 'tabata'
      expect(blocks.first.session_sets.first.time_duration).to eq 80
      expect(blocks.first.session_sets.second.time_duration).to eq 50
      expect(blocks.first.session_sets.second.session_set_type).to eq 'to_the_one'
      expect(ExerciseSet.count).to eq 4
    end

    it 'updates if session_update_id present' do
      csv_file =  File.read(Rails.public_path + 'session_example_with_ex_id.csv')
      updated_csv_file = File.read(Rails.public_path + 'updated_session_example_with_ex_id.csv')
      ImportSession.new(csv_file: csv_file, user: user, session_attributes: session_attributes, program_id: program.id).call
      old_session = Session.first
      old_session.update(name_en: 'dsad', name_es: 'spanins', description_en: 'description_en', description_es: 'description_es')
      expect(Session.count).to eq 1
      ImportSession.new(csv_file: updated_csv_file, user: user, session_attributes: session_attributes, program_id: program.id, session_update_id: Session.last.id).call
      new_session = program.sessions.last
      program.reload
      expect(Session.count).to eq 2
      expect(program.sessions.count).to eq 1
      expect(new_session.id).not_to eq old_session.id
      expect(new_session.exercise_sets.first.reps).to eq 40
      expect(new_session.name_en).to eq old_session.name_en
      expect(new_session.name_es).to eq old_session.name_es
      expect(new_session.description_en).to eq old_session.description_en
      expect(new_session.description_es).to eq old_session.description_es
    end 

    it 'imports the session when using exercise ids ' do
      csv_file =  File.read(Rails.public_path + 'session_example_with_ex_id.csv')
      ImportSession.new(csv_file: csv_file, user: user, session_attributes: session_attributes, program_id: program.id).call
      session = Session.first
      blocks = session.session_blocks
      expect(session.present?).to be_truthy
      expect(ProgramSession.count).to eq 1
      expect(blocks.count).to eq 1
      expect(blocks.first.block_type).to eq 'tabata'
      expect(blocks.first.session_sets.count).to eq 2
      expect(blocks.first.session_sets.first.session_set_type).to eq 'tabata'
      expect(blocks.first.session_sets.second.session_set_type).to eq 'to_the_one'
      expect(ExerciseSet.count).to eq 4
    end

    it 'raises error and sends including the row it to sentry' do
      # we introduce error with exercise not found
      csv_file =  File.read(Rails.public_path + 'session_example_with_errors.csv')
      expect { ImportSession.new(csv_file: csv_file, user: user, session_attributes: session_attributes, program_id: program.id).call }.to raise_error('not found exercise')
      expect(Session.count).to eq 0
      expect(ProgramSession.count).to eq 0
      expect(SessionSet.count).to eq 0
      expect(ExerciseSet.count).to eq 0 
    end

  end

end