
require 'rails_helper'

RSpec.describe ProProgram::Create, type: :model do

  context '#call' do
    
    let!(:user_admin) { FactoryBot.create(:user, is_admin: true, email: 'dev@mhunters.com' ) }
    let!(:user) { FactoryBot.create(:user, body_fat: 20, gender: :male, activity_level: :medium_active, goal: 1, 
                                    t1_push: 10, t1_legs: 10, t1_core: 10, t1_full: 10) } 
   
    let!(:description_en) { 'Description en' }
    let!(:description_es) { 'Description es' }

    before(:each) do
      setup_exercises
      ProProgram::Create.new(user: user).call
    end

    it 'should create a pro program with t1 session for the user' do
      program = user.current_program
      session = program.sessions.first
      expect(program.auto_generated).to be_truthy
      expect(program.present?).to be_truthy
      expect(program.code_name).to eq 'pro_auto'
      expect(program.name_en).to eq 'Pro'
      expect(program.name_es).to eq 'Innegociable'          
      expect(program.pro).to be_truthy 
      expect(program.description_es).to eq description_es
      expect(program.description_en).to eq description_en
      expect(session.session_type).to eq 'challenge' #first test
      expect(session.order).to eq  1
      expect(program.sessions.count).to eq 1
      #expect(session.id).to eq user.current_session.id
    end
  end

end
