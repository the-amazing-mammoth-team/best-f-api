require 'rails_helper'

RSpec.describe ProProgram::SessionsGenerator, type: :model do

  context '#call' do
    
    let!(:user_admin) { FactoryBot.create(:user, is_admin: true, email: 'dev@mhunters.com' ) }
    let!(:exercise_names) { [:push_up, :squat, :burpee, :plank_pivot, :rest, :semi_burpee, :knee_push_up, :pull_up, :run]}

    def get_exercise(name)
      Exercise.find_by(name_en: name)
    end

    before(:each) do
      setup_exercises
      setup_many_other_exercises
      setup_many_other_exercises
      pro_program = ProProgram::Create.new(user: user).call.program
      test_2 = SessionGenerator::TestSession.new.t2(user) 
      ProgramSession.create!(program: pro_program, session: test_2)
      @pro_generator = ProProgram::SessionsGenerator.new(user: user, sessions_number: 40, exercises: Exercise.all, program: pro_program)
    end

    context 'user with tests done' do

      let!(:user) { FactoryBot.create(:user, t1_push: 60, t1_core: 60, t1_legs: 60, t1_full: 60, t1_push_exercise: 5252, t1_full_exercise: 5236,
                                      t2_reps: 80, t2_steps: 4, t2_reps_push: 20, t2_reps_core: 20, t2_reps_legs: 20, t2_reps_full: 20) }

      it 'should give me 40 session generators array' do
        expect(@pro_generator.sessions_to_generate.count).to eq 40
      end

      it 'should add 40 sessions to their pro program' do
        @pro_generator.call
        program = user.current_program
        expect(program.auto_generated).to be_truthy
        expect(program.present?).to be_truthy
        expect(program.name_en).to eq 'Pro'      
        expect(program.sessions.count).to eq 42 # 2 tests and 40 others 
      end
    end

    context 'user without their tests done' do
      let!(:user) { FactoryBot.create(:user, t1_push: 60, t1_core: 60, t1_legs: 60, t1_full: 60, t1_push_exercise: 5252, t1_full_exercise: 5236,
                                      t2_reps: 0) }

      it 'should rise an error when called' do
        pro_program = ProProgram::Create.new(user: user).call.program
        expect { ProProgram::SessionsGenerator.new(user: user, sessions_number: 40, exercises: Exercise.all, program: pro_program).call }.to raise_error 'user has not finished strengh tests'
      end
    end

  end
end
