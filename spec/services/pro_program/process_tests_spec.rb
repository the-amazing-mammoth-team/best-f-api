require 'rails_helper'

RSpec.describe ProProgram::ProcessTests, type: :model do

  let!(:user_admin) { FactoryBot.create(:user, is_admin: true, email: 'dev@mhunters.com' ) }
  let!(:muscle_female_user) { FactoryBot.create(:user, body_fat: 25, gender: :female, activity_level: :medium_active, goal: 1) }
  let!(:muscle_female_user) { FactoryBot.create(:user, body_fat: 25, gender: :female, activity_level: :medium_active, goal: 1) }
  let!(:user_without_t_values) { FactoryBot.create(:user)}

  let!(:user) { FactoryBot.create(:user, body_fat: 20, gender: :male, activity_level: :medium_active, goal: 1, 
                                  t1_push: 10, t1_legs: 10, t1_core: 10, t1_full: 10) }

  let!(:exercise_names) { ['Push up', 'Squat', 'Burpee', 'Plank pivot', 'rest', 'Semi burpee', 'Knee push up', 'Plank balance'] }
  
  def exercises
    Exercise.where(name_en:exercise_names)
  end

  def get_exercise(name)
    exercises.find_by(name_en: name)
  end
  
  before(:each) do
    setup_exercises
    pro_program = ProProgram::Create.new(user: user).call
    @test_1 = user.current_program.sessions.first
  end

  context '#process_t1' do
    context 't1 done with push ups and burpees' do
      it 'should process test and setup t1 values in user' do
        test_done = test_track(session: @test_1, program: user.current_program, rest: get_exercise('rest'), push_up: get_exercise('Push up'), 
                               squat: get_exercise('Squat'), plank_pivot: get_exercise('Plank pivot'), burpee: get_exercise('Burpee'), reps: 60)
        session_execution =  SaveSessionExecutionService.new(session_execution_params: test_done, user: user).call

        ProProgram::ProcessTests.new(user: user, session_execution: session_execution).process_t1
        expect(user.t1_push).to eq 60
        expect(user.t1_core).to eq 60
        expect(user.t1_legs).to eq 60
        expect(user.t1_full).to eq 60
        expect(user.t1_push_exercise).to eq  get_exercise('Push up').id
        expect(user.t1_full_exercise).to eq  get_exercise('Burpee').id
      end
    end

    context 't1 done with knee push ups and burpees' do
      it 'should process test and setup t1 values in user' do
        test_done = test_track(session: @test_1, program: user.current_program, rest: get_exercise('rest'), push_up: get_exercise('Knee push up'), 
                               squat: get_exercise('Squat'), plank_pivot: get_exercise('Plank pivot'), burpee: get_exercise('Burpee'), reps: 60)
        session_execution =  SaveSessionExecutionService.new(session_execution_params: test_done, user: user).call

        ProProgram::ProcessTests.new(user: user, session_execution: session_execution).process_t1
        expect(user.t1_push).to eq 60
        expect(user.t1_core).to eq 60
        expect(user.t1_legs).to eq 60
        expect(user.t1_full).to eq 60
        expect(user.t1_push_exercise).to eq  get_exercise('Knee push up').id
        expect(user.t1_full_exercise).to eq  get_exercise('Burpee').id
      end
    end

    context 't1 done with push ups and semi burpees' do
      it 'should process test and setup t1 values in user' do
        test_done = test_track(session: @test_1, program: user.current_program, rest: get_exercise('rest'), push_up: get_exercise('Push up'), 
                               squat: get_exercise('Squat'), plank_pivot: get_exercise('Plank pivot'), burpee: get_exercise('Semi burpee'), reps: 60)
        session_execution =  SaveSessionExecutionService.new(session_execution_params: test_done, user: user).call

        ProProgram::ProcessTests.new(user: user, session_execution: session_execution).process_t1
        expect(user.t1_push).to eq 60
        expect(user.t1_core).to eq 60
        expect(user.t1_legs).to eq 60
        expect(user.t1_full).to eq 60
        expect(user.t1_push_exercise).to eq  get_exercise('Push up').id
        expect(user.t1_full_exercise).to eq  get_exercise('Semi burpee').id
      end
    end

    context 't1 done with less than 10 push ups and burpees' do
      it 'should process test and setup t1 values in user' do
        test_done = test_track(session: @test_1, program: user.current_program, rest: get_exercise('rest'), push_up: get_exercise('Push up'), 
                               squat: get_exercise('Squat'), plank_pivot: get_exercise('Plank pivot'), burpee: get_exercise('Burpee'), reps: 9)
        session_execution =  SaveSessionExecutionService.new(session_execution_params: test_done, user: user).call

        ProProgram::ProcessTests.new(user: user, session_execution: session_execution).process_t1
        expect(user.t1_push).to eq 18
        expect(user.t1_core).to eq 9
        expect(user.t1_legs).to eq 9
        expect(user.t1_full).to eq 18
        expect(user.t1_push_exercise).to eq  get_exercise('Knee push up').id
        expect(user.t1_full_exercise).to eq  get_exercise('Semi burpee').id
      end
    end

    context 't2 done' do

      before(:each) do
        # using a user with 60 reps in every exercise of t1
        test_done = test_track(session: @test_1, program: user.current_program, rest: get_exercise('rest'), push_up: get_exercise('Push up'), 
                               squat: get_exercise('Squat'), plank_pivot: get_exercise('Plank pivot'), burpee: get_exercise('Burpee'), reps: 60)
        session_execution =  SaveSessionExecutionService.new(session_execution_params: test_done, user: user).call

        ProProgram::ProcessTests.new(user: user, session_execution: session_execution).process_t1
        @test_2 = SessionGenerator::TestSession.new.t2(user)
        ProgramSession.create!(program: user.current_program, session: @test_2)
      end

      it 'should process tests and setup t2 values in user' do
        test_done = test2_track(session: @test_2, program: user.current_program, push_up: get_exercise('Push up'), 
                               squat: get_exercise('Squat'), plank_pivot: get_exercise('Plank pivot'), burpee: get_exercise('Burpee'), reps: 20)
        session_execution =  SaveSessionExecutionService.new(session_execution_params: test_done, user: user).call

        ProProgram::ProcessTests.new(user: user, session_execution: session_execution).process_t2
        expect(user.t2_reps).to eq 80
        expect(user.t2_steps).to eq 4
        expect(user.t2_reps_push).to eq 20
        expect(user.t2_reps_core).to eq 20
        expect(user.t2_reps_legs).to eq  20
        expect(user.t2_reps_legs).to eq  20
        # TODO ADD TIMES AND AVERAGES OR DELETE THEM FROM USER DATA
      end
    end
  end
end