require 'rails_helper'

RSpec.describe ProcessSessionExecution, type: :model do

  context '#call' do

    let!(:user_admin) { FactoryBot.create(:user, is_admin: true, email: 'dev@mhunters.com' ) }
    let!(:user) { FactoryBot.create(:user, body_fat: 20, gender: :male, activity_level: :medium_active, goal: 1) } 
    let!(:exercise_names) { exercises_data }
    let!(:program) { FactoryBot.create(:program) }
    let!(:session) { FactoryBot.create(:session, order: 1)}
    let!(:program_session) { FactoryBot.create(:program_session, program: program, session: session) }
    
    def get_exercise(name)
      Exercise.find exercises_data[name.to_sym]
    end

    context 'finish program' do

      before(:each) do
        setup_exercises
        user_program =  SetUserProgram.new(user: user, program_id: program.id).call
        session_done = test_track(session: session, program: user.current_program, rest: get_exercise('rest'), push_up: get_exercise('push_up'), 
        squat: get_exercise('squat'), plank_pivot: get_exercise('plank_pivot'), burpee: get_exercise('burpee'), reps: 60)
        session_execution =  SaveSessionExecutionService.new(session_execution_params: session_done, user: user).call
        # session_execution_summary = ProcessSessionExecution.new(session_execution: session_execution).call
        # expect(session_execution_summary.id.present?).to be_truthy
        sleep 2
        user.reload
      end

      it 'checks the user_program as completed after completing all program sessions' do
        expect(user.user_program.completed).to be_truthy
      end

    end

    context 'tests' do
      before(:each) do
        setup_exercises
        setup_many_other_exercises
        exercises = Exercise.where.not(name_en: ['rest', 'run'])
        pro_program = ProProgram::Create.new(user: user).call.program
        @test_1 = pro_program.sessions.first
      end  

      context 'after t1' do

        before(:each) do
          test_done = test_track(session: @test_1, program: user.current_program, rest: get_exercise('rest'), push_up: get_exercise('push_up'), 
          squat: get_exercise('squat'), plank_pivot: get_exercise('plank_pivot'), burpee: get_exercise('burpee'), reps: 60)
          session_execution =  SaveSessionExecutionService.new(session_execution_params: test_done, user: user).call
          # session_execution_summary = ProcessSessionExecution.new(session_execution: session_execution).call
          # expect(session_execution_summary.id.present?).to be_truthy
          sleep 2
          user.reload
        end

        it 'process the t1 values of the user and assigns the next session' do
          expect(user.current_session.session_type).to eq 'challenge'
          expect(user.current_session.order).to eq 2
          expect(user.current_program.sessions.count).to eq 2
          expect(user.t1_push).to eq 60
          expect(user.t1_core).to eq 60
          expect(user.t1_legs).to eq 60
          expect(user.t1_full).to eq 60
          expect(user.t1_push_exercise).to eq  get_exercise('push_up').id
          expect(user.t1_full_exercise).to eq  get_exercise('burpee').id
        end 

        it 'process the streaks' do
          # test_done = test_track(session: @test_1, program: user.current_program, rest: get_exercise('rest'), push_up: get_exercise('push_up'), 
          #                       squat: get_exercise('squat'), plank_pivot: get_exercise('plank_pivot'), burpee: get_exercise('burpee'), reps: 60)
          # session_execution =  SaveSessionExecutionService.new(session_execution_params: test_done, user: user).call
          # # session_execution_summary = ProcessSessionExecution.new(session_execution: session_execution).call
          # # expect(session_execution_summary.id.present?).to be_truthy
          # sleep 2
          # user.reload
          expect(user.current_weekly_streak).to eq 1
          expect(user.best_weekly_streak).to eq 1
        end

        it 'doesnt finishes pro for tests' do
          expect(user.user_program.completed).to be_falsey
        end
      end

      context 'after t2' do
        before(:each) do
          test_done = test_track(session: @test_1, program: user.current_program, rest: get_exercise('rest'), push_up: get_exercise('push_up'), 
                                squat: get_exercise('squat'), plank_pivot: get_exercise('plank_pivot'), burpee: get_exercise('burpee'), reps: 60)
          session_execution =  SaveSessionExecutionService.new(session_execution_params: test_done, user: user).call
          sleep 2
        end

        it 'process the t2 values of the user and generates the other 20 sessions for the program' do
          expect(user.current_session.session_type).to eq 'challenge'
          expect(user.current_session.order).to eq 2
          test2_done = test2_track(session: user.current_session, program: user.current_program, push_up: get_exercise('push_up'), 
                                squat: get_exercise('squat'), plank_pivot: get_exercise('plank_pivot'), burpee: get_exercise('burpee'), reps: 20)
          session_execution =  SaveSessionExecutionService.new(session_execution_params: test2_done, user: user).call
          sleep 2
          user.reload
          expect(user.t2_reps).to eq 80
          expect(user.t2_steps).to eq 4
          expect(user.t2_reps_push).to eq 20
          expect(user.t2_reps_core).to eq 20
          expect(user.t2_reps_legs).to eq  20
          expect(user.t2_reps_legs).to eq  20        
          expect(user.current_program.sessions.count).to eq 22
          expect(user.current_weekly_streak).to eq 1
          expect(user.best_weekly_streak).to eq 1
        end
      end
    end
  end

end