require 'rails_helper'

RSpec.describe SessionGenerator::ToTheOne, type: :model do

  let!(:exercise_names) { [:push_up, :squat, :burpee, :plank_pivot, :rest, :semi_burpee, :knee_push_up, :pull_up, :run]}

  def get_exercise(name)
    Exercise.find_by(name_en: name)
  end

  before(:each) do
    setup_exercises
    setup_many_other_exercises
    exercise_names.each { |exercise_name| FactoryBot.create(:exercise, exercise_name) }
  end

  context 'with 3 reps and 2 exercises as params' do
    let!(:user) { FactoryBot.create(:user, t1_push: 60, t1_core: 60, t1_legs: 60, t1_full: 60, t1_push_exercise: get_exercise('Push up').id, t1_full_exercise: get_exercise('Burpee').id,
                                    t2_reps: 80, t2_steps: 4, t2_reps_push: 20, t2_reps_core: 20, t2_reps_legs: 20, t2_reps_full: 20) }

    it 'creates a session with 3 session_sets and 3 exercise_sets each' do
      session_generator_params = { exercises: Exercise.all }
      session_generator =  SessionGenerator::ToTheOne.new(session_generator_params: session_generator_params, user: user)
      session = session_generator.call
      session_block = session.session_blocks.first
      expect(session.present?).to be_truthy
      expect(session.session_type).to eq 'to_the_one'
      expect(session_block.session_sets.count).to eq 8
      expect(session_block.session_sets.first.exercise_sets.first.reps).to eq 8
      expect(session_block.session_sets.second.exercise_sets.first.reps).to eq 7
      expect(session_block.session_sets.third.exercise_sets.first.reps).to eq 6
    end
  end

end