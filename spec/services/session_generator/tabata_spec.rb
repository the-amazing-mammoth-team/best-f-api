require 'rails_helper'

RSpec.describe SessionGenerator::Tabata, type: :model do

  let!(:crunch) { FactoryBot.create(:exercise, name_en: 'crunch') }
  let!(:pull_up) { FactoryBot.create(:exercise, name_en: 'pull up') }
  let!(:exercise_names) { ['Push up', 'Squat', 'Burpee', 'Plank pivot', 'rest', 'Semi burpee', 'Knee push up'] }

  def exercises
    Exercise.where(name_en:exercise_names)
  end

  def get_exercise(name)
    exercises.find_by(name_en: name)
  end

  before(:each) do
    setup_exercises
    exercise_names.each { |exercise_name| FactoryBot.create(:exercise, name_en: exercise_name) }
  end

  context '#call with a user with t2_reps == 80' do

    let!(:user) { FactoryBot.create(:user, t1_push: 60, t1_core: 60, t1_legs: 60, t1_full: 60, t1_push_exercise: get_exercise('Push up').id, t1_full_exercise: get_exercise('Burpee').id,
                                  t2_reps: 80, t2_steps: 4, t2_reps_push: 20, t2_reps_core: 20, t2_reps_legs: 20, t2_reps_full: 20) }

    it 'returns a session with 15 sets with 8 exercises intercalated by rests (per exercise) and a set of rest with 60 secs' do
      exercises = Exercise.where.not(name_en: 'rest')
      session_generator_params = { exercises: exercises }
      session = SessionGenerator::Tabata.new(session_generator_params: session_generator_params, user: user).call
      session_sets_1 = session.session_blocks.first.session_sets
      user_level = SessionGenerator::Tabata.new(session_generator_params: session_generator_params, user: user).user_level
      expect(user_level[:min_t2_reps]).to eq 0
      expect(session.present?).to be_truthy
      expect(session.session_type).to eq 'tabata'
      expect(session.session_blocks.count).to eq 1
      expect(session_sets_1.first.exercise_sets.count).to eq 15
      #expect(session_sets_1.first.exercise_sets.last.exercise.name_en).to eq exercises.first.name_en
      expect(session_sets_1.first.exercise_sets.where.not(exercise: get_exercise('rest')).count).to eq 8
      expect(session_sets_1.first.exercise_sets.where.not(exercise: get_exercise('rest')).first.reps).to eq 0 
      expect(session_sets_1.first.exercise_sets.first.time_duration).to eq 20
      # expect(session_sets_2.first.exercise_sets.count).to eq 1
      # expect(session_sets_3.first.exercise_sets.count).to eq 15
    end
  end

  #todo add changes in t1 
  #todo add other user data for bigger cicles


end