require 'rails_helper'

RSpec.describe SessionGenerator::IntervalAtTime, type: :model do

  let!(:exercise_names) { [:push_up, :squat, :burpee, :plank_pivot, :rest, :semi_burpee, :knee_push_up, :pull_up, :run]}

  def get_exercise(name)
    Exercise.find_by(name_en: name)
  end

  before(:each) do
    setup_exercises
    setup_many_other_exercises
  end

  context '#call user' do
    let!(:user) { FactoryBot.create(:user, t1_push: 60, t1_core: 60, t1_legs: 60, t1_full: 60, t1_push_exercise: get_exercise('Push up').id, t1_full_exercise: get_exercise('Burpee').id,
                                    t2_reps: 80, t2_steps: 4, t2_reps_push: 20, t2_reps_core: 20, t2_reps_legs: 20, t2_reps_full: 20) }

    it 'returns a session with n sets (in this case 2) with 2 exercises each separated by 10 secs rest and with a minute rest between sets' do
      exercises = Exercise.where.not(name_en: ['rest', 'run'])
      session_generator_params = { exercises: exercises, add_pause: false }
      session_generator = SessionGenerator::IntervalAtTime.new(session_generator_params: session_generator_params, user: user)
      session = session_generator.call
      user_level = session_generator.user_level
      session_sets = session.session_blocks.first.session_sets
      expect(session.time_duration).to eq 7 * 60
      expect(session.present?).to be_truthy
      expect(session.session_type).to eq 'interval_at_time'
      expect(session_sets.count).to eq 1
      expect((user_level[:exercises_min]..user_level[:exercises_max]).include?(session.exercises.count)).to be_truthy
    end
  end

  context '#call with pause' do
    let!(:user) { FactoryBot.create(:user, t1_push: 60, t1_core: 60, t1_legs: 60, t1_full: 60, t1_push_exercise: get_exercise('Push up').id, t1_full_exercise: get_exercise('Burpee').id,
                                    t2_reps: 80, t2_steps: 4, t2_reps_push: 20, t2_reps_core: 20, t2_reps_legs: 20, t2_reps_full: 20) }

    it 'returns a session with n sets (in this case 2) with 2 exercises each separated by 10 secs rest and with a minute rest between sets' do
      exercises = Exercise.where.not(name_en: ['rest', 'run'])
      session_generator_params = { exercises: exercises, add_pause: true }
      session_generator = SessionGenerator::IntervalAtTime.new(session_generator_params: session_generator_params, user: user)
      session = session_generator.call
      user_level = session_generator.user_level
      session_sets = session.session_blocks.first.session_sets
      expect(session.time_duration).to eq 7 * 60
      expect(session.present?).to be_truthy
      expect(session.session_type).to eq 'interval_at_time_with_pause'
      #check this expect((user_level[:exercises_min]..user_level[:exercises_max]).include?(session.exercises.count)).to be_truthy
    end
  end
end