require 'rails_helper'

RSpec.describe SessionGenerator::SpartanRace, type: :model do

  let!(:exercise_names) { [:push_up, :squat, :burpee, :plank_pivot, :rest, :semi_burpee, :knee_push_up, :pull_up, :run]}

  def get_exercise(name)
    Exercise.find_by(name_en: name)
  end

  before(:each) do
    setup_exercises
    setup_many_other_exercises
  end

  context '#call with 2 exercises' do
    let!(:user) { FactoryBot.create(:user, t1_push: 60, t1_core: 60, t1_legs: 60, t1_full: 60, t1_push_exercise: get_exercise('Push up').id, t1_full_exercise: get_exercise('Burpee').id,
                                    t2_reps: 80, t2_steps: 4, t2_reps_push: 20, t2_reps_core: 20, t2_reps_legs: 20, t2_reps_full: 20) }

    it 'returns a session with n sets (in this case 4) with 2 exercises ending with  240  secs of run and decreasing' do
      exercises = Exercise.where.not(name_en: 'run')
      session_generator_params = { exercises: exercises }
      session_generator = SessionGenerator::SpartanRace.new(session_generator_params: session_generator_params, user: user)
      session = session_generator.call
      session_sets = session.session_blocks.first.session_sets
      expect(session.present?).to be_truthy
      expect(session.session_type).to eq 'spartan_race'
      expect(session_sets.count).to eq 4
      expect(session_sets.first.exercise_sets.count).to eq 3 # 2 exercises + run
      expect(session_sets.first.exercise_sets.last.exercise.name_en).to eq 'run'
      expect(session_sets.first.exercise_sets.last.time_duration).to eq 240
      expect(session_sets.second.exercise_sets.count).to eq 3
      expect(session_sets.second.exercise_sets.last.exercise.name_en).to eq 'run'
      expect(session_sets.second.exercise_sets.last.time_duration).to eq 180
      expect(session_sets.second.exercise_sets.first.reps).to eq (session_sets.first.exercise_sets.first.reps * 0.75).to_i
      expect(session_sets.second.exercise_sets.second.reps).to eq (session_sets.first.exercise_sets.second.reps * 0.75).to_i
    end
  end
end