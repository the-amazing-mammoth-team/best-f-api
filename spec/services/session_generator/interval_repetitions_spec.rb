require 'rails_helper'

RSpec.describe SessionGenerator::IntervalRepetitions, type: :model do

  let!(:exercise_names) { [:push_up, :squat, :burpee, :plank_pivot, :rest, :semi_burpee, :knee_push_up, :pull_up]}

  def get_exercise(name)
    Exercise.find_by(name_en: name)
  end

  before(:each) do
    setup_exercises
    setup_many_other_exercises
    setup_many_other_exercises
    setup_many_other_exercises
  end

  context '#call with user' do
    let!(:user) { FactoryBot.create(:user, t1_push: 60, t1_core: 60, t1_legs: 60, t1_full: 60, t1_push_exercise: get_exercise('Push up').id, t1_full_exercise: get_exercise('Burpee').id,
                                    t2_reps: 80, t2_steps: 4, t2_reps_push: 20, t2_reps_core: 20, t2_reps_legs: 20, t2_reps_full: 20) }

    it 'returns a session with n sets (in this case 1) with 5..10 (user level exercises min) exercises each' do
      exercises = Exercise.all
      session_generator_params = { exercises: exercises }
      session_generator = SessionGenerator::IntervalRepetitions.new(session_generator_params: session_generator_params, user: user)
      user_level = session_generator.user_level
      session = session_generator.call
      session_sets = session.session_blocks.first.session_sets
      expect(session.present?).to be_truthy
      expect(session.interval_repetitions?).to be_truthy
      expect(session_sets.count).to eq 1
      expect((user_level[:exercises_min]..user_level[:exercises_max]).include?(session.exercises.count)).to be_truthy
      expect(session_sets.first.exercise_sets.count).to eq session.exercises.count
      #expect(session_sets.first.exercise_sets.first.reps) this uses the random reps generator
    end
  end
end