require 'rails_helper'

RSpec.describe SessionGenerator::HardPaleoRun, type: :model do

  let!(:exercise_names) { [:push_up, :squat, :burpee, :plank_pivot, :rest, :semi_burpee, :knee_push_up, :pull_up, :run]}

  def get_exercise(name)
    Exercise.find_by(name_en: name)
  end

  before(:each) do
    setup_exercises
    setup_many_other_exercises
    setup_many_other_exercises
  end

  context '#call user at level 0' do

    let!(:user) { FactoryBot.create(:user, t1_push: 60, t1_core: 60, t1_legs: 60, t1_full: 60, t1_push_exercise: get_exercise('Push up').id, t1_full_exercise: get_exercise('Burpee').id,
                                    t2_reps: 80, t2_steps: 4, t2_reps_push: 20, t2_reps_core: 20, t2_reps_legs: 20, t2_reps_full: 20) }

    it 'returns a session with level_sets (in this case 4) with 3 level_exercises each separated by 90 secs of run' do
      exercises = Exercise.where.not(id: [Exercise.base_exercises[:run]])
      session_generator_params = { exercises: exercises }
      session_generator = SessionGenerator::HardPaleoRun.new(session_generator_params: session_generator_params, user: user)
      session = session_generator.call
      session_sets = session.session_blocks.first.session_sets
      expect(session.present?).to be_truthy
      expect(session.hard_paleo_run?).to be_truthy
      expect(session_sets.count).to eq 4
      expect(session_sets.first.exercise_sets.count).to eq 4 # 3 exercises plus a run exercise
      expect(session_sets.first.exercise_sets.last.exercise.name_en).to eq 'run'
      expect(session_sets.first.exercise_sets.last.time_duration).to eq 90
      expect(session_sets.second.exercise_sets.count).to eq 4
      expect(session_sets.second.exercise_sets.last.exercise.name_en).to eq 'run'
    end
  end
end