require 'rails_helper'

RSpec.describe SessionGenerator::ToTheOne, type: :model do

  let!(:user) { FactoryBot.create(:user, t1_push: 10, t1_legs: 10, t1_core: 10, t1_full: 10) }
  let!(:user_without_t_values) { FactoryBot.create(:user)}

  before(:each) do
    setup_exercises
  end

  context 'creating t1' do
    it 'creates a session with push ups, squats, burpees, plank_pivots and rest betweeen them' do
      session = SessionGenerator::TestSession.new.t1
      session_block = session.session_blocks.first
      expect(session.present?).to be_truthy
      expect(session.session_type).to eq 'challenge'
      expect(session.order).to eq 1
      expect(session_block.session_sets.first.exercise_sets.count).to eq 7
      expect(session_block.session_sets.first.exercise_sets.first.time_duration).to eq 60
      expect(session_block.session_sets.first.exercise_sets.second.time_duration).to eq 60
      expect(session_block.session_sets.first.exercise_sets.third.time_duration).to eq 60
    end
  end

  context 'creating t2' do
    it 'creates a session with push ups, squats, burpees, plank_pivots when the user has the data' do
      session = SessionGenerator::TestSession.new.t2(user)
      session_block = session.session_blocks.first
      expect(session.present?).to be_truthy
      expect(session.order).to eq 2
      expect(session.session_type).to eq 'challenge'
      expect(session_block.session_sets.first.time_duration).to eq 8 * 60
      expect(session_block.session_sets.first.exercise_sets.count).to eq 4
      expect(session_block.session_sets.first.exercise_sets.first.time_duration).to eq 0
      expect(session_block.session_sets.first.exercise_sets.second.time_duration).to eq 0
      expect(session_block.session_sets.first.exercise_sets.third.time_duration).to eq 0
      expect(session_block.session_sets.first.exercise_sets.fourth.time_duration).to eq 0
    end

    it 'it raises error if the user has not done the first test' do
      expect { SessionGenerator::TestSession.new.t2(user_without_t_values) }.to raise_error
    end
  end

end