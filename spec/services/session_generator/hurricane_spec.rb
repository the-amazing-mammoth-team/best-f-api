require 'rails_helper'

RSpec.describe SessionGenerator::Hurricane, type: :model do

  let!(:exercise_names) { [:push_up, :squat, :burpee, :plank_pivot, :rest, :semi_burpee, :knee_push_up, :pull_up]}

  def get_exercise(name)
    Exercise.find_by(name_en: name)
  end

  before(:each) do
    setup_exercises
    setup_many_other_exercises
    setup_many_other_exercises
  end

  context '#call with 2 exercises' do
    let!(:user) { FactoryBot.create(:user, t1_push: 60, t1_core: 60, t1_legs: 60, t1_full: 60, t1_push_exercise: Exercise.base_exercises[:push_up], t1_full_exercise: Exercise.base_exercises[:burpee],
                                    t2_reps: 80, t2_steps: 4, t2_reps_push: 20, t2_reps_core: 20, t2_reps_legs: 20, t2_reps_full: 20) }

    it 'returns a session with n sets (in this case 2) with 2 exercises each separated by 10 secs rest and with a minute rest between sets' do
      exercises = Exercise.where.not(id: [Exercise.base_exercises[:rest]])
      session_generator_params = { exercises: exercises }
      session_generator = SessionGenerator::Hurricane.new(session_generator_params: session_generator_params, user: user)
      session = session_generator.call
      session_sets = session.session_blocks.first.session_sets
      expect(session.present?).to be_truthy
      expect(session_sets.count).to eq 3 #level_sets + rest in the middle
      expect(session_sets.first.exercise_sets.count).to eq 4
      expect(session_sets.second.exercise_sets.count).to eq 1
      expect(session_sets.second.exercise_sets.last.exercise.name_en).to eq 'rest'
      expect(session_sets.third.exercise_sets.count).to eq 4
    end
  end
end