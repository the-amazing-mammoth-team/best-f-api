require 'rails_helper'

RSpec.describe SessionGenerator::PaleoRun, type: :model do

  let!(:exercise_names) { [:push_up, :squat, :burpee, :plank_pivot, :rest, :semi_burpee, :knee_push_up, :pull_up, :run]}

  def get_exercise(name)
    Exercise.find_by(name_en: name)
  end

  before(:each) do
    setup_exercises
    setup_many_other_exercises
    setup_many_other_exercises
  end

  context '#call with a user with this params' do
    let!(:user) { FactoryBot.create(:user, t1_push: 60, t1_core: 60, t1_legs: 60, t1_full: 60, t1_push_exercise: get_exercise('Push up').id, t1_full_exercise: get_exercise('Burpee').id,
                                    t2_reps: 80, t2_steps: 4, t2_reps_push: 20, t2_reps_core: 20, t2_reps_legs: 20, t2_reps_full: 20) }

    it 'returns a session with n sets (in this case 2) with 2 exercises each separated by 60 secs of run' do
      exercises = Exercise.where.not(name_en: 'run')
      session_generator_params = { exercises: exercises }
      session_generator = SessionGenerator::PaleoRun.new(session_generator_params: session_generator_params, user: user)
      session = session_generator.call
      user_level = session_generator.user_level
      session_sets = session.session_blocks.first.session_sets
      expect(session.present?).to be_truthy
      expect(session.session_type).to eq 'paleo_run'
      expect(session_sets.count).to eq session_generator.sets_number(session.total_reps) #in this example is 1
      expect((user_level[:exercises_min]..user_level[:exercises_max]).include?(session.exercises.count)).to be_truthy
      expect(session_sets.first.exercise_sets.second.exercise.name_en).to eq 'run'
      expect(session_sets.first.exercise_sets.last.exercise.name_en).to eq 'run'
    end
  end
end