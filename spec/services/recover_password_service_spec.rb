require 'rails_helper'

RSpec.describe RecoverPasswordService, type: :model do

  context '#call' do
    let!(:user) { FactoryBot.create(:user, email: 'pablo@mhunters.com', password: '123password', sign_in_count: 1)}

    context 'with a registered email' do

      it 'sets a recover_password_code and sends an email to the user registered email with the 6 numbers code ' do
        RecoverPasswordService.call(email: user.email)
        user.reload
        expect(user.recover_password_code.present?).to be_truthy 
      end
    end

    context 'with a non registered email' do
      it 'raises a standard error with user not found in the db' do
        expect { RecoverPasswordService.call(email: 'nonexistent@gmail.com') }.to raise_error('user not found')
      end
    end
  end

end