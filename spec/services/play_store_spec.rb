require 'rails_helper'

RSpec.describe Payments::PlayStore, type: :model do

  let!(:admin_user) { FactoryBot.create(:user, email: 'pablo@mhunters.com', password: '123password', sign_in_count: 1)} 
  let!(:programs) {FactoryBot.create_list(:program, 5, user: user, available: true)}
  let!(:apple_3_months) {FactoryBot.create(:product, store: :apple, price: 33)}
  let!(:apple_1_year) {FactoryBot.create(:product, store: :apple, price: 100)}
  let!(:google_1_year) {FactoryBot.create(:product, store: :google_play, price: 80)}
  let!(:program_pro) { FactoryBot.create(:program, code_name: 'pro_automatic', pro: true, available:  true) }
  let!(:user) { FactoryBot.create(:user, email: 'pablo2@mhunters.com', password: '123password', sign_in_count: 1)} 
  let!(:jwt) { User.authenticate({ email: user.email, password: '123password' }).jwt }
  let!(:exercise_names) { [:push_up, :squat, :burpee, :plank_pivot, :rest, :semi_burpee, :knee_push_up, :pull_up, :run, :plank_balance]}

  

  context '#playstore' do
    it 'loads the object' do
      subscription = SubscribeUser.new(user: user, program_id: programs.first.id, product_id: google_1_year.id, 
        transaction_body: 'body of transaction', extra_params: {'affiliate_code'=> 'affiliate_code', 'offer_code' => 'offer_code'}).call
      payment_service = Payments::PlayStore.new(subscription: subscription)
      expect(payment_service.present?).to be_truthy
    end
  end

  context '#appstore' do
    it 'loads the object' do
      subscription = SubscribeUser.new(user: user, program_id: programs.first.id, product_id: apple_1_year.id, 
        transaction_body: 'body of transaction', extra_params: {'affiliate_code'=> 'affiliate_code', 'offer_code' => 'offer_code'}).call
      payment_service = Payments::AppStore.new(subscription: subscription)
      expect(payment_service.present?).to be_truthy
    end
  end


end