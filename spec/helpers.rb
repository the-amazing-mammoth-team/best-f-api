module Helpers

  def exercises_data
    Exercise.base_exercises
  end

  def setup_many_other_exercises
    exercise_names = [:push_up, :squat, :burpee, :plank_pivot, :rest, :semi_burpee, :knee_push_up]
    exercise_names.each { |exercise_name| FactoryBot.create(:exercise, exercise_name, coach: admin) }
    exercise_names.each { |exercise_name| FactoryBot.create(:exercise, exercise_name, coach: admin) }
    exercise_names.each { |exercise_name| FactoryBot.create(:exercise, exercise_name, coach: admin) }
    exercise_names.each { |exercise_name| FactoryBot.create(:exercise, exercise_name, coach: admin) }
  end

  def setup_exercises
    exercises_data.each do |exercise_name, value|
      puts "setting up: #{exercise_name}"
      #exercise = Exercise.new(name_en: exercise_name, id: value, family)
      exercise = FactoryBot.build(:exercise, exercise_name.to_sym, id: value, coach: admin)
      exercise.save
      #exercise = exercise.update_column(:id, value)
    end
  end

  def setup_user_programs(user)
    #setup challenges, active rest and main program
    Program.where(code_name: ['challenges', 'active rests']).each { |program| UserProgram.create!(user: user, program: program) }
    main_program = Program.where.not(code_name: ['challenges', 'active rests']).first
    UserProgram.create!(user: user, program: main_program, active: true)
  end

  def setup_program_sessions(program)
    implement = FactoryBot.create(:implement)
    FactoryBot.create(:exercise_implement, exercise: Exercise.first, implement: implement)
    session = FactoryBot.create(:session)
    session.session_blocks << FactoryBot.create(:session_block, session: session)
    session.session_blocks.first.session_sets << FactoryBot.create(:session_set, session_block: session.session_blocks.first)
    session.session_blocks.first.session_sets.first.exercise_sets << FactoryBot.create(:exercise_set, exercise: Exercise.first, session_set: session.session_blocks.first.session_sets.first)
    program.sessions << session
    program.save!
  end

  def admin
    admin ||= User.find_by(email: 'admin@mhunters.com')
    return admin if admin.present?
    FactoryBot.create(:user, email: 'admin@mhunters.com')
  end

  def setup_programs_and_user_programs(user=nil)
    challenges = FactoryBot.create(:program, user: admin, code_name: 'challenges')
    active_rests = FactoryBot.create(:program, user: admin, code_name: 'active rests')
    warmups = FactoryBot.create(:program, user: admin, code_name: 'warmups')
    FactoryBot.create(:program, user: admin, code_name: 'not like others', name_en: 'english name', name_es: 'spanish name', priority_order: 2)
    Program.all.each { |program| setup_program_sessions(program) }
    setup_user_programs(user) if user
  end

  def test_track(session:, program:, rest:, push_up:, squat:, plank_pivot:, burpee:, reps:)
    { session_execution: {:front_end_id=>"5a63c546-d2ef-11ea-87d0-0242ac130003",
    :session_id=>"#{session.id}",
    :program_id=>"#{program.id}",
      :session_block_executions=>
        [{:order=>1,
          :session_set_executions=>
          [{:order=>1, :exercise_executions=>[{:exercise_id=>"#{push_up.id}", :order=>1, :reps_executed=>reps, :execution_time=>60},
                                              {:exercise_id=>"#{rest.id}", :order=>2, :reps_executed=>0, :execution_time=>60},
                                              {:exercise_id=>"#{squat.id}", :order=>3, :reps_executed=>reps, :execution_time=>60},
                                              {:exercise_id=>"#{rest.id}", :order=>4, :reps_executed=>0, :execution_time=>60},
                                              {:exercise_id=>"#{plank_pivot.id}", :order=>5, :reps_executed=>reps, :execution_time=>60},
                                              {:exercise_id=>"#{rest.id}", :order=>6, :reps_executed=>0, :execution_time=>60},
                                              {:exercise_id=>"#{burpee.id}", :order=>7, :reps_executed=>reps, :execution_time=>60}
                                            ]},
            
          ]},
        ]}
    }
  end

  def test2_track(session:, program:, push_up:, squat:, plank_pivot:, burpee:, reps:)
    { session_execution: {:front_end_id=>"5a63c546-d2ef-11ea-87d0-0242ac130003",
    :session_id=>"#{session.id}",
    :program_id=>"#{program.id}",
      :session_block_executions=>
        [{:order=>1,
          :session_set_executions=>
          [{:order=>1, :exercise_executions=>[{:exercise_id=>"#{push_up.id}", :order=>1, :reps_executed=>reps, :execution_time=>60},
                                              {:exercise_id=>"#{squat.id}", :order=>3, :reps_executed=>reps, :execution_time=>60},
                                              {:exercise_id=>"#{plank_pivot.id}", :order=>5, :reps_executed=>reps, :execution_time=>60},
                                              {:exercise_id=>"#{burpee.id}", :order=>7, :reps_executed=>reps, :execution_time=>60}
                                            ]},
            
          ]},
        ]}
    }
  end
end