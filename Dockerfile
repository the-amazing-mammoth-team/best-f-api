FROM phusion/passenger-ruby26

RUN curl -sL https://deb.nodesource.com/setup_10.x | bash 
RUN apt-get install -y nodejs

RUN apt-get update && apt-get install -y tzdata && apt-get install imagemagick -y
RUN apt-get install shared-mime-info
# Install imagemagick + dependencies

RUN bash -lc 'rvm install ruby-2.7.5'

RUN bash -lc 'rvm --default use ruby-2.7.5'

RUN mkdir /myapp
WORKDIR /myapp
COPY Gemfile /myapp/Gemfile
COPY Gemfile.lock /myapp/Gemfile.lock
RUN bundle install --jobs 4
COPY . /myapp


# Add a script to be executed every time the container starts.
COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
EXPOSE 3000
EXPOSE 587
EXPOSE 80
#EXPOSE 443

# Start the main process.
CMD ["bundle", "exec", "rails", "server", "-b", "0.0.0.0"]
