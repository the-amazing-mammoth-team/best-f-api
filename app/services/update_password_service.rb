class UpdatePasswordService

  attr_accessor :email, :password, :code, :old_password

  def initialize(email: , code:, password:, old_password: nil)
    @email = email
    @code = code
    @password = password
    @old_password = old_password
  end

  def call
    user = check_user_existence
    check_code(user) if old_password.nil? && code.present?
    update_password(user)
  end

  def check_user_existence
    user = User.find_by(email: email)
    raise 'user not found' if user.nil?
    user
  end

  def check_code(user)
    if user.recover_password_code != code # add tries here
      attempts = user.recover_password_attempts + 1
      if attempts > 2
        user.update(recover_password_attempts: 0, recover_password_code: nil)
        raise 'too many tries'
      else
        user.update(recover_password_attempts: attempts)
        raise 'wrong recover password code' 
      end
    end
  end

  def update_password(user)
    if old_password.present?
      user = User.authenticate({email: user.email, password: old_password})
      user.update(password: password, recover_password_attempts: 0, recover_password_code: nil)
      user = User.authenticate({email: user.email, password: password})
    elsif user.recover_password_code == code && code.present?
      user.update(password: password, recover_password_attempts: 0, recover_password_code: nil)
      User.authenticate({email: email, password: password})
    end
  end
end