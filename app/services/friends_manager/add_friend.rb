module FriendsManager

  class AddFriend

    attr_accessor :user_id, :friend_id

    def initialize(user_id:, friend_id:)
      @user_id = user_id
      @friend_id = friend_id
    end

    def call
      friendship = Friendship.new(user_id: user_id, friend_id: friend_id)
      notify_friendship if friendship.save
      friendship
    end

    def notify_friendship
      #call for a job that notifies their device
    end

  end

end