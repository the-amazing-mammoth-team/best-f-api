# it can be id_H.mp4, id_M.mp4, or id.mp4
# https://stage-dev-new.s3-eu-west-1.amazonaws.com/exercises/5995_M.mp4

class LoadAwsVideos

  def self.aws_videos
    require "aws-sdk-s3"
    s3 = Aws::S3::Resource.new(region: 'eu-west-1', access_key_id: 'AKIAI4LGQTRJJ4YFYMRQ', secret_access_key: '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' )
    s3.bucket("stage-dev-new").objects(prefix: 'exercises/', delimiter: 'delimiter').collect(&:key).drop(1) # drop the first because it is exercises/
  end

  def self.call
    Exercise.all.each do |exercise|
      exercise.video, exercise.video_male, exercise.video_female = nil
      exercise.video_male = build_url_for(exercise, :male) if exists_in_aws?(exercise, :male)
      exercise.video_female = build_url_for(exercise, :female) if exists_in_aws?(exercise, :female)
      exercise.video = build_url_for(exercise, nil) if exists_in_aws?(exercise, nil)
      exercise.save!
    end
  end

  def self.build_url_for(exercise, person)
    return "https://stage-dev-new.s3-eu-west-1.amazonaws.com/exercises/#{exercise.id}_H.mp4" if person == :male
    return "https://stage-dev-new.s3-eu-west-1.amazonaws.com/exercises/#{exercise.id}_M.mp4" if person == :female
    "https://stage-dev-new.s3-eu-west-1.amazonaws.com/exercises/#{exercise.id}.mp4" if person.nil?
  end

  def self.exists_in_aws?(exercise, person)
    return aws_videos.include? "exercises/#{exercise.id}_H.mp4" if person == :male
    return aws_videos.include? "exercises/#{exercise.id}_M.mp4" if person == :female
    aws_videos.include? "exercises/#{exercise.id}.mp4" if person.nil?
  end

end