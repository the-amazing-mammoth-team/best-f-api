class SetupUnicornAccount

  attr_accessor :user
  
  def initialize(user: )
    @user = user
  end

  def call
    product = Product.where(store: :unicorn).first
    raise 'no unicorn product' if product.nil?
    user.subscriptions.update_all(status: :inactive)
    unicorn_subscription = user.subscriptions.where(product: product)
    if unicorn_subscription.present?
      puts 'updating'
      unicorn_subscription.update(status: :active, end_date: (Date.today + 10.years))
    else
      puts 'createing'
      unicorn_subscription = user.subscriptions.create!(product: product, end_date: (Date.today + 10.years), status: :active, program: Program.first)
    end
  end

end