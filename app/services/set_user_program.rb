class SetUserProgram

  attr_accessor :user, :program_id, :restart, :program

  def initialize(user:, program_id:, restart: false)
    @user =  user
    @program_id = program_id
    @program = Program.find program_id
    @restart = restart
  end

  def call
    user_program = user.user_programs.find_by(program_id: program_id)

    if user_program.present? && !restart #already started the user_program before
      user_program.update(active: true)
    elsif  program.code_name == 'MH_UNN'
      user_program = ProProgram::Create.new(user: user).call
    else
      program = Program.find program_id
      user_program = user.user_programs.create(program_id: program_id, active: true) #, current_session: program.sessions.first
    end
    user.user_programs.where.not(id: user_program.id).update_all(active: false)
    user_program
  end

end