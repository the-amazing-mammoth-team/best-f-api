# id
# nombre_es
# nombre_en	
# notes_es	
# notes_en	
# description_es
# description_en	
# indicaciones_tecnicas 
# program	
# family	
# subfamily	
# easier_variation	
# harder_variation	
# musculos	
# articulaciones
# material
# link 
#
# Table name: exercises
#
#  id          :bigint           not null, primary key
#  video       :string
#  reps        :integer
#  time        :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  name        :string
#  description :string
#
#   has_paper_trail
#   translates :name, :description

class ImportExercises

  require 'csv'

  attr_accessor :csv_file, :user

  def initialize(csv_file:, user:)
    @csv_file = csv_file
    @user = user
  end

  def call
    ActiveRecord::Base.transaction do
      PaperTrail.request.whodunnit = user.id
      csv = CSV.new(csv_file, col_sep: ';', headers: true)
      csv.each do |row|
        exercise = Exercise.find(row.field('id')) if row.field('id').present?
        exercise = Exercise.find_by(legacy_id: row.field('legacy_id')) if row.field('legacy_id').present?
        if exercise
          update_exercise(exercise, row)
        else
          add_new_exercise(row)
        end
      rescue StandardError, ActiveRecord::RecordInvalid => e
        UserMailer.importer_error(user, 'exercise importer', e, row.to_hash).deliver_now if Rails.env != 'test'
        Raven.tags_context error_data: row.to_hash
        raise e
      end
      UserMailer.importer_success(user, 'exercise importer').deliver_now if Rails.env != 'test'
    end
  end

  def update_exercise(exercise, row)
    #add variations here
    exercise = set_exercise_fields(exercise, row)
    exercise.save!
    exercise
  end

  def add_new_exercise(row)
    exercise = set_exercise_fields(Exercise.new, row)
    exercise.save!
    exercise
  end

  def set_exercise_fields(exercise, row)
    exercise.video = row.field('link') if row.field('link').present?
    exercise.video_female = row.field('link_female') if row.field('link_female').present?
    exercise.video_male = row.field('link_male') if row.field('link_male').present?
    exercise.name_es = row.field('nombre_es') if row.field('nombre_es').present?
    exercise.name_en = row.field('nombre_en') if row.field('nombre_en').present?
    exercise.description_es = row.field('description_es') if row.field('description_es')
    exercise.description_en = row.field('description_en') if row.field('description_en')
    exercise.legacy_id = row.field('legacy_id') if row.field('legacy_id')
    exercise.replacement_legacy_id = row.field('replacement_legacy_id') if row.field('replacement_legacy_id')
    exercise.deprecated = row.field('deprecated') if row.field('deprecated')
    exercise.reps = 0
    exercise.time = 0
    exercise.t1_min = row.field('t1_min') if row.field('t1_min')
    exercise.t1_max = row.field('t1_max') if row.field('t1_max')
    exercise.met_multiplier = row.field('met_multiplier') if row.field('met_multiplier')
    exercise.test_correction = row.field('test_correction') if row.field('test_correction')
    exercise.test_equivalent_id = row.field('test_equivalent') if row.field('test_equivalent')
    exercise.execution_time = row.field('execution_time') if row.field('execution_time')
    exercise.coach_id = row.field('coach_id') if row.field('coach_id')
    # body
    exercise.family = row.field('family') if row.field('family')
    exercise.sub_family = row.field('sub_family')&.gsub('SUBFAMILY_', '')&.downcase if row.field('sub_family')
    exercise.body_parts_focused = row.field('body_parts_focused')&.split(',') if row.field('body_parts_focused')
    exercise.muscles = row.field('muscles')&.downcase&.split(',') if row.field('muscles')
    exercise.joints = row.field('joints')&.downcase&.split(',') if row.field('joints')
    #variations
    exercise.harder_variation_id = row.field('harder_variation') if row.field('harder_variation')
    exercise.easier_variation_id = row.field('easier_variation') if row.field('easier_variation')
    exercise.implement_variation_id = row.field('implement_variation') if row.field('implement_variation')
    #notes
    exercise.notes_en = row.field('notes_en') if row.field('notes_en')
    exercise.notes_es = row.field('notes_es') if row.field('notes_es')
    #implements
    exercise.exercise_implements.build(implement_id: row.field('implement_id').to_i) if row.field('implement_id').present?
    exercise.exercise_implements.build(implement_id: row.field('implement_id2').to_i) if row.field('implement_id2').present?
    exercise
  end

end