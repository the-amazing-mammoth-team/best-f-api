class SubscribeStripe
  require 'stripe'

  attr_accessor :stripe_token, :product, :user

  def initialize(stripe_token:, product: , user:)
    @stripe_token = stripe_token
    @product = product
    @user = user
  end

  def create_customer
    stripe_customer = Stripe::Customer.create({
      description: 'subscription buy',
      email: user.email
    })
    user.update(stripe_id: stripe_customer.id)
  end

  def create_customer_card
    raise 'no stripe customer' if user.stripe_id.nil?
    Stripe::Customer.create_source( user.stripe_id, {source: stripe_token},)
  end

  def create_stripe_subscription
    raise 'no stripe customer' if user.stripe_id.nil?
    price_id = product.store_reference
    raise 'no subscription product found' if price_id.nil?
    Stripe::Subscription.create({customer: user.stripe_id, items: [ {price: price_id},],})
  end

  def call
    create_customer
    create_customer_card
    create_stripe_subscription
  end

end