class CalculateDiagnostic 

    attr_accessor :user

    def initialize(user: )
        @user = user
    end

    def call
      { weight: weight, bmi: bmi, body_fat: body_fat, muscle_weight: muscle_weight, target_bmi: target_bmi, ideal_weight: ideal_weight, 
        target_body_fat: target_body_fat, ideal_muscle_weight: ideal_muscle_weight, fat_to_lose: fat_to_lose, muscle_to_gain: muscle_to_gain,
        target_calories: target_calories, weeks_to_target_round: weeks_to_target_round }
    end

    def weight
        user.weight
    end

    def height
      user.height
    end

    def height_meters
        user.height / 100
    end

    def bmi
        (user.weight / (height_meters * height_meters)).round(1)
    end

    def body_fat
        user.body_fat
    end

    def fat_weight
        weight * (body_fat / 100)
    end

    def muscle_weight
        weight - fat_weight
    end

    def target_bmi
      ideal_bmi[user.gender.to_sym][user.goal.to_sym][user.body_type.to_sym]
    end

    def ideal_bmi
      { male: { loss_weight: { lean: 23, medium: 23, strong: 24 }, antiaging: { lean: 23, medium: 24, strong: 25},
                gain_muscle: { lean: 24, medium: 25, strong: 26 },},
        female: { loss_weight: { lean: 20, medium: 20, strong: 21, }, antiaging: { lean: 20, medium: 21, strong: 22, },
                gain_muscle: { lean: 21, medium: 22, strong: 23, },},
      }
    end

    def ideal_weight
      (target_bmi * (height_meters * height_meters)).round(1)       
    end

    def ideal_body_fat
      { male: 10, female: 15}
    end

    def target_body_fat
      ideal_body_fat[user.gender.to_sym]
    end

    def ideal_fat_weight
      ideal_weight * (target_body_fat / 100.0)
    end

    def ideal_muscle_weight
      (ideal_weight - ideal_fat_weight).round(1)
    end
    
    def fat_to_lose
      ([fat_weight - ideal_fat_weight, 0].max).round(1)
    end

    def muscle_to_gain 
      ([ideal_muscle_weight - muscle_weight, 0].max).round(1)
    end

    def activity_level_co 
      { sedentary: 1.15, medium_active: 1.35, very_active: 1.55, }
    end

    def goal_multiplier 
      { loss_weight: 0.8, antiaging: 1, gain_muscle: 1, }
    end

    def years_old 
      ((Time.zone.now - user.date_of_birth.to_time) / 1.year.seconds).floor
    end

    def gender_factor
      if user.gender == 'male'
        5
      else
        -161
      end
    end

    def bmr
      9.99 * weight + 6.25 * height - 4.92 * years_old + gender_factor
    end

    def rest_bmr
      bmr * activity_level_co[user.activity_level.to_sym]
    end

    def target_calories 
      (rest_bmr * goal_multiplier[user.goal.to_sym]).round()
    end

    def daily_target_deficit
      rest_bmr - target_calories
    end

    def muscle_to_gain_per_week_ratio
      if user.gender == 'male'
        0.25
      else
        0.13
      end
    end

    def days_to_target
      fat_to_lose * 9000 / daily_target_deficit
    end

    def weeks_to_target
      if user.goal == 'gain_muscle'
        muscle_to_gain / muscle_to_gain_per_week_ratio
      else
        days_to_target / 7
      end
    end

    def weeks_to_target_round
    (weeks_to_target).round()
    end


end
