class ExportExercises

  def call(csv_name: )
    require 'csv'
    CSV.open(csv_name, "wb") do |csv|
      csv << ["id", "legacy_id", "name", "description", 'link']
      Exercise.all.each do |exercise|
        exercise_info = []
        exercise_info << exercise.id
        exercise_info << exercise.legacy_id
        exercise_info << exercise.name
        exercise_info << exercise.description
        exercise_info << exercise_link(exercise)
        csv << exercise_info
      end
    end
  end

  def exercise_link(exercise)
    return "localhost:3000/admin/exercises/#{exercise.id}" if Rails.env != 'production'
    "http://backend.mhunters.com/admin/exercises/#{exercise.id}"
  end

end