class RecoverPasswordService

  def self.call(email:)
    user = User.find_by email: email
    raise 'user not found' if user.nil?
    user.update!(recover_password_code: recover_password_code = rand(100000..999999))
    send_recover_password_code_email(user)
  end

  def self.send_recover_password_code_email(user)
    job = RecoverPasswordJob.perform_later(user)
    job.present? ? true : false
  end

end
