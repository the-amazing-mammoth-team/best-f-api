class LegacySessionsImporter

  attr_accessor :file

  def initialize(file)
    @file = file
  end

  def create_or_find_legacy
    legacy = Program.find_by(code_name: 'legacy')
    if legacy.nil?
      legacy = Program.create(code_name: 'legacy', name_en: 'legacy', available: false, pro: true, priority_order: 0)
      legacy_session = Session.create(name_en: 'legacy session', name_es: 'sesion legacy', order: 0, code_name: 'legacy')
      session_program = ProgramSession.create(program: legacy, session: legacy_session)
    end
    legacy
  end

  def call
    #make big transaction
    create_or_find_legacy
    file_processing
  end

  def file_processing
    csv_read= File.read(file)
    csv = CSV.new(csv_read, col_sep: ',', headers: true)
    error = []
    csv.each do |row|
      row_processing(row)
    end
  end

  def row_processing(row)
    job = UserLegacySessionsImportJob.perform_later(row.field('email'), row.field('exercises'))
    raise 'error job' if job.nil?
    # email = row.field('email')
    # user = User.find_by(email: email)
    # if user
    #   load_user_data(user, row)
    # else
    #   puts "not found id: #{email}"
    # end
  end

  def setup_legacy_program(user)
    UserProgram.create(user: user, program: create_or_find_legacy, active: false, completed: true, current_session_id: nil)
  end

  def load_user_data(user, row)
    setup_legacy_program(user)
    exercises = JSON.parse row.field('exercises')
    exercises.each do |session_execution_data|
      session_execution_data = JSON.parse session_execution_data
      session_execution = create_session_execution(user, session_execution_data)
      create_session_execution_summary(user, session_execution_data, session_execution) unless session_execution.nil?
    end
  end

  def create_session_execution(user, session_execution_data)
    legacy_program = Program.find_by(code_name: 'legacy')
    user_legacy_program = user.user_programs.where(program: legacy_program).first
    raise 'no legacy do setup' if user_legacy_program.nil?
    legacy_session = legacy_program.sessions.first
    session_execution = SessionExecution.new(session: legacy_session, user_program: user_legacy_program, imported: true)
    session_execution.reps_executed = session_execution_data['completed_reps']
    session_execution.execution_time = session_execution_data['time']
    session_execution.created_at = session_execution_data['created_at']
    session_execution.enjoyment_feedback = delight_t(session_execution_data['delight'])
    session_execution.difficulty_feedback = difficulty_t(session_execution_data['difficulty'])
    session_execution.save!
    session_execution
  end

  def difficulty_t(difficulty)
    #hard, perfect, easyf
    return 1 if difficulty == 'easy'
    return 5 if difficulty == 'perfect'
    return 10 if difficulty == 'hard'
  end

  def delight_t(delight)
    return 1 if delight == 'no'
    return 3 if delight == 'meh'
    return 5 if delight == 'awesome'
    #meh, no, awesp,e
  end

  def create_session_execution_summary(user, session_execution_data, session_execution)
    session_execution_summary = SessionExecutionSummary.new(session_execution: session_execution)
    session_execution_summary.total_reps = session_execution_data['completed_reps']
    session_execution_summary.total_time = session_execution_data['time']
    session_execution_summary.created_at = session_execution_data['created_at']
    session_execution_summary.value_of_session = delight_t(session_execution_data['delight'])
    session_execution_summary.effort = difficulty_t(session_execution_data['difficulty'])
    session_execution_summary.points = session_execution_data['points']
    session_execution_summary.total_kcal =session_execution_data['met']
    session_execution_summary.save!
    session_execution_summary
  end

end