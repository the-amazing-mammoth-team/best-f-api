# setups cancelled(true) and cancelled_at
# user is active until end_date of subscription
# if the user has their current_subscription cancelled it wont renew their subscription at the end_date of sub
# if the user has their current_subscription cancelled 

class CancelSubscriptionService

  attr_accessor :user, :cancellation_reason

  def initialize(user: , cancellation_reason:)
    @user = user
    @cancellation_reason = cancellation_reason
  end

  def call
    validate_subscription_to_cancel
    cancel_subscription
  end

  def validate_subscription_to_cancel
    raise 'no subscription to cancel' if user.current_subscription.nil? || user.current_subscription.cancelled
  end

  def cancel_subscription
    subscription = user.current_subscription
    subscription.update!(cancelled: true, cancelled_at: Date.today, cancellation_reason: cancellation_reason)
    cancel_stripe if stripe?
    subscription
  end

  def stripe?
    false
  end

  def cancel_stripe
  end

end