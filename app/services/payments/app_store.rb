
module Payments

  #appstore will check on sandbox mode so in production should be a sandbox request first then a production

  class AppStore

    attr_accessor :subscription

    def initialize(subscription:)
      @subscription = subscription
    end

    def call
      response = receipt_verifier(subscription.transaction_body, :sandbox)
      response = receipt_verifier(subscription.transaction_body, :production) if response.class == CandyCheck::AppStore::VerificationFailure
      #first error its for the store asking in sandbox mode to check
      if response.class == CandyCheck::AppStore::VerificationFailure
        #subscription.update(cancellation_reason: response.message, status: :checking)
        subscription.update(cancellation_reason: response.message, status: :inactive)
      end
      update_subscription_data(response) if response.class == CandyCheck::AppStore::ReceiptCollection #CandyCheck::AppStore::Receipt
    end
    #correct response CandyCheck::AppStore::ReceiptCollection

    def update_subscription_data(response)
      #subscription.status = 'checking' if response.expires_at > Time.now
      #subscription.status = 'inactive' if response.expires_at + 2.days  > Time.now
      subscription.receipt_data = response.receipts
      #see what happens here
      #subs_data = response.receipts.sort_by {|receipt| receipt.expires_date}
      subscription.end_date = response.expires_at
      subscription.update(status: 'active') if response.overdue_days < 0
      subscription.update(status: 'checking') if response.overdue_days > 0
      subscription.update(status: 'inactive') if response.overdue_days > 2
      subscription.save!
      subscription
    end

    def receipt_verifier(receipt_data, environment)
      config = CandyCheck::AppStore::Config.new(
        environment: environment
      )
      verifier = CandyCheck::AppStore::Verifier.new(config)
      secret = Rails.application.secrets.appstore_secret
      verifier.verify_subscription(receipt_data, secret)
    end
  end

end
