#correct response class CandyCheck::PlayStore::SubscriptionPurchases::SubscriptionPurchase
module Payments
  class PlayStore
    
    attr_accessor :subscription

    def initialize(subscription:)
      @subscription = subscription
    end

    def authorization
      authorization = CandyCheck::PlayStore.authorization(Rails.application.secrets.playstore_json)
    end

    def call
      acknowledge_sub
      purchase_verifier
    end
    
    def receipt
      verifier = playstore_verifier
      body = JSON.parse subscription.transaction_body                            
      verifier.verify_subscription_purchase(package_name: Rails.application.secrets.playstore_package,
                                       subscription_id: subscription.product.store_reference,
                                       token: body["purchaseToken"]) 
    end

    def acknowledge_sub
      body = JSON.parse subscription.transaction_body                            
      acknowledger = CandyCheck::PlayStore::Acknowledger.new(authorization: authorization)
      result = acknowledger.acknowledge_product_purchase(
        package_name: Rails.application.secrets.playstore_package,
        product_id: subscription.product.store_reference,
        token: body["purchaseToken"]
      )
      if result.class == CandyCheck::PlayStore::ProductAcknowledgements::Response
        true
      else
        raise 'error acknowled play'
      end
      # => ProductAcknowledgements::Response
    end

    def purchase_verifier
      if receipt.class == CandyCheck::PlayStore::SubscriptionPurchases::SubscriptionPurchase
        update_subscription(receipt)
      elsif receipt.class == CandyCheck::PlayStore::VerificationFailure
        subscription.update(status: 'inactive', cancellation_reason: receipt.message)
      else
        raise 'verification error non listed'
      end
    rescue StandardError => e
      subscription.update!(status: 'inactive', cancellation_reason: e.message)
    end

    def playstore_verifier
      CandyCheck::PlayStore::Verifier.new(authorization: authorization)
    end

    def update_subscription(receipt)
      subscription.start_date = receipt.starts_at
      subscription.end_date = receipt.expires_at
      subscription.receipt_data = receipt.subscription_purchase.to_h
      subscription.status = 'active' if receipt.overdue_days < 0                          
      subscription.status = 'checking' if receipt.overdue_days > 0
      subscription.status = 'inactive' if receipt.overdue_days > 2
      subscription.save!
      subscription
    end

    ##add cancelation
    def check_cancellation
      if receipt.class == CandyCheck::PlayStore::VerificationFailure 
        subscription.update(status: 'inactive', cancellation_reason: receipt.message)
      else
        subscription.status = 'active' if receipt.overdue_days < 0                          
        subscription.status = 'checking' if receipt.overdue_days > 0
        subscription.status = 'inactive' if receipt.overdue_days > 2
        subscription.save!
      end
    rescue StandardError, ActiveRecord::RecordInvalid => e
      raise e
    end
  end
end
