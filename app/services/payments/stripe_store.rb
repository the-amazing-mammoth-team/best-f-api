module Payments
  class StripeStore
    
    require 'stripe'

    attr_accessor :subscription, :user
    
    def initialize(subscription: )
      @subscription = subscription
      @user = subscription.user
    end

    def stripe_customer
      return nil if subscription.store_metadata.nil?
      stripe_customer_id = subscription.store_metadata['stripe_customer_id']
      stripe_customer_id = subscription.store_metadata['customer'] if stripe_customer_id.nil?
      customer = Stripe::Customer.retrieve(stripe_customer_id)
    end

    def stripe_subscriptions(customer)
      return nil if customer.deleted?
      customer.subscriptions
    end

    def stripe_subscription(customer)
      return nil if customer && customer.deleted?
      return Stripe::Subscription.retrieve(subscription.store_metadata['id']) if subscription.store_metadata
      return Stripe::Subscription.retrieve(JSON.parse(subscription.transaction_body)['id']) if subscription.transaction_body
      customer.subscriptions.last
    end

    def save_subscription_data
      s_subscription = stripe_subscription(stripe_customer)
      if s_subscription.nil?
        subscription.status = 'inactive'
        subscription.cancellation_reason = 'no subscription found'
      else
        subscription.end_date = Time.at(s_subscription.current_period_end).to_datetime
        subscription.start_date = Time.at(s_subscription.current_period_start).to_datetime
        subscription.store_metadata = s_subscription
      end
      subscription.save!
      subscription
    end

    def check_cancellation
      s_subscription = stripe_subscription(stripe_customer)
      if s_subscription.nil?
        subscription.status = 'inactive'
        subscription.cancellation_reason = 'no subscription found'
      else
        subscription.end_date = Time.at(s_subscription.current_period_end).to_datetime
        subscription.start_date = Time.at(s_subscription.current_period_start).to_datetime
        subscription.store_metadata = s_subscription
        subscription.status = 'active' if subscription.end_date > Date.today
        subscription.status = 'checking' if subscription.end_date < Date.today
        subscription.status = 'inactive' if subscription.end_date + 3.days < Date.today
      end
      subscription.save!
      subscription
    end
    
    def get_product(sub_data)
      sub_data.items.last
    end

  end
end