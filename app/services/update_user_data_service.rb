class UpdateUserDataService

  attr_accessor :user, :args, :implement_ids

  def initialize(user:, args:, implement_ids:)
    @args = args
    @user = user
    @implement_ids = implement_ids
  end

  def call
    ActiveRecord::Base.transaction do
      user.update(args.to_h)
      if implement_ids.present?
        implements_to_delete = user.implements.pluck(:id) - implement_ids
        implements_to_add = implement_ids - user.implements.pluck(:id)
        UserImplement.where(implement_id: implements_to_delete, user: user).destroy_all
        implements_to_add.each {|implement_id| UserImplement.create(user: user, implement_id: implement_id) }
      end
      user
    end
  end
end