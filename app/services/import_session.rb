# id
# nombre_es
# nombre_en	
# notes_es	
# notes_en	
# description_es
# description_en	
# indicaciones_tecnicas 
# program	
# family	
# subfamily	
# easier_variation	
# harder_variation	
# musculos	
# articulaciones
# material
# link 
#
# Table name: exercises
#
#  id          :bigint           not null, primary key
#  video       :string
#  reps        :integer
#  time        :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  name        :string
#  description :string
#
#   has_paper_trail
#   translates :name, :description

class ImportSession

  require 'csv'

  attr_accessor :csv_file, :user, :session_attributes, :program_id, :session_update_id

  def initialize(csv_file:, user:, session_attributes:, program_id:, session_update_id: nil)
    @csv_file = csv_file
    @user = user
    @session_attributes = session_attributes
    @program_id = program_id
    @session_update_id = session_update_id
  end

  def call
    disconnect_from_program if session_update_id.present?
    create_session
  end

  def disconnect_from_program
    ActiveRecord::Base.transaction do
      old_session = Session.find(session_update_id)
      program_session = ProgramSession.where(program_id: program_id, session: old_session).first
      if program_session.destroy!
        session_attributes['order'] = old_session.order
        session_attributes[:description_en] = old_session.description_en
        session_attributes[:description_es] = old_session.description_es
        session_attributes[:name_en] = old_session.name_en
        session_attributes[:name_es] = old_session.name_es
      end
    end
  end

  def create_session
    ActiveRecord::Base.transaction do
      PaperTrail.request.whodunnit = user.id
      session = Session.new(session_attributes)
      session.save!
      program = Program.find program_id
      ProgramSession.create!(program_id: program_id, session: session) if program_id.present?
      program.store_implements
      add_sets_and_exercises(session)
      UserMailer.importer_success(user, 'session importer').deliver_now
    end
  end

  def add_exercise_set(row)
    exercise_set = ExerciseSet.new
    exercise = Exercise.find(row.field('ex_id').to_i) if row.field('ex_id').present?
    #exercise = Exercise.find_by(name: row.field('ex_name').strip) if exercise.nil?
    raise 'not found exercise' if exercise.nil?
    exercise_set.exercise = exercise
    exercise_set.order = row.field('ex_order')
    exercise_set.track_reps = row.field('track_reps')
    exercise_set.time_duration = row.field('time_duration')
    exercise_set.reps = row.field('reps')
    exercise_set
  end

  def set_session_block(row, session)
    block_type = row.field('block_type') || 'undefined'
    session_block = SessionBlock.where(order: row.field('block').to_i, session: session).first 
    session_block = SessionBlock.create(session: session, order: row.field('block').to_i, block_type: block_type) unless session_block.present?
    session_block
  end

  def set_session_set(row, session_block)
    set_type = row.field('set_type')
    set_time = row.field('set_time')
    session_set = SessionSet.where(order: row.field('set').to_i, session_block: session_block).first 
    session_set = SessionSet.create(session_block: session_block, order: row.field('set').to_i, session_set_type: set_type, time_duration: set_time) unless session_set.present?
    session_set
  end

  def add_sets_and_exercises(session)
    csv = CSV.new(csv_file, col_sep: ';', headers: true)
    csv.each do |row|
      if row.field('block').present?
        session_block = set_session_block(row, session)
        session_set = set_session_set(row, session_block)
        session_set.exercise_sets << add_exercise_set(row)
        session_set.save!
      end
    rescue StandardError, ActiveRecord::RecordInvalid => e
      UserMailer.importer_error(user, 'session importer', e, row.to_hash).deliver_now if Rails.env != 'test'
      Raven.tags_context error_data: row&.to_hash&.to_s
      raise e
    end
  end

end