class DiscardSessionExecution

  attr_accessor :session_id, :program_id, :user, :discard_reason

  def initialize(session_id: , program_id:, user:, discard_reason:)
    @session_id = session_id
    @program_id = program_id
    @user = user
    @discard_reason = discard_reason
  end

  def call
    user_program = UserProgram.where(user: user, program_id: program_id).last
    ActiveRecord::Base.transaction do
      session_execution = discard(user_program)
      set_next_session_for_user(user_program, session_execution)
      session_execution
    rescue StandardError, ActiveRecord::RecordInvalid => e
      raise e
    end
  end

  def set_next_session_for_user(user_program, session_execution)
    program = user_program.program
    next_session = program.sessions.where(order: session_execution.session.order + 1).first if program.sessions&.include?(session_execution.session)
    user_program.update(current_session: next_session) if next_session
  end

  def discard(user_program)
    se = SessionExecution.new(session_id: session_id, discarded: true, discard_reason: discard_reason, user_program: user_program)
    se.save!
    se
  end

end