# receives registry params and returns a user
class SignUpService

  def self.call(user_params:)
    user = User.new(user_params)
    unless user.valid?
      I18n.locale = :en
      puts user.errors.to_s
    end
    if user.save!
      send_mix_panel_data(user)
      set_challenges_and_active_rest(user)
      user.jwt = JsonWebToken.encode(user_id: user.id)
    end
    user
  end

  def self.set_challenges_and_active_rest(user)
    programs = Program.where(code_name: ['challenges', 'active rests'])
    programs.each do |program|
      UserProgram.create!(user: user, program: program, active: false)
    end
  end

  def self.send_mix_panel_data(user)
    job = SendMixPanelDataJob.perform_later(user)
    job2 = SendEventAnalyticsJob.perform_later(user, 'Signup completed')
    #send mix panel event
    raise 'error creating summary job' unless job.present? and job2.present?
  end
end
