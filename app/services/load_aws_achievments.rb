class LoadAwsAchievments

  def self.aws_thumbnails #for level
    require "aws-sdk-s3"
    s3 = Aws::S3::Resource.new(region: 'eu-west-1', access_key_id: 'AKIAI4LGQTRJJ4YFYMRQ', secret_access_key: '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' )
    s3.bucket("stage-dev-new").objects(prefix: 'icons/level/', delimiter: 'delimiter').collect(&:key).drop(1) # drop the first because it is exercises/
  end

  def self.call
    level = 1
    Achievment.all.order("points asc").each do |achievment|
      if achievment.achievment_type == 'level'
        achievment.icon_url = nil
        achievment.icon_url = build_url_for(achievment, level) if exists_in_aws?(achievment, level)
        achievment.congratulations_en = "Congratulations, you have reached #{achievment.name_en} level!"
        achievment.congratulations_es = "¡Felicidades, has llegado al nivel #{achievment.name_es }!"
        achievment.save!
        level = level +1
      end
    end
  end

  def self.build_url_for(exercise, level)
    "https://stage-dev-new.s3-eu-west-1.amazonaws.com/icons/level/Icono_Rombo_Gris_#{level}_72ppi.png"
  end

  def self.exists_in_aws?(achievment, level)
    thumbnails = aws_thumbnails
    thumbnails.include? "icons/level/Icono_Rombo_Gris_#{level}_72ppi.png"
  end

end