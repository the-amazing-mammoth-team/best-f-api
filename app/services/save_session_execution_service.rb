class SaveSessionExecutionService

  attr_accessor :user

  def initialize(session_execution_params:, user:)
    @session_execution = session_execution_params[:session_execution]
    @user = user
  end
  # TODO: ADD SERVICE TAT USES SIDEKIQ, 
  def call
    ActiveRecord::Base.transaction do
      user_program = UserProgram.where(user: user, program_id: @session_execution[:program_id]).last
      session_execution_saved = SessionExecution.new( 
        user_program: user_program,
        session_id: @session_execution[:session_id].to_i,
        difficulty_feedback: @session_execution[:difficulty_feedback],
        enjoyment_feedback: @session_execution[:enjoyment_feedback],
        feedback_comment: @session_execution[:feedback_comment],
      )
      session_execution_saved.save!
      add_session_data(session_execution_saved)
      create_session_execution_summary(session_execution_saved)
      session_execution_saved.update!(reps_executed: session_execution_saved.exercise_executions.sum(&:reps_executed))
      session_execution_saved
    rescue StandardError, ActiveRecord::RecordInvalid => e
      Raven.tags_context( error_data: e.to_json, user: user&.to_json)
      Raven.extra_context(session_execution_blocks: @session_execution[:session_block_executions].to_json, program_id:@session_execution[:program_id] )
      Raven.capture_exception(e)
      raise e
    end
  end

  def create_session_execution_summary(session_execution)
    job = CreateSessionExecutionSummaryJob.perform_later(session_execution)
    raise 'error creating summary job' unless job.present?
  end

  def add_session_data(session_execution_saved)
    @session_execution[:session_block_executions].each do |block_params|
      session_block_execution = session_execution_saved.session_block_executions.new(block_params.to_hash.except(:session_set_executions))
      session_block_execution.save!
      block_params.to_hash[:session_set_executions].each do |set_params|
        session_set_execution = session_block_execution.session_set_executions.new(set_params.to_hash.except(:exercise_executions))
        session_set_execution.save!
        set_params.to_hash[:exercise_executions].each do |exercise_params|
          exercise_execution = session_set_execution.exercise_executions.new(exercise_params)
          exercise_execution.save!
        end
      end
    end
  end
end