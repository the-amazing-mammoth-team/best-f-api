class SuggestedProgramsService

  def self.call(user_id: , locale:, onboarding_params:)
    user = User.find user_id
    #use something different than map to make the query faster
    programs = user_profiles(user).map(&:programs).flatten.select { |program| program.available == true }.sort_by!(&:priority_order)
    affiliate_program = get_affiliate_program(onboarding_params[:affiliate_code], onboarding_params[:program_id]) if onboarding_params
    programs.unshift(affiliate_program) if affiliate_program
    return default_programs if programs.empty?
    programs
  end

  def self.get_affiliate_program(affiliate_code, program_id)
    if affiliate_code && program_id
      user = User.find_by affiliate_code: affiliate_code
      Program.where(user_id: user.id, id: program_id).first
    elsif affiliate_code
      user = User.find_by affiliate_code: affiliate_code
      Program.find_by(user_id: user.id)
    elsif program_id
      Program.find_by(id: program_id)
    end
  end

  def self.user_profiles(user)
    Profile.where(goal: user.goal, gender: user.gender, activity_level: user.activity_level).where("min_fat_level <= #{user.body_fat} AND max_fat_level >= #{user.body_fat}").includes(:programs)
  end

  def self.default_programs
    codenames = ['MH_UNN']
    Program.where(code_name: codenames)
  end

end

