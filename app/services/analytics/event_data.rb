module Analytics
  class EventData

    attr_accessor :user, :event_data

    def initialize(user: , event_data: [])
      @user = user
      @event_data = event_data
    end

    def sign_up_attributes
      method = 'google' if user.google_uid.present? 
      method = 'facebook' if user.facebook_uid.present?
      method = 'apple' if user.apple_id_token.present?
      method = 'email'
      platform = user.platform #['iOS', 'Android', 'Web']
      if user.created_at < Date.today - 4.day
        time = ((Time.now - 3.days).to_f * 1000).to_i
      else
        time =(user.created_at.to_f * 1000).to_i
      end
      {'method' => method, 'platform' => platform, 'time' => time}
    end

    def subscribe_attributes
      product = user.current_subscription.product
      { 
        full_price: product.price,
        has_trial: product.has_trial,
        trial_days:product.trial_days,
        currency: product.currency,
        plans_payment_method: product.store,
        plan_id: product.store_reference,
        offer: user.current_subscription.offer_code
      }
    end

    def event_attributes
      attributes = {}
      attributes = attributes.merge(sign_up_attributes) if event_data.include?('sign_up_attributes')
      attributes = attributes.merge(subscribe_attributes) if event_data.include?('subscribe_attributes')
      attributes
    end
  end
end