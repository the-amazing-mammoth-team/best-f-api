module Analytics
  class UserData
    attr_accessor :user

    def initialize(user: )
      @user = user
    end

    def user_attributes
      attributes = user.attributes.except!("encrypted_password")
      attributes.merge!(extra_attributes)
      attributes.merge!(subscription_attributes)
    end

    def signup_method
      return 'Gmail' if user.google_uid.present?
      return 'Facebook' if user.facebook_uid.present?
      return 'Apple' if user.apple_id_token.present?
      'Email'
    end

    def extra_attributes
      extra = {
        '$email' => user.email,
        '$name' => user.names,
        locale: user.language,
        signup_method: signup_method,
        '$unsubscribed' => !user.newsletter_subscription,
        newsletter: user.newsletter_subscription,
        scientific_studies: user.scientific_data_usage,
      }
    end

    def subscription_status(subscription)
      return 'free' if subscription == nil
      return 'trial' if subscription&.status == 'active' && (subscription.product.has_trial && Date.today > subscription.created_at + 14.days )
      return 'pro' if subscription&.status == 'active'
      'free' 
    end

    def subscription_attributes
      subscription = user.current_subscription
      sub_data = {
        subscription_status: subscription_status(subscription),
        current_subscription: subscription&.product&.store_reference,
        expiration_date: subscription&.end_date,
        subscription_history: user.subscriptions&.to_json
      }
    end

  end
end