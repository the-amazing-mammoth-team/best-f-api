module Analytics
  class CreateMixPanelUser

    require 'mixpanel-ruby'

    attr_accessor :tracker, :user

    def initialize(user: )
      @tracker = Mixpanel::Tracker.new('745581c49f04c81ecf450cceeea8a514') #appyprogram
      @user = user
    end

    def call
      create_user
    end

    def create_user
      tracker.people.set(user.email, user_attributes, ip= 0)
    end

    def user_attributes
      Analytics::UserData.new(user: user).user_attributes
    end
  end
end