
module Analytics
  class DeleteMixPanelUser

    require 'mixpanel-ruby'

    attr_accessor :tracker, :user

    def initialize(user: )
      @tracker = Mixpanel::Tracker.new('bf47184b8fc05bef8c88adb01de19387') #appyprogram
      @user = user
    end

    def call
      tracker.people.delete_user(user.id)
    end
  end
end