module Analytics
  class SendMixPanelEvent

    require 'mixpanel-ruby'

    attr_accessor :tracker, :user, :event

    def initialize(user: , event: )
      @error_handler = MyErrorHandler.new
      @tracker = Mixpanel::Tracker.new('745581c49f04c81ecf450cceeea8a514', @error_handler) #appyprogram
      @user = user
      @event = event
    end

    def event_data
      event_attributes = ['sign_up_attributes'] if event == 'Signup completed'
      event_attributes = ['subscribe_attributes'] if event == 'Subscribe' 
      Analytics::EventData.new(user: user, event_data: event_attributes).event_attributes if event_attributes
    end

    def call
      tracker.track(user.email, event, event_data)
    rescue StandardError => e
      raise e
    end

  end
end