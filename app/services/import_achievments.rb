class ImportAchievments

  require 'csv'

  attr_accessor :csv_file, :user

  def initialize(csv_file: , user:)
    @csv_file = csv_file
    @user = user
  end

  def add_new_achievment(row)
    achievment = set_achievment_fields(Achievment.new, row)
    achievment.save!
    achievment
  end

  def set_achievment_fields(achievment, row)
    achievment.icon_url = row.field('icon_url') if row.field('icon_url').present?
    achievment.name_es = row.field('name_es') if row.field('name_es').present?
    achievment.name_en = row.field('name_en') if row.field('name_en').present?
    achievment.description_es = row.field('description_es') if row.field('description_es')
    achievment.description_en = row.field('description_en') if row.field('description_en')
    achievment.congratulations_en = row.field('congratulations_en') if row.field('congratulations_en')
    achievment.congratulations_es = row.field('congratulations_es') if row.field('congratulations_es')
    achievment.achievment_type = row.field('achievment_type') if row.field('achievment_type')
    achievment.points = row.field('points') if row.field('points')
    achievment
  end

  def update_achievment
    true
  end

  def call
    ActiveRecord::Base.transaction do
      PaperTrail.request.whodunnit = user.id
      csv = CSV.new(csv_file, col_sep: ';', headers: true)
      csv.each do |row|
        achievment = achievment.find(row.field('id')) if row.field('id').present?
        if achievment
          update_achievment(achievment, row)
        else
          add_new_achievment(row)
        end
      rescue StandardError, ActiveRecord::RecordInvalid => e
        UserMailer.importer_error(user, 'achievment importer', e, row.to_hash).deliver_now if Rails.env != 'test'
        Raven.tags_context error_data: row.to_hash
        raise e
      end
      UserMailer.importer_success(user, 'achievment importer').deliver_now if Rails.env != 'test'
    end
  end

end