class ProcessSessionExecution

  attr_accessor :session_execution

  def initialize(session_execution: )
    @session_execution = session_execution
  end

  def call
    ActiveRecord::Base.transaction do
      user = session_execution.user_program.user
      session_execution_summary = SessionExecutionSummary.new(session_execution: session_execution)
      session_execution_summary.set_values
      if session_execution_summary.save!
        send_push_notification
        process_tests(user)
        set_next_session_for_user(user)
        #update_streaks(user)
      end
      session_execution_summary
    rescue StandardError, ActiveRecord::RecordInvalid => e
      Raven.tags_context( error_data: e.to_json )
      Raven.capture_exception(e)
      raise e
    end
  end

  def set_next_session_for_user(user)
    program = user.user_program.program
    next_session = program.sessions.where(order: session_execution.session.order + 1).first if program.sessions&.include?(session_execution.session)
    user.user_program.update(completed: true) if user.user_program.progress == 100
    user.user_program.update(current_session: next_session) if next_session
  end

  def send_push_notification
    user = session_execution.user_program.user
  end

  def process_tests(user)
    if session_execution.user_program.program.code_name == 'pro_auto' && session_execution.session.order == 1
      ProProgram::ProcessTests.new(user: user , session_execution: session_execution).process_t1
      test_2 = SessionGenerator::TestSession.new.t2(user)
      ProgramSession.create!(program: user.current_program, session: test_2)
    elsif session_execution.user_program.program.code_name == 'pro_auto' && session_execution.session.order == 2
      puts 'process 2'
      ProProgram::ProcessTests.new(user: user , session_execution: session_execution).process_t2
      # this family values is used to avoid the nil family returned exercises error
      exercises = Exercise.where(family: families).where.not(id: [Exercise.base_exercises[:run], Exercise.base_exercises[:rest]])#.where.not('family is NULL')
      user.reload
      sessions = ProProgram::SessionsGenerator.new(user: user, sessions_number: 20, exercises: exercises, program: user.current_program).call
    end
  end

  def families
    [0,1,2,3,6,10,11]
    #enum family: {core: 0, lower_body: 1, full_body: 2, upper_body: 3, pending: 4, runs: 5, back_body: 6, metabolic: 7, sports: 8, rest: 9, back_body: 10, upper_body: 11}
  end
end