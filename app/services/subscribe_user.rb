
class SubscribeUser

  attr_accessor :user, :program_id, :product_id, :transaction_body, :upgrade, :affiliate_code, :offer_code

  # todo #check for canceling old sub and creating a new one
  # todo check for keeping the user_program if upgrading doesnt matter cos it will be sending the program as param
  def call
    ActiveRecord::Base.transaction do
      if upgrade
        validate_cant_upgrade_between_products
        sub = upgrade_subscription
        VerifyPaymentJob.perform_later(user: user)
      else
        sub = create_subscription
        VerifyPaymentJob.perform_later(user: user)
      end
      send_mix_panel_event('Subscribe') if Rails.env  == 'production'
      sub
    end
  end

  def old_subscription
    Subscription.where(user: user, status: :active).first
  end

  def validate_cant_upgrade_between_products
    raise 'cant upgrade between product stores' if upgrade && old_subscription.product.store != Product.find(product_id).store
  end

  def subscription_active?
    user.subscriptions.where(status: :active).first.present?
  end

  def upgrade_subscription
    end_old_subscription
    create_subscription(upgrade: true)
  end

  def end_old_subscription
    old_sub = old_subscription
    old_sub.end_date = Date.today
    old_sub.status = :inactive
    old_sub.save!
  end

  def initialize(user:, program_id:, product_id:, transaction_body: , upgrade: false, extra_params: nil)
    @user = user
    @program_id = program_id
    @product_id = product_id
    @transaction_body = transaction_body
    @upgrade = upgrade 
    @affiliate_code = extra_params[:affiliate_code]
    @offer_code = extra_params[:offer_code]
  end

  def create_subscription(upgrade=false)
    subscription = subscribe
    if subscription.id?
      enroll(upgrade)
    end
    subscription
  end

  def subscribe
    user.update(affiliate_code_signup: affiliate_code) if affiliate_code
    sub = Subscription.new(user: user, program_id: program_id, product_id: product_id, transaction_body: transaction_body, 
                           status: :active, affiliate_code: affiliate_code, offer_code: offer_code)
    sub.save!
    sub
  end

  def enroll(upgrade)
    program = Program.find program_id
    if program.code_name == 'MH_UNN'
      user_program = ProProgram::Create.new(user: user).call
    else
      UserProgram.create!(user: user, program_id: program.id, current_session: program.sessions.first) unless upgrade
    end
  end

  def send_mix_panel_event(event_name)
    ## sends the event and updates user info
    ##Analytics::SendMixPanelEvent.new(user: user, event: event_name).call
    job = SendMixPanelDataJob.perform_later(user)
    job2 = SendEventAnalyticsJob.perform_later(user, event_name)
  end

end