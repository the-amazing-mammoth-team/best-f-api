# conocido como intervalo a repeticiones, n numero de sets, se repite el mismo set de ejercicios
# trackea el tiempo, reps indicadas por el ejercicio en su nivel correspondiente
# n veces [ex1, ex2, ex3]

module SessionGenerator
  class IntervalRepetitions

    include Leveleable

    attr_accessor :exercises, :user

    LEVELS = [{ exercise_rep_percent_min: 50, exercise_rep_percent_max: 80, min_t2_reps: 0, exercises_min: 5, exercises_max: 10, reps_min: 100,reps_max: 200},
              { exercise_rep_percent_min: 50, exercise_rep_percent_max: 80, min_t2_reps: 100, exercises_min: 5, exercises_max: 10, reps_min: 150,reps_max: 250 },
              { exercise_rep_percent_min: 50, exercise_rep_percent_max: 80, min_t2_reps: 150, exercises_min: 5, exercises_max: 10, reps_min: 200,reps_max: 300},
              { exercise_rep_percent_min: 50, exercise_rep_percent_max: 80, min_t2_reps: 200, exercises_min: 5, exercises_max: 10, reps_min: 250,reps_max: 350},
              { exercise_rep_percent_min: 50, exercise_rep_percent_max: 100, min_t2_reps: 250, exercises_min: 5, exercises_max: 10, reps_min: 300,reps_max: 450}, 
              { exercise_rep_percent_min: 50, exercise_rep_percent_max: 110, min_t2_reps: 300, exercises_min: 5, exercises_max: 10, reps_min: 350,reps_max: 500} ]

    def initialize(session_generator_params:, user:)
      @exercises = session_generator_params[:exercises]
      @user = user
    end

    def call
      ActiveRecord::Base.transaction do
        #PaperTrail.request.whodunnit = user&.id
        session = Session.new(name: 'Interval repetions', description: 'to the one description', session_type: :interval_repetitions)
        session.save!
        add_session_sets(session)
        session
      rescue StandardError, ActiveRecord::RecordInvalid => e
        Raven.tags_context error_data: e.to_s
        raise e
      end
    end

    def add_session_sets(session)
      session_block = SessionBlock.create!(session: session, block_type: :interval_repetitions)
      n = 1
      sets = 1
      while n <= sets
        session_set = SessionSet.create(session_block: session_block, order: n, session_set_type: :interval_repetitions)
        session_exercises.each_with_index do |exercise, index|
          session_set.exercise_sets << add_exercise_set(exercise, index)
          session_set.save!
        end
        sets = sets_number(session.total_reps)
        n = n + 1
      end
    end

    def add_exercise_set(exercise, order)
      exercise_set = ExerciseSet.new
      exercise_set.exercise = exercise
      exercise_set.order = order
      exercise_set.track_reps = false
      reps_time = calculate_exercise_repetitions(exercise)
      exercise_set.time_duration = reps_time[:time] # add the time per exercise or user level
      exercise_set.reps = reps_time[:reps] # add the time per exercise or user level # check that only has reps
      exercise_set
    end
  end
end