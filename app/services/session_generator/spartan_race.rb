
# [ex1 , ex2, run]
module SessionGenerator

  class SpartanRace

    include Leveleable

    LEVELS = [{ exercise_rep_percent_min: 80, exercise_rep_percent_max: 100, min_t2_reps: 0, exercises_min: 2, exercises_max: 2, laps_min: 4, laps_max: 4 },
              { exercise_rep_percent_min: 80, exercise_rep_percent_max: 100, min_t2_reps: 100, exercises_min: 2, exercises_max: 3, laps_min: 4, laps_max: 4 },
              { exercise_rep_percent_min: 80, exercise_rep_percent_max: 100, min_t2_reps: 150, exercises_min: 3, exercises_max: 3, laps_min: 4, laps_max: 4 },
              { exercise_rep_percent_min: 100, exercise_rep_percent_max: 100, min_t2_reps: 200, exercises_min: 3, exercises_max: 4, laps_min: 4, laps_max: 4 },
              { exercise_rep_percent_min: 100, exercise_rep_percent_max: 120, min_t2_reps: 250, exercises_min: 4, exercises_max: 5, laps_min: 4, laps_max: 4 }, 
              { exercise_rep_percent_min: 120, exercise_rep_percent_max: 120, min_t2_reps: 300, exercises_min: 4, exercises_max: 5, laps_min: 4, laps_max: 4 } ]

    attr_accessor :exercises, :user

    def initialize(session_generator_params:, user:)
      @exercises = session_generator_params[:exercises]
      @user = user
    end
    
    def call
      ActiveRecord::Base.transaction do
        #PaperTrail.request.whodunnit = user&.id
        session = Session.new(name_en: 'Spartan Race', description: 'Spartan Race description', session_type: :spartan_race)
        session.save!
        add_session_sets(session)
        session
      rescue StandardError, ActiveRecord::RecordInvalid => e
        Raven.tags_context error_data: e.to_s
        raise e
      end
    end
    
    def add_session_sets(session)
      session_block = SessionBlock.create!(session: session, block_type: :spartan_race)
      spartan_exercises = session_exercises.each {|spartan_exercise| spartan_exercise.reps = calculate_exercise_repetitions(spartan_exercise)[:reps]}
      spartan_exercises
      n = 1
      while n <= 4 #anyway its always 4
        spartan_race_multiplier = (1 - ((n -1) * 0.25))
        session_set = SessionSet.create(session_block: session_block, order: n, session_set_type: :spartan_race)
        spartan_exercises.each_with_index do |exercise, index|
          session_set.exercise_sets << add_exercise_set(exercise, index, spartan_race_multiplier)
        end
        session_set.exercise_sets << add_run(spartan_exercises.count, spartan_race_multiplier)
        session_set.save!
        n = n + 1
      end
    end

    def add_run(order, spartan_race_multiplier)
      run = Exercise.find Exercise.base_exercises[:run]
      exercise_set = ExerciseSet.new
      exercise_set.exercise = run
      exercise_set.order = order
      exercise_set.track_reps = false
      exercise_set.time_duration = 4 * 60 * spartan_race_multiplier # laps * carreratime * multiplier going lower
      exercise_set
    end

    def add_exercise_set(exercise, order, spartan_race_multiplier)
      exercise_set = ExerciseSet.new
      exercise_set.exercise = exercise
      exercise_set.order = order
      exercise_set.track_reps = false
      exercise_set.time_duration = nil # add the time per exercise or user level
      exercise_set.reps = exercise.reps * spartan_race_multiplier # add the time per exercise or user level
      exercise_set
    end
  end
end