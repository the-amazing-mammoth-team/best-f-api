# 1 set por ejercicio de 8 vueltas con 20’’ activida por ejercicio  y 10 ‘’ descanso entre ejercicios en la última vuelta sin descanso
# 1 min de descanso entre sets
# set example => ['ex','rest','ex','rest','ex','rest','ex','rest','ex','rest','ex','rest','ex','rest','ex']
# TODO ADD SPECIFIC EXERCISES FOR THE SESSION

module SessionGenerator
  class Tabata

    EXERCISES_MIN = 2
    EXERCISES_MAX = 3

    LEVELS = [{exercises_min: 1, exercises_max: 1, min_t2_reps: 0 }, {exercises_min: 1, exercises_max: 2, min_t2_reps: 100}, {exercises_min: 3, exercises_max: 4, min_t2_reps: 150}, {exercises_min: 3, exercises_max: 4, min_t2_reps: 200}, {exercises_min: 3, exercises_max: 5, min_t2_reps: 250}]

    attr_accessor :exercises, :user

    def initialize(session_generator_params:, user:)
      @exercises = session_generator_params[:exercises]
      @user = user
    end

    def session_exercises
      exercises.where.not(name_en: 'rest').sample(user_level[:exercises_min])
    end

    def user_level
      LEVELS.select {|level| level[:min_t2_reps] <= user.t2_reps }.last
    end

    def call
      ActiveRecord::Base.transaction do
        #PaperTrail.request.whodunnit = user&.id
        session = Session.new(name_en: 'Tabata', description: 'tabata description', session_type: :tabata)
        session.save!
        add_session_sets(session, session_exercises)
        session
      end
    end

    def add_session_sets(session, exercises)
      exercises.each_with_index do |exercise, index|
        session_block = SessionBlock.create!(session: session, block_type: :tabata) 
        session_set = SessionSet.create(session_block: session_block, order: (index + 1), session_set_type: :tabata)
        n = 1
        while n <= 15
          session_set.exercise_sets << add_exercise_set(exercise, n)
          n = n + 1
          session_set.exercise_sets << add_rest(10, n) unless n > 15
          session_set.save!
          n = n + 1
        end
        add_rest_set(session, (index + 1)) unless exercises.length == index + 1
      rescue StandardError, ActiveRecord::RecordInvalid => e
        Raven.tags_context error_data: e.to_s
        raise e
      end
    end

    def add_rest_set(session, order)
      session_block = SessionBlock.create!(session: session, block_type: :pause) 
      session_set = SessionSet.new(session_block: session_block, order: order)
      session_set.exercise_sets << add_rest(60, 1)
      session_set.save!
    end

    def add_rest(time, order)
      rest = Exercise.find Exercise.base_exercises[:rest]
      exercise_set = ExerciseSet.new
      exercise_set.exercise = rest
      exercise_set.order = order
      exercise_set.track_reps = false
      exercise_set.time_duration = time
      exercise_set
    end

    def add_exercise_set(exercise, order)
      exercise_set = ExerciseSet.new
      exercise_set.exercise = exercise
      exercise_set.order = order
      exercise_set.track_reps = false
      exercise_set.time_duration = 20
      exercise_set.reps = 0 #reps #user_level[:min_t2_reps] #min_t2_reps are 0 and nil
      exercise_set
    end

  end
end