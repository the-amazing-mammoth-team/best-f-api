
# [ex1, run , ex2, run]
module SessionGenerator

  class PaleoRun

    include Leveleable

    LEVELS = [{ exercise_rep_percent_min: 50, exercise_rep_percent_max: 80, min_t2_reps: 0, exercises_min: 5, exercises_max: 10, reps_min: 100, reps_max: 150 },
              { exercise_rep_percent_min: 50, exercise_rep_percent_max: 80, min_t2_reps: 100, exercises_min: 5, exercises_max: 10, reps_min: 150, reps_max: 250 },
              { exercise_rep_percent_min: 50, exercise_rep_percent_max: 80, min_t2_reps: 150, exercises_min: 5, exercises_max: 10, reps_min: 200, reps_max: 300},
              { exercise_rep_percent_min: 50, exercise_rep_percent_max: 80, min_t2_reps: 200, exercises_min: 5, exercises_max: 10, reps_min: 250, reps_max: 350},
              { exercise_rep_percent_min: 50, exercise_rep_percent_max: 100, min_t2_reps: 250, exercises_min: 5, exercises_max: 10, reps_min: 300, reps_max: 450}, 
              { exercise_rep_percent_min: 50, exercise_rep_percent_max: 110, min_t2_reps: 300, exercises_min: 5, exercises_max: 10, reps_min: 350, reps_max: 500} ]

    attr_accessor :exercises, :user

    def initialize(session_generator_params:, user:)
      @exercises = session_generator_params[:exercises]
      @user = user
    end
    
    def call
      ActiveRecord::Base.transaction do
        #PaperTrail.request.whodunnit = user&.id
        session = Session.new(name_en: 'Paleo Run', description: 'Paleo run description', session_type: :paleo_run)
        session.save!
        add_session_sets(session)
        session
      rescue StandardError, ActiveRecord::RecordInvalid => e
        Raven.tags_context error_data: e.to_s
        raise e
      end
    end
    
    def add_session_sets(session)
      session_block = SessionBlock.create!(session: session, block_type: :paleo_run)
      n = 1
      sets_number = 1 
      while n <= sets_number
        session_set = SessionSet.create(session_block: session_block, order: n, session_set_type: :paleo_run)
        session_exercises.each_with_index do |exercise, index|
          session_set.exercise_sets << add_exercise_set(exercise, (index * 2) + 1 )
          session_set.exercise_sets << add_run((index * 2) + 2)
        end
        session_set.save!
        n = n + 1
        sets_number = sets_number(session.total_reps)
      end
    end

    def add_run(order)
      run = Exercise.find Exercise.base_exercises[:run]
      exercise_set = ExerciseSet.new
      exercise_set.exercise = run
      exercise_set.order = order
      exercise_set.track_reps = false
      exercise_set.reps = 0
      exercise_set.time_duration = 60
      exercise_set
    end

    def add_exercise_set(exercise, order)
      exercise_set = ExerciseSet.new
      exercise_set.exercise = exercise
      exercise_set.order = order
      exercise_set.track_reps = false
      reps_time = calculate_exercise_repetitions(exercise)
      exercise_set.time_duration = reps_time[:time] # add the time per exercise or user level
      exercise_set.reps = reps_time[:reps] # add the time per exercise or user level
      exercise_set
    end
  end
end