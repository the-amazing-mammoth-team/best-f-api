module SessionGenerator
  class ToTheOne

    include Leveleable

    LEVELS = [{ min_t2_reps: 0, exercises_min: 2, exercises_max: 2, laps_min: 8, laps_max: 8 },
              { min_t2_reps: 100, exercises_min: 2, exercises_max: 3, laps_min: 8, laps_max: 10 },
              { min_t2_reps: 150, exercises_min: 3, exercises_max: 4, laps_min: 10, laps_max: 10 },
              { min_t2_reps: 200, exercises_min: 3, exercises_max: 5, laps_min: 10, laps_max: 12 },
              { min_t2_reps: 250, exercises_min: 4, exercises_max: 5, laps_min: 12, laps_max: 15 }, 
              { min_t2_reps: 300, exercises_min: 5, exercises_max: 5, laps_min: 15, laps_max: 15 } ]

    attr_accessor :exercises, :user

    def initialize(session_generator_params:, user: )
      @exercises = session_generator_params[:exercises]
      @user = user
    end

    def call
      ActiveRecord::Base.transaction do
        #PaperTrail.request.whodunnit = user&.id
        session = Session.new(name: 'To the one', description: 'to the one description', session_type: :to_the_one)
        session.save!
        session_block = SessionBlock.create!(session: session, block_type: :to_the_one)
        to_the_one_exercises = session_exercises
        n = level_sets
        while n > 0
          session_set = SessionSet.create(session_block: session_block, order: (level_sets - n + 1), session_set_type: :to_the_one)
          to_the_one_exercises.each_with_index do |exercise, index|
            session_set.exercise_sets << add_exercise_set(exercise, (index +1), n)
            session_set.save!
          rescue StandardError, ActiveRecord::RecordInvalid => e
            Raven.tags_context error_data: e.to_s
            raise e
          end
          n -= 1
        end
        session
      end
    end

    def add_exercise_set(exercise, order, reps)
      exercise_set = ExerciseSet.new
      exercise_set.exercise = exercise
      exercise_set.order = order
      exercise_set.track_reps = false
      exercise_set.time_duration = exercise.time.present? ? (exercise.time * reps) : reps
      exercise_set.reps = reps
      exercise_set
    end

  end
end