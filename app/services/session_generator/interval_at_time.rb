# [ex1, ex2, ex3] repetir por tiempo necesario, this is a counterclock workout session
module SessionGenerator
  class IntervalAtTime

    include Leveleable

    attr_accessor :exercises, :add_pause, :user

     LEVELS = [{ exercise_rep_percent_min: 30, exercise_rep_percent_max: 60, min_t2_reps: 0, exercises_min: 3, exercises_max: 6, time_min: 7, time_max: 7 },
              { exercise_rep_percent_min: 30, exercise_rep_percent_max: 60, min_t2_reps: 100, exercises_min: 3, exercises_max: 6, time_min: 7, time_max: 10 },
              { exercise_rep_percent_min: 30, exercise_rep_percent_max: 60, min_t2_reps: 150, exercises_min: 3, exercises_max: 6, time_min: 9, time_max: 12 },
              { exercise_rep_percent_min: 30, exercise_rep_percent_max: 60, min_t2_reps: 200, exercises_min: 3, exercises_max: 6, time_min: 11, time_max: 15 },
              { exercise_rep_percent_min: 30, exercise_rep_percent_max: 60, min_t2_reps: 250, exercises_min: 3, exercises_max: 6, time_min: 13, time_max: 17 }, 
              { exercise_rep_percent_min: 30, exercise_rep_percent_max: 60, min_t2_reps: 300, exercises_min: 3, exercises_max: 6, time_min: 15, time_max: 18 } ]

    def initialize(session_generator_params: , user:)
      @exercises = session_generator_params[:exercises]
      @add_pause = session_generator_params[:add_pause]
      @user = user
    end

    def call
      ActiveRecord::Base.transaction do
        session = Session.new(session_args)
        session.save!
        session_block = SessionBlock.create!(session: session, block_type: :interval_at_time, time_duration: session_total_time )
        session_set = SessionSet.new(session_block: session_block, order: 1, session_set_type: :interval_at_time)

        s_exercises = session_exercises

        s_exercises.each_with_index do |exercise, index|
          session_set.exercise_sets << add_exercise_set(exercise, index + 1)
        end
        session_set.save!

        if add_pause
          pause_block = SessionBlock.create!(session: session, block_type: :pause)
          session_set = SessionSet.new(session_block: pause_block, order: s_exercises.count + 1)
          session_set.exercise_sets << add_rest(15, 1) 
          session_set.save!
        end

        session
      end
    end

    def session_args
      if add_pause
        { name_en: 'Interval At Time with pause', description: 'Interval At Time wit pause description',
          session_type: :interval_at_time_with_pause, time_duration: session_total_time }
      else
        { name_en: 'Interval At Time', description: 'Interval At Time description', session_type: :interval_at_time,
          time_duration: session_total_time }
      end
    end

    def add_exercise_set(exercise, order)
      exercise_set = ExerciseSet.new
      exercise_set.exercise = exercise
      exercise_set.order = order
      exercise_set.track_reps = false
      reps_time = calculate_exercise_repetitions(exercise)
      #exercise_set.time_duration = reps_time[:time] # add the time per exercise or user level for interval at time this is nil
      exercise_set.reps = reps_time[:reps] # add the time per exercise or user level
      exercise_set
    end

    def add_rest(time, order)
      rest = Exercise.find_by name_en: 'rest'
      exercise_set = ExerciseSet.new
      exercise_set.exercise = rest
      exercise_set.order = order
      exercise_set.track_reps = false
      exercise_set.time_duration = time
      exercise_set
    end

  end
end