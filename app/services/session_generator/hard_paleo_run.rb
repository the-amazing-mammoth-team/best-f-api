
# [ex1 , ex2, run]
module SessionGenerator

  class HardPaleoRun

    include Leveleable

     LEVELS = [{ exercise_rep_percent_min: 50, exercise_rep_percent_max: 70, min_t2_reps: 0, exercises_min: 3, exercises_max: 3, run_time_min: 90, run_time_max: 90, laps_min: 4, laps_max: 4 },
              {  exercise_rep_percent_min: 60, exercise_rep_percent_max: 80, min_t2_reps: 100, exercises_min: 3, exercises_max: 3, run_time_min: 90, run_time_max: 90, laps_min: 4, laps_max: 5 },
              {  exercise_rep_percent_min: 60, exercise_rep_percent_max: 90, min_t2_reps: 150, exercises_min: 3, exercises_max: 3, run_time_min: 70, run_time_max: 90, laps_min: 5, laps_max: 5},
              {  exercise_rep_percent_min: 70, exercise_rep_percent_max: 90, min_t2_reps: 200, exercises_min: 3, exercises_max: 3, run_time_min: 60, run_time_max: 80, laps_min: 5, laps_max: 5},
              {  exercise_rep_percent_min: 70, exercise_rep_percent_max: 90, min_t2_reps: 250, exercises_min: 3, exercises_max: 3, run_time_min: 90, run_time_max: 90, laps_min: 5, laps_max: 5}, 
              {  exercise_rep_percent_min: 80, exercise_rep_percent_max: 90, min_t2_reps: 300, exercises_min: 3, exercises_max: 3, run_time_min: 45, run_time_max: 60, laps_min: 5, laps_max: 5} ]

    attr_accessor :exercises, :user

    def initialize(session_generator_params:, user:)
      @exercises = session_generator_params[:exercises]
      @user = user
    end
    
    def call
      ActiveRecord::Base.transaction do
        #PaperTrail.request.whodunnit = user&.id
        session = Session.new(name_es: 'Carrera Paleo Hard', name_en: 'Hard Paleo Run', description_en: 'Hard Paleo run description', session_type: :hard_paleo_run)
        session.save!
        add_session_sets(session)
        session
      rescue StandardError, ActiveRecord::RecordInvalid => e
        Raven.tags_context error_data: e.to_s
        raise e
      end
    end
    
    def add_session_sets(session)
      session_block = SessionBlock.create!(session: session, block_type: :hard_paleo_run)
      sets_number = level_sets
      hard_paleo_run_exercises = session_exercises
      run_time = level_run_time
      n = 1
      while n <= sets_number
        session_set = SessionSet.create(session_block: session_block, order: n, session_set_type: :hard_paleo_run)
        hard_paleo_run_exercises.each_with_index do |exercise, index|
          session_set.exercise_sets << add_exercise_set(exercise, index )
        end
        session_set.exercise_sets << add_run(hard_paleo_run_exercises.size, run_time)
        session_set.save!
        n = n + 1
      end
    end

    def add_run(order, run_time)
      run = Exercise.find Exercise.base_exercises[:run]
      exercise_set = ExerciseSet.new
      exercise_set.exercise = run
      exercise_set.order = order
      exercise_set.track_reps = false
      #exercise_set.reps = 0
      exercise_set.time_duration = run_time
      exercise_set
    end

    def add_exercise_set(exercise, order)
      exercise_set = ExerciseSet.new
      exercise_set.exercise = exercise
      exercise_set.order = order
      exercise_set.track_reps = false
      reps_time = calculate_exercise_repetitions(exercise)
      exercise_set.time_duration = reps_time[:time] # add the time per exercise or user level, check that exercise only shows reps #check for planks 
      exercise_set.reps = reps_time[:reps] # add the time per exercise or user level
      exercise_set
    end
  end
end
