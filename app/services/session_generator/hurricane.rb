# [ex1, rest(10), ex2, rest(19), ex3] [rest(60)] as long as the session goes in time?
# there is an issue by tracking reps here there is no reps 

module SessionGenerator
  class Hurricane

    include Leveleable

    LEVELS = [{ min_t2_reps: 0, exercises_min: 4, exercises_max: 4, laps_min: 2, laps_max: 2 },
              { min_t2_reps: 100, exercises_min: 4, exercises_max: 4, laps_min: 2, laps_max: 2 },
              { min_t2_reps: 150, exercises_min: 4, exercises_max: 4, laps_min: 2, laps_max: 3 },
              { min_t2_reps: 200, exercises_min: 4, exercises_max: 4, laps_min: 3, laps_max: 4 },
              { min_t2_reps: 250, exercises_min: 4, exercises_max: 4, laps_min: 4, laps_max: 5 }, 
              { min_t2_reps: 300, exercises_min: 4, exercises_max: 4, laps_min: 5, laps_max: 5 } ]

    attr_accessor :exercises, :user

    def initialize(session_generator_params:, user: )
      @exercises = session_generator_params[:exercises]
      @user = user
    end

    def call
      ActiveRecord::Base.transaction do
        session = Session.new(name_es: 'Huracán', name_en: 'Hurricane', description: 'Hurricane description', session_type: :hurricane)
        session.save!
        session_block = SessionBlock.create!(session: session, block_type: :hurricane)
        sets_number = level_sets
        hurricane_exercises = session_exercises
        x = 1

        while x <= (sets_number * 2) -1

          session_set = SessionSet.create(session_block: session_block, order: x, session_set_type: :hurricane)

          hurricane_exercises.each_with_index do |exercise, index|
            session_set.exercise_sets << add_exercise_set(exercise, index)
            #session_set.exercise_sets << add_rest(10, n ) unless exercise == exercises.last
            session_set.save!
          end

          x = x + 1

          if x < (sets_number * 2) -1
            session_set = SessionSet.create(session_block: session_block, order: x )
            session_set.exercise_sets << add_rest(60, 1) 
            session_set.save!
          end

          x = x + 1

        end

        session
      end
    end

     def add_rest(time, order)
      rest = Exercise.find Exercise.base_exercises[:rest]
      exercise_set = ExerciseSet.new
      exercise_set.exercise = rest
      exercise_set.order = order
      exercise_set.track_reps = false
      exercise_set.time_duration = time
      exercise_set
    end

    def add_exercise_set(exercise, order)
      exercise_set = ExerciseSet.new
      exercise_set.exercise = exercise
      exercise_set.order = order
      exercise_set.track_reps = false #add track reps
      exercise_set.time_duration = 60
      exercise_set.reps = 0#calculate_exercise_repetitions(exercise)[:reps]
      exercise_set
    end

  end
end