# this service generates tests 1 and 2 for the auto generated program
module SessionGenerator
  class TestSession

    def t1_values_not_present(user)
       true if user.t1_push.zero? && user.t1_legs.zero? && user.t1_core.zero? && user.t1_full.zero?
    end

    def exercise_names
      { push_up: 5252 , squat: 5242, burpee: 5236, plank_pivot: 5217, plank_balance: 5218 }
    end

    def exercises
      #push up 5252
      #squat 5242
      #burpee 5236
      #plank_pivot 5217
      #plank balance 5218
      #rest 5968
      Exercise.where(id: exercise_names.values)
    end

    def get_exercise(id)
      Exercise.find(id)
    end

    def t1
      session = Session.create!(session_type: :challenge, name_en: 'Test 1', name_es: 'Test 1', order: 1, 
                                description_en: I18n.t('pro_program.t1', locale: :en), description_es: I18n.t('pro_program.t1', locale: :es))
      session_block = SessionBlock.create!(session: session, block_type: :test, order: 1)
      session_set = SessionSet.create!(session_block: session_block, order: 1)
      #update use different sets per exercise
      # add track reps condition
      session_set.exercise_sets << add_exercise_set(get_exercise(exercise_names[:push_up]), 1, nil, true, 60 )
      session_set.exercise_sets << add_rest(60, 2)
      session_set.exercise_sets << add_exercise_set(get_exercise(exercise_names[:squat]), 3, nil, true, 60)
      session_set.exercise_sets << add_rest(60, 4)
      session_set.exercise_sets << add_exercise_set(get_exercise(exercise_names[:plank_balance]), 5, nil, true, 60) #balance here changes the pivot
      session_set.exercise_sets << add_rest(60, 6)
      session_set.exercise_sets << add_exercise_set(get_exercise(exercise_names[:burpee]), 7, nil, true, 60)
      #if has pull up bar add 
      #session_set.exercise_sets << add_exercise_set(get_exercise('Burpee'), 8, 60) por un ejercicio
      # if no push ups replace for easier variations 
      session_set.save!
      session
    end

    def t2(user)
      # USE SAME EXERCISE USED IN T1 
      raise 'cant create t1 data not ready' if t1_values_not_present(user)
      # need to add the t1s in zeroes if 
      session = Session.create!(session_type: :challenge, order: 2, name_en: 'Test 2', name_es: 'Test 2', order: 2)
      session_block = SessionBlock.create!(session: session, block_type: :test, order: 1)
      session_set = SessionSet.create!(session_block: session_block, order: 1, time_duration: 8*60, loop: true) # add time here for loop
      #add track reps
      session_set.exercise_sets << add_exercise_set(get_exercise(exercise_names[:push_up]), 1, (user.t1_push/3).floor, false, 0)
      session_set.exercise_sets << add_exercise_set(get_exercise(exercise_names[:squat]), 2, (user.t1_legs/3).floor, false, 0)
      session_set.exercise_sets << add_exercise_set(get_exercise(exercise_names[:plank_balance]), 3, (user.t1_core/3).floor, false, 0) #balance here too
      session_set.exercise_sets << add_exercise_set(get_exercise(exercise_names[:burpee]), 4, (user.t1_full/3).floor, false, 0)
      session_set.save!
      session    
    end

    def add_rest_set(session, order)
      session_block = SessionBlock.create!(session: session, block_type: :pause) 
      session_set = SessionSet.new(session_block: session_block, order: order)
      session_set.exercise_sets << add_rest(60, 1)
      session_set.save!
    end

    def add_rest(time, order)
      rest = Exercise.find 5968
      exercise_set = ExerciseSet.new
      exercise_set.exercise = rest
      exercise_set.order = order
      exercise_set.track_reps = false
      exercise_set.time_duration = time
      exercise_set.reps = 0
      exercise_set
    end

    def add_exercise_set(exercise, order, reps, track, time)
      exercise_set = ExerciseSet.new
      exercise_set.exercise = exercise
      exercise_set.order = order
      exercise_set.track_reps = track
      exercise_set.time_duration = time
      exercise_set.reps = reps
      exercise_set
    end

  end
end