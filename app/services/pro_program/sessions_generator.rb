# this generator adds to the automatic generated program for the user a number of sessions indicated in the params
module ProProgram
  class SessionsGenerator

    attr_accessor :user, :sessions_number, :exercises, :program

    def initialize(user: , sessions_number:, exercises:, program:)
      @user = user
      @sessions_number = sessions_number
      @exercises = exercises
      @program =  program
    end

    def call
      ActiveRecord::Base.transaction do
        raise 'user has not finished strengh tests' if tests_not_finished
        sessions = []
        sessions_to_generate.each_with_index do |session_name, index|
          sessions << generate_session(session_name, index)
        end
        add_sessions_to_program(sessions)
      rescue StandardError, ActiveRecord::RecordInvalid => e
        Raven.tags_context error_data: e.to_s if Rails.env != 'test'
        raise e
      end
    end

    def tests_not_finished
      # maybe something better like checking if tests sessions are done
      user.t2_reps < 1
    end

    def add_sessions_to_program(sessions)
      puts 'adding sessions to program'
      sessions.each_with_index do |session, index|
        ProgramSession.create!(program: program, session: session)
      end
    end

    def generate_session(session_name, order)
      raise 'not included type of session' unless session_generators.include?(session_name)
      session = "SessionGenerator::#{session_name}".constantize.new(session_generator_params: {exercises: exercises}, user: user).call
      # +3 cos we have the tests as 1 and 2
      session.order = order + 3
      session.save!
      session
    end

    def session_generators
      #check  interval at time 
      ['HardPaleoRun', 'Hurricane', 'IntervalAtTime', 'IntervalRepetitions', 'PaleoRun', 'SpartanRace', 'Tabata', 'ToTheOne']
    end

    def sessions_to_generate
      sessions = []
      sessions = session_generators if sessions_number >= 8
      while sessions.count < sessions_number
        sessions << session_generators.sample
      end
      sessions
    end
  end
end