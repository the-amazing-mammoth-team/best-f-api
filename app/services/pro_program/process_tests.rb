module ProProgram
  # this service setups the t values for the user after he finishes a test
  # TODO use better naming for the t1 t2 variables as they are from the old app to track better
  class ProcessTests

    attr_accessor :user, :session_execution

    def initialize(user:, session_execution:)
      @user = user
      @session_execution = session_execution
    end

    def exercises
      Exercise.base_exercises
    end

    def process_t1
      #add test correction to every one in case of using different exercises
      exercise_executions = session_execution.exercise_executions
      user.t1_push = exercise_executions.select { |ex| ex.exercise_id == exercises[:push_up] || ex.exercise_id == exercises[:knee_push_up] }.first.reps_executed
      user.t1_core = exercise_executions.select { |ex| ex.exercise_id == exercises[:plank_pivot] || ex.exercise_id == exercises[:plank_balance] }.first.reps_executed # update to balance
      user.t1_legs = exercise_executions.select { |ex| ex.exercise_id == exercises[:squat] }.first.reps_executed
      user.t1_full = exercise_executions.select { |ex| ex.exercise_id == exercises[:burpee] || ex.exercise_id == exercises[:semi_burpee] }.first.reps_executed # burpee or SEMI_burpee
      user.t1_push_exercise = exercise_executions.select { |ex| ex.exercise_id == exercises[:push_up] || ex.exercise_id == exercises[:knee_push_up] }.first.exercise_id #this is an id
      user.t1_full_exercise = exercise_executions.select { |ex| ex.exercise_id == exercises[:burpee] || ex.exercise_id == exercises[:semi_burpee] }.first.exercise_id
      if user.t1_push_exercise == exercises[:push_up] && user.t1_push < 10
        user.t1_push_exercise = exercises[:knee_push_up]
        user.t1_push          *= 2
        user.t1_full_exercise = exercises[:semi_burpee]
        user.t1_full          *= 2
      end
      user.save!
      user
    end

    def process_t2
      exercise_executions = session_execution.exercise_executions
      user.t2_reps = session_execution.reps_executed
      user.t2_steps = exercise_executions.count #this is sets number
      #check if its more simple adds only the half
      user.t2_reps_push = exercise_executions.select { |ex| ex.exercise_id == exercises[:push_up] || ex.exercise_id == exercises[:knee_push_up] }.first.reps_executed
      user.t2_reps_core = exercise_executions.select { |ex| ex.exercise_id == exercises[:plank_pivot] || ex.exercise_id == exercises[:plank_balance] }.first.reps_executed #check plank balance here
      user.t2_reps_legs = exercise_executions.select { |ex| ex.exercise_id == exercises[:squat] }.first.reps_executed
      user.t2_reps_full = exercise_executions.select { |ex| ex.exercise_id == exercises[:burpee] || ex.exercise_id == exercises[:semi_burpee] }.first.reps_executed # burpee or SEMI_burpee
      user.save!
      user
    end
    
  end

end