module ProProgram
  class Create

    include ActionView::Helpers

    attr_accessor :user

    def initialize(user:)
      @user = user
    end

    def call
      ActiveRecord::Base.transaction do
        program = create_program
        add_t1(program)
        user_program = UserProgram.create!(user: user, program: program, current_session: program.sessions.first, active: true )
        user_program 
      rescue StandardError, ActiveRecord::RecordInvalid => e
        Raven.tags_context error_data: e.to_s
        raise e
      end
    end

    def add_t1(program)
      session = SessionGenerator::TestSession.new.t1
      program_session = ProgramSession.new(session: session, program: program)
      program_session.save!
      program_session
    end

     def add_program_goals(program)
      es_chars = I18n.t("pro_program.goals", locale: :es)
      en_chars = I18n.t("pro_program.goals", locale: :en)
      chars = es_chars.map.with_index {|char, index| ProgramCharacteristic.new(program: program, value_es: char, value_en: en_chars[index], objective: true) } 
      program.program_characteristics << chars #this saves seems to be
      program
    end

    def add_program_characteristics(program)
      es_chars = I18n.t("pro_program.characteristics", locale: :es)
      en_chars = I18n.t("pro_program.characteristics", locale: :en)
      chars = es_chars.map.with_index {|char, index| ProgramCharacteristic.new(program: program, value_es: char, value_en: en_chars[index]) } 
      program.program_characteristics << chars #this saves seems to be
      program
    end

    def create_program
      admin = User.find_by(email: 'admin@mhunters.com')
      program = Program.new(user: admin, name_es: 'Innegociable', name_en: 'Pro', pro: true, auto_generated: true, code_name: 'pro_auto',
                            description_en: I18n.t("pro_program.description", locale: :en), description_es: I18n.t("pro_program.description", locale: :es),
                            priority_order: 10)
      program = add_program_characteristics(program)
      program = add_program_goals(program)
      program.save!
      program
    end
  end

end