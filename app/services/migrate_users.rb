class MigrateUsers


  def initialize(file)
    @file = file
  end

  def call
    ActiveRecord::Base.transaction do
      csv = CSV.new(csv_file, col_sep: ';', headers: true)
      csv.each do |row|
        user = User.find_by(email: row.field('email')) if row.field('email').present?
        if user
          puts 'user found really doesnt matter'
        else
          user = create_user(row)
          add_subscription_data(user, row) if user
          send_password_email(user, generate_password)
        end
      end
    end
  end

  def create_user(row)
    user = User.new
    user.email = row.field('email')
    user.gender = row.field('gender')     #male or female string
    user.date_of_birth = nil    #birthday give a utc date       
    user.height = row.field('height_cm') #height_cm
    user.weight = row.field('weight_gr')/1000.0 #weight_gr
    user.activity_level = nil #row.field('activity_level') 1,2,3
    user.goal = nil #row field goal
    user.body_type = #row.field('constitution')
    user.body_fat = row.field('body_fat')
    user.newsletter_subscription = row.field('newsletter_accepted?')
    user.names = row.field('name')
    user.facebook_uid = nil
    user.google_uid = nil
    user.language = row.field('locale')

#  t1_push                       :integer          default(0)
#  t1_core                       :integer          default(0)
#  t1_legs                       :integer          default(0)
#  t1_full                       :integer          default(0)
#  t1_push_exercise              :integer          default(0)
#  t1_pull_up                    :integer          default(0)
#  t2_reps                       :integer          default(0)
#  t2_steps                      :integer          default(0)
#  t2_reps_push                  :integer          default(0)
#  t2_reps_core                  :integer          default(0)
#  t2_reps_legs                  :integer          default(0)
#  t2_reps_full                  :integer          default(0)
#  t2_time_push                  :integer          default(0)
#  t2_time_core                  :integer          default(0)
#  t2_time_legs                  :integer          default(0)
#  t2_time_full                  :integer          default(0)
#  t1_full_exercise              :integer          default(0)
#  t1_pull_up_exercise           :integer
#  warmup_setting                :boolean
#  warmup_session_id             :integer
#  stripe_id                     :string
#  provider                      :string           default(""), not null
#  uid                           :string           default(""), not null
#  affiliate_code                :string
#  affiliate_code_signup         :string
#  best_weekly_streak            :integer          default(0)
#  current_weekly_streak         :integer          default(0)
#  total_sessions                :integer
#  total_time                    :integer
#  kcal_per_session              :decimal(, )
#  reps_per_session              :integer

  end



end