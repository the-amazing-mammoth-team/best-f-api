class LoadAwsExercisesThumbnails

  def self.aws_thumbnails
    @aws_thumbnails ||= get_aws_thumbnails
  end

  def self.aws_thumbnails_400
    @aws_thumbnails_400 ||= get_aws_thumbnails_400
  end 

  def self.get_aws_thumbnails
    require "aws-sdk-s3"
    s3 = Aws::S3::Resource.new(region: 'eu-west-1', access_key_id: 'AKIAI4LGQTRJJ4YFYMRQ', secret_access_key: '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' )
    s3.bucket("stage-dev-new").objects(prefix: 'exercises/thumbnails/', delimiter: 'delimiter').collect(&:key).drop(1) # drop the first because it is exercises/
  end

  def self.get_aws_thumbnails_400
    require "aws-sdk-s3"
    s3 = Aws::S3::Resource.new(region: 'eu-west-1', access_key_id: 'AKIAI4LGQTRJJ4YFYMRQ', secret_access_key: '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' )
    s3.bucket("stage-dev-new").objects(prefix: 'exercises/thumbnails/400x400', delimiter: 'delimiter').collect(&:key).drop(1) 
  end

  def self.call
    Exercise.all.each do |exercise|
      exercise.thumbnail, exercise.thumbnail_male, exercise.thumbnail_female = nil
      exercise.thumbnail_male = build_url_for(exercise, :male) if exists_in_aws?(exercise, :male)
      exercise.thumbnail_female = build_url_for(exercise, :female) if exists_in_aws?(exercise, :female)
      exercise.thumbnail = build_url_for(exercise, nil) if exists_in_aws?(exercise, nil)
      exercise.thumbnail_400, exercise.thumbnail_400_male, exercise.thumbnail_400_female = nil
      exercise.thumbnail_400_male = build_url400_for(exercise, :male) if exists_in_aws400?(exercise, :male)
      exercise.thumbnail_400_female = build_url400_for(exercise, :female) if exists_in_aws400?(exercise, :female)
      exercise.thumbnail_400 = build_url400_for(exercise, nil) if exists_in_aws400?(exercise, nil)
      exercise.save!
    end
  end

  def self.build_url_for(exercise, person)
    return "https://stage-dev-new.s3-eu-west-1.amazonaws.com/exercises/thumbnails/#{exercise.id}_H.jpg" if person == :male
    return "https://stage-dev-new.s3-eu-west-1.amazonaws.com/exercises/thumbnails/#{exercise.id}_M.jpg" if person == :female
    "https://stage-dev-new.s3-eu-west-1.amazonaws.com/exercises/thumbnails/#{exercise.id}.jpg" if person.nil?
  end

  def self.build_url400_for(exercise, person)
    return "https://stage-dev-new.s3-eu-west-1.amazonaws.com/exercises/thumbnails/400x400/#{exercise.id}_H.jpg" if person == :male
    return "https://stage-dev-new.s3-eu-west-1.amazonaws.com/exercises/thumbnails/400x400/#{exercise.id}_M.jpg" if person == :female
    "https://stage-dev-new.s3-eu-west-1.amazonaws.com/exercises/thumbnails/400x400/#{exercise.id}.jpg" if person.nil?
  end

  def self.exists_in_aws400?(exercise, person)
    thumbnails = aws_thumbnails_400
    return thumbnails.include? "exercises/thumbnails/400x400/#{exercise.id}_H.jpg" if person == :male
    return thumbnails.include? "exercises/thumbnails/400x400/#{exercise.id}_M.jpg" if person == :female
    thumbnails.include? "exercises/thumbnails/400x400/#{exercise.id}.jpg" if person.nil?
  end


  def self.exists_in_aws?(exercise, person)
    thumbnails = aws_thumbnails
    return thumbnails.include? "exercises/thumbnails/#{exercise.id}_H.jpg" if person == :male
    return thumbnails.include? "exercises/thumbnails/#{exercise.id}_M.jpg" if person == :female
    thumbnails.include? "exercises/thumbnails/#{exercise.id}.jpg" if person.nil?
  end

end