# we should import only pro users
# users already present should not be imported, but can be updated in case of subscription changes
# users should have their user_program already set, and
# we should map every user_data from one platform to the other
#distinct id on mixpanel is the user id of the old app

class ImportUsers

  require 'csv'

  attr_accessor :csv_file, :user_type

  def initialize(csv_file: , user_type: )
    @csv_file = csv_file
    @user_type = user_type
  end

  def call
     ActiveRecord::Base.transaction do
      updated_users = []
      created_users = []
      csv_read= File.read(Rails.public_path + csv_file)
      csv = CSV.new(csv_read, col_sep: ';', headers: true)
      csv.each do |row|
        if (program_names(row).present? && user_type == 'programs') || user_type != 'programs'
          user = User.find_by(email: row.field('email')) if row.field('email').present?
          if user
            puts 'user found'
            user = update_user(user, row)
            updated_users << user
            if user_type != 'programs'
              delete_old_subscriptions(user)
              subscription = create_subscription(row, user)
              enroll(user, row)
            else
              setup_bought_programs(user, row)            
            end
          else
            puts 'creating new'
            user = add_new_user(row)
            created_users << user
            if user_type != 'programs'
              subscription = create_subscription(row, user)
              enroll(user, row)
            else
              setup_bought_programs(user, row) if program_names(row).present?
            end
            #send_password_email(user, generate_password)
          end
        end
      end
      user_admin = User.find_by email: 'pablo@mhunters.com'
      puts "add #{created_users.count} updated :#{updated_users.count}"
      UserMailer.importer_success(user_admin, 'users importer', extra: { created_users: created_users.map(&:email), updated_users: updated_users.map(&:email) } ).deliver_now if Rails.env != 'test'
    end
  end

  def delete_old_subscriptions(user)
    user.subscriptions&.destroy_all
  end

  def enroll(user, row)
    enroll_program = set_program(user, row)
    programs = Program.where(code_name: ['challenges', 'active rests'])
    programs.each do |program|
      UserProgram.create!(user: user, program: program, active: false)
    end
    user_program = user.user_programs.create(program_id: enroll_program.id, active: true) #, current_session: program.sessions.first
  end

  def set_user_data(user, row)
    user.email = row.field('email')
    user.points = user.points.to_i + row.field('points').to_i if row.field('points')
    user.imported = true
    user.gender = row.field('gender') || 'male'
    user.date_of_birth = row.field('birthday') || Date.today # to check
    user.height = row.field('height_cm') || 170
    user.weight = row.field('weight_kg') || 70
    user.activity_level = set_activity_level(row) || 1 # to check 123
    user.goal = set_goal(row) || 1
    user.body_fat = row.field('body_fat') || 25
    user.body_type = set_body_type(row) # thin, average, strong 
    user.names = row.field('name')
    user.newsletter_subscription = row.field('newsletter_accepted') #echk also
    user.country = row.field('country')
    user.training_days_setting = 3 #row.field('training_days')
    user.password = 'somerandom1234'
    user.language = 'es'
    user.mix_panel_id = row.field('id')
    user = set_test_values(user, row)
    user
  end

  def set_test_values(user, row)
    user.t1_push                       =   row.field('t1_push') if row.field('t1_push')          
    user.t1_core                       =   row.field('t1_core') if row.field('t1_core')          
    user.t1_legs                       =   row.field('t1_legs') if row.field('t1_legs')          
    user.t1_full                       =   row.field('t1_full') if row.field('t1_full')  
    user.t1_push_exercise              =   get_exercise(row.field('t1_push_exercise').to_i ) if row.field('t1_push_exercise')          
    user.t1_pull_up                    =   row.field('t1_pull_up') if row.field('t1_pull_up')          
    user.t2_reps                       =   row.field('t2_reps') if row.field('t2_reps')          
    user.t2_steps                      =   row.field('t2_steps') if row.field('t2_steps')          
    user.t2_reps_push                  =   row.field('t2_reps_push') if row.field('t2_reps_push')          
    user.t2_reps_core                  =   row.field('t2_reps_core') if row.field('t2_reps_core')          
    user.t2_reps_legs                  =   row.field('t2_reps_legs') if row.field('t2_reps_legs')          
    user.t2_reps_full                  =   row.field('t2_reps_full') if row.field('t2_reps_full')          
    user.t2_time_push                  =   row.field('t2_time_push') if row.field('t2_time_push')          
    user.t2_time_core                  =   row.field('t2_time_core') if row.field('t2_time_core')          
    user.t2_time_legs                  =   row.field('t2_time_legs') if row.field('t2_time_legs')          
    user.t2_time_full                  =   row.field('t2_time_full') if row.field('t2_time_full')          
    user.t1_full_exercise              =   get_exercise(row.field('t1_full_exercise').to_i ) if row.field('t1_full_exercise')          
    user.t1_pull_up_exercise           =   get_exercise(row.field('t1_pull_up_exercise').to_i ) if row.field('t1_pull_up_exercise')
    user
  end

  def get_exercise(mh_id)
    code = exercises.values.select { |e| e.first == mh_id }
    code&.first&.second
  end

  def exercises
    { semi_burpee: [193, 5279], burpee: [81, 5236], squat: [90, 5242], push_up: [100, 5252], knee_push_up: [115, 5264],
      plank_pivot: [62, 5217], plank: [61, 5218], pull_up: [112, 5262], short_run: [89, 5241]}
  end

  def set_goal(row)
    return :antiaging if row.field('goal') == 'recomp'
    return :gain_muscle if row.field('goal') == 'gain'
    return :loss_weight if row.field('goal') == 'loss'
  end

  def set_activity_level(row)
    return :sedentary if row.field('activity_level') == '1'  || row.field('activity_level') == 1
    return :medium_active if row.field('activity_level') == '2'|| row.field('activity_level') == 2
    return :very_active if row.field('activity_level') == '3' || row.field('activity_level') == 3
  end

  def set_body_type(row)
    return :lean if row.field('constitution') == 'thin'
    return :medium if row.field('constitution') == 'average'
    return :strong if row.field('constitution') == 'strong'
  end

  def add_new_user(row)
    user = User.new
    user = set_user_data(user, row)
    puts 'saved!' if user.save!
    user
  end

  def update_user(user, row)
    user = set_user_data(user, row)
    puts 'saved!' if user.save!
    user
  end

  def set_product(row)
    store  = set_store(row.field('store'))
    store_reference = row.field('store_reference')
    raise "no store, user id:  #{row.field('id')}" if store.nil?
    product = Product.where(store: store, store_reference: store_reference).first
    product = Product.where(store: :unicorn).first if user_type == 'unicorn'
    product = Product.create(store: store, store_reference: store_reference, price: 0, name: "#{store} #{store_reference}") if product.nil?
    return product if product.id
    nil
  end

  def set_program(user, row)
    #pro is id 2
    if user_type != 'programs'
      program_pairs = { muscle: {old: '5daac8f1421aa94cd9fc2cee', new: 34}, rea: {old: '59edb3a3a6eb510011a587db', new: 31}, 
                      pro: { old: '53ab10373933300002530000', new: nil}, unb: { old: '571f984af0204700065d7966', new: 29}, 
                      ring: { old: '58ff3bfd833de10007161daa', new: 30}}
      program_id = program_pairs.values.select {|p| p[:old] == row.field('legacy_program_id') if row.field('legacy_program_id')}
      program = Program.find_by(id: program_id.first[:new]) if program_id.first
    else
    end 
    program = SuggestedProgramsService.call(user_id: user.id, locale: 'es', onboarding_params: {}).first if program.nil?
    program
  end

  def program_names(row)
    program_names = JSON.parse(row.field('programs')) if row.field('programs').present?
    return nil if program_names.nil?
    permited_programs = ["unbreakable", "rea", "ringmaster", "musclehunters"]
    program_names = program_names.select {|p_name| permited_programs.include?(p_name) } 
    return nil if program_names.empty?
    program_names
  end

  def setup_bought_programs(user, row)
    program_pairs = { muscle: {old: "musclehunters", new: 34}, rea: {old: "rea", new: 31}, 
                      pro: { old: '53ab10373933300002530000', new: nil}, unb: { old: "unbreakable", new: 29}, 
                      ring: { old: "ringmaster", new: 30}}
    program_names = program_names(row)
    if !program_names.nil? && program_names.present? && program_names.count > 0
      program_ids = program_pairs.values.select {|p| program_names.include?(p[:old])}
      program_ids.each do |pro_ids|
        UserProgram.create!(user: user, program_id: pro_ids[:new], active: false)
      end
      program_id = program_pairs.values.select {|p| program_names.last == p[:old] }.first[:new]
      user_program = user.user_programs.where(program_id: program_id).first.update(active: true)
    end
    programs = Program.where(code_name: ['challenges', 'active rests'])
    programs.each do |program|
      UserProgram.create!(user: user, program: program, active: false)
    end
  rescue StandardError => e
    raise e
  end

  def create_subscription(row, user)
    program = set_program(user, row)
    product = set_product(row)
    subscription = Subscription.new(product: product, program: program, user: user)
    if user_type == 'unicorn'
      subscription.start_date = Date.today 
      subscription.end_date = Date.today + 100.years
      subscription.store_metadata = { unicorn: true }
    else
      subscription.cancelled = row.field('is_cancelled')
      subscription.end_date = row.field('expiration_date')
      subscription.transaction_body = row.field('itunes_receipt_raw') if product.apple?
      subscription.transaction_body = row.field('play_subscription_token') if product.google_play? 
      subscription.store_metadata = { stripe_data: row.field('stripe_data'), stripe_customer_id: row.field('stripe_customer_id')} if product.stripe?
    end
    subscription.save!
    subscription
  end

  def set_store(store)
    return 'apple' if store == 'itunes_payment_service' || store == 'itunes'
    return 'google_play' if store == 'play_payment_service' || store == 'play'
    return 'stripe' if store == 'stripe_payment_service' || store == 'stripe'
    return 'pay_pal' if store == 'paypal_payment_service' || store == 'paypal'
    return 'unicorn' if user_type == 'unicorn'
  end

end

