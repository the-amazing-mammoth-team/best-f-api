# == Schema Information
#
# Table name: implements
#
#  id         :bigint           not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  name_en    :string
#  name_es    :string
#
class Implement < ApplicationRecord
  include Attachable
  has_paper_trail

  has_one_attached :image
  translates :name

end
