# == Schema Information
#
# Table name: discount_coupons
#
#  id                  :bigint           not null, primary key
#  stripe_id           :string
#  product_id          :bigint           not null
#  stripe_body         :jsonb
#  discount_percentage :decimal(, )
#  discount_forever    :boolean
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#
class DiscountCoupon < ApplicationRecord
  has_paper_trail

  belongs_to :product, optional: true

  validates :stripe_id, presence: true
  validate :product_stripe
  before_create :create_on_stripe
  before_destroy :delete_on_stripe

  def product_stripe
    product&.store == 'stripe'
  end

  def create_on_stripe
    duration = discount_forever == true ? 'forever' : 'once'
    stripe_coupon = Stripe::Coupon.create({
      duration: duration,
      id: stripe_id,
      percent_off: discount_percentage,
    })
    #self.stripe_id = #se puede asignar por antes o si no viene vfeamos q onda
    self.stripe_body = stripe_coupon.to_json
  end

  def delete_on_stripe
    Stripe::Coupon.delete(
      stripe_id,
    )
  end

end
