# == Schema Information
#
# Table name: daily_reports
#
#  id                      :bigint           not null, primary key
#  report_data             :jsonb
#  report_date             :datetime
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  users_created           :integer
#  subscriptions_activated :integer
#  workouts_completed      :integer
#  workouts_discarded      :integer
#  subscriptions_money     :decimal(, )
#
class DailyReport < ApplicationRecord
  has_paper_trail

  def recalculate
    self.report_data = data
    save!
  end

  def start_date
    report_date
  end

  def end_date
    report_date + 1.day
  end

  def subscriptions
    unicorns = Product.where(store: :unicorn)
    Subscription.where(created_at: (start_date .. end_date), status: :active).where.not(product: unicorns)
  end

  def daily_users_created
    User.where(created_at: (start_date .. end_date), imported: false).count
  end

  def subscriptions_active
    subscriptions.count
  end

  def daily_workouts_done
    SessionExecution.where(created_at: (start_date .. end_date), discarded: false).count
  end

  def daily_workouts_discarded
    SessionExecution.where(created_at: (start_date .. end_date), discarded: true).count
  end

  def subs_money
    money = 0
    subscriptions.each {|s| money = money + s.product.price}
    money
  end

  def set_data
    self.users_created = daily_users_created
    self.subscriptions_activated = subscriptions_active
    self.workouts_completed = daily_workouts_done
    self.workouts_discarded = daily_workouts_discarded
    self.subscriptions_money = subs_money
  end

  def self.all_workouts(start_date=nil, end_date=nil)
    DailyReport.where(report_date: start_date..end_date).sum(:workouts_completed)
  end

  def self.income(start_date=nil, end_date=nil)
    DailyReport.where(report_date: start_date..end_date).sum(:subscriptions_money)
  end

  def self.count_users(start_date=nil, end_date=nil)
    DailyReport.where(report_date: start_date..end_date).sum(:users_created)
  end

end

