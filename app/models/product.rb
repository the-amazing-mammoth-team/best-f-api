# == Schema Information
#
# Table name: products
#
#  id                  :bigint           not null, primary key
#  price               :decimal(, )
#  name                :string
#  store_reference     :string
#  currency            :integer
#  local               :string
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  store               :integer
#  available           :boolean          default(FALSE)
#  has_trial           :boolean          default(FALSE)
#  trial_days          :integer          default(0)
#  discount_percentage :decimal(, )
#  discount_forever    :boolean
#  period              :integer          default("yearly")
#

class Product < ApplicationRecord
  
  #discount consts associated to coupons on stripe
  
  has_paper_trail

  enum currency: [:us, :eur]
  enum store: { apple: 0, google_play: 1, stripe: 2, pay_pal: 3, unicorn: 4 }
  enum period: { yearly: 0, trimester: 1, monthly: 2}
  
  has_one :discount_coupon, dependent: :destroy
  accepts_nested_attributes_for :discount_coupon, allow_destroy: true

end
