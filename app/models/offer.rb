# == Schema Information
#
# Table name: offers
#
#  id         :bigint           not null, primary key
#  offer_code :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Offer < ApplicationRecord
  has_many :product_offers
  has_many :products, through: :product_offers

  accepts_nested_attributes_for :product_offers, allow_destroy: true

end
