# == Schema Information
#
# Table name: documents
#
#  id              :bigint           not null, primary key
#  name            :string
#  description     :string
#  associated_mode :string
#  user_id         :bigint
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
class Document < ApplicationRecord
  include Attachable
  has_one_attached :doc_file
  belongs_to :user
end
