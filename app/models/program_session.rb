# == Schema Information
#
# Table name: program_sessions
#
#  id         :bigint           not null, primary key
#  program_id :bigint           not null
#  session_id :bigint           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class ProgramSession < ApplicationRecord
  has_paper_trail
  belongs_to :program, inverse_of: :program_sessions
  belongs_to :session, inverse_of: :program_sessions

  scope :ordered_by_session, -> { joins(:session).merge(Session.order(:order)) }

  accepts_nested_attributes_for :session
end
