# == Schema Information
#
# Table name: session_executions
#
#  id                  :bigint           not null, primary key
#  scheduled_at        :date
#  user_program_id     :bigint           not null
#  difficulty_feedback :integer
#  enjoyment_feedback  :integer
#  feedback_comment    :string
#  reps_executed       :integer
#  execution_time      :integer
#  order               :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  front_end_id        :uuid
#  session_id          :bigint           not null
#  discarded           :boolean          default(FALSE)
#  discard_reason      :integer
#  imported            :boolean          default(FALSE)
#
class SessionExecution < ApplicationRecord
  has_paper_trail
  has_paper_trail
  belongs_to :user_program
  belongs_to :session

  has_many :session_block_executions, dependent: :destroy
  has_one :session_execution_summary, dependent: :destroy

  enum discard_reason: { too_easy: 0, too_hard: 1, no_time: 2, no_like: 3, other: 4, other_programs: 5 }

  def exercise_executions
    session_sets = SessionSetExecution.where(session_block_execution_id: session_block_executions.pluck(:id)).order(:order)
    session_sets&.map {|ss| ss.exercise_executions }&.flatten
  end
end

# ssh -i id_rsa bitnami@3.249.179.140

# sudo du -a /opt/bitnami/mysql/data | sort -n -r | head -n 20

# less /etc/passwd

# mysql user is mysql

# mysql -u mysql -p


# sudo du -a / | sort -n -r | head -n 20
