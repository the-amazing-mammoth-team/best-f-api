# == Schema Information
#
# Table name: product_programs
#
#  id         :bigint           not null, primary key
#  product_id :bigint           not null
#  program_id :bigint           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class ProductProgram < ApplicationRecord
  has_paper_trail
  belongs_to :product
  belongs_to :program
end
