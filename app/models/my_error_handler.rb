require 'logger'
class MyErrorHandler < Mixpanel::ErrorHandler
  def initialize
    @logger = Logger.new('mylogfile.log')
    @logger.level = Logger::ERROR
  end

  def handle(error)
    @logger.error "#{error.inspect}\n Backtraceor.backtrace}"
  end
end