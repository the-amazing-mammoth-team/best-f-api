# == Schema Information
#
# Table name: program_implements
#
#  id           :bigint           not null, primary key
#  program_id   :bigint           not null
#  implement_id :bigint           not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
class ProgramImplement < ApplicationRecord
  belongs_to :program
  belongs_to :implement
end
