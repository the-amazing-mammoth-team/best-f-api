# == Schema Information
#
# Table name: achievments
#
#  id                 :bigint           not null, primary key
#  name_en            :string
#  name_es            :string
#  icon_url           :string
#  congratulations_en :string
#  congratulations_es :string
#  description_en     :string
#  description_es     :string
#  achievment_type    :integer
#  points             :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
class Achievment < ApplicationRecord
  has_many :user_achievments, dependent: :destroy

  enum achievment_type: { level: 0,  challenge: 1, program: 2, trophie: 3 }

  translates :name, :description, :congratulations

  # def session_points(session)
  #   return 100 if session.challenge?
  #   return 50 if session.active_rest?
  # end

  def self.user_obtains_level(user_points)
    Achievment.where(" achievments.points <= ? and achievments.achievment_type = 0", user_points)
  end

  def self.user_level(user_points)
    Achievment.where(" achievments.points <= ? and achievments.achievment_type = 0", user_points).order("points desc").first
  end

  def self.user_next_level(user_points)
    Achievment.where(" achievments.points > ? and achievments.achievment_type = 0", user_points).order("points asc").first
  end

end
