# == Schema Information
#
# Table name: friendships
#
#  id         :bigint           not null, primary key
#  user_id    :bigint           not null
#  friend_id  :bigint           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Friendship < ApplicationRecord
  has_paper_trail
  belongs_to :user
  belongs_to :friend, class_name: 'User'

  #agregar validacion de no amigos repetidos
end
