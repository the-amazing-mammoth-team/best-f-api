# == Schema Information
#
# Table name: session_execution_summaries
#
#  id                             :bigint           not null, primary key
#  session_execution_id           :bigint           not null
#  total_reps                     :integer
#  total_time                     :integer
#  reps_per_min                   :integer
#  total_kcal                     :integer
#  reps_per_exercise              :jsonb
#  mins_per_exercise              :jsonb
#  reps_per_min_per_exercise      :jsonb
#  created_at                     :datetime         not null
#  updated_at                     :datetime         not null
#  reps_set_per_block             :jsonb
#  time_set_per_block             :jsonb
#  average_reps_min_set_per_block :jsonb
#  reps_min_set_block             :jsonb
#  effort                         :integer
#  points                         :integer
#  value_of_session               :integer
#  body_parts_spider              :jsonb
#  name                           :string
#

# total time a minutos no segundos per exercise pasarlo a string minutos_segundos mins_per_exercise
# 
#
class SessionExecutionSummary < ApplicationRecord
  has_paper_trail
  belongs_to :session_execution
  #belongs_to :historic, class_name: 'SessionHistoricSummary', foreign_key: 'historic_id'

  def friends_reps_per_min; end

  def rest
    rest ||= Exercise.where(name_en: ['rest', 'Rest'])&.first
  end

  def blocks
    blocks ||= session_execution.session&.session_blocks
  end

  def update_streaks(user)
    ##check user session executions this week
    last_week_executions = user.last_week_executions.count
    if last_week_executions == 0
      user.current_weekly_streak = 1
    elsif last_week_executions > 0 && user.weekly_executions.count == 1
      user.current_weekly_streak = user.current_weekly_streak + 1
    end
    # update best weekly
    user.best_weekly_streak = user.current_weekly_streak if user.current_weekly_streak.to_i > user.best_weekly_streak.to_i
    user
  end

  def update_historics_and_user_data
    user = session_execution.user_program.user
    user.points = user.points + self.points
    user.total_sessions = user.total_sessions.to_i + 1
    user.total_time = user.total_time.to_i + self.total_time
    user.kcal_per_session = (user.kcal_per_session.to_f + self.total_kcal) / user.total_sessions
    user.reps_per_session = (user.reps_per_session.to_i + self.total_reps) / user.total_sessions
    user = update_streaks(user)
    add_user_achievments(user)
    user.save!
  end

  def add_user_achievments(user)
    levels = Achievment.user_obtains_level(user.points) - user.achievments
    levels.each { |level| UserAchievment.create!(user: user, achievment: level) }
  end

  def historic_reps_per_min
    historic&.reps_per_min
  end
  
  def set_values
    #update historics should go to the job or in a better part also add activerecord transaction here
    #self.historic_id = set_historic
    set_totals
    set_variables_per_block
    set_variables_per_exercise
    update_historics_and_user_data
  end

  def set_historic
    user = session_execution.user_program.user
    historic_summary = SessionHistoricSummary.where(session_id: session_execution.session_id, user: user)&.first
    historic_summary = SessionHistoricSummary.create(user: user, session_id: session_execution.session_id) unless historic_summary.present?
    historic_summary.id
  end

  def session_points
    challenges = Program.where(code_name: 'challenges')&.first&.sessions || []
    return 50 if challenges.include?(session_execution.session)
    100
  end

  def set_totals
    self.total_reps = session_execution.exercise_executions.sum(&:reps_executed)
    self.total_time = session_execution.exercise_executions.sum(&:execution_time)
    self.reps_per_min = total_reps / (total_time / 60.0)
    self.total_kcal = calculate_calories
    self.points = session_points #this will have more logic in the future
    # points if
    self.effort = session_execution.difficulty_feedback
    self.value_of_session = session_execution.enjoyment_feedback
  end

  def calculate_calories
    user = session_execution.user_program.user
    total_calories = 0
    session_execution.exercise_executions.each do |exercise_execution|
      total_calories += (exercise_execution.exercise.met_multiplier.to_f * exercise_execution.execution_time.to_i)/3600.0
    end
    total_calories&.ceil&.to_i * user.weight.to_i
  end

  def set_reps_per_exercise(variables_per_exercise)
    self.reps_per_exercise = {}
    variables_per_exercise.each {|key, value| self.reps_per_exercise[key] = value.except(:execution_time)}
  end

  def set_mins_per_exercise(variables_per_exercise)
    self.mins_per_exercise = {}
    variables_per_exercise.each {|key, value| self.mins_per_exercise[key] = value.except(:reps)}
  end

  def set_reps_per_min_per_exercise(variables_per_exercise)
    self.reps_per_min_per_exercise = {}
    variables_per_exercise.each do |key, value|
      self.reps_per_min_per_exercise[key] = {} 
      self.reps_per_min_per_exercise[key][:name_en] = value[:name_en]
      self.reps_per_min_per_exercise[key][:name_es] = value[:name_es]
      if value[:execution_time] == 0
        self.reps_per_min_per_exercise[key][:value] = 0
      else
        self.reps_per_min_per_exercise[key][:value] = (value[:reps].to_f / (value[:execution_time] / 60.0)).ceil
      end
    end
  end 

  def set_variables_per_exercise
    vpe = variables_per_exercise
    set_reps_per_exercise(vpe)
    set_mins_per_exercise(vpe)
    set_reps_per_min_per_exercise(vpe)
  end
  
  #vamos a mantener un hash por cada grafico
  def variables_per_exercise
    variables_per_exercise = {}  #this could be like 
    session_execution&.exercise_executions.each do |exercise_execution|
      next if exercise_execution.exercise.id == rest&.id
      if variables_per_exercise[exercise_execution.exercise_id].nil?
        variables_per_exercise[exercise_execution.exercise_id] = {}
        variables_per_exercise[exercise_execution.exercise_id][:name_en] = exercise_execution.exercise.name_en
        variables_per_exercise[exercise_execution.exercise_id][:name_es] = exercise_execution.exercise.name_es
        variables_per_exercise[exercise_execution.exercise_id][:reps] = exercise_execution.reps_executed
        variables_per_exercise[exercise_execution.exercise_id][:execution_time] = exercise_execution.execution_time
      else
        variables_per_exercise[exercise_execution.exercise_id][:reps] = variables_per_exercise[exercise_execution.exercise_id][:reps] + exercise_execution.reps_executed
        variables_per_exercise[exercise_execution.exercise_id][:execution_time] = variables_per_exercise[exercise_execution.exercise_id][:execution_time] + exercise_execution.execution_time
      end
    end
    variables_per_exercise
  end

  def rest_block(block_execution)
    exercise_execution_ids = block_execution.exercise_executions.map(&:exercise_id).uniq
    return false if exercise_execution_ids.count > 1
    return true if exercise_execution_ids.count == 1 && exercise_execution_ids.first == rest&.id
  end

  def rest_set(set_execution)
    exercise_execution_ids = set_execution.exercise_executions.map(&:exercise_id).uniq
    return false if exercise_execution_ids.count > 1
    return true if exercise_execution_ids.count == 1 && exercise_execution_ids.first == rest&.id
  end

  def variables_per_block
    variables_per_block = {}  #this could be like 
    session_execution&.session_block_executions.order(:order).each_with_index do |block_execution, b_index|
      next if rest_block(block_execution)
      variables_per_block[b_index + 1] = {}
      block_name = blocks.where(order: (b_index + 1)).first&.translate_block.to_s
      #variables_per_block[b_index + 1][:block_name] = block_name #use block index here
      block_execution.session_set_executions.order(:order).each_with_index do |set_execution, s_index|
        next if rest_set(set_execution)
        variables_per_block[b_index + 1][s_index + 1] = {}
        exercise_executions = set_execution.exercise_executions.reject {|ex_execution| ex_execution.exercise_id == rest&.id }
        variables_per_block[b_index + 1][s_index + 1][:block_name] = block_name
        variables_per_block[b_index + 1][s_index + 1][:reps] = exercise_executions&.sum(&:reps_executed)
        variables_per_block[b_index + 1][s_index + 1][:execution_time] = exercise_executions&.sum(&:execution_time)
      end
    end
    variables_per_block
  end

  def set_variables_per_block
    vpb = variables_per_block
    set_reps_set_per_block(vpb)             
    set_time_set_per_block(vpb)
    set_average_reps_min_set_per_block(vpb) 
  end

  def set_reps_set_per_block(variables_per_block)
    self.reps_set_per_block = {}
    variables_per_block.each do |key, block|
      self.reps_set_per_block[key] = {}
      block.each do |key2, set|
        self.reps_set_per_block[key][key2] = set.except(:execution_time)
      end
    end
  end

  def set_time_set_per_block(variables_per_block)
    self.time_set_per_block  = {}
    variables_per_block.each do |key, block|
      self.time_set_per_block[key] = {}
      block.each do |key2, set|
        self.time_set_per_block[key][key2] = set.except(:reps)
      end
    end
  end

  def set_average_reps_min_set_per_block(variables_per_block)
    self.average_reps_min_set_per_block = {}
    variables_per_block.each do |key, block|
      self.average_reps_min_set_per_block[key] = {}
      block.each do |key2, set|
        self.average_reps_min_set_per_block[key][key2] = set[:reps] / (set[:execution_time] / 60.0)
      end
    end
  end

  def set_reps_min_set_block #per_exercise
    self.reps_min_set_block = {}
    variables_per_block_per_exercise.each do |key, block|
      self.reps_min_set_block[key] = {}
      block.each do |key2, set|
        self.reps_min_set_block[key][key2] = set[:reps] / (set[:execution_time] / 60.0)
      end
    end
  end

  def variables_per_block_per_exercise
    variables_per_block_per_exercise = {}  #this could be like 
    session_execution&.session_block_executions.each_with_index do |block_execution, b_index|
      variables_per_block_per_exercise[b_index + 1] = {}
      variables_per_block_per_exercise[b_index + 1][:block_name] = blocks.where(order: (b_index + 1)).first&.translate_block.to_s #to_s used for nil values
      block_execution.session_set_executions.each_with_index do |set_execution, s_index|
        variables_per_block_per_exercise[b_index + 1][s_index + 1] = {}
        variables_per_block_per_exercise[b_index + 1][s_index + 1][:reps] = set_execution.exercise_executions&.sum(&:reps_executed)
        variables_per_block_per_exercise[b_index + 1][s_index + 1][:execution_time] = set_execution.exercise_executions&.sum(&:execution_time)
      end
    end
    variables_per_block_per_exercise
  end

end
