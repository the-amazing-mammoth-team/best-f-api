# == Schema Information
#
# Table name: user_achievments
#
#  id            :bigint           not null, primary key
#  user_id       :bigint           not null
#  achievment_id :bigint           not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
class UserAchievment < ApplicationRecord
  belongs_to :user
  belongs_to :achievment
end
