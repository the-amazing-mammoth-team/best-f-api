# == Schema Information
#
# Table name: users
#
#  id                            :bigint           not null, primary key
#  email                         :string           default(""), not null
#  encrypted_password            :string           default(""), not null
#  reset_password_token          :string
#  reset_password_sent_at        :datetime
#  remember_created_at           :datetime
#  created_at                    :datetime         not null
#  updated_at                    :datetime         not null
#  gender                        :integer          not null
#  date_of_birth                 :date             not null
#  height                        :float            not null
#  weight                        :float            not null
#  activity_level                :integer          not null
#  goal                          :integer          not null
#  body_type                     :integer          not null
#  body_fat                      :float            not null
#  newsletter_subscription       :boolean          default(FALSE), not null
#  is_admin                      :boolean
#  names                         :string
#  last_name                     :string
#  sign_in_count                 :integer          default(0), not null
#  current_sign_in_at            :datetime
#  last_sign_in_at               :datetime
#  current_sign_in_ip            :inet
#  last_sign_in_ip               :inet
#  recover_password_code         :integer
#  recover_password_attempts     :integer          default(0)
#  facebook_uid                  :string
#  workout_setting_voice_coach   :boolean          default(TRUE)
#  workout_setting_sound         :boolean          default(TRUE)
#  workout_setting_vibration     :boolean          default(TRUE)
#  workout_setting_mobility      :boolean          default(TRUE)
#  workout_setting_cardio_warmup :boolean          default(TRUE)
#  workout_setting_countdown     :boolean          default(TRUE)
#  notifications_setting         :boolean          default(TRUE)
#  training_days_setting         :integer          default(1)
#  google_uid                    :string
#  language                      :string           default("en")
#  country                       :string
#  points                        :integer          default(0)
#  scientific_data_usage         :boolean          default(FALSE)
#  t1_push                       :integer          default(0)
#  t1_core                       :integer          default(0)
#  t1_legs                       :integer          default(0)
#  t1_full                       :integer          default(0)
#  t1_push_exercise              :integer          default(0)
#  t1_pull_up                    :integer          default(0)
#  t2_reps                       :integer          default(0)
#  t2_steps                      :integer          default(0)
#  t2_reps_push                  :integer          default(0)
#  t2_reps_core                  :integer          default(0)
#  t2_reps_legs                  :integer          default(0)
#  t2_reps_full                  :integer          default(0)
#  t2_time_push                  :integer          default(0)
#  t2_time_core                  :integer          default(0)
#  t2_time_legs                  :integer          default(0)
#  t2_time_full                  :integer          default(0)
#  t1_full_exercise              :integer          default(0)
#  t1_pull_up_exercise           :integer
#  warmup_setting                :boolean
#  warmup_session_id             :integer
#  stripe_id                     :string
#  provider                      :string           default(""), not null
#  uid                           :string           default(""), not null
#  best_weekly_streak            :integer          default(0)
#  current_weekly_streak         :integer          default(0)
#  affiliate_code                :string
#  affiliate_code_signup         :string
#  total_sessions                :integer
#  total_time                    :integer
#  kcal_per_session              :decimal(, )
#  reps_per_session              :integer
#  moengage_id                   :string
#  mix_panel_id                  :string
#  apple_id_token                :string
#  imported                      :boolean          default(FALSE)
#  platform                      :string
#  login_token                   :uuid
#  login_token_generated_at      :datetime
#
class User < ApplicationRecord
  devise :database_authenticatable, :trackable, :registerable, :recoverable, :rememberable, :validatable,
         :omniauthable, omniauth_providers: [:facebook, :google_oauth2]

  has_paper_trail

  attr_accessor :jwt, :login_errors

  enum gender: { female: 0, male: 1 }
  enum goal: { loss_weight: 0, gain_muscle: 1,  antiaging: 2 } 
  enum activity_level: {very_active: 0, medium_active:1, sedentary: 2}
  enum body_type: { lean: 0, medium: 1, strong: 2}
 
  has_many :user_implements, dependent: :destroy
  has_many :implements, through: :user_implements
  has_many :user_programs, dependent: :destroy
  has_many :programs, through: :user_programs
  has_many :subscriptions, dependent: :destroy
  has_many :friendships, dependent: :destroy 
  has_many :friends, through: :friendships
  has_many :user_achievments, dependent: :destroy
  has_many :achievments, through: :user_achievments, dependent: :destroy

  accepts_nested_attributes_for :user_programs, :subscriptions, allow_destroy: true

  validates_presence_of :gender, :goal, :activity_level, :height, :weight, :body_fat, :body_type, :date_of_birth
  validates :email, uniqueness: true
  validates :affiliate_code, uniqueness: true, allow_blank: true

  before_create :set_login_token

  def set_login_token
   self.login_token = SecureRandom.uuid 
   self.login_token_generated_at = Date.today
  end

  def generate_login_token!
    update(login_token: SecureRandom.uuid, login_token_generated_at: Date.today)
  end

  def set_affiliate_code
    self.affiliate_code = SecureRandom.hex.slice(0,6)
  end

  def self.authenticate(args)
    user = User.find_by(email: args[:email]) if args[:email].present?
    user = User.find_by(facebook_uid: args[:facebook_uid]) if args[:facebook_uid].present?
    user = User.find_by(google_uid: args[:google_uid]) if args[:google_uid].present?
    user = User.find_by(apple_id_token: args[:apple_id_token]) if args[:apple_id_token].present?

    user = User.new unless user.present?
    raise StandardError.new 'user not in the db' if user.new_record? && ( !args[:google_uid].present? || !args[:facebook_uid].present? || !args[:apple_id_token].present?)
    raise StandardError.new 'User wrong password' if !user.valid_password?(args[:password]) && !(args[:facebook_uid].present? || args[:google_uid].present? || args[:apple_id_token].present? )
    if user.persisted?
      user.jwt = JsonWebToken.encode(user_id: user.id)
      user.update_tracked
      user.save!
    end
    user
  end

  def self.get_user_from_jwt(jwt)
    decoded = JsonWebToken.decode(jwt)
    user_id = decoded['user_id'] if decoded
    User.find_by(id: user_id)
  end

  def update_tracked
    old_current, new_current = self.current_sign_in_at, Time.now.utc
    self.last_sign_in_at     = old_current || new_current
    self.current_sign_in_at  = new_current
    self.sign_in_count ||= 0
    self.sign_in_count += 1
  end

  def owns?(obj)
    obj&.user_id == id || obj.nil?
  end

  def age
    now = Time.now.utc.to_date
    now.year - date_of_birth.year - ((now.month > date_of_birth.month || (now.month == date_of_birth.month && now.day >= date_of_birth.day)) ? 0 : 1)
  end

  def self.create_user(args)
    user = nil
    args = args.with_indifferent_access
    ActiveRecord::Base.transaction do
      args['password'] = 'esmagnifico1313'#SecureRandom.urlsafe_base64(32) if args['password'].blank?
      user = User.create!(user_args(args))
      if user.persisted?
        user.jwt = JsonWebToken.encode(user_id: user.id)
      end
    end
    user
  end

  def user_program
    user_programs.where(active: true).first
  end

  def current_program
    user_programs.where(active: true)&.last&.program
  end

  def current_session
    user_programs.where(active: true)&.first&.current_session
  end

  def challenges
    programs.where(code_name: 'challenges')&.first
  end

  def active_rests
    programs.where(code_name: 'active rests')&.first
  end

  def current_subscription
    subscriptions&.last
  end

  def is_pro
    return true if subscriptions&.where(status: [:active, :checking]).present?
    false
  end

  def warmup_session
    Session.find_by(id: warmup_session_id)
  end

  def weekly_executions
    from = Date.today.beginning_of_week
    to = Date.today.end_of_week
    UserProgram.user_executions(from: from, to: to, user: self)
  end

  def last_week_executions
    from = Date.today.beginning_of_week - 1.week
    to = Date.today.end_of_week - 1.week
    UserProgram.user_executions(from: from, to: to, user: self)
  end

  def level
    Achievment.user_level(self.points) 
  end

  def next_level
    Achievment.user_next_level(self.points)
  end

  def self.clean_users_without_data
    user_programs = SessionExecution.pluck(:user_program_id)
    user_ids = UserProgram.where(id: user_programs).pluck(:user_id).uniq
    user_ids << Program.all.pluck(:user_id).uniq
    user_ids.flatten!
    User.where.not(id: user_ids).destroy_all
  end

end
