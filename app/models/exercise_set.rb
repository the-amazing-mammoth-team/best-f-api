# == Schema Information
#
# Table name: exercise_sets
#
#  id                    :bigint           not null, primary key
#  session_set_id        :bigint           not null
#  exercise_id           :bigint           not null
#  order                 :integer
#  intensity_modificator :float
#  time_duration         :integer
#  reps                  :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  track_reps            :boolean          default(FALSE)
#
class ExerciseSet < ApplicationRecord
  has_paper_trail
  belongs_to :session_set
  belongs_to :exercise

  default_scope { order(order: :asc) }

  before_save :validate_reps
  before_validation :set_no_reps

  def set_no_reps
    self.reps = 0 if reps.nil?
  end

  def validate_reps
    raise 'no reps assignes' if reps.to_i < 0 || reps.nil?
  end

end
