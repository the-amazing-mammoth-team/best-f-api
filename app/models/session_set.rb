# == Schema Information
#
# Table name: session_sets
#
#  id               :bigint           not null, primary key
#  order            :integer
#  level            :integer
#  time_duration    :integer
#  reps             :integer
#  session_set_type :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  session_block_id :bigint           not null
#  loop             :boolean          default(FALSE)
#
class SessionSet < ApplicationRecord
  has_paper_trail
  belongs_to :session_block
  has_many :exercise_sets, dependent: :destroy
  has_many :exercises, through: :exercise_sets

  enum session_set_type: { undefined: 0, to_the_one: 1, interval_repetitions: 2, hurricane: 3,
                       interval_at_time: 4, interval_at_time_with_pause: 5, warmup: 6, hard_paleo_run: 7, spartan_race: 8, tabata: 9, paleo_run: 10 }

  accepts_nested_attributes_for :exercise_sets, allow_destroy: true

  validates :time_duration, presence: true, if: -> { loop == true }

  default_scope { order(order: :asc) }

end
