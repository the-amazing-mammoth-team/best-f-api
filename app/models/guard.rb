# attempt of creating a class to handle authorization inside the resolvers
class Guard

  def self.not_owned_error
    raise StandardError.new 'you are no the owner!'
  end

  def self.illegal_creation_error
    raise StandardError.new 'illegal action! you cant create stuff for other ppl'
  end

  def self.no_action_error
    raise StandardError.new 'illegal action! you cant create stuff without action or actor'
  end

  def self.not_admin
    raise StandardError.new 'illegal action! you are not an admin'
  end

  def self.owner_actions
    ['subscribe', 'save_workout_session']
  end

  def self.read_actions
    []
  end

  def self.admin_actions
    []
  end

  def self.custom_actions#for actions that do many actions i.e service object
    []
  end

  def self.unauthorized_read
    raise StandardError.new "you can't read this"
  end

  def self.check_permissions(user, action, obj, owner_id)
    if !user.is_admin?
      illegal_creation_error if owner_id != user.id
      unauthorized_read if read_actions.include?(action) && !user.owns?(obj)
      not_owned_error if owner_actions.include?(action) && !user.owns?(obj)
      not_admin if admin_actions.include?(action)
    end
  end

  def self.can?(user, action, obj, owner_id)
    no_action_error if action.blank? || owner_id.blank? || user&.id.nil?
    check_permissions(user, action, obj, owner_id)
  end

end
