# == Schema Information
#
# Table name: session_blocks
#
#  id            :bigint           not null, primary key
#  session_id    :bigint           not null
#  time_duration :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  order         :integer
#  block_type    :integer
#  loop          :boolean          default(FALSE)
#
class SessionBlock < ApplicationRecord
  has_paper_trail

  belongs_to :session
  has_many :session_sets, dependent: :destroy

  enum block_type: { undefined: 0, to_the_one: 1, interval_repetitions: 2, hurricane: 3,
                     interval_at_time: 4, interval_at_time_with_pause: 5, warmup: 6, hard_paleo_run: 7,
                     spartan_race: 8, tabata: 9, paleo_run: 10, pause: 11, warmup: 12, rest: 13, time_trial: 14, cooldown: 15, 
                     super_series: 16, piramid: 17, interval_repetitions_with_pause: 18, test: 19 }

  
  accepts_nested_attributes_for :session_sets, allow_destroy: true

  validates :time_duration, presence: true, if: -> { loop == true }

  default_scope { order(order: :asc) }

  def block_names
    { undefined: {en: '', es: ''}, to_the_one: {en: 'to the one', es: 'tto'}, interval_repetitions: {en: 'interval repetitions', es: 'ir'}, 
      hurricane: {en: 'hurricane', es: 'huracan'}, interval_at_time: {en: 'interval at time', es: 'intervalo a tiempo'}, interval_at_time_with_pause: {en: 'interval at time with pause', es: 'intervalo a tiempo con pausa'},
      warmup: {en: 'warm up', es: 'calentamiento'}, hard_paleo_run: {en: 'hard paleo run', es: 'carrera paleo hard'}, spartan_race: {en: 'spartan race', es: 'carrera espartana'}, tabata: {en: 'tabata', es: 'tabata'},
      paleo_run: {en: 'paleo run', es: 'carrera paleo'}, pause: {en: 'pause', es: 'pausa'}, rest: {en: 'rest', es: 'descanso'}, time_trial: {en: 'time trial', es: 'prueba de tiempo'}, cooldown: {en: 'cooldown', es: 'cooldown'}, 
      super_series: {en: 'super serie', es: 'super serie'}, piramid: {en: 'piramid', es: 'pirámide'}, interval_repetitions_with_pause: {en: 'interval repetitions with pause', es: 'intervalo a repeticiones con pausa'}, test: {en: 'test', es: 'test'} }
  end

  def translate_block
    locale = I18n.locale || :en
    return block_names[block_type.to_sym][:en].capitalize if locale == :en
    block_names[block_type.to_sym][:es].capitalize
  end
end
