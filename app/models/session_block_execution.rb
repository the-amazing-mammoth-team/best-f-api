# == Schema Information
#
# Table name: session_block_executions
#
#  id                   :bigint           not null, primary key
#  session_execution_id :bigint           not null
#  block_type           :integer
#  reps_executed        :integer
#  order                :integer
#  execution_time       :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#
class SessionBlockExecution < ApplicationRecord
  has_paper_trail

  belongs_to :session_execution
  has_many :session_set_executions, dependent: :destroy

  def exercise_executions
    session_set_executions&.map {|ss| ss.exercise_executions }&.flatten    
  end
end
