# == Schema Information
#
# Table name: exercises
#
#  id                     :bigint           not null, primary key
#  video                  :string
#  reps                   :integer
#  time                   :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  legacy_id              :string
#  deprecated             :boolean
#  replacement_legacy_id  :integer
#  family                 :integer
#  sub_family             :integer
#  body_parts_focused     :string           is an Array
#  muscles                :string           is an Array
#  joints                 :string           is an Array
#  met_multiplier         :decimal(, )
#  video_female           :string
#  video_male             :string
#  harder_variation_id    :bigint
#  easier_variation_id    :bigint
#  name_en                :string
#  name_es                :string
#  description_en         :string
#  description_es         :string
#  implement_variation_id :bigint
#  test_correction        :decimal(, )
#  thumbnail              :string
#  thumbnail_male         :string
#  thumbnail_female       :string
#  notes_en               :string
#  notes_es               :string
#  execution_time         :decimal(, )
#  thumbnail_400          :string
#  thumbnail_400_male     :string
#  thumbnail_400_female   :string
#  coach_id               :bigint
#  test_equivalent_id     :bigint
#  t1_min                 :integer          default(0)
#  t1_max                 :integer          default(1000)
#  excluded               :boolean          default(FALSE)
#
class Exercise < ApplicationRecord
  has_paper_trail
  translates :name, :description, :notes

  has_many :exercise_implements
  has_many :implements, through: :exercise_implements
  
  belongs_to :implement_variation, optional: true, class_name: 'Exercise'

  belongs_to :harder_variation, optional: true, class_name: 'Exercise'
  belongs_to :easier_variation, optional: true, class_name: 'Exercise'
  belongs_to :test_equivalent, optional: true, class_name: 'Exercise'
  belongs_to :coach, optional: true, class_name: 'User'  

  enum family: {core: 0, lower_body: 1, full_body: 2, upper_body: 3, pending: 4, runs: 5, back_body: 6, metabolic: 7, sports: 8, rest: 9}
  enum sub_family: {plank: 0, lumbar: 1, abd: 2, salto: 3, sprints: 4, burpee: 5, sent: 6, zanc: 7, cohete: 8, flex: 9, fondo: 10, dom: 11, global: 12}

  accepts_nested_attributes_for :exercise_implements

  CORRELATION_KNEE_PUSH_UP     = 2
  CORRELATION_SEMI_BURPEE      = 2
  CORRELATION_ASSISTED_PULL_UP = 2

  def self.base_exercises
    { push_up: 5252 , squat: 5242, burpee: 5236, plank_pivot: 5217, plank_balance: 5218, pull_up: 5262,
      knee_push_up: 5264, rest: 5968, run: 5241, semi_burpee: 5279, assisted_pull_up: 5359 }
  end

  def self.body_parts
    locale = I18n.locale || 'en'
    {"Brazos" => 'Arms', "Core" => 'Core', "Espalda" => 'Back', "Pectorales" => 'Chest', "Piernas" =>'Legs', "Hombros" => 'Shoulders',
      "Glúteos" => 'Glutes', "Todo el cuerpo" => 'Whole body'}
  end

  def readable_family
    locale = I18n.locale || 'en'
    families_en = {'core' => 'core', 'lower_body' => 'lower body', 'full_body' => 'full body', 'upper_body' => 'upper body', 'pending' => 'pending', 'runs' => 'runs', 'back_body' => 'back body', 'metabolic' => 'metabolic', 'sports' =>'sports', 'rest' => 'rest'}
    families_es = {'core' => 'core', 'lower_body' => 'tronco inferior', 'full_body' => 'cuerpo completo', 'upper_body' => 'tronco superior', 'pending' => 'pending', 'runs' => 'correr', 'back_body' => 'espalda', 'metabolic' => 'metabolico', 'sports' =>'deporte', 'rest' => 'descanso'}
    return families_en[family] if locale == :en
    return families_es[family] if locale == :es
  end

  def video(gender=nil)
    return ( video_male || self.attributes['video'] || video_female ) if gender == 'male'
    return (video_female || self.attributes['video'] || video_male) if gender == 'female'
    (self.attributes['video'] || video_male || video_female)
  end

  def thumbnail(gender=nil)
    return ( thumbnail_male || self.attributes['thumbnail'] || thumbnail_female ) if gender == 'male'
    return (thumbnail_female || self.attributes['thumbnail'] || thumbnail_male) if gender == 'female'
    (self.attributes['thumbnail'] || thumbnail_male || thumbnail_female)
  end

  def thumbnail_400(gender=nil)
    return ( thumbnail_400_male || self.attributes['thumbnail_400'] || thumbnail_400_female ) if gender == 'male'
    return (thumbnail_400_female || self.attributes['thumbnail_400'] || thumbnail_400_male) if gender == 'female'
    (self.attributes['thumbnail_400'] || thumbnail_400_male || thumbnail_400_female)
  end

  def max_reps_for_user(user:)
    raise "no family for this exercise #{id} : name #{name_en}" if family.nil?
    if self.upper_body?
      #DIVIDE BY TEST EQUIVALENT UPDATE HERE
      return [ user.t1_push / CORRELATION_KNEE_PUSH_UP, 9 ].max if user.t1_push_exercise.present? && user.t1_push_exercise == Exercise.where("LOWER(name_en)=LOWER('knee push up')").first&.id
      return [ user.t1_push , 9].max
    end
    if self.full_body?  
      return [ user.t1_full / CORRELATION_SEMI_BURPEE, 9 ].max if user.t1_push_exercise.present? && user.t1_push_exercise == Exercise.where("LOWER(name_en)=LOWER('semi burpee')").first.id
      return [ user.t1_full , 9].max
    end
    if self.back_body?
      return [ user.t1_pull_up/CORRELATION_ASSISTED_PULL_UP, 3 ].max if user.t1_pull_up_exercise.present? && user.t1_pull_up_exercise == Exercise.where("LOWER(name_en)=LOWER('assisted pull up')").first&.id
      return [ user.t1_pull_up.to_i, 3].max
    end
    return [user.t1_legs, 15].max if self.lower_body?
    return [user.t1_core, 21].max if self.core?
  end
end
