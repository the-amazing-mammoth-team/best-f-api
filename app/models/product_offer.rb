# == Schema Information
#
# Table name: product_offers
#
#  id         :bigint           not null, primary key
#  offer_id   :bigint           not null
#  product_id :bigint           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  code_name  :integer
#
class ProductOffer < ApplicationRecord
  belongs_to :offer
  belongs_to :product

  enum code_name: { one_year: 0, one_year_trial: 1, three_months: 2 }

end
