# == Schema Information
#
# Table name: program_profiles
#
#  id         :bigint           not null, primary key
#  program_id :bigint           not null
#  profile_id :bigint           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class ProgramProfile < ApplicationRecord
  has_paper_trail

  belongs_to :program
  belongs_to :profile
end
