# == Schema Information
#
# Table name: profiles
#
#  id             :bigint           not null, primary key
#  gender         :integer
#  activity_level :integer
#  goal           :integer
#  fat_level      :float
#  name           :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  max_fat_level  :float
#  min_fat_level  :float
#
class Profile < ApplicationRecord
  has_paper_trail
  has_many :program_profiles
  has_many :programs, through: :program_profiles

  enum gender: { female: 0, male: 1 }
  enum goal: { loss_weight: 0, gain_muscle: 1,  antiaging: 2 } 
  enum activity_level: {very_active: 0, medium_active:1, sedentary: 2} 
end
