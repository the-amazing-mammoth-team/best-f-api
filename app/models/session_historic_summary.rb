# == Schema Information
#
# Table name: session_historic_summaries
#
#  id                   :bigint           not null, primary key
#  user_id              :bigint           not null
#  session_id           :bigint           not null
#  reps_per_min         :jsonb
#  friends_reps_per_min :jsonb
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#
class SessionHistoricSummary < ApplicationRecord
  has_paper_trail

  #belongs_to :user
  #belongs_to :session
  #has_many :session_execution_summaries, dependent: :destroy
end
