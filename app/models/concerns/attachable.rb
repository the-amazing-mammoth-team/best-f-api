module Attachable
  include ActiveSupport::Concern
  def image_url
    self.image.service_url(expires_in: 1.week) if self.image.present?
  end

  def image2_url
    self.image2.service_url(expires_in: 1.week) if self.image2.present?
  end

  def public_url
    "https://stage-dev-new.s3-eu-west-1.amazonaws.com/#{self.image.key}"
  end
end

#    amazon_config = Rails.application.config.active_storage.service_configurations.amazon
