module Leveleable
  include ActiveSupport::Concern

  def admin
    admin ||= User.find_by(email: 'admin@mhunters.com')
    raise 'no admin' unless admin.present?
    admin
  end

  def families_stack(exercise_count, back_body)
    #returns something [1, 2, 3, 1]
    families = {core: 0, lower_body: 1, full_body: 2, upper_body: 3}
    families[:back_body] = 6 if back_body
    family_values = families.values
    families_stack = []
    while families_stack.count < exercise_count
      add_f = family_values.sample
      families_stack << add_f
      family_values - [add_f]
      family_values = families.values if family_values.count == 0
    end
    families_stack
  end

  def family_t1_max_min(family)
    #this should add at lte and gte for t1_min, t1_max
    return user.t1_push if family == 'tronco superior' || family == 'core'
    return user.t1_legs if family == 'tronco inferior' || family =='lower_core'
    return user.t1_core if family == 'core'
    return user.t1.full_body if family == 'full_body' || family == 'cuerpo completo'
    return user.t1_pull_up if family == 'back_body' || family == 'espalda'
   end

  def exercises_filtered(exercise_count, back_body=false, exercises)
    #add filters by families, add filters by t1_min t1_max per family
    stack = families_stack(exercise_count, back_body)
    exercise_ids = []
    while stack.count > 0
      exercise_id = exercises.where(family: stack.shift, deprecated: [nil, false]).where.not(id: exercise_ids).sample&.id
      if exercise_id
        exercise_ids << exercise_id
      else
        raise "error: no id #{exercise_count}"
      end
    end
    exercise_ids
  end

  # Asumming our class has methods exercises and attributes levels
  def session_exercises #level_exercises better naming
    exercises_count = rand(user_level[:exercises_min]..user_level[:exercises_max])
    exercises = Exercise.where.not(id: [Exercise.base_exercises[:run], Exercise.base_exercises[:rest] ] ) #esto pasa en tbt?
    exercises = Exercise.where(id: exercises_filtered(exercises_count, false, exercises), coach: admin)
  end

  def user_level
    self.class::LEVELS.select {|level| level[:min_t2_reps] <=  user.t2_reps }.last
  end
  #differentiate between level_reps and level_reps_percent
  def level_reps
    rand(user_level[:reps_min]..user_level[:reps_max])
  end

  def level_reps_percent #for exercises
    rand((user_level[:exercise_rep_percent_min])..(user_level[:exercise_rep_percent_max]))
  end

  def level_sets
    rand((user_level[:laps_min])..(user_level[:laps_max]))
  end

  def level_run_time
    rand((user_level[:run_time_min])..(user_level[:run_time_max]))
  end

  def calculate_exercise_repetitions(exercise)
    reps = exercise.max_reps_for_user(user: user) * ( level_reps_percent / 100.0 ) * (exercise.test_correction || 1.0)
    reps = 1 if reps.to_i.zero?
    reps = reps.to_i
    reps = reps.odd? ? reps + 1 : reps
    {reps: reps, time: reps} 
  end

  def sets_number(session_total_reps) # also known as laps
    (level_reps.to_f/session_total_reps.to_f).ceil
  end

  #times for interval with pauses
  def session_total_time
    rand(user_level[:time_min]..user_level[:time_max]) * 60
  end

  #TODO ADD RUN TIMES # they are not being used still just 60 and 90 static for now
end