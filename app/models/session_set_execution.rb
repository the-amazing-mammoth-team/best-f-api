# == Schema Information
#
# Table name: session_set_executions
#
#  id                         :bigint           not null, primary key
#  reps_executed              :integer
#  execution_time             :integer
#  order                      :integer
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  session_block_execution_id :bigint           not null
#
class SessionSetExecution < ApplicationRecord
  has_paper_trail
  belongs_to :session_block_execution
  has_many :exercise_executions, dependent: :destroy

  accepts_nested_attributes_for :exercise_executions
end
