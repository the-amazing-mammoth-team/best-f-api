# == Schema Information
#
# Table name: user_programs
#
#  id                 :bigint           not null, primary key
#  user_id            :bigint           not null
#  program_id         :bigint           not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  active             :boolean          default(TRUE)
#  current_session_id :bigint
#  completed          :boolean          default(FALSE)
#  enjoyment          :string
#  enjoyment_notes    :string
#
class UserProgram < ApplicationRecord
  has_paper_trail
  belongs_to :user
  belongs_to :program
  has_many :session_executions, dependent: :destroy
  
  belongs_to :current_session, class_name: 'Session', foreign_key: 'current_session_id', optional: true

  before_save :set_first_session, if: Proc.new { |user_program| user_program.current_session_id.nil? }

  def program_sessions_count
    program.sessions.count
  end

  def executed_sessions_count
    session_executions.pluck(:session_id).uniq.count
  end

  def progress
    executed_sessions_count * 100/ program_sessions_count.to_f
  end

  def set_first_session
    self.current_session = program.sessions.order(:order).first if program.sessions.present?
  end

  def set_next_current_session
    number = session_executions.map(&:session).map(&:order)&.sort&.last
    self.current_session = program.sessions.where(order: number + 1).first if number
  end

  def executed_sessions
    Session.where(id: session_executions&.pluck(:session_id)).uniq
  end

  def self.user_executions(from:, to:, user:)
    user_programs = user.user_programs
    SessionExecution.where(user_program: user_programs, created_at: (from..to), discarded: false)
  end
  
end
