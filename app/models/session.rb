# == Schema Information
#
# Table name: sessions
#
#  id             :bigint           not null, primary key
#  level          :integer
#  order          :integer
#  session_type   :integer
#  time_duration  :integer
#  reps           :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  strength       :integer          default(0)
#  endurance      :integer          default(0)
#  technique      :integer          default(0)
#  flexibility    :integer          default(0)
#  intensity      :integer          default(0)
#  code_name      :string
#  name_en        :string
#  name_es        :string
#  description_en :string
#  description_es :string
#  calories       :integer
#  warmup_id      :bigint
#  cooldown_id    :bigint
#
class Session < ApplicationRecord
  include Attachable
  has_paper_trail
  #has_many :session_historic_summaries, dependent: :destroy
  has_many :program_sessions, dependent: :destroy
  has_many :programs, through: :program_sessions
  has_many :session_blocks, dependent: :destroy
  belongs_to :warmup, optional: true, class_name: 'Session'
  belongs_to :cooldown, optional: true, class_name: 'Session'

  
  enum session_type: { challenge: 0, test: 1, normal: 2, to_the_one: 3, interval_repetitions: 4, hurricane: 5,
                       interval_at_time: 6, interval_at_time_with_pause: 7, cooldown: 8, paleo_run: 9, hard_paleo_run: 10, spartan_race: 11, tabata: 12 }

                       #tiempo a nivel de bloque it 

  translates :name, :description
  
  accepts_nested_attributes_for :session_blocks, allow_destroy: true

  has_one_attached :image

  #after_save :update_program_attributes

  default_scope { includes(:programs).order(order: :asc)}
  #default_scope { order(order: :asc) }


  def thumbnail
    exercises.first.thumbnail
  end

  def thumbnail_400
    exercises.first.thumbnail_400
  end

  def total_reps
    exercise_sets.map(&:reps).sum
  end

  def exercise_sets
    session_sets = SessionSet.where(session_block_id: session_blocks.pluck(:id))
    session_sets&.map {|ss| ss.exercise_sets }&.flatten&.uniq
  end

  def exercises
    session_sets = SessionSet.where(session_block_id: session_blocks.pluck(:id))
    session_sets&.map {|ss| ss.exercises }&.flatten&.uniq
  end

  def implements
    exercises&.map {|exercise| exercise.implements}&.flatten&.uniq
  end

  def translate_body_parts(body_parts)
    locale = I18n.locale || :en
    return body_parts if locale == :es
    body_parts.each do |bp|
      bp[:name] = Exercise.body_parts[bp[:name]]
    end
  end

  def body_parts_focused
    body_parts = Exercise.body_parts.keys.each_with_object([]){ |item, array| array << {name: item, value: 0}}
    number_of_body_parts = exercises.pluck(:body_parts_focused).flatten.compact.count
    exercises.each do |exercise|
      exercise.body_parts_focused&.each do |body_part|
        found = body_parts.find {|fe| fe[:name] == body_part }
        found[:value] = found[:value] + (1.0 /number_of_body_parts) * 100 if found.present? 
      end
    end
    translate_body_parts(body_parts)
  end

  def update_program_attributes
    programs.each {|program| program.update_physical_attributes}
  end

  def warmups
    Program.find_by(code_name: 'warmups')&.sessions
  end

  def cooldowns
    Program.find_by(code_name: 'Cooldown')&.sessions
  end

  def set_calories
    calories = 0
    exercise_sets.each do |exs|
      time = exs.time_duration.to_i
      if exs.reps.to_i > 0 
        calories = calories + (exs.reps.to_i * exs.exercise.execution_time.to_i * exs.exercise.met_multiplier.to_f / 3600.0)
      elsif  time > 0
        calories = calories + (time * exs.exercise.met_multiplier.to_f / 3600.0)
      end
    end
    calories.ceil
  end

  def set_time
    session_time = 0
    exercise_sets.each do |exs|
      if exs.time_duration && exs.time_duration > 0
        session_time = session_time + (exs.time_duration)
      elsif exs.reps > 0
        session_time = session_time +  (exs.reps * exs.exercise.execution_time.to_f)
      end
    end
    session_time.ceil
  end

end
