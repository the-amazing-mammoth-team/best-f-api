# == Schema Information
#
# Table name: subscriptions
#
#  id                  :bigint           not null, primary key
#  user_id             :bigint           not null
#  product_id          :bigint           not null
#  program_id          :bigint           not null
#  platform            :integer
#  transaction_body    :text
#  status              :integer
#  start_date          :datetime
#  end_date            :datetime
#  subscription_type   :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  cancelled_at        :date
#  cancelled           :boolean          default(FALSE)
#  store_metadata      :jsonb
#  affiliate_code      :string
#  offer_code          :string
#  cancellation_reason :string
#  receipt_data        :jsonb
#
#Subscription.joins(:product).where(products: {store: 0 }).last

class Subscription < ApplicationRecord
  has_paper_trail
  belongs_to :user
  belongs_to :product, optional: true
  belongs_to :program, optional: true

  enum status: { active: 0, inactive: 1, checking: 2}

  validate :only_one_active_per_user

  def self.unicorn
    Subscription.joins(:product).where(products: {store: 3 })
  end

  def self.apple
    Subscription.joins(:product).where(products: {store: 0 })
  end

  def self.playstore
    Subscription.joins(:product).where(products: {store: 1 })
  end

  def self.stripe
    Subscription.joins(:product).where(products: {store: 2 })
  end

  def only_one_active_per_user
    errors.add(:active, 'no more than 1 subscription active per user') if Subscription.where(user: user, status: :active).where.not(id: [self.id, nil]).present?
  end

  def upgrade_options
    Product.where(store: self.product.store, price: self.product.price + 1..Float::INFINITY).where.not(id: self.product.id)
  end
end
