# == Schema Information
#
# Table name: programs
#
#  id              :bigint           not null, primary key
#  user_id         :bigint
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  pro             :boolean          default(TRUE), not null
#  available       :boolean          default(FALSE)
#  strength        :integer          default(0)
#  endurance       :integer          default(0)
#  technique       :integer          default(0)
#  flexibility     :integer          default(0)
#  intensity       :integer          default(0)
#  code_name       :string
#  name_es         :string
#  name_en         :string
#  description_en  :string
#  description_es  :string
#  auto_generated  :boolean          default(FALSE)
#  priority_order  :integer
#  next_program_id :integer
#
class Program < ApplicationRecord

  include Attachable
  has_paper_trail
  belongs_to :user, optional: true
  belongs_to :next_program, optional: true, class_name: 'Program'

  #in order to delete a program must not have user_programs associated
  has_many :user_programs
  has_many :program_sessions, -> { ordered_by_session }, dependent: :delete_all
  has_many :sessions, through: :program_sessions
  has_many :product_programs, dependent: :delete_all #delete this maybe
  has_many :products, through: :product_programs
  has_many :program_profiles, dependent: :delete_all
  has_many :profiles, through: :program_profiles
  has_many :program_characteristics, dependent: :delete_all
  has_many :program_implements, dependent: :delete_all
  has_many :implements, through: :program_implements

  accepts_nested_attributes_for :sessions, :product_programs, :program_characteristics, :program_profiles, :program_sessions, allow_destroy: true

  translates :name, :description

  validates :priority_order, presence: true

  has_one_attached :image
  has_one_attached :image2

  def store_implements
    program_implements.destroy_all
    implements_query.each do |implement|
      program_implements.create(implement: implement)
    end
  end

  def sessions_count
    program_sessions.count
  end
  
  def author
    User.find_by(user_id)
  end

  def exercises
    sessions&.map {|ss| ss.exercises }&.flatten
  end

  def warmup_exercises
    warmup = Program.find_by(code_name: 'warmups')
    warmup&.exercises
  end

  def implements_query
    exercises&.map {|exercise| exercise.implements}&.flatten&.uniq
  end

  def products
    return Product.where(available: true) if pro?
    nil
  end

  def max_attributes
    { strength: strength, endurance: endurance, technique: technique, 
      flexibility: flexibility, intensity: intensity }.sort_by {|k,v| v}.last(2)
  end

  def max_atributes_translations
    { 'strength'=> 'fuerza', "endurance"=> 'resistencia', "technique"=> 'tecnica', "flexibility" => 'flexibilidad', "intensity"=> 'intensidad' }
  end

  def max_attribute
    locale = I18n.locale || :en
    attr = max_attributes.last
    key = locale == :en ? attr.first.to_s : max_atributes_translations[attr.first.to_s]
    OpenStruct.new({key: key.capitalize, value: attr.last, text: nil})
  end

  def max_attribute2
    locale = I18n.locale || :en
    attr = max_attributes.first
    key = locale == :en ? attr.first.to_s : max_atributes_translations[attr.first.to_s]
    OpenStruct.new({key: key.capitalize, value: attr.last, text: nil})
  end

  def update_physical_attributes
    sessions_count = sessions.count.to_f
    sessions_count = 1 if sessions_count == 0
    self.strength = (sessions.map(&:strength).compact.sum / sessions_count).ceil
    self.endurance = (sessions.map(&:endurance).compact.sum / sessions_count).ceil
    self.technique = (sessions.map(&:technique).compact.sum / sessions_count).ceil
    self.flexibility = (sessions.map(&:flexibility).compact.sum / sessions_count).ceil
    self.intensity = (sessions.map(&:intensity).compact.sum / sessions_count).ceil
    self.save!
  end
end
