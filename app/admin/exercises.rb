ActiveAdmin.register Exercise do

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :video, :reps, :time, :name_en, :name_es, :description_en, :description_es, :implement_variation_id, :met_multiplier, :coach_id,
                :deprecated, exercise_implements_attributes: [:id, :exercise_id, :implement_id, :notes_en, :notes_es, :_destroy]
  
  ActiveAdmin.register_page "Load exercises" do
    content do
      render partial: 'admin/exercises/load_exercises'
    end
  end

  ActiveAdmin.register Exercise do

    filter :name_en
    filter :name_es
    filter :description_en
    filter :description_es
    filter :coach_id
    filter :video
    filter :video_female
    filter :video_male
    filter :harder_variation
    filter :easier_variation
    filter :implement_variation
    filter :deprecated
    filter :legacy_id
    filter :replacement_legacy_id
    filter :muscles
    filter :body_parts_focused
    filter :met_multiplier
    filter :updated_at
    filter :created_at

    form partial: 'form'

    collection_action :import_csv, method: :post do
      document = Document.create!(doc_file: params[:load_exercises][:csv], name: 'import_csv exercise', user: current_user)
      ImportExercisesJob.perform_later(document, current_user)
      redirect_to collection_path, notice: "CSV being processed we will email you when the process is done"
    end

    action_item :view, only: :index do
      link_to 'Load exercises', 'load_exercises'
    end

    show do
      render partial: 'show', locals: { exercise: exercise }
    end

    index do
      selectable_column
      id_column
      column :name_en
      column :name_es
      column :coach_id
      column :video
      column :video_female
      column :video_male
      column :harder_variation
      column :easier_variation
      column :implement_variation
      column :deprecated
      column :legacy_id
      column :replacement_legacy_id
      column :muscles
      column :body_parts_focused
      column :met_multiplier
      column :updated_at
      column :created_at
      actions
    end

    csv do
      column :id
      column :coach_id
      column :name_en
      column :name_es
      column :description_en
      column :description_es
      column :notes_en
      column :notes_es
      column :video
      column :video_female
      column :video_male
      column :harder_variation_id
      column :easier_variation_id
      column :implement_variation_id
      column :deprecated
      column :legacy_id
      column :replacement_legacy_id
      column :muscles
      column :body_parts_focused
      column :updated_at
      column :created_at
      column :met_multiplier
      column :family
      column :sub_family
      column :joints
      column(:implement_ids) { |exercise| exercise.implements.pluck(:id) }
    end

  end
end
