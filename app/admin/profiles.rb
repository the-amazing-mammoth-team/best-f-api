ActiveAdmin.register Profile do

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  #permit_params :gender, :activity_level, :goal, :fat_level, :name
  #
  # or
  #
  permit_params do
    permitted = [:gender, :activity_level, :goal, :name, :max_fat_level, :min_fat_level]
    permitted << :other if params[:action] == 'create' && current_user.is_admin?
    permitted
  end

  ActiveAdmin.register Profile do
    form partial: 'form'
  end

  filter :id
  
end
