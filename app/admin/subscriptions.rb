ActiveAdmin.register Subscription do

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  #permit_params :user_id, :product_id, :program_id, :platform, :transaction_body, :status, :start_date, :end_date, :subscription_type
  #
  # or
  #
  permit_params do
    permitted = [:user_id, :product_id, :program_id, :platform, :transaction_body, :status, :start_date, :end_date, :subscription_type]
    permitted << :other if params[:action] == 'create' && current_user.is_admin?
    permitted
  end

  index do
    selectable_column
    id_column
    column :user
    column :product
    column :created_at
    column :start_date
    column :end_date
    column :status
    column :offer_code
    column :cancelled
    column :cancellation_reason
  end

  filter :user
  filter :product
  filter :created_at
  filter :start_date
  filter :end_date
  filter :status
  filter :offer_code
  filter :cancelled
  filter :cancellation_reason

  csv do
    column :id
    column :product_id
    column :user_id
    column(:user_created_at) { |user| user.created_at }
    column :created_at
    column :start_date
    column :end_date
    column :status
    column :offer_code
    column :affiliate_code
    column :program_id
    column :cancelled
    column :cancellation_reason
    column :transaction_body
    column :store_metadata
  end
end
