ActiveAdmin.register User do
  permit_params :email, :password, :password_confirmation, :id, :gender, :date_of_birth, :height,                       
                :weight, :activity_level, :goal, :body_type, :body_fat, :newsletter_subscription, :is_admin,                     
                :names, :last_name, :facebook_uid, :workout_setting_voice_coach, :workout_setting_sound,       
                :workout_setting_vibration, :workout_setting_mobility, :workout_setting_cardio_warmup,
                :workout_setting_countdown, :notifications_setting, :training_days_setting, :google_uid, :language,                     
                :country, :points, :scientific_data_usage, :t1_push, :t1_core, :t1_legs, :t1_full, :t1_push_exercise,            
                :t1_pull_up, :t2_reps, :t2_steps, :t2_reps_push, :t2_reps_core, :t2_reps_legs, :t2_reps_full, :t2_time_push,                  
                :t2_time_core, :t2_time_legs, :t2_time_full, :t1_full_exercise, :t1_pull_up_exercise, :affiliate_code,
                user_programs_attributes: [ :id, :program_id, :_destroy, :active, :created_at],
                subscriptions_attributes: [ :id, :product_id, :program_id, :transaction_body, :status, :start_date, :end_date, :subscription_type, 
                                            :cancelled, :cancellation_reason, :store_metadata, :created_at, :_destroy]   

  ActiveAdmin.register User do
    form partial: 'form'

    member_action :make_subscription, :method => :post do
      subscription = resource
  
      # update workbook using params[:workbook] ...
  
      workbook.save!
      redirect_to :action => :show
    end
  end

  index do
    column resource_selection_toggle_cell, class: 'ola', sortable: false do |resource|
      resource_selection_cell resource
    end
    id_column
    column :email
    column :current_sign_in_at
    column :sign_in_count
    column :created_at
    actions
  end

  filter :email
  filter :current_sign_in_at
  filter :sign_in_count
  filter :created_at

  show do
    render partial: 'show', locals: { user: user }
  end

  member_action :destroy_execution, method: [:get] do
    # This code is evaluated within the controller class
    execution = SessionExecution.find params[:execution_id]
    execution.destroy!
    puts 'found it' 
    redirect_to resource_path(params[:id]), notice: "Destroyed Execution!"
  end

  collection_action :regenerate_pro_sessions, method: :post do
    user = User.find params[:user][:id]
    # this goes inside a jobsessions = ProProgram::SessionsGenerator.new(user: user, sessions_number: 20, exercises: exercises, program: user.current_program).call
     redirect_to user_path(user), notice: "Sessions being re processed"
  end
end
