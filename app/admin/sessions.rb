ActiveAdmin.register Session do
  I18n.locale = :en

  filter :code_name
  filter :strength
  filter :endurance
  filter :technique
  filter :flexibility
  filter :intensity
  filter :created_at
  filter :updated_at

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params do
    permitted = [:code_name, :name, :level, :order, :session_type, :time_duration, :reps, :image, :warmup_id, :cooldown_id,
                 :strength, :endurance, :technique, :flexibility, :intensity, :name_en, :description_en, :name_es, :description_es]
    permitted << :other if params[:action] == 'create' && current_user.is_admin?
    permitted << [ session_blocks_attributes: [:id, :session_id, :order, :block_type, :time_duration, :loop, :_destroy, 
                   session_sets_attributes: [:id, :session_id, :name, :level, :order, :session_set_type, :time_duration, :reps, :loop, :_destroy,
                   exercise_sets_attributes: [:id, :track_reps, :exercise_id, :order, :reps, :time_duration, :_destroy] ]] ]
    permitted << [load_session: [:csv, :name, :order, :strength, :endurance, :technique, :flexibility, :intensity, :program_id, :session_update_id]]
    permitted << [ session_sets_attributes: [:id, :session_id, :name, :level, :order, :session_set_type, :time_duration, :reps, :loop, :_destroy] ]
    permitted
  end
  #
  # or
  #
  # permit_params do
  #   permitted = [:name, :level, :order, :session_type, :time_duration, :reps]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
  index do
    selectable_column
    id_column
    column :code_name
    column :programs do |session|
      session.programs.pluck(:code_name)
    end
    column :strength
    column :endurance
    column :technique
    column :flexibility
    column :intensity
    column :created_at
    column :updated_at
    actions
  end

  ActiveAdmin.register_page "Load session" do
    content do
      render partial: 'admin/sessions/load_session'
    end
  end

  ActiveAdmin.register Session do
    form partial: 'form'

    collection_action :import_csv, method: :post do
      document = Document.create!(doc_file: params[:load_session][:csv], name: 'import_csv session', user: current_user)
      l_session = params[:load_session]
      session_attributes = Session.new(name: l_session[:name], order: l_session[:order], code_name: l_session[:code_name],
                                       strength: l_session[:strength], endurance: l_session[:endurance], technique: l_session[:technique], 
                                       flexibility: l_session[:flexibility], intensity: l_session[:intensity]).attributes
      ImportSessionJob.perform_later(csv_file: document, user: current_user, session_attributes: session_attributes, program_id: params[:load_session][:program_id].to_i, session_update_id: params[:load_session][:session_update_id])
      redirect_to collection_path, notice: "CSV being processed we will email you when the process is done"
    end

    action_item :view, only: :index do
      link_to 'Load session', 'load_session'
    end

    show do
      render partial: 'show', locals: { session: session }
    end

  end
end
