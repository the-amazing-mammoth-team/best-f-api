ActiveAdmin.register Offer do

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  # permit_params :offer_code
  #
  # or
  #
  permit_params do
    permitted = [:offer_code, product_offers_attributes: [:id, :product_id, :offer_id, :_destroy]]
    permitted << :other if params[:action] == 'create' && current_user.is_admin?
    permitted
  end
 
  ActiveAdmin.register Offer do
    form partial: 'form'
  end

  filter :id

end
