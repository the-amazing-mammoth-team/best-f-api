ActiveAdmin.register DiscountCoupon do

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  form partial: 'form'
  permit_params :stripe_id, :product_id, :stripe_body, :discount_percentage, :discount_forever
  #
  # or
  #
  # permit_params do
  #   permitted = [:stripe_id, :product_id, :stripe_body, :discount_percentage, :discount_forever]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
  
end
