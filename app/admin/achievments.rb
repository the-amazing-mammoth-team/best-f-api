ActiveAdmin.register Achievment do

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  # permit_params :name_en, :name_es, :icon_url, :congratulations_en, :congratulations_es, :description_en, :description_es, :achievment_type, :points
  #
  # or
  #
  permit_params do
    permitted = [:name_en, :name_es, :icon_url, :congratulations_en, :congratulations_es, :description_en, :description_es, :achievment_type, :points]
    permitted << :other if params[:action] == 'create' && current_user.is_admin?
    permitted
  end

  filter :name_en
  
end
