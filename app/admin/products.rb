ActiveAdmin.register Product do

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :price, :name, :store_reference, :currency, :local, :store, :available, 
                :has_trial, :trial_days, :discount_forever, :discount_percentage, :period
  #
  # or
  #
  # permit_params do
  #   permitted = [:price, :name, :store_reference, :currency, :local]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
  ActiveAdmin.register Product do
    form partial: 'form'
  end

  filter :name
  filter :price
  filter :period
  filter :store
  filter :store_reference
  filter :currency
  filter :available
  filter :has_trial
  filter :trial_days
  filter :created_at

end
