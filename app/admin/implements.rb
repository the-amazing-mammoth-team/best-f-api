ActiveAdmin.register Implement do

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :name_en, :name_es, :image
  #
  # or
  #
  # permit_params do
  #   permitted = [:name, :image_file_name, :image_content_type, :image_file_size, :image_updated_at]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end

  filter :name_en
  filter :name_es
  
  ActiveAdmin.register Implement do
    form partial: 'form'
  end
end
