ActiveAdmin.register Program do
  scope("DEFAULT", default: true) { |scope| scope.where(auto_generated: false) }

  I18n.locale = :en
  filter :code_name
  filter :name_en
  filter :name_es
  filter :available
  filter :sessions_count
  filter :user
  filter :available
  filter :pro
  filter :strength
  filter :endurance
  filter :technique
  filter :flexibility
  filter :intensity
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  # permit_params :name, :user_id
  #
  # or
  #
  permit_params do
    permitted = [:name, :code_name, :user_id, :pro, :description, :available, :image, :image2, :strength, :endurance, :priority_order,
                 :technique, :flexibility, :intensity, :name_en, :description_en, :name_es, :description_es, :auto_generated, :next_program_id ]
    permitted << :other if params[:action] == 'create' && current_user.is_admin?
    permitted << [program_profiles_attributes: [:id, :program_id, :profile_id, :_destroy]]
    permitted << [sessions_attributes: [:id, :program_id, :name_en, :description_en, :name_es, :description_es, :level, :order, :session_type, :time_duration, :reps, :_destroy]]
    permitted << [product_programs_attributes: [:id, :program_id, :product_id, :_destroy]]
    permitted << [program_characteristics_attributes: [:id, :objective, :program_id, :value_en, :value_es, :_destroy]]
    permitted << [program_sessions_attributes: [:id, :program_id, :session_id, :_destroy]]

    permitted
  end

  index do
    selectable_column
    id_column
    column 'Image', sortable: :image do |program| link_to 'Image', program.image_url if program.image_url  end
    column 'Image2', sortable: :image do |program| link_to 'Image2', program.image2_url if program.image2_url  end
    column :code_name
    column :name
    column :available
    column :sessions_count
    column 'Author', :user
    column :available
    column :pro
    column :strength
    column :endurance
    column :technique
    column :flexibility
    column :intensity
    actions
  end

  ActiveAdmin.register Program do
    form partial: 'form'
  end

  show do
    render partial: 'show', locals: { program: program }
  end

end