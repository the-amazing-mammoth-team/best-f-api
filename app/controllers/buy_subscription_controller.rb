class BuySubscriptionController < ApplicationController
    # each step will be set in order down diagnostic, diagnostic_results, sign_up, suggested_programs, pay_selected_plan,  thanks
    before_action :set_spanish
    before_action :check_current_or_redirect_to_diagnostic, only: [:suggested_programs, :select_plan, :pay_selected_plan, :subscribe_stripe, :thanks]
    # dont use the before_filter auhtneticate it breaks the controller
  
    # todo add subscribe flow
    include Devise::Controllers

    def set_spanish
      I18n.locale = :es
    end
    
    def suggested_programs
      
      @programs = SuggestedProgramsService.call(user_id: current_user.id , locale: :es, 
                                                onboarding_params: {program_id: cookies[:program_id], affiliate_code: cookies[:affiliate_code], offer_code: cookies[:offer_code]})
    end
  
    def more_programs
      @programs = SuggestedProgramsService.call(user_id: current_user.id , locale: :es, 
        onboarding_params: {program_id: cookies[:program_id], affiliate_code: cookies[:affiliate_code], offer_code: cookies[:offer_code]})
    end
  
    def program_detail
      @program = Program.find(params[:program_id])
    end
  
    def choose_subscription #choose subscription on design
      offer_codes =  [ 'onboarding_web_1y', 'onboarding_web_3m']
      offer_codes = [cookies[:offer_code]] if cookies[:offer_code].present?
      @products = get_products(offer_codes: offer_codes, device: 'stripe')
      @program = Program.find(params[:program_id])
    end
  
    def payment_page
      # add error here for people that already has a subscription
      @product = Product.find params[:product_id]
      @program = Program.find params[:program_id]
    end
  
    def subscribe_stripe
      sub = Subscription.new(subscription_params)
      stripe_subscription = SubscribeStripe.new(stripe_token: params[:stripe_token], product: Product.find(sub.product_id.to_i) , user: current_user).call
      if stripe_subscription.present?
        subscription = SubscribeUser.new(user: current_user, program_id: sub.program_id, product_id: sub.product_id, 
                                         transaction_body: stripe_subscription, extra_params: {affiliate_code: cookies[:affiliate_code], offer_code: cookies[:offer_code]}).call
         
        redirect_to buy_subscription_thank_you_page_path
      else
        render html: 'error  de tarjeta vuelva a intentarlo'
      end
    end

    def lite_option
      # setup other programs
      user = current_user
      user_program = SetUserProgram.new(user: user, program_id: params[:program_id]).call if user && params[:program_id]
      programs = Program.where(code_name: ['challenges', 'active rests'])
      programs.each do |program|
        UserProgram.create!(user: user, program: program, active: false)
      end
      if user_program.present?
        redirect_to buy_subscription_thank_you_page_path 
      else
        render html: 'error'
      end
    end
  
    def thank_you_page
      @program = current_user.current_program
      offer_codes = [ 'onboarding_web_1y']
      @products = get_products(offer_codes: offer_codes, device: 'stripe') if !current_user.is_pro
      @program = current_user.current_program || Program.first
    end

    def upsale
      offer_codes = [ '1y_7999t', '3m_4999t', '1y_7999_ft_14t']
      @products = get_products(offer_codes: offer_codes, device: 'stripe')
      @program = Program.find(params[:program_id])
    end
   
    def check_current_or_redirect_to_diagnostic
      redirect_to onboarding_diagnostic_path unless current_user
    end

    def get_products(offer_codes: , device:)
      # 1y_5999, 1y_7999, 3m_4999, 1y_7999_ft_14
      # for tests we will use offer_codes = ['1y_5999t', '1y_7999t', '3m_4999t', '1y_7999_ft_14t']
      offers = Offer.where(offer_code: offer_codes)
      return nil if offers.empty?
      products = []
      offers.each { |offer| products << offer.products&.where(store: device).first }
      products
    end

    def offer_login
      user = User.find_by(login_token: params[:token])
      if user.present?
        sign_in :user, user
        sign_in :buy_subscription, user
        setup_offer_login_cookies(params)
        redirect_to buy_subscription_suggested_programs_path, notice: 'Logged in successfully!'
      else
        render html: "The link you used was invalid. Please request a new login link token #{params[:token]}"
      end
    end

    def setup_offer_login_cookies(params)
      reset_cookies
      cookies[:program_id] = params[:program_id] if params[:program_id]
      cookies[:affiliate_code] = params[:affiliate_code] if params[:affiliate_code]
      cookies[:offer_code] = params[:offer_code] if params[:offer_code]
    end

    def reset_cookies
      cookies.delete :program_id
      cookies.delete :affiliate_code
      cookies.delete :offer_code
    end

    private 
      def user_params
        params.require(:user).permit(:goal, :activity_level, :gender, :weight, :height, :body_fat, :date_of_birth,
                                           :body_type, :newsletter_subscription, :scientific_data_usage, :email, :names, :password)
      end
  
      def subscription_params
        params.require(:subscription).permit(:program_id, :user_id, :product_id, :transaction_body)
      end
  
  end
  