class OnboardingController < ApplicationController
  # each step will be set in order down diagnostic, diagnostic_results, sign_up, suggested_programs, pay_selected_plan,  thanks
  before_action :set_spanish
  before_action :check_current_or_redirect_to_diagnostic, only: [:suggested_programs, :select_plan, :pay_selected_plan, :subscribe_stripe, :thanks]
  # dont use the before_filter auhtneticate it breaks the controller

  # todo add subscribe flow
  include Devise::Controllers

  def set_spanish
    I18n.locale = :es
  end

  def diagnostic
    cookies[:program_id] = params[:program_id]
    cookies[:affiliate_code] = params[:affiliate_code]
    cookies[:offer_code] = params[:offer_code]
    @user = User.new
  end
  
  def sign_up_options#diagnostic and signup can vary
    @user = User.new(user_params)
  end
  
  def new_sign_up
    @mix_panel_id = JSON.parse(cookies[:mp_745581c49f04c81ecf450cceeea8a514_mixpanel])["distinct_id"] if cookies[:mp_745581c49f04c81ecf450cceeea8a514_mixpanel]
    @user = User.new(user_params)
  end

  def sign_up
    #  training_days_setting         :integer          default(1)
    #@user = User.new(user_params.merge({affiliate_code: cookies[:affiliate_code]}))
    @user = SignUpService.call(user_params: user_params.merge({affiliate_code_signup: cookies[:affiliate_code]}))
    if @user.persisted?
      sign_in :user, @user
      sign_in :onboarding, @user
      redirect_to onboarding_diagnostic_results_path
    end
    rescue StandardError => e
      I18n.locale = :es
      @user = User.new(user_params.merge({affiliate_code_signup: cookies[:affiliate_code]}))
      @user.validate
      render 'new_sign_up'
      #redirect_to onboarding_new_sign_up_path
  end

  def diagnostic_results
    @diagnostic = CalculateDiagnostic.new(user: current_user).call
  end

  private 
    def user_params
      params.require(:user).permit(:goal, :activity_level, :gender, :weight, :height, :body_fat, :date_of_birth, :mix_panel_id,
                                         :body_type, :newsletter_subscription, :scientific_data_usage, :email, :names, :password)
    end

    def subscription_params
      params.require(:subscription).permit(:program_id, :user_id, :product_id, :transaction_body)
    end

end
