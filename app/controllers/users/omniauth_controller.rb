#facebook params
# puts request.env['omniauth.auth']
# puts request.env['omniauth.auth']['provider']
# puts request.env['omniauth.auth']['uid']
# puts request.env['omniauth.auth'].raw_info['email']
# puts request.env['omniauth.auth']['image']
class Users::OmniauthController < ApplicationController
  # todo add subscribe flow
  include Devise::Controllers
  # facebook callback
  def set_user(form_params, omni_params, provider)
    user = User.new(form_params)
    user.facebook_uid = omni_params['uid'] if provider == 'facebook'
    user.google_uid = omni_params['uid'] if provider == 'google'
    user.email = omni_params.info['email']
    user
  end

  def facebook
    user = set_user(request.env["omniauth.params"]["user"], request.env['omniauth.auth'], 'facebook')
    saved_user = SignUpService.call(user_params: user.attributes.merge({password: Devise.friendly_token[0, 20]}))
    if saved_user.persisted?
      sign_in :user, saved_user
      sign_in :onboarding, saved_user
      redirect_to onboarding_diagnostic_results_path
    else
      render plain: "There was a problem signing you in through Facebook. Please register or try signing in later. #{saved_user.errors.to_h.to_s} ola".html_safe
      #redirect_to onboarding_diagnostic_results_path
    end
  rescue ActiveRecord::RecordInvalid => e
    render plain: "There was a problem signing you in through face. Please register or try signing in later. #{e.message.to_s} ola".html_safe
  end

  # google callback
  def google_oauth2
    user = set_user(request.env["omniauth.params"]["user"], request.env['omniauth.auth'], 'google')
    saved_user = SignUpService.call(user_params: user.attributes.merge({password: Devise.friendly_token[0, 20]}))
    if saved_user.persisted?
      sign_in :user, saved_user
      sign_in :onboarding, saved_user
      redirect_to onboarding_diagnostic_results_path
    else
      render plain: "There was a problem signing you in through google. Please register or try signing in later. #{saved_user.errors.to_h.to_s} ola".html_safe
      #redirect_to onboarding_diagnostic_results_path
    end
  rescue ActiveRecord::RecordInvalid => e
    render plain: "There was a problem signing you in through google. Please register or try signing in later. #{e.message.to_s} ola".html_safe
  end

  def failure
    flash[:error] = 'There was a problem signing you in. Please register or try signing in later.' 
    redirect_to new_user_registration_url #add route to login
  end

  # def passthru
  #   super
  # end
end
