class ApplicationController < ActionController::Base
  skip_before_action :verify_authenticity_token
  after_action :allow_videos_iframe

  def access_denied(exception)
    redirect_to unauthorized_path, alert: exception.message
  end

  private
  
  def allow_videos_iframe
    response.headers['X-Frame-Options'] = 'ALLOWALL' 
  end
 
end
