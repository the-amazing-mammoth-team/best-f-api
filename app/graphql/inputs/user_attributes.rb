#  id                        :bigint           not null, primary key
#  email                     :string           default(""), not null
#  encrypted_password        :string           default(""), not null
#  reset_password_token      :string
#  reset_password_sent_at    :datetime
#  remember_created_at       :datetime
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  gender                    :integer          not null
#  date_of_birth             :date             not null
#  height                    :float            not null
#  weight                    :float            not null
#  activity_level            :integer          not null
#  goal                      :integer          not null
#  body_type                 :integer          not null
#  body_fat                  :float            not null
#  newsletter_subscription   :boolean          default("false"), not null
#  is_admin                  :boolean
#  names                     :string
#  last_name                 :string
#  sign_in_count             :integer          default("0"), not null
#  current_sign_in_at        :datetime
#  last_sign_in_at           :datetime
#  current_sign_in_ip        :inet
#  last_sign_in_ip           :inet
#  recover_password_code     :integer
#  recover_password_attempts :integer          default("0")
#  facebook_uid              :string
#
class Inputs::UserAttributes < Types::BaseInputObject
  description "Attributes for creating or updating user attributes"
  argument :id, Integer, required: false
  argument :email, String, required: false
  argument :date_of_birth, String, required: false
  argument :height,                    String, required: false
  argument :weight,                    String, required: false
  argument :activity_level,            String, required: false
  argument :goal,                      String, required: false
  argument :body_type,                 String, required: false
  argument :body_fat,                  String, required: false
  argument :newsletter_subscription,   Boolean, required: false
  argument :is_admin,                  Boolean, required: false
  argument :names,                     String, required: false
  argument :last_name,                 String, required: false
  argument :workout_setting_voice_coach, Boolean, required: false
  argument :workout_setting_sound, Boolean, required: false
  argument :workout_setting_vibration, Boolean, required: false
  argument :workout_setting_mobility, Boolean, required: false
  argument :workout_setting_cardio_warmup, Boolean, required: false
  argument :workout_setting_countdown, Boolean, required: false
  argument :training_days_setting, Integer, required: false
  argument :language, String, required: false
  argument :gender, String, required: false
  argument :country, String, required: false
  argument :warmup_setting, Boolean, required: false
  argument :warmup_session_id, Integer, required: false
  argument :notifications_setting, Boolean, required: false
  argument :scientific_data_usage, Boolean, required: false
end
