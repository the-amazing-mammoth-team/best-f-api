#  id                   :bigint           not null, primary key
#  session_execution_id :bigint           not null
#  reps_executed        :integer
#  execution_time       :integer
#  order                :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#
class Inputs::SessionSetExecutionAttributes < Types::BaseInputObject
  description "Attributes for creating or updating a session set execution"
  argument :order, Integer, required: false
  argument :exercise_executions, [Inputs::ExerciseExecutionAttributes], required: false
end
