#  user_program_id     :bigint           not null
#  difficulty_feedback :integer
#  enjoyment_feedback  :integer
#  feedback_comment    :string
#  reps_executed       :integer
#  execution_time      :integer
#  order               :integer
class Inputs::SessionExecutionAttributes < Types::BaseInputObject
  description "Attributes for creating or updating a session execution"
  argument :front_end_id, String, "Use uuid to send this", required: true
  argument :session_id, ID, required: true
  argument :program_id, ID, required: true
  argument :user_program_id, ID, required: false
  argument :difficulty_feedback, Integer, "easy, perfect, hard", required: false
  argument :enjoyment_feedback, Integer, "horrible, ok, great", required: false
  argument :feedback_comment, String, 'a comment', required: false
  argument :session_block_executions, [Inputs::SessionBlockExecutionAttributes], required: false
end
