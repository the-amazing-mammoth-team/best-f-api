class Inputs::SessionBlockExecutionAttributes < Types::BaseInputObject
  description "Attributes for creating or updating a session execution"
  argument :order, Integer, required: false
  argument :session_set_executions, [Inputs::SessionSetExecutionAttributes], required: false
end
