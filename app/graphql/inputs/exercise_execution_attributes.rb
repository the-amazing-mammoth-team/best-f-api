# == Schema Information
#
# Table name: exercise_executions
#
#  id                       :bigint           not null, primary key
#  exercise_id              :bigint           not null
#  session_set_execution_id :bigint           not null
#  reps_executed            :integer
#  execution_time           :integer
#  order                    :integer
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#

class Inputs::ExerciseExecutionAttributes < Types::BaseInputObject
  description "Attributes for creating or updating an exercise"
  argument :exercise_id, ID, required: true
  argument :order, Integer, required: false
  argument :reps_executed, Integer, required: false
  argument :execution_time, Integer, required: true
end
