module Mutations
  class ProgramFeedback < BaseMutation
    argument :enjoyment, String, required: true
    argument :enjoyment_notes, String, required: false
    argument :user_program_id, ID, required: true
    
    type Types::UserProgramType

    def resolve(args)
      user = context[:current_user]
      user_program = UserProgram.find args[:user_program_id]
      if user_program&.user.id == user.id
        user_program.update(enjoyment: args[:enjoyment], enjoyment_notes: args[:enjoyment_notes])
      end
      user_program
    rescue ActiveRecord::RecordInvalid => e
      GraphQL::ExecutionError.new("Invalid input: #{e.record.errors.full_messages.join(', ')}")
    rescue StandardError, UserReadableError => e
      GraphQL::ExecutionError.new(e.message)
    end
  end

end
