module Mutations
  class AddFriend < BaseMutation
    argument :friend_id, ID, required: true
    
    type Types::FriendType

    def resolve(args)
      user = context[:current_user]
      friendship = FriendsManager::AddFriend.new(user_id: user.id, friend_id: args[:friend_id]).call
      friendship.friend
    end

  end

end