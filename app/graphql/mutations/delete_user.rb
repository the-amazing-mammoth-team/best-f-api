module Mutations
  class DeleteUser < BaseMutation

    argument :user_id, ID, required: true
    type Boolean

    def resolve(user_id:)
      user = User.find user_id
      return user.destroy! if user == context[:current_user]
      rescue ActiveRecord::RecordInvalid => e
        GraphQL::ExecutionError.new("Invalid input: #{e.record.errors.full_messages.join(', ')}")
      rescue StandardError, CustomError, UserReadableError => e
        GraphQL::ExecutionError.new(e.message)
    end
  end
end