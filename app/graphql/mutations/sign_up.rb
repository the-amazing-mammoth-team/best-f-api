
module Mutations
  class SignUp < BaseMutation
    argument :email, String, required: true
    argument :password, String, required: false
    argument :height, Float, required: true
    argument :weight, Float, required: true
    argument :goal, String, required: true
    argument :gender, String, required: true
    argument :body_fat, Float, required: true
    argument :body_type, String, required: true
    argument :language, String, required: true
    argument :activity_level, String, required: true
    argument :date_of_birth, String, required: true
    argument :newsletter_subscription, Boolean, required: false
    argument :names, String, required: false
    argument :last_name, String, required: false
    argument :facebook_uid, String, required: false
    argument :google_uid, String, required: false
    argument :scientific_data_usage, Boolean, required: false
    argument :mix_panel_id, String, required: false
    argument :moengage_id, String, required: false
    argument :apple_id_token, String, required: false


    type Types::UserType

    def resolve(args)
      args[:password] = 'esmagnifico1313' if args[:facebook_uid].present? || args[:google_uid].present? || args[:apple_id_token].present?
      SignUpService.call(user_params: args)
    rescue ActiveRecord::RecordInvalid => e
      GraphQL::ExecutionError.new("Invalid input: #{e.record.errors.full_messages.join(', ')}")
    end
  end
end