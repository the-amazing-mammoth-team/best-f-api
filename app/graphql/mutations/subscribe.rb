
module Mutations
  class Subscribe < BaseMutation
    argument :program_id, ID, required: true
    argument :product_id, ID, required: true
    argument :transaction_body, String, required: true
    argument :upgrade, Boolean, required: false
  
    type Types::SubscriptionType

    def resolve(args)
      user = context[:current_user]
      raise 'no user!' unless user
      subscription = SubscribeUser.new(user: user, program_id: args[:program_id], product_id: args[:product_id],
                                       transaction_body: args[:transaction_body], upgrade: args[:upgrade], extra_params: {}).call
      subscription
    # ADD TESTS, ADD SERVICE WITH THE MULTIPLE THINGS THAT NEEDS TO DO
    rescue ActiveRecord::RecordInvalid => e
      GraphQL::ExecutionError.new("Invalid input: #{e.record.errors.full_messages.join(', ')}")
    end
  end
end