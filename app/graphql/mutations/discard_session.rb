module Mutations
  class DiscardSession < BaseMutation

    argument :program_id, ID, required: true
    argument :session_id, ID, required: true
    argument :discard_reason, String, required: true
    
    type Types::SessionExecutionType

    def resolve(args)
      DiscardSessionExecution.new(session_id: args[:session_id], program_id: args[:program_id], 
                                  user: context[:current_user], discard_reason: args[:discard_reason] ).call
      rescue ActiveRecord::RecordInvalid => e
        GraphQL::ExecutionError.new("Invalid input: #{e.record.errors.full_messages.join(', ')}")
      rescue StandardError, CustomError, UserReadableError => e
        GraphQL::ExecutionError.new(e.message)
    end
  end
end
