module Mutations
  class UpdatePassword < BaseMutation
    argument :email, String, required: true
    argument :code, Integer, required: false
    argument :password, String, required: true
    argument :old_password, String, required: false

    type Types::UserType

    def resolve(args)
      return UpdatePasswordService.new(email: args[:email], code: args[:code], password: args[:password], old_password: args[:old_password]).call
    rescue ActiveRecord::RecordInvalid => e
      GraphQL::ExecutionError.new("Invalid input: #{e.record.errors.full_messages.join(', ')}")
    rescue StandardError, UserReadableError => e
      GraphQL::ExecutionError.new(e.message)
    end
  end

end
