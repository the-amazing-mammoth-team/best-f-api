
module Mutations
  class SaveSessionExecution < BaseMutation
    # arguments passed to the `resolve` method
    argument :session_execution, Inputs::SessionExecutionAttributes, required: true
    # return type from the mutation
    type Types::SessionExecutionType

    def resolve(args)
      user = context[:current_user]
      #Guard.can?(user, 'save_workout_session', subscription, subscription.user_id)
      SaveSessionExecutionService.new(session_execution_params: args, user: user).call
    rescue StandardError => e
      GraphQL::ExecutionError.new(e.message)
    end
  end
end