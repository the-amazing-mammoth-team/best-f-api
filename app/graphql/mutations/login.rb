module Mutations
  class Login < BaseMutation
    argument :email, String, required: false
    argument :password, String, required: false
    argument :facebook_uid, String, required: false
    argument :google_uid, String, required: false
    argument :apple_id_token, String, required: false

    
    type Types::UserType

    def resolve(args)
      User.authenticate(args)
    rescue ActiveRecord::RecordInvalid => e
      GraphQL::ExecutionError.new("Invalid input: #{e.record.errors.full_messages.join(', ')}")
    rescue StandardError, CustomError, UserReadableError => e
      #Utility.trigger_rollbar(e)
      GraphQL::ExecutionError.new(e.message)
    end
  end

end
