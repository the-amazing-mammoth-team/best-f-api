
module Mutations
  class SetCurrentUserProgram < BaseMutation
    # arguments passed to the `resolve` method
    argument :program_id, ID, required: true
    # return type from the mutation
    #type Types::UserProgramType
    type Boolean

    def resolve(args)
      user = context[:current_user]
      #Guard.can?(user, 'save_workout_session', subscription, subscription.user_id)
      user_program = SetUserProgram.new(user: user, program_id: args[:program_id]).call if user && args[:program_id]
      user_program.present?
      # return user_program.program if user_program.present?
      # nil
    rescue StandardError, UserReadableError => e
      GraphQL::ExecutionError.new(e.message)
    end
  end
end