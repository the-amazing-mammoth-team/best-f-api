module Mutations
  class RemoveFriend < BaseMutation
    argument :friend_id, ID, required: true
    
    type Boolean

    def resolve(args)
      user = context[:current_user]
      friendship = Friendship.where(user_id: user.id, friend_id: args[:friend_id]).first
      return friendship.destroy if friendship #this raises if not destroyed
      false
    end

  end

end