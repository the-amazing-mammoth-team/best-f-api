
module Mutations
  class UpdateUserData < BaseMutation
    # arguments passed to the `resolve` method
    argument :user_data, Inputs::UserAttributes, required: true
    argument :implement_ids, [ID], required: false
    # return type from the mutation
    type Types::UserType

    def resolve(args)
      user = context[:current_user]
      UpdateUserDataService.new(user: user, args: args[:user_data], implement_ids: args[:implement_ids]).call
    rescue StandardError, ActiveRecord::RecordInvalid => e
      GraphQL::ExecutionError.new(e.message)
    end
  end
end