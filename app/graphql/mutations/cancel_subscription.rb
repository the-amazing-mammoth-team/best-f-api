module Mutations
  class CancelSubscription < BaseMutation

    argument :cancellation_reason, String, required: false

    type Types::SubscriptionType

     def resolve(args)
      subscription = CancelSubscriptionService.new(user: context[:current_user], cancellation_reason: args[:cancellation_reason]).call
    rescue ActiveRecord::RecordInvalid => e
      GraphQL::ExecutionError.new("Invalid input: #{e.record.errors.full_messages.join(', ')}")
    rescue StandardError, CustomError, UserReadableError => e
      GraphQL::ExecutionError.new(e.message)
    end
  end
end
