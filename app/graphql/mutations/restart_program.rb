
module Mutations
  class RestartProgram < BaseMutation
    argument :user_id, ID, required: true
    type Types::UserType

    def resolve(args)
      user = context[:current_user]
      raise 'not the user' if args[:user_id].to_i != user.id
      SetUserProgram.new(user: user, program_id: user.current_program.id, restart: true).call
      user.reload
    rescue StandardError => e
      GraphQL::ExecutionError.new(e.message)
    end
  end
end