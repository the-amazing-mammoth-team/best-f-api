module Mutations
  class RecoverPassword < BaseMutation
    argument :email, String, required: true
    
    type Boolean

    def resolve(args)
      return {success: RecoverPasswordService.call(email: args[:email])}
    rescue ActiveRecord::RecordInvalid => e
      GraphQL::ExecutionError.new("Invalid input: #{e.record.errors.full_messages.join(', ')}")
    rescue StandardError, UserReadableError => e
      GraphQL::ExecutionError.new(e.message)
    end
  end

end
