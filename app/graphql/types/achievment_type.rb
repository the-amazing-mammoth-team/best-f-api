#  name_en            :string
#  name_es            :string
#  icon_url           :string
#  congratulations_en :string
#  congratulations_es :string
#  description_en     :string
#  description_es     :string
#  achievment_type    :integer
#  points             :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
module Types
  class AchievmentType < Types::BaseObject
    description "achievment"
    field :id, ID, null: true
    field :points, Integer, null: true
    field :name, String, null: true
    field :description, String, null: true
    field :congratulations, String, null: true
    field :icon_url, String, null: true
    field :achievment_type, String, null: true
  end
end