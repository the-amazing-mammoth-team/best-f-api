# here you expose the mutations
module Types
  class MutationType < Types::BaseObject
    field :sign_up, mutation: Mutations::SignUp
    field :login, mutation: Mutations::Login
    field :subscribe, mutation: Mutations::Subscribe
    field :recover_password, mutation: Mutations::RecoverPassword
    field :update_password, mutation: Mutations::UpdatePassword
    field :save_session_execution, mutation: Mutations::SaveSessionExecution
    field :update_user_data, mutation: Mutations::UpdateUserData
    field :set_current_user_program, mutation: Mutations::SetCurrentUserProgram
    field :add_friend, mutation: Mutations::AddFriend
    field :remove_friend, mutation: Mutations::RemoveFriend
    field :cancel_subscription, mutation: Mutations::CancelSubscription
    field :discard_session, mutation: Mutations::DiscardSession
    field :delete_user, mutation: Mutations::DeleteUser
    field :restart_program, mutation: Mutations::RestartProgram
    field :program_feedback, mutation: Mutations::ProgramFeedback
  end
end