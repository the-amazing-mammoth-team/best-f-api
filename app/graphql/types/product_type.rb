#
#  id              :bigint           not null, primary key
#  price           :integer
#  name            :string
#  store_reference :string
#  currency        :integer
#  local           :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null

module Types
  class ProductType < Types::BaseObject
    description "A SUBSCRIPTION PRICE DEFINITION"
    field :id, ID, null: true
    field :price, Float, null: true
    field :name, String, null: true
    field :store_reference, String, null: true
    field :store, String, null: false
    field :currency, String, null: true
    field :local, String, null: true
    field :pro, String, null: false
    field :updated_at, String, null: false
    field :created_at, String, null: false
    field :has_trial, Boolean, null: false
    field :trial_days, Integer, null: false
  end
end
