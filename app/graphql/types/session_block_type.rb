# == Schema Information
#
# Table name: session_blocks
#
#  id            :bigint           not null, primary key
#  session_id    :bigint           not null
#  time_duration :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  order         :integer
#
module Types
  class SessionBlockType < Types::BaseObject
    description "A session block thats part form a session"
    field :id, ID, null: true
    field :session_id, ID, null: true
    field :block_type, String, null: true
    field :order, Integer, null: true
    field :time_duration, Integer, null: true
    field :session_sets, [SessionSetType], null: true

    def block_type
      self.object&.block_type&.humanize
    end
  end
end
