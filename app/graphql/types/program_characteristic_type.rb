module Types
  class ProgramCharacteristicType < Types::BaseObject
    description "A characteristic from program"
    field :id, ID, null: true
    field :value, String, null: true
    field :objective, Boolean, null: true
  end
end
