# == Schema Information
#
# Table name: exercises
#
#  id          :bigint           not null, primary key
#  video       :string
#  reps        :integer
#  time        :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  name        :string
#  description :string
#
module Types
  class ExerciseType < Types::BaseObject
    description "An exercise"
    field :id, ID, null: true
    field :video, String, null: true
    field :reps, Integer, null: true
    field :time, Integer, null: true
    field :name, String, null: true
    field :description, String, null: true
    field :updated_at, String, null: false
    field :created_at, String, null: false
    field :harder_variation, ExerciseType, null: true
    field :easier_variation, ExerciseType, null: true
    field :implement_variation, ExerciseType, null: true
    field :thumbnail, String, null: true
    field :thumbnail_400, String, null: true
    field :deprecated, Boolean, null: true
    field :notes, String, null: true
    field :met_multiplier, Float, null: true
    field :body_parts_focused, [String], null: true

    def video
      user = context[:current_user]
      self.object.video(user&.gender)
    end

    def thumbnail
      user = context[:current_user]
      self.object.thumbnail(user&.gender)
    end

    def thumbnail_400
      user = context[:current_user]
      self.object.thumbnail_400(user&.gender)
    end
  end
end
