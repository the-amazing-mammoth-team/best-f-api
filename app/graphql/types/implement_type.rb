module Types
  class ImplementType < Types::BaseObject
    description "Implements needed for one exercise"
    field :id, ID, null: true
    field :name, String, null: true
    field :image_url, String, null: true
  end
end
