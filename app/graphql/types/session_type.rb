module Types
  class SessionType < Types::BaseObject
    description "A session in the program"
    field :id, ID, null: true
    field :name, String, null: true
    field :session_type, String, null: true
    field :description, String, null: true
    field :updated_at, String, null: false
    field :created_at, String, null: false
    field :image_url, String, null: true
    field :strength, Integer, null: true
    field :endurance, Integer, null: true
    field :tecnique, Integer, null: true
    field :flexibility, Integer, null: true
    field :intensity, Integer, null: true
    field :session_blocks, [SessionBlockType], null: true
    field :implements, [ImplementType], null: true
    field :body_parts_focused, [GraphQL::Types::JSON], null: true
    field :warmups, [SessionType], null: true
    field :warmup, SessionType, null: true
    field :cooldowns, [SessionType], null: true
    field :cooldown, SessionType, null: true
    field :order, Integer, null: true
    field :calories, Integer, null: true
    field :time_duration, Integer, null: true
    field :reps, Integer, null: true
    field :thumbnail, String, null: true
    field :thumbnail_400, String, null: true
  end
end
