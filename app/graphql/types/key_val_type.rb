module Types
  class KeyValType < Types::BaseObject
    description "A key val very useful very nice"
    field :key, String, null: true
    field :value, Integer, null: true
    field :text, String, null: true
  end
end
