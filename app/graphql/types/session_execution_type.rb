module Types
  class SessionExecutionType < Types::BaseObject
    description "A session execution summary with their data"
    field :id, ID, null: true
    field :session, SessionType, null: true
    field :session_id, ID, null: true
    field :program_id, ID, null: true
    field :discarded, Boolean, null: true
    field :discard_reason, String, null: true
    field :updated_at, String, null: true
    field :created_at, String, null: true
    field :user_program, UserProgramType, null: true
    field :enjoyment_feedback, Integer, null: true
    field :difficulty_feedback, Integer, null: true
    field :feedback_comment, String, null: true
    field :imported, Boolean, null: true
  end
end