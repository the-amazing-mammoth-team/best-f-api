module Types
  class ProgramType < Types::BaseObject
    description "A training program"
    field :id, ID, null: true
    field :user_id, ID, null: true
    field :name, String, null: true
    field :code_name, String, null: true
    field :description, String, null: true
    field :pro, String, null: false
    field :updated_at, String, null: false
    field :created_at, String, null: false
    field :sessions, [Types::SessionType], null: true
    field :user, Types::UserType, null: false
    field :products, [Types::ProductType], null: true
    field :image_url, String, null: true
    field :image2_url, String, null: true
    field :program_characteristics, [Types::ProgramCharacteristicType], null: true
    field :implements, [Types::ImplementType], null: true
    field :strength, Integer, null: true
    field :endurance, Integer, null: true
    field :technique, Integer, null: true
    field :flexibility, Integer, null: true
    field :intensity, Integer, null: true
    field :max_attribute, KeyValType, null: true
    field :max_attribute2, KeyValType, null: true
    field :warmup_exercises, [Types::ExerciseType], null: true
    field :exercises, [Types::ExerciseType], null: true
    field :priority_order, Integer, null: true
  end
end
