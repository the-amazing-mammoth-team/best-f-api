#  id                             :bigint           not null, primary key
#  session_execution_id           :bigint           not null
#  total_reps                     :integer
#  total_time                     :integer
#  reps_per_min                   :integer
#  total_kcal                     :integer
#  reps_per_exercise              :jsonb
#  mins_per_exercise              :jsonb
#  reps_per_min_per_exercise      :jsonb
#  created_at                     :datetime         not null
#  updated_at                     :datetime         not null
#  reps_set_per_block             :jsonb
#  time_set_per_block             :jsonb
#  average_reps_min_set_per_block :jsonb
#  reps_min_set_block             :jsonb

module Types
  class SessionExecutionSummaryType < Types::BaseObject
    description "A session execution summary with their data"
    field :id, ID, null: true
    field :session_execution_id, ID, null: true
    field :total_reps                     , Integer, null: true
    field :total_time                     , Integer, null: true
    field :reps_per_min                   , Integer, null: true
    field :total_kcal                     , Integer, null: true
    field :reps_per_exercise              , GraphQL::Types::JSON, null: true
    field :mins_per_exercise              , GraphQL::Types::JSON, null: true
    field :reps_per_min_per_exercise      , GraphQL::Types::JSON, null: true
    field :created_at                     , String, null: false         
    field :updated_at                     , String, null: false         
    field :reps_set_per_block             , GraphQL::Types::JSON, null: true
    field :time_set_per_block             , GraphQL::Types::JSON, null: true
    field :average_reps_min_set_per_block , GraphQL::Types::JSON, null: true
    field :reps_min_set_block             , GraphQL::Types::JSON, null: true
    
    field :effort                         , Integer, null: true
    field :points                         , Integer, null: true
    field :value_of_session               , Integer, null: true
    field :body_parts_spider              , GraphQL::Types::JSON, null: true 
    
    field :historic_reps_per_min          , GraphQL::Types::JSON, null: true
    field :friends_reps_per_min           , GraphQL::Types::JSON, null: true

    field :session_execution              , SessionExecutionType, null: :true
    field :name                           , String, null: :true
  end
end
