module Types
  class UserProgramType < Types::BaseObject
    description "Contains info about the user progress in a program" 
    field :id, ID, null: true
    field :program, ProgramType, null: true
    field :progress, Float, null: true
    field :active, Boolean, null: true
    field :completed, Boolean, null: true
    field :program_sessions_count, Integer, null: true
    field :executed_sessions_count, Integer, null: true
    field :executed_sessions, [SessionType], null: true
    field :session_executions, [SessionExecutionType], null: true
    field :enjoyment, String, null: true
    field :enjoyment_notes, String, null: true
  end
end