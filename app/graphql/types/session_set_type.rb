# == Schema Information
#
# Table name: session_sets
#
#  id               :bigint           not null, primary key
#  session_id       :bigint           not null
#  order            :integer
#  level            :integer
#  time_duration    :integer
#  reps             :integer
#  session_set_type :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

module Types
  class SessionSetType < Types::BaseObject
    description "A session set in the program"
    field :id, ID, null: true
    field :session_block_id, ID, null: true
    field :order, Integer, null: true
    field :level, Integer, null: true
    field :time_duration, Integer, null: true
    field :reps, Integer, null: true
    field :session_set_type, String, null: true
    field :session_block, [SessionBlockType], null: true
    field :exercises, [ExerciseType], null: true
    field :exercise_sets, [ExerciseSetType], null: true

    def session_set_type
      self.object&.session_set_type&.humanize
    end
  end
end
