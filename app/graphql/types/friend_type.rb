module Types
  class FriendType < Types::BaseObject
    description "A user as a friend"
    field :id, ID, null: true
    field :email, String, null: true
    field :names, String, null: true
  end
end