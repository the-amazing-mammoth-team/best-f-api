module Types
  class QueryType < Types::BaseObject
    # Add root-level fields here.
    # They will be entry points for queries on your schema.
    field :search_friend, FriendType, null: true do
      description 'find user by email' 
      argument :email, String, required: true
    end

    field :user, UserType, null: false do
      description "Find user by ID"
      argument :id, ID, required: true
      argument :locale, String, required: false, default_value: 'es'
    end

    field :all_users, [UserType], null: true do
      description "Find all users"
    end

    field :suggested_programs, [ProgramType], null: true do
      description "sends the suggested program for the user as first and then other 2"
      argument :user_id, ID, required: true
      argument :locale, String , required: true
    end

    field :available_programs, [ProgramType], null: true do
      description "gets all available the programs"
      argument :locale, String , required: true
    end

    field :get_session, SessionType, null: true do
      description "gets an specific session"
      argument :locale, String, required: true
      argument :session_id, ID, required: true
    end

    field :get_alternative_exercises, [ExerciseType], null: true do
      description "gets the alternative exercises" 
      argument :locale, String, required: true
      argument :exercise_id, ID, required: true
    end

    field :get_session_execution_summary, SessionExecutionSummaryType, null: true do
      description "gets the summary from an execution of a session" 
      argument :session_execution_id, ID, required: true
      argument :locale, String, required: false, default_value: 'es'
    end

    field :get_user_data, UserType, null: true do
      description "reloads current_user data"
      argument :locale, String, required: false, default_value: 'es'
    end

    field :get_program_data, ProgramType, null: true do
      description 'gets a program data'
      argument :id, ID, required: true
    end

    field :get_exercise_data, ExerciseType, null: true do
      description 'gets an exercise data'
      argument :id, ID, required: true
      argument :locale, String, required: false, default_value: 'es'
    end

    field :products, [ProductType], null: true do
      description 'gets an exercise data'
      argument :offer_codes, [String], required: true
      argument :device, String, required: true
    end

    field :implements, [ImplementType], null: true do
      description 'gets the list of available sport equipment'
      argument :locale, String, required: true
    end

    field :search_exercise, [ExerciseType], null: true do
      description 'searches for an exercise'
      argument :name, String, required: true
      argument :locale, String, required: true
    end

    field :get_executions, [SessionExecutionType], null: true do
      description 'searches for an session execution'
      argument :from, GraphQL::Types::ISO8601DateTime, required: true
      argument :to, GraphQL::Types::ISO8601DateTime, required: true
    end

    def search_exercise(locale:, name:)
      I18n.locale = locale
      return Exercise.where("lower(name_en) LIKE ? AND deprecated IS NOT TRUE","%#{name.downcase}%") if locale == 'en'
      return Exercise.where("lower(name_es) LIKE ? AND deprecated IS NOT TRUE","%#{name.downcase}%") if locale == 'es'
      nil
    end

    def get_alternative_exercises(locale:, exercise_id:)
      I18n.locale = locale
      exercise = Exercise.find(exercise_id)
      exercise.alternative_exercises
    end

    def get_session(locale:, session_id:)
      I18n.locale = locale
      Session.find(session_id)
    end

    def available_programs(locale:)
      I18n.locale = locale
      Program.where(available: true)
    end

    def suggested_programs(user_id:, locale:)
      I18n.locale = locale
      SuggestedProgramsService.call(user_id: user_id, locale: locale, onboarding_params: nil)
    end

    def user(id:, locale:)
      I18n.locale = locale
      User.find(id)
    end

    def all_users
      User.all
    end

    def search_friend(email:)
      User.find_by(email: email)
    end

    def get_session_execution_summary(session_execution_id:, locale: )
      I18n.locale = locale
      SessionExecutionSummary.find_by(session_execution_id: session_execution_id)
    end

    def get_user_data(locale: )
      I18n.locale = locale || context[:current_user].language
      context[:current_user]
    end

    def get_program_data(id:)
      Program.find_by(id: id)
    end

    def get_exercise_data(id:, locale: )
      I18n.locale = locale
      Exercise.find_by(id: id)
    end

    def get_executions(from:, to:)
      # from = start_date #Date.new(start_date)
      # to = end_date#Date.new(end_date)
      user = context[:current_user]
      UserProgram.user_executions(from: from, to: to, user: user)
    end

    def products(offer_codes: , device:)
      # 1y_5999, 1y_7999, 3m_4999, 1y_7999_ft_14
      # for tests we will use offer_codes = ['1y_7999t', '3m_4999t', '1y_7999_trial_test']
      offers = Offer.where(offer_code: offer_codes)
      return nil if offers.empty?
      products = []
      offers.each { |offer| products << offer.products&.where(store: device).first }
      products
    end

    def implements(locale:)
      I18n.locale = locale
      Implement.all
    end

  end
end
