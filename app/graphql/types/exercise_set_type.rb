# == Schema Information
#
# Table name: exercise_sets
#
#  id                    :bigint           not null, primary key
#  session_set_id        :bigint           not null
#  exercise_id           :bigint           not null
#  order                 :integer
#  intensity_modificator :float
#  time_duration         :integer
#  reps                  :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#
module Types
  class ExerciseSetType < Types::BaseObject
    description "An exercise"
    field :id, ID, null: true
    field :session_set_id, ID, null: true
    field :exercise, ExerciseType, null: true
    field :order, Integer, null: true
    field :time_duration, Integer, null: true
    field :reps, Integer, null: true
    field :track_reps, Boolean, null: false
    field :updated_at, String, null: false
    field :created_at, String, null: false
  end
end
