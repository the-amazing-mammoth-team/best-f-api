#
# Table name: subscriptions
#
#  id                :bigint           not null, primary key
#  user_id           :bigint           not null
#  product_id        :bigint           not null
#  program_id        :bigint           not null
#  platform          :integer
#  transaction_body  :text
#  status            :integer
#  start_date        :datetime
#  end_date          :datetime
#  subscription_type :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

module Types
  class SubscriptionType < Types::BaseObject
    description "A subscription from an user"
    field :id, ID, null: true
    field :user, Types::UserType, null: false
    field :product, Types::ProductType, null: false
    field :program, Types::ProgramType, null: false
    field :transaction_body, String, null: true
    field :status, String, null: true
    field :start_date, String, null: true
    field :end_date , String, null: true
    field :subscription_type, String, null: true
    field :updated_at, String, null: false
    field :created_at, String, null: false
    field :upgrade_options, [ProductType], null: true
    field :cancelled, Boolean, null: false
    field :cancelled_at, String, null: true
    field :cancellation_reason, String, null: true
  end
end
