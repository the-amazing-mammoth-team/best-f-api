class UserMailer < ApplicationMailer

  def recover_password(user)
    @user = user
    mail(to: @user.email, subject: 'update your user password')
  end

  def importer_success(user, importer_name, extra= nil)
    @user = user
    @importer_name = importer_name
    @extra = extra
    mail(to: @user.email, subject: "import process #{importer_name} has finished without errors")
  end

  def importer_error(user, importer_name, errors, row_data)
    @user = user
    @importer_name = importer_name
    @errors = errors
    @row_data = row_data
    mail(to: @user.email, subject: "import process #{importer_name} has failed")
  end
end
