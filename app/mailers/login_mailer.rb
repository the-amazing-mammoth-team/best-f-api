class LoginMailer < ApplicationMailer
  def magic_login_email(user)
    @magic_login_url = magic_logins_url(token: user.login_token)
    puts @magic_login_url
    mail(to: user.email, subject: "Here's your login link")
  end
end