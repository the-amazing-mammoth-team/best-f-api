class ApplicationMailer < ActionMailer::Base
  default from: 'help@mhunters.com'
  layout 'mailer'
end
