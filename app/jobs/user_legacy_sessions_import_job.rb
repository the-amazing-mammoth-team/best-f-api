class UserLegacySessionsImportJob < ApplicationJob
  queue_as :default

  def perform(email, data)
    UserLegacySessions.new(email, data).call
  end
end