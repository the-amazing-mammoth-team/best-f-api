class CreateSessionExecutionSummaryJob < ApplicationJob
  queue_as :critical
  #urgency :high

  def perform(session_execution)
    ProcessSessionExecution.new(session_execution: session_execution).call
  end
end
