class SendMixPanelDataJob < ApplicationJob
  queue_as :default

  def perform(user)
    Analytics::CreateMixPanelUser.new(user: user).call
  end
end
