class ImportExercisesJob < ApplicationJob
  queue_as :default

  def perform(csv_file, user)
    csv_file = csv_file.doc_file.download
    ImportExercises.new(csv_file: csv_file, user: user).call
  end
end
