class RecoverPasswordJob < ApplicationJob
  queue_as :default

  def perform(user)
    UserMailer.recover_password(user).deliver_later
  end
end
