class ImportSessionJob < ApplicationJob
  queue_as :default

  def perform(csv_file:, user:, session_attributes:, program_id:, session_update_id:)
    csv_file = csv_file.doc_file.download
    ImportSession.new(csv_file: csv_file, user: user, session_attributes: session_attributes, program_id: program_id, session_update_id: session_update_id).call
  end
end
