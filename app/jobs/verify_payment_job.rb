#this works also for cancellations check
class VerifyPaymentJob < ApplicationJob
  queue_as :default

  def perform(user:)
    subscription = user.current_subscription

    return if subscription.blank? || subscription.product.blank?

    Payments::AppStore.new(subscription: subscription).call if subscription.product.store == 'apple'
    Payments::PlayStore.new(subscription: subscription).call if subscription.product.store == 'google_play'
    Payments::StripeStore.new(subscription: subscription).check_cancellation if subscription.product.store == 'stripe'
  end
end
