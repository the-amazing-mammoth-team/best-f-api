class SendEventAnalyticsJob < ApplicationJob
  queue_as :default

  def perform(user, event_data)
    Analytics::SendMixPanelEvent.new(user: user, event: 'Signup completed').call if event_data == 'Signup completed'
    Analytics::SendMixPanelEvent.new(user: user, event: 'Subscribe').call if event_data == 'Subscribe'
  end

end