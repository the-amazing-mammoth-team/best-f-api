class AddThumbnailsToExercise < ActiveRecord::Migration[6.0]
  def change
    add_column :exercises, :thumbnail, :string
    add_column :exercises, :thumbnail_male, :string
    add_column :exercises, :thumbnail_female, :string
  end
end
