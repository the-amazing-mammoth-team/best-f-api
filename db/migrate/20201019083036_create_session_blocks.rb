class CreateSessionBlocks < ActiveRecord::Migration[6.0]
  def change
    create_table :session_blocks do |t|
      t.references :session, null: false, foreign_key: true
      t.integer :time_duration

      t.timestamps
    end
  end
end
