class AddDescriptionTranslationToProgram < ActiveRecord::Migration[6.0]
  def change
    reversible do |dir|
      dir.up do
        Program.add_translation_fields! :description => :text
      end

      dir.down do
        remove_column :program_translations, :description
      end
    end
  end 
end
