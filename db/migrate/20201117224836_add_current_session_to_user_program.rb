class AddCurrentSessionToUserProgram < ActiveRecord::Migration[6.0]
  def change
    add_reference :user_programs, :current_session, null: true, foreign_key: {to_table: :sessions}
    add_column :user_programs, :completed, :boolean, default: false
  end
end
