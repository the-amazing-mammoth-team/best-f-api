class CreateProgramImplements < ActiveRecord::Migration[6.0]
  def change
    create_table :program_implements do |t|
      t.references :program, null: false, foreign_key: true
      t.references :implement, null: false, foreign_key: true

      t.timestamps
    end
  end
end
