class AddBiggerThumbnailsToExercise < ActiveRecord::Migration[6.0]
  def change
    add_column :exercises, :thumbnail_400, :string
    add_column :exercises, :thumbnail_400_male, :string
    add_column :exercises, :thumbnail_400_female, :string
  end
end
