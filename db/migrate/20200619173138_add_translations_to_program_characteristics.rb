class AddTranslationsToProgramCharacteristics < ActiveRecord::Migration[6.0]
   def change
    reversible do |dir|
      dir.up do
        ProgramCharacteristic.create_translation_table! :value => :string
      end

      dir.down do
        ProgramCharacteristic.drop_translation_table!
      end
    end
  end
end
