class AddStreaksToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :best_weekly_streak, :integer, default: 0
    add_column :users, :current_weekly_streak, :integer, default: 0
  end
end
