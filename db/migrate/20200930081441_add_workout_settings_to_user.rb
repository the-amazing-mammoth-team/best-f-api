class AddWorkoutSettingsToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :workout_setting_voice_coach, :boolean, default: :true
    add_column :users, :workout_setting_sound, :boolean, default: :true
    add_column :users, :workout_setting_vibration, :boolean, default: :true
    add_column :users, :workout_setting_mobility, :boolean, default: :true
    add_column :users, :workout_setting_cardio_warmup, :boolean, default: :true
    add_column :users, :workout_setting_countdown, :boolean, default: :true
    add_column :users, :notifications_setting, :boolean, default: :true
    add_column :users, :training_days_setting, :integer, default: 1
  end
end
