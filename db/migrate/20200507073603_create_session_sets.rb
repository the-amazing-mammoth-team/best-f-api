class CreateSessionSets < ActiveRecord::Migration[6.0]
  def change
    create_table :session_sets do |t|
      t.references :session, null: false, foreign_key: true
      t.integer :order
      t.integer :level
      t.integer :time_duration
      t.integer :reps
      t.integer :session_set_type

      t.timestamps
    end
  end
end
