class CreateSessionHistoricSummaries < ActiveRecord::Migration[6.0]
  def change
    create_table :session_historic_summaries do |t|
      t.references :user, null: false, foreign_key: true
      t.references :session, null: false, foreign_key: true
      t.jsonb :reps_per_min
      t.jsonb :friends_reps_per_min

      t.timestamps
    end
  end
end
