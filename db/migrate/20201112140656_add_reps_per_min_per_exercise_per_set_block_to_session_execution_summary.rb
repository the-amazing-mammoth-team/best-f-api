class AddRepsPerMinPerExercisePerSetBlockToSessionExecutionSummary < ActiveRecord::Migration[6.0]
  def change
    add_column :session_execution_summaries, :reps_min_set_block, :jsonb
  end
end
