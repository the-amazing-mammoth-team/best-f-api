class AddTracoFieldsToSession < ActiveRecord::Migration[6.0]
  def change
    add_column :sessions, :name_en, :string
    add_column :sessions, :name_es, :string
    add_column :sessions, :description_en, :string
    add_column :sessions, :description_es, :string
  end
end
