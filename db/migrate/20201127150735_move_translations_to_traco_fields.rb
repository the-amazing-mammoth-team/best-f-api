class MoveTranslationsToTracoFields < ActiveRecord::Migration[6.0]
  def change
    ActiveRecord::Base.transaction do
      Program.all.each do |program|
        es_translation = get_translation(program.id, 'es')
        en_translation = get_translation(program.id, 'en')
        program.name_es = es_translation['name'] if es_translation
        program.description_es = es_translation['description'] if es_translation
        program.name_en = en_translation['name'] if en_translation
        program.description_en = en_translation['description'] if en_translation
        program.save!
      end
    end
    remove_column(:programs, :name)
    remove_column(:programs, :description)
  end

  def get_translation(id, locale)
    conn = ActiveRecord::Base.connection
    @page_records = []
    result = conn.execute("SELECT * FROM program_translations where program_id = #{id} and locale = '#{locale}' ")
    result.first if result
  end
end
