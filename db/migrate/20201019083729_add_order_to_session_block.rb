class AddOrderToSessionBlock < ActiveRecord::Migration[6.0]
  def change
    add_column :session_blocks, :order, :integer
  end
end
