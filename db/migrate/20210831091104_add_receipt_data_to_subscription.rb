class AddReceiptDataToSubscription < ActiveRecord::Migration[6.0]
  def change
    add_column :subscriptions, :receipt_data, :jsonb
  end
end
