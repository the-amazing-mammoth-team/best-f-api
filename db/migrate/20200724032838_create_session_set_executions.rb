class CreateSessionSetExecutions < ActiveRecord::Migration[6.0]
  def change
    create_table :session_set_executions do |t|
      t.references :session_execution, null: false, foreign_key: true
      t.integer :reps_executed
      t.integer :execution_time
      t.integer :order

      t.timestamps
    end
  end
end
