class AddWarmupIdToSession < ActiveRecord::Migration[6.0]
  def change
    add_reference :sessions, :warmup, null: true, foreign_key: {to_table: :sessions}
    add_reference :sessions, :cooldown, null: true, foreign_key: {to_table: :sessions}
  end
end
