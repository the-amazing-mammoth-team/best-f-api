class CreateExerciseExecutions < ActiveRecord::Migration[6.0]
  def change
    create_table :exercise_executions do |t|
      t.references :exercise, null: false, foreign_key: true
      t.references :session_set_execution, null: false, foreign_key: true
      t.integer :reps_executed
      t.integer :execution_time
      t.integer :order

      t.timestamps
    end
  end
end
