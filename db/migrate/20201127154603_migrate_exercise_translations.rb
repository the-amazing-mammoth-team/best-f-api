class MigrateExerciseTranslations < ActiveRecord::Migration[6.0]
  def change
    ActiveRecord::Base.transaction do
      Exercise.all.each do |exercise|
        es_translation = get_translation(exercise.id, 'es')
        en_translation = get_translation(exercise.id, 'en')
        exercise.name_es = es_translation['name'] if es_translation
        exercise.description_es = es_translation['description'] if es_translation
        exercise.name_en = en_translation['name'] if en_translation
        exercise.description_en = en_translation['description'] if en_translation
        exercise.save!
      end
    end
    remove_column(:exercises, :name)
    remove_column(:exercises, :description)
  end

  def get_translation(id, locale)
    conn = ActiveRecord::Base.connection
    @page_records = []
    result = conn.execute("SELECT * FROM exercise_translations where exercise_id = #{id} and locale = '#{locale}' ")
    result.first if result
  end
end
