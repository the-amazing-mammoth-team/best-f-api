class AddAppleIdTokenToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :apple_id_token, :string
  end
end
