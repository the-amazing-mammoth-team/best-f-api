class AddRecoverPasswordCodeToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :recover_password_code, :integer
    add_column :users, :recover_password_attempts, :integer, default: 0
  end
end
