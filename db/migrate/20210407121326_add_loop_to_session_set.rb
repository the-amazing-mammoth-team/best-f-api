class AddLoopToSessionSet < ActiveRecord::Migration[6.0]
  def change
    add_column :session_sets, :loop, :boolean, default: false
  end
end
