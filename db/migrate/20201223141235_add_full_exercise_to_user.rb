class AddFullExerciseToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :t1_full_exercise, :integer, default: 0
  end
end
