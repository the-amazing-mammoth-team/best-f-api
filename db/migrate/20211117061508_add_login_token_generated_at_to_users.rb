class AddLoginTokenGeneratedAtToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :login_token_generated_at, :datetime
  end
end
