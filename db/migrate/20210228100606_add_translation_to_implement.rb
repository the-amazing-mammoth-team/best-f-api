class AddTranslationToImplement < ActiveRecord::Migration[6.0]
  def change
    add_column :implements, :name_en, :string
    add_column :implements, :name_es, :string
  end
end
