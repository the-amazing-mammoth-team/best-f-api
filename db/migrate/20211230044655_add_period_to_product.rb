class AddPeriodToProduct < ActiveRecord::Migration[6.0]
  def change
    add_column :products, :period, :integer, default: 0
  end
end
