class UpdateToFloatProductPrice < ActiveRecord::Migration[6.0]
  def up
    change_column :products, :price, :decimal
  end

  def down
    change_column :products, :price, :integer
  end
end
