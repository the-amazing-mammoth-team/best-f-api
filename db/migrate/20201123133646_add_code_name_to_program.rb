class AddCodeNameToProgram < ActiveRecord::Migration[6.0]
  def change
    add_column :programs, :code_name, :string
  end
end
