class AddDailyReport < ActiveRecord::Migration[6.0]
  def change
    create_table :daily_reports do |t|
      t.jsonb :report_data
      t.datetime :report_date
      t.timestamps
    end
  end
end
