class AddCodeNameToProductOffer < ActiveRecord::Migration[6.0]
  def change
    add_column :product_offers, :code_name, :integer
  end
end
