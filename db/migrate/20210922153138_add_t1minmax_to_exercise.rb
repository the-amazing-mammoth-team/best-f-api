class AddT1minmaxToExercise < ActiveRecord::Migration[6.0]
  def change
    add_column :exercises, :t1_min, :integer, default: 0
    add_column :exercises, :t1_max, :integer, default: 1000
  end
end
