class AddNameToSessionExecutionSummary < ActiveRecord::Migration[6.0]
  def change
    add_column :session_execution_summaries, :name, :string
  end
end
