class AddTranslationToSessionAndProgram < ActiveRecord::Migration[6.0]
  def change
    reversible do |dir|
      dir.up do
        Session.create_translation_table! :name => :string
        Program.create_translation_table! :name => :string
      end

      dir.down do
        Session.drop_translation_table!
        Program.drop_translation_table!
      end
    end
  end
end
