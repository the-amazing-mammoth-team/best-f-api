class RenameLegacyId < ActiveRecord::Migration[6.0]
  def change
    rename_column :exercises, :legacy_program_id, :legacy_id
    add_index :exercises, :legacy_id, unique: true
  end
end
