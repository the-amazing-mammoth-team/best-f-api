class CreateProfiles < ActiveRecord::Migration[6.0]
  def change
    create_table :profiles do |t|
      t.integer :gender
      t.integer :activity_level
      t.integer :goal
      t.float :fat_level
      t.string :name

      t.timestamps
    end
  end
end
