class CreateSubscriptions < ActiveRecord::Migration[6.0]
  def change
    create_table :subscriptions do |t|
      t.references :user, null: false, foreign_key: true
      t.references :product, null: false, foreign_key: true
      t.references :program, null: false, foreign_key: true
      t.integer :platform
      t.text :transaction_body
      t.integer :status
      t.datetime :start_date
      t.datetime :end_date
      t.integer :subscription_type

      t.timestamps
    end
  end
end
