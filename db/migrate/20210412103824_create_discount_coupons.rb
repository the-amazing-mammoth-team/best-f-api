class CreateDiscountCoupons < ActiveRecord::Migration[6.0]
  def change
    create_table :discount_coupons do |t|
      t.string :stripe_id
      t.references :product, null: false, foreign_key: true
      t.jsonb :stripe_body
      t.decimal :discount_percentage
      t.boolean :discount_forever

      t.timestamps
    end
  end
end
