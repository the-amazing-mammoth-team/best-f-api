class CreateProgramProfiles < ActiveRecord::Migration[6.0]
  def change
    create_table :program_profiles do |t|
      t.references :program, null: false, foreign_key: true
      t.references :profile, null: false, foreign_key: true

      t.timestamps
    end
  end
end
