class RemoveSessionExecutionFromSessionSetExecution < ActiveRecord::Migration[6.0]
  def change
    remove_column(:session_set_executions, :session_execution_id)
  end
end
