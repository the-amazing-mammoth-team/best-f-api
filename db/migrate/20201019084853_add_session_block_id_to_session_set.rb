class AddSessionBlockIdToSessionSet < ActiveRecord::Migration[6.0]
  def change
    add_reference :session_sets, :session_block, null: false, foreign_key: true
  end
end
