class AddAffiliateCodeToSubscription < ActiveRecord::Migration[6.0]
  def change
    add_column :subscriptions, :affiliate_code, :string
    add_column :subscriptions, :offer_code, :string
  end
end
