class AddCancelAttributesToSubscription < ActiveRecord::Migration[6.0]
  def change
    add_column :subscriptions, :cancelled_at, :date
    add_column :subscriptions, :cancelled, :boolean, default: false
  end
end
