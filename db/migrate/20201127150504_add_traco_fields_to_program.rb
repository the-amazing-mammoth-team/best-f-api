class AddTracoFieldsToProgram < ActiveRecord::Migration[6.0]
  def change
    add_column :programs, :name_es, :string
    add_column :programs, :name_en, :string
    add_column :programs, :description_en, :string
    add_column :programs, :description_es, :string
  end
end
