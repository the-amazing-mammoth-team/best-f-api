class CreateExerciseSets < ActiveRecord::Migration[6.0]
  def change
    create_table :exercise_sets do |t|
      t.references :session_set, null: false, foreign_key: true
      t.references :exercise, null: false, foreign_key: true
      t.integer :order
      t.float :intensity_modificator
      t.integer :time_duration
      t.integer :reps

      t.timestamps
    end
  end
end
