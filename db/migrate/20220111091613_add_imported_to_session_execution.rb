class AddImportedToSessionExecution < ActiveRecord::Migration[6.0]
  def change
    add_column :session_executions, :imported, :boolean, default: false
  end
end
