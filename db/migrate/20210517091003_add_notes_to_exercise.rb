class AddNotesToExercise < ActiveRecord::Migration[6.0]
  def change
    add_column :exercises, :notes_en, :string
    add_column :exercises, :notes_es, :string
  end
end
