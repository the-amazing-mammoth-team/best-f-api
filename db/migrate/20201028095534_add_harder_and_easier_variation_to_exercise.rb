class AddHarderAndEasierVariationToExercise < ActiveRecord::Migration[6.0]
  def change
    add_reference :exercises, :harder_variation, null: true, foreign_key: {to_table: :exercises}
    add_reference :exercises, :easier_variation, null: true, foreign_key: {to_table: :exercises}
  end
end
