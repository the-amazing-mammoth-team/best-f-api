class AddActiveToUserProgram < ActiveRecord::Migration[6.0]
  def change
    add_column :user_programs, :active, :boolean, default: true
  end
end
