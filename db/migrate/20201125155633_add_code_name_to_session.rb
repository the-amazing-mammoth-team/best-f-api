class AddCodeNameToSession < ActiveRecord::Migration[6.0]
  def change
    add_column :sessions, :code_name, :string
  end
end
