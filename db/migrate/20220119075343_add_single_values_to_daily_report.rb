class AddSingleValuesToDailyReport < ActiveRecord::Migration[6.0]
  def change
    add_column :daily_reports, :users_created, :integer
    add_column :daily_reports, :subscriptions_activated, :integer
    add_column :daily_reports, :workouts_completed, :integer
    add_column :daily_reports, :workouts_discarded, :integer
    add_column :daily_reports, :subscriptions_money, :decimal
  end
end
