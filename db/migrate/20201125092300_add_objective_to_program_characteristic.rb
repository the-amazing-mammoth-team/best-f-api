class AddObjectiveToProgramCharacteristic < ActiveRecord::Migration[6.0]
  def change
    add_column :program_characteristics, :objective, :boolean, default: :false
  end
end
