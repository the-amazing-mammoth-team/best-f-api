class AddTestAttributesToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :t1_push, :integer, default: 0
    add_column :users, :t1_core, :integer, default: 0
    add_column :users, :t1_legs, :integer, default: 0
    add_column :users, :t1_full, :integer, default: 0
    add_column :users, :t1_push_exercise, :integer, default: 0
    add_column :users, :t1_pull_up, :integer, default: 0
    add_column :users, :t2_reps, :integer, default: 0
    add_column :users, :t2_steps, :integer, default: 0
    add_column :users, :t2_reps_push, :integer, default: 0
    add_column :users, :t2_reps_core, :integer, default: 0
    add_column :users, :t2_reps_legs, :integer, default: 0
    add_column :users, :t2_reps_full, :integer, default: 0
    add_column :users, :t2_time_push, :integer, default: 0
    add_column :users, :t2_time_core, :integer, default: 0
    add_column :users, :t2_time_legs, :integer, default: 0
    add_column :users, :t2_time_full, :integer, default: 0
  end
end
