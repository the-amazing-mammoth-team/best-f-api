class AddDiscardDataToSessionExecution < ActiveRecord::Migration[6.0]
  def change
    add_column :session_executions, :discarded, :boolean, default: false
    add_column :session_executions, :discard_reason, :integer
  end
end
