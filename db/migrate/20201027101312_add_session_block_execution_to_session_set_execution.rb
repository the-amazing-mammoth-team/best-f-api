class AddSessionBlockExecutionToSessionSetExecution < ActiveRecord::Migration[6.0]
  def change
    add_reference :session_set_executions, :session_block_execution, null: false, foreign_key: true
  end
end
