class AddScienceDataAgreementToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :scientific_data_usage, :boolean, default: false
  end
end
