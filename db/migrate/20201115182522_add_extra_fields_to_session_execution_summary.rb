class AddExtraFieldsToSessionExecutionSummary < ActiveRecord::Migration[6.0]
  def change
    add_column :session_execution_summaries, :effort, :integer
    add_column :session_execution_summaries, :points, :integer
    add_column :session_execution_summaries, :value_of_session, :integer
    add_column :session_execution_summaries, :body_parts_spider, :jsonb
  end
end
