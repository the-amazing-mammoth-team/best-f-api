class AddStoreMetadataToSubscription < ActiveRecord::Migration[6.0]
  def change
    add_column :subscriptions, :store_metadata, :jsonb
  end
end
