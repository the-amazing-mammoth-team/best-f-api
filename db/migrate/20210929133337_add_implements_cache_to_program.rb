class AddImplementsCacheToProgram < ActiveRecord::Migration[6.0]
  def change
    add_column :programs, :implements, :jsonb
  end
end
