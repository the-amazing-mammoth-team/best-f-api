class AddDiscountsToProduct < ActiveRecord::Migration[6.0]
  def change
    add_column :products, :discount_percentage, :decimal
    add_column :products, :discount_forever, :boolean
  end
end
