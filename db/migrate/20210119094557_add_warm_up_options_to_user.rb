class AddWarmUpOptionsToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :warmup_setting, :boolean
    add_column :users, :warmup_session_id, :integer
  end
end
