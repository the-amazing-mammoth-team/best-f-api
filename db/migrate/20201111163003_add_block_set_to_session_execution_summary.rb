class AddBlockSetToSessionExecutionSummary < ActiveRecord::Migration[6.0]
  def change
    add_column :session_execution_summaries, :reps_set_per_block, :jsonb
    add_column :session_execution_summaries, :time_set_per_block, :jsonb
    add_column :session_execution_summaries, :average_reps_min_set_per_block, :jsonb
  end
end
