class AddImportToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :imported, :boolean, default: false
  end
end
