class CreateSessionBlockExecutions < ActiveRecord::Migration[6.0]
  def change
    create_table :session_block_executions do |t|
      t.references :session_execution, null: false, foreign_key: true
      t.integer :block_type
      t.integer :reps_executed
      t.integer :order
      t.integer :execution_time

      t.timestamps
    end
  end
end
