class AddMaleAndFemaleVideoLinksToExercise < ActiveRecord::Migration[6.0]
  def change
    add_column :exercises, :video_female, :string
    add_column :exercises, :video_male, :string
  end
end
