class CreateSessionExecutions < ActiveRecord::Migration[6.0]
  def change
    create_table :session_executions do |t|
      t.date :scheduled_at
      t.references :user_program, null: false, foreign_key: true
      t.integer :difficulty_feedback
      t.integer :enjoyment_feedback
      t.string :feedback_comment
      t.integer :reps_executed
      t.integer :execution_time
      t.integer :order

      t.timestamps
    end
  end
end
