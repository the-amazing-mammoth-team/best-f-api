class AddNewFieldsToProgram < ActiveRecord::Migration[6.0]
  def change
    add_column :programs, :priority_order, :integer
    add_column :programs, :next_program_id, :integer
  end
end
