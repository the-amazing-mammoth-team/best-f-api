class AddTranslationToSessionDescription < ActiveRecord::Migration[6.0]
  def change
    reversible do |dir|
      dir.up do
        Session.add_translation_fields! :description => :text
      end

      dir.down do
        remove_column :program_translations, :description
      end
    end
  end
end
