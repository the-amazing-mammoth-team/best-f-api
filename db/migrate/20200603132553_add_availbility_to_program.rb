class AddAvailbilityToProgram < ActiveRecord::Migration[6.0]
  def change
    add_column :programs, :available, :boolean, default: false
  end
end
