class AddAffilliateCodeToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :affiliate_code, :string
    add_column :users, :affiliate_code_signup, :string
  end
end
