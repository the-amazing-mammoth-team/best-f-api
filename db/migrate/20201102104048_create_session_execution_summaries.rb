class CreateSessionExecutionSummaries < ActiveRecord::Migration[6.0]
  def change
    create_table :session_execution_summaries do |t|
      t.references :session_execution, null: false, foreign_key: true
      t.integer :total_reps
      t.integer :total_time
      t.integer :reps_per_min 
      t.integer :total_kcal
      t.jsonb :reps_per_exercise
      t.jsonb :mins_per_exercise
      t.jsonb :reps_per_min_per_exercise

      t.timestamps
    end
  end
end
