class AddTracoFieldsToProgramCharacteristic < ActiveRecord::Migration[6.0]
  def change
    add_column :program_characteristics, :value_en, :string
    add_column :program_characteristics, :value_es, :string
  end
end
