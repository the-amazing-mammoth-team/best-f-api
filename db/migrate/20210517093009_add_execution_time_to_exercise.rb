class AddExecutionTimeToExercise < ActiveRecord::Migration[6.0]
  def change
    add_column :exercises, :execution_time, :decimal
  end
end
