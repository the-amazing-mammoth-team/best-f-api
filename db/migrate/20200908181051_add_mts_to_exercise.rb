class AddMtsToExercise < ActiveRecord::Migration[6.0]
  def change
    add_column :exercises, :met_multiplier, :decimal
  end
end
