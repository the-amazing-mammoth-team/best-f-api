class AddSessionHistoricSummarytoSessionExecutionSummary < ActiveRecord::Migration[6.0]
  def change
    add_reference :session_execution_summaries, :historic, null: true, foreign_key: {to_table: :session_historic_summaries}
  end
end
