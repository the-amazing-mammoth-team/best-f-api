class AddStoreToProduct < ActiveRecord::Migration[6.0]
  def change
    add_column :products, :store, :integer
  end
end
