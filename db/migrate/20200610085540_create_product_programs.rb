class CreateProductPrograms < ActiveRecord::Migration[6.0]
  def change
    create_table :product_programs do |t|
      t.references :product, null: false, foreign_key: true
      t.references :program, null: false, foreign_key: true

      t.timestamps
    end
  end
end
