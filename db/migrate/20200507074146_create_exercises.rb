class CreateExercises < ActiveRecord::Migration[6.0]
  def change
    create_table :exercises do |t|
      t.string :name
      t.string :description
      t.string :video
      t.integer :reps
      t.integer :time

      t.timestamps
    end

    reversible do |dir|
      dir.up do
        Exercise.create_translation_table! :name => :string, :description => :string
      end

      dir.down do
        Exercise.drop_translation_table!
      end
    end
  end
end
