class AddLoopToSessionBlock < ActiveRecord::Migration[6.0]
  def change
    add_column :session_blocks, :loop, :boolean, default: false
  end
end
