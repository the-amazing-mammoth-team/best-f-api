class DeleteNameFromImplement < ActiveRecord::Migration[6.0]
  def change
    remove_column :implements, :name
  end
end
