class DeleteHistoricIdFromSessionExecutionSummary < ActiveRecord::Migration[6.0]
  def change
    remove_column(:session_execution_summaries, :historic_id)
  end
end
