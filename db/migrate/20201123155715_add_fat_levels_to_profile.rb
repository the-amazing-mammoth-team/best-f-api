class AddFatLevelsToProfile < ActiveRecord::Migration[6.0]
  def change
    add_column :profiles, :max_fat_level, :float
    add_column :profiles, :min_fat_level, :float
  end
end
