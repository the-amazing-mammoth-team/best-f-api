class CreateDocuments < ActiveRecord::Migration[6.0]
  def change
    create_table :documents do |t|
      t.string :name
      t.string :description
      t.string :associated_mode
      t.references :user

      t.timestamps
    end
  end
end
