class AddAvailabilityToProduct < ActiveRecord::Migration[6.0]
  def change
    add_column :products, :available, :bool, default: false
  end
end
