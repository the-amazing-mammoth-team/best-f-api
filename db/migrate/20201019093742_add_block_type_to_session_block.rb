class AddBlockTypeToSessionBlock < ActiveRecord::Migration[6.0]
  def change
    add_column :session_blocks, :block_type, :integer
  end
end
