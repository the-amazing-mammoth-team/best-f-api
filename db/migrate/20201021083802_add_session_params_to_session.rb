class AddSessionParamsToSession < ActiveRecord::Migration[6.0]
  def up
    add_column :sessions, :strength, :integer, default: 0
    add_column :sessions, :endurance, :integer, default: 0
    add_column :sessions, :technique, :integer, default: 0
    add_column :sessions, :flexibility, :integer, default: 0
    add_column :sessions, :intensity, :integer, default: 0
  end

  def down
    remove_column(:sessions, :strength)
    remove_column(:sessions, :endurance)
    remove_column(:sessions, :technique)
    remove_column(:sessions, :flexibility)
    remove_column(:sessions, :intensity)
  end
end
