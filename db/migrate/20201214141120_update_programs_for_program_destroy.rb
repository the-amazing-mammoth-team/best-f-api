class UpdateProgramsForProgramDestroy < ActiveRecord::Migration[6.0]
  def change
    remove_index :programs, name: "index_programs_on_user_id"
    change_column :programs, :user_id, :bigint, :null => true
  end
end
