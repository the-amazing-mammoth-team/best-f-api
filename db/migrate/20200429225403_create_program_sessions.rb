class CreateProgramSessions < ActiveRecord::Migration[6.0]
  def change
    create_table :program_sessions do |t|
      t.references :program, null: false, foreign_key: true
      t.references :session, null: false, foreign_key: true

      t.timestamps
    end
  end
end
