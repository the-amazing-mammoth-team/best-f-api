class DestroyImplementsCacheInPrograms < ActiveRecord::Migration[6.0]
  def change
    if column_exists? :programs, :implements
      remove_column :programs, :implements
    end
  end
end
