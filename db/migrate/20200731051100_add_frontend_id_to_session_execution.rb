class AddFrontendIdToSessionExecution < ActiveRecord::Migration[6.0]
  def change
    add_column :session_executions, :front_end_id, :uuid
  end
end
