class AddCoachIdToExercise < ActiveRecord::Migration[6.0]
  def change
    add_reference :exercises, :coach, null: true, foreign_key: {to_table: :users}
  end
end
