class AddTrackerIdsToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :moengage_id, :string
    add_column :users, :mix_panel_id, :string
  end
end
