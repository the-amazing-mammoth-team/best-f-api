class AddLegacyProgramIdToExercise < ActiveRecord::Migration[6.0]
  def change
    add_column :exercises, :legacy_program_id, :string, index: true
  end
end
