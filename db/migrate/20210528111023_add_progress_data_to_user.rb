class AddProgressDataToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :total_sessions, :integer
    add_column :users, :total_time, :integer
    add_column :users, :kcal_per_session, :decimal
    add_column :users, :reps_per_session, :integer
  end
end
