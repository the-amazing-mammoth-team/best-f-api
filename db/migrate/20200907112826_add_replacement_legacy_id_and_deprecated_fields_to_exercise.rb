class AddReplacementLegacyIdAndDeprecatedFieldsToExercise < ActiveRecord::Migration[6.0]
  def change
    add_column :exercises, :deprecated, :boolean
    add_column :exercises, :replacement_legacy_id, :integer, unique: true
  end
end
