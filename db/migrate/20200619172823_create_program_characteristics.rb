class CreateProgramCharacteristics < ActiveRecord::Migration[6.0]
  def change
    create_table :program_characteristics do |t|
      t.string :value
      t.references :program, null: false, foreign_key: true

      t.timestamps
    end
  end
end
