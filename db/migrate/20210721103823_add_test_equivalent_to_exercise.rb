class AddTestEquivalentToExercise < ActiveRecord::Migration[6.0]
  def change
    add_reference :exercises, :test_equivalent, null: true, foreign_key: {to_table: :exercises}
  end
end
