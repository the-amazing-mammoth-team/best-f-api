class AddTracoFieldsToExercise < ActiveRecord::Migration[6.0]
  def change
    add_column :exercises, :name_en, :string
    add_column :exercises, :name_es, :string
    add_column :exercises, :description_en, :string
    add_column :exercises, :description_es, :string
  end
end
