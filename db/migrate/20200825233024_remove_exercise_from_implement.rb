class RemoveExerciseFromImplement < ActiveRecord::Migration[6.0]
  def change
    remove_column :implements, :exercise_id
  end
end
