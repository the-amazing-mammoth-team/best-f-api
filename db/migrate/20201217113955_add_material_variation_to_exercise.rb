class AddMaterialVariationToExercise < ActiveRecord::Migration[6.0]
  def change
    add_reference :exercises, :implement_variation, null: true, foreign_key: {to_table: :exercises}
  end
end
