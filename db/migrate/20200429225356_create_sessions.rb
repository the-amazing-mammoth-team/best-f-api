class CreateSessions < ActiveRecord::Migration[6.0]
  def change
    create_table :sessions do |t|
      t.string :name
      t.integer :level
      t.integer :order
      t.integer :session_type
      t.integer :time_duration
      t.integer :reps

      t.timestamps
    end
  end
end
