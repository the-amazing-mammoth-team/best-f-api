class AddHasTrialAndTrialDaysToProduct < ActiveRecord::Migration[6.0]
  def change
    add_column :products, :has_trial, :boolean, default: false
    add_column :products, :trial_days, :integer, default: 0
  end
end
