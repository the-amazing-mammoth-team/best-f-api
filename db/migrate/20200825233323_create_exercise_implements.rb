class CreateExerciseImplements < ActiveRecord::Migration[6.0]
  def change
    create_table :exercise_implements do |t|
      t.references :exercise, null: false, foreign_key: true
      t.references :implement, null: false, foreign_key: true

      t.timestamps
    end
  end
end
