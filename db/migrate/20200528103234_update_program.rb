class UpdateProgram < ActiveRecord::Migration[6.0]
  def up
    add_column :programs, :pro, :boolean, default: true, null: false
    add_column :programs, :description, :string
  end

  def down
    remove_column(:programs, :pro)
    remove_column(:programs, :description)
  end
end
