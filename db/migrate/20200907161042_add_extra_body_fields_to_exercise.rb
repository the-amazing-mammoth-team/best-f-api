class AddExtraBodyFieldsToExercise < ActiveRecord::Migration[6.0]
  def change
    add_column :exercises, :family, :integer
    add_column :exercises, :sub_family, :integer
    add_column :exercises, :body_parts_focused, :string, array: true
    add_column :exercises, :muscles, :string, array: true
    add_column :exercises, :joints, :string, array: true
    add_index :exercises, :family
    add_index :exercises, :sub_family
    add_index :exercises, :body_parts_focused
  end
end
