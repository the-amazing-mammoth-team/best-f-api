class AddLevelsToProgram < ActiveRecord::Migration[6.0]
  def change
    add_column :programs, :strength, :integer, default: 0
    add_column :programs, :endurance, :integer, default: 0
    add_column :programs, :technique, :integer, default: 0
    add_column :programs, :flexibility, :integer, default: 0
    add_column :programs, :intensity, :integer, default: 0
  end
end
