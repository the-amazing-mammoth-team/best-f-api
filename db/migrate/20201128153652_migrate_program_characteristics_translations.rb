class MigrateProgramCharacteristicsTranslations < ActiveRecord::Migration[6.0]
  def change
    ActiveRecord::Base.transaction do
      ProgramCharacteristic.all.each do |pr|
        es_translation = get_translation(pr.id, 'es')
        en_translation = get_translation(pr.id, 'en')
        pr.value_es = es_translation['value'] if es_translation
        pr.value_en = en_translation['value'] if en_translation
        pr.save!
      end
    end
    remove_column(:program_characteristics, :value)
  end

  def get_translation(id, locale)
    conn = ActiveRecord::Base.connection
    @page_records = []
    result = conn.execute("SELECT * FROM program_characteristic_translations where program_characteristic_id = #{id} and locale = '#{locale}' ")
    result.first if result
  end
end
