class RemoveSessionIdToFromSessionSet < ActiveRecord::Migration[6.0]
  def change
    remove_column(:session_sets, :session_id)
  end
end
