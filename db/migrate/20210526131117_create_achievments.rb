class CreateAchievments < ActiveRecord::Migration[6.0]
  def change
    create_table :achievments do |t|
      t.string :name_en
      t.string :name_es
      t.string :icon_url
      t.string :congratulations_en
      t.string :congratulations_es
      t.string :description_en
      t.string :description_es
      t.integer :achievment_type
      t.integer :points

      t.timestamps
    end
  end
end
