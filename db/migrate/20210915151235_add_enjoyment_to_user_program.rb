class AddEnjoymentToUserProgram < ActiveRecord::Migration[6.0]
  def change
    add_column :user_programs, :enjoyment, :string
    add_column :user_programs, :enjoyment_notes, :string
  end
end
