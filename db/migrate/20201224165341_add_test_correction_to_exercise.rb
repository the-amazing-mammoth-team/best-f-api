class AddTestCorrectionToExercise < ActiveRecord::Migration[6.0]
  def change
    add_column :exercises, :test_correction, :decimal
  end
end
