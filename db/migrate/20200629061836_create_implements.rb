class CreateImplements < ActiveRecord::Migration[6.0]
  def change
    create_table :implements do |t|
      t.string :name
      t.references :exercise, null: false, foreign_key: true

      t.timestamps
    end
  end
end
