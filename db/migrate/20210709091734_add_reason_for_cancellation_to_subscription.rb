class AddReasonForCancellationToSubscription < ActiveRecord::Migration[6.0]
  def change
    add_column :subscriptions, :cancellation_reason, :string
  end
end
