class AddBodyParamsToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :gender, :integer, null: false
    add_column :users, :date_of_birth, :date, null: false
    add_column :users, :height, :float, null: false
    add_column :users, :weight, :float, null: false
    add_column :users, :activity_level, :integer, null: false
    add_column :users, :goal, :integer, null: false
    add_column :users, :body_type, :integer, null: false
    add_column :users, :body_fat, :float, null: false
    add_column :users, :newsletter_subscription, :boolean, null: false, default: false
  end
end