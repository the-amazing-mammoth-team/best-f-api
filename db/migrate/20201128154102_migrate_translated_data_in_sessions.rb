class MigrateTranslatedDataInSessions < ActiveRecord::Migration[6.0]
  def change
    ActiveRecord::Base.transaction do
      Session.all.each do |session|
        es_translation = get_translation(session.id, 'es')
        en_translation = get_translation(session.id, 'en')
        session.name_es = es_translation['name'] if es_translation
        session.description_es = es_translation['description'] if es_translation
        session.name_en = en_translation['name'] if en_translation
        session.description_en = en_translation['description'] if en_translation
        session.save!
      end
    end
    remove_column(:sessions, :name)
    remove_column(:sessions, :description)
  end
  def get_translation(id, locale)
    conn = ActiveRecord::Base.connection
    @page_records = []
    result = conn.execute("SELECT * FROM session_translations where session_id = #{id} and locale = '#{locale}' ")
    result.first if result
  end
end
