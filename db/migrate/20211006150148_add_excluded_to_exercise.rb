class AddExcludedToExercise < ActiveRecord::Migration[6.0]
  def change
    add_column :exercises, :excluded, :boolean, default: false
  end
end
