class AddTrackRepsToExerciseSet < ActiveRecord::Migration[6.0]
  def change
    add_column :exercise_sets, :track_reps, :boolean, default: false
  end
end
