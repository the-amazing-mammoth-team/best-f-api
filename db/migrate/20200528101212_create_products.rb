class CreateProducts < ActiveRecord::Migration[6.0]
  def change
    create_table :products do |t|
      t.integer :price
      t.string :name
      t.string :store_reference
      t.integer :currency
      t.string :local

      t.timestamps
    end
  end
end
