# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_01_19_075343) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "achievments", force: :cascade do |t|
    t.string "name_en"
    t.string "name_es"
    t.string "icon_url"
    t.string "congratulations_en"
    t.string "congratulations_es"
    t.string "description_en"
    t.string "description_es"
    t.integer "achievment_type"
    t.integer "points"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "active_admin_comments", force: :cascade do |t|
    t.string "namespace"
    t.text "body"
    t.string "resource_type"
    t.bigint "resource_id"
    t.string "author_type"
    t.bigint "author_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace"
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"
  end

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "daily_reports", force: :cascade do |t|
    t.jsonb "report_data"
    t.datetime "report_date"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "users_created"
    t.integer "subscriptions_activated"
    t.integer "workouts_completed"
    t.integer "workouts_discarded"
    t.decimal "subscriptions_money"
  end

  create_table "discount_coupons", force: :cascade do |t|
    t.string "stripe_id"
    t.bigint "product_id", null: false
    t.jsonb "stripe_body"
    t.decimal "discount_percentage"
    t.boolean "discount_forever"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["product_id"], name: "index_discount_coupons_on_product_id"
  end

  create_table "documents", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.string "associated_mode"
    t.bigint "user_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_documents_on_user_id"
  end

  create_table "exercise_executions", force: :cascade do |t|
    t.bigint "exercise_id", null: false
    t.bigint "session_set_execution_id", null: false
    t.integer "reps_executed"
    t.integer "execution_time"
    t.integer "order"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["exercise_id"], name: "index_exercise_executions_on_exercise_id"
    t.index ["session_set_execution_id"], name: "index_exercise_executions_on_session_set_execution_id"
  end

  create_table "exercise_implements", force: :cascade do |t|
    t.bigint "exercise_id", null: false
    t.bigint "implement_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["exercise_id"], name: "index_exercise_implements_on_exercise_id"
    t.index ["implement_id"], name: "index_exercise_implements_on_implement_id"
  end

  create_table "exercise_sets", force: :cascade do |t|
    t.bigint "session_set_id", null: false
    t.bigint "exercise_id", null: false
    t.integer "order"
    t.float "intensity_modificator"
    t.integer "time_duration"
    t.integer "reps"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "track_reps", default: false
    t.index ["exercise_id"], name: "index_exercise_sets_on_exercise_id"
    t.index ["session_set_id"], name: "index_exercise_sets_on_session_set_id"
  end

  create_table "exercise_translations", force: :cascade do |t|
    t.bigint "exercise_id", null: false
    t.string "locale", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "name"
    t.string "description"
    t.index ["exercise_id"], name: "index_exercise_translations_on_exercise_id"
    t.index ["locale"], name: "index_exercise_translations_on_locale"
  end

  create_table "exercises", force: :cascade do |t|
    t.string "video"
    t.integer "reps"
    t.integer "time"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "legacy_id"
    t.boolean "deprecated"
    t.integer "replacement_legacy_id"
    t.integer "family"
    t.integer "sub_family"
    t.string "body_parts_focused", array: true
    t.string "muscles", array: true
    t.string "joints", array: true
    t.decimal "met_multiplier"
    t.string "video_female"
    t.string "video_male"
    t.bigint "harder_variation_id"
    t.bigint "easier_variation_id"
    t.string "name_en"
    t.string "name_es"
    t.string "description_en"
    t.string "description_es"
    t.bigint "implement_variation_id"
    t.decimal "test_correction"
    t.string "thumbnail"
    t.string "thumbnail_male"
    t.string "thumbnail_female"
    t.string "notes_en"
    t.string "notes_es"
    t.decimal "execution_time"
    t.string "thumbnail_400"
    t.string "thumbnail_400_male"
    t.string "thumbnail_400_female"
    t.bigint "coach_id"
    t.bigint "test_equivalent_id"
    t.integer "t1_min", default: 0
    t.integer "t1_max", default: 1000
    t.boolean "excluded", default: false
    t.index ["body_parts_focused"], name: "index_exercises_on_body_parts_focused"
    t.index ["coach_id"], name: "index_exercises_on_coach_id"
    t.index ["easier_variation_id"], name: "index_exercises_on_easier_variation_id"
    t.index ["family"], name: "index_exercises_on_family"
    t.index ["harder_variation_id"], name: "index_exercises_on_harder_variation_id"
    t.index ["implement_variation_id"], name: "index_exercises_on_implement_variation_id"
    t.index ["legacy_id"], name: "index_exercises_on_legacy_id", unique: true
    t.index ["sub_family"], name: "index_exercises_on_sub_family"
    t.index ["test_equivalent_id"], name: "index_exercises_on_test_equivalent_id"
  end

  create_table "friendships", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "friend_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["friend_id"], name: "index_friendships_on_friend_id"
    t.index ["user_id"], name: "index_friendships_on_user_id"
  end

  create_table "implements", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "name_en"
    t.string "name_es"
  end

  create_table "offers", force: :cascade do |t|
    t.string "offer_code"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "product_offers", force: :cascade do |t|
    t.bigint "offer_id", null: false
    t.bigint "product_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "code_name"
    t.index ["offer_id"], name: "index_product_offers_on_offer_id"
    t.index ["product_id"], name: "index_product_offers_on_product_id"
  end

  create_table "product_programs", force: :cascade do |t|
    t.bigint "product_id", null: false
    t.bigint "program_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["product_id"], name: "index_product_programs_on_product_id"
    t.index ["program_id"], name: "index_product_programs_on_program_id"
  end

  create_table "products", force: :cascade do |t|
    t.decimal "price"
    t.string "name"
    t.string "store_reference"
    t.integer "currency"
    t.string "local"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "store"
    t.boolean "available", default: false
    t.boolean "has_trial", default: false
    t.integer "trial_days", default: 0
    t.decimal "discount_percentage"
    t.boolean "discount_forever"
    t.integer "period", default: 0
  end

  create_table "profiles", force: :cascade do |t|
    t.integer "gender"
    t.integer "activity_level"
    t.integer "goal"
    t.float "fat_level"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.float "max_fat_level"
    t.float "min_fat_level"
  end

  create_table "program_characteristic_translations", force: :cascade do |t|
    t.bigint "program_characteristic_id", null: false
    t.string "locale", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "value"
    t.index ["locale"], name: "index_program_characteristic_translations_on_locale"
    t.index ["program_characteristic_id"], name: "index_47d41cd15cc6c171dbc0131486149488b9f623ab"
  end

  create_table "program_characteristics", force: :cascade do |t|
    t.bigint "program_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "objective", default: false
    t.string "value_en"
    t.string "value_es"
    t.index ["program_id"], name: "index_program_characteristics_on_program_id"
  end

  create_table "program_implements", force: :cascade do |t|
    t.bigint "program_id", null: false
    t.bigint "implement_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["implement_id"], name: "index_program_implements_on_implement_id"
    t.index ["program_id"], name: "index_program_implements_on_program_id"
  end

  create_table "program_profiles", force: :cascade do |t|
    t.bigint "program_id", null: false
    t.bigint "profile_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["profile_id"], name: "index_program_profiles_on_profile_id"
    t.index ["program_id"], name: "index_program_profiles_on_program_id"
  end

  create_table "program_sessions", force: :cascade do |t|
    t.bigint "program_id", null: false
    t.bigint "session_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["program_id"], name: "index_program_sessions_on_program_id"
    t.index ["session_id"], name: "index_program_sessions_on_session_id"
  end

  create_table "program_translations", force: :cascade do |t|
    t.bigint "program_id", null: false
    t.string "locale", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "name"
    t.text "description"
    t.index ["locale"], name: "index_program_translations_on_locale"
    t.index ["program_id"], name: "index_program_translations_on_program_id"
  end

  create_table "programs", force: :cascade do |t|
    t.bigint "user_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "pro", default: true, null: false
    t.boolean "available", default: false
    t.integer "strength", default: 0
    t.integer "endurance", default: 0
    t.integer "technique", default: 0
    t.integer "flexibility", default: 0
    t.integer "intensity", default: 0
    t.string "code_name"
    t.string "name_es"
    t.string "name_en"
    t.string "description_en"
    t.string "description_es"
    t.boolean "auto_generated", default: false
    t.integer "priority_order"
    t.integer "next_program_id"
  end

  create_table "session_block_executions", force: :cascade do |t|
    t.bigint "session_execution_id", null: false
    t.integer "block_type"
    t.integer "reps_executed"
    t.integer "order"
    t.integer "execution_time"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["session_execution_id"], name: "index_session_block_executions_on_session_execution_id"
  end

  create_table "session_blocks", force: :cascade do |t|
    t.bigint "session_id", null: false
    t.integer "time_duration"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "order"
    t.integer "block_type"
    t.boolean "loop", default: false
    t.index ["session_id"], name: "index_session_blocks_on_session_id"
  end

  create_table "session_execution_summaries", force: :cascade do |t|
    t.bigint "session_execution_id", null: false
    t.integer "total_reps"
    t.integer "total_time"
    t.integer "reps_per_min"
    t.integer "total_kcal"
    t.jsonb "reps_per_exercise"
    t.jsonb "mins_per_exercise"
    t.jsonb "reps_per_min_per_exercise"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.jsonb "reps_set_per_block"
    t.jsonb "time_set_per_block"
    t.jsonb "average_reps_min_set_per_block"
    t.jsonb "reps_min_set_block"
    t.integer "effort"
    t.integer "points"
    t.integer "value_of_session"
    t.jsonb "body_parts_spider"
    t.string "name"
    t.index ["session_execution_id"], name: "index_session_execution_summaries_on_session_execution_id"
  end

  create_table "session_executions", force: :cascade do |t|
    t.date "scheduled_at"
    t.bigint "user_program_id", null: false
    t.integer "difficulty_feedback"
    t.integer "enjoyment_feedback"
    t.string "feedback_comment"
    t.integer "reps_executed"
    t.integer "execution_time"
    t.integer "order"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.uuid "front_end_id"
    t.bigint "session_id", null: false
    t.boolean "discarded", default: false
    t.integer "discard_reason"
    t.boolean "imported", default: false
    t.index ["session_id"], name: "index_session_executions_on_session_id"
    t.index ["user_program_id"], name: "index_session_executions_on_user_program_id"
  end

  create_table "session_historic_summaries", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "session_id", null: false
    t.jsonb "reps_per_min"
    t.jsonb "friends_reps_per_min"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["session_id"], name: "index_session_historic_summaries_on_session_id"
    t.index ["user_id"], name: "index_session_historic_summaries_on_user_id"
  end

  create_table "session_set_executions", force: :cascade do |t|
    t.integer "reps_executed"
    t.integer "execution_time"
    t.integer "order"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "session_block_execution_id", null: false
    t.index ["session_block_execution_id"], name: "index_session_set_executions_on_session_block_execution_id"
  end

  create_table "session_sets", force: :cascade do |t|
    t.integer "order"
    t.integer "level"
    t.integer "time_duration"
    t.integer "reps"
    t.integer "session_set_type"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "session_block_id", null: false
    t.boolean "loop", default: false
    t.index ["session_block_id"], name: "index_session_sets_on_session_block_id"
  end

  create_table "session_translations", force: :cascade do |t|
    t.bigint "session_id", null: false
    t.string "locale", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "name"
    t.text "description"
    t.index ["locale"], name: "index_session_translations_on_locale"
    t.index ["session_id"], name: "index_session_translations_on_session_id"
  end

  create_table "sessions", force: :cascade do |t|
    t.integer "level"
    t.integer "order"
    t.integer "session_type"
    t.integer "time_duration"
    t.integer "reps"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "strength", default: 0
    t.integer "endurance", default: 0
    t.integer "technique", default: 0
    t.integer "flexibility", default: 0
    t.integer "intensity", default: 0
    t.string "code_name"
    t.string "name_en"
    t.string "name_es"
    t.string "description_en"
    t.string "description_es"
    t.integer "calories"
    t.bigint "warmup_id"
    t.bigint "cooldown_id"
    t.index ["cooldown_id"], name: "index_sessions_on_cooldown_id"
    t.index ["warmup_id"], name: "index_sessions_on_warmup_id"
  end

  create_table "subscriptions", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "product_id", null: false
    t.bigint "program_id", null: false
    t.integer "platform"
    t.text "transaction_body"
    t.integer "status"
    t.datetime "start_date"
    t.datetime "end_date"
    t.integer "subscription_type"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.date "cancelled_at"
    t.boolean "cancelled", default: false
    t.jsonb "store_metadata"
    t.string "affiliate_code"
    t.string "offer_code"
    t.string "cancellation_reason"
    t.jsonb "receipt_data"
    t.index ["product_id"], name: "index_subscriptions_on_product_id"
    t.index ["program_id"], name: "index_subscriptions_on_program_id"
    t.index ["user_id"], name: "index_subscriptions_on_user_id"
  end

  create_table "user_achievments", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "achievment_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["achievment_id"], name: "index_user_achievments_on_achievment_id"
    t.index ["user_id"], name: "index_user_achievments_on_user_id"
  end

  create_table "user_implements", force: :cascade do |t|
    t.bigint "implement_id", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["implement_id"], name: "index_user_implements_on_implement_id"
    t.index ["user_id"], name: "index_user_implements_on_user_id"
  end

  create_table "user_programs", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "program_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "active", default: true
    t.bigint "current_session_id"
    t.boolean "completed", default: false
    t.string "enjoyment"
    t.string "enjoyment_notes"
    t.index ["current_session_id"], name: "index_user_programs_on_current_session_id"
    t.index ["program_id"], name: "index_user_programs_on_program_id"
    t.index ["user_id"], name: "index_user_programs_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "gender", null: false
    t.date "date_of_birth", null: false
    t.float "height", null: false
    t.float "weight", null: false
    t.integer "activity_level", null: false
    t.integer "goal", null: false
    t.integer "body_type", null: false
    t.float "body_fat", null: false
    t.boolean "newsletter_subscription", default: false, null: false
    t.boolean "is_admin"
    t.string "names"
    t.string "last_name"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.integer "recover_password_code"
    t.integer "recover_password_attempts", default: 0
    t.string "facebook_uid"
    t.boolean "workout_setting_voice_coach", default: true
    t.boolean "workout_setting_sound", default: true
    t.boolean "workout_setting_vibration", default: true
    t.boolean "workout_setting_mobility", default: true
    t.boolean "workout_setting_cardio_warmup", default: true
    t.boolean "workout_setting_countdown", default: true
    t.boolean "notifications_setting", default: true
    t.integer "training_days_setting", default: 1
    t.string "google_uid"
    t.string "language", default: "en"
    t.string "country"
    t.integer "points", default: 0
    t.boolean "scientific_data_usage", default: false
    t.integer "t1_push", default: 0
    t.integer "t1_core", default: 0
    t.integer "t1_legs", default: 0
    t.integer "t1_full", default: 0
    t.integer "t1_push_exercise", default: 0
    t.integer "t1_pull_up", default: 0
    t.integer "t2_reps", default: 0
    t.integer "t2_steps", default: 0
    t.integer "t2_reps_push", default: 0
    t.integer "t2_reps_core", default: 0
    t.integer "t2_reps_legs", default: 0
    t.integer "t2_reps_full", default: 0
    t.integer "t2_time_push", default: 0
    t.integer "t2_time_core", default: 0
    t.integer "t2_time_legs", default: 0
    t.integer "t2_time_full", default: 0
    t.integer "t1_full_exercise", default: 0
    t.integer "t1_pull_up_exercise"
    t.boolean "warmup_setting"
    t.integer "warmup_session_id"
    t.string "stripe_id"
    t.string "provider", default: "", null: false
    t.string "uid", default: "", null: false
    t.integer "best_weekly_streak", default: 0
    t.integer "current_weekly_streak", default: 0
    t.string "affiliate_code"
    t.string "affiliate_code_signup"
    t.integer "total_sessions"
    t.integer "total_time"
    t.decimal "kcal_per_session"
    t.integer "reps_per_session"
    t.string "moengage_id"
    t.string "mix_panel_id"
    t.string "apple_id_token"
    t.boolean "imported", default: false
    t.string "platform"
    t.uuid "login_token"
    t.datetime "login_token_generated_at"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "versions", force: :cascade do |t|
    t.string "item_type", null: false
    t.bigint "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.text "object"
    t.datetime "created_at"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "discount_coupons", "products"
  add_foreign_key "exercise_executions", "exercises"
  add_foreign_key "exercise_executions", "session_set_executions"
  add_foreign_key "exercise_implements", "exercises"
  add_foreign_key "exercise_implements", "implements"
  add_foreign_key "exercise_sets", "exercises"
  add_foreign_key "exercise_sets", "session_sets"
  add_foreign_key "exercises", "exercises", column: "easier_variation_id"
  add_foreign_key "exercises", "exercises", column: "harder_variation_id"
  add_foreign_key "exercises", "exercises", column: "implement_variation_id"
  add_foreign_key "exercises", "exercises", column: "test_equivalent_id"
  add_foreign_key "exercises", "users", column: "coach_id"
  add_foreign_key "friendships", "users"
  add_foreign_key "friendships", "users", column: "friend_id"
  add_foreign_key "product_offers", "offers"
  add_foreign_key "product_offers", "products"
  add_foreign_key "product_programs", "products"
  add_foreign_key "product_programs", "programs"
  add_foreign_key "program_characteristics", "programs"
  add_foreign_key "program_implements", "implements"
  add_foreign_key "program_implements", "programs"
  add_foreign_key "program_profiles", "profiles"
  add_foreign_key "program_profiles", "programs"
  add_foreign_key "program_sessions", "programs"
  add_foreign_key "program_sessions", "sessions"
  add_foreign_key "programs", "users"
  add_foreign_key "session_block_executions", "session_executions"
  add_foreign_key "session_blocks", "sessions"
  add_foreign_key "session_execution_summaries", "session_executions"
  add_foreign_key "session_executions", "sessions"
  add_foreign_key "session_executions", "user_programs"
  add_foreign_key "session_historic_summaries", "sessions"
  add_foreign_key "session_historic_summaries", "users"
  add_foreign_key "session_set_executions", "session_block_executions"
  add_foreign_key "session_sets", "session_blocks"
  add_foreign_key "sessions", "sessions", column: "cooldown_id"
  add_foreign_key "sessions", "sessions", column: "warmup_id"
  add_foreign_key "subscriptions", "products"
  add_foreign_key "subscriptions", "programs"
  add_foreign_key "subscriptions", "users"
  add_foreign_key "user_achievments", "achievments"
  add_foreign_key "user_achievments", "users"
  add_foreign_key "user_implements", "implements"
  add_foreign_key "user_implements", "users"
  add_foreign_key "user_programs", "programs"
  add_foreign_key "user_programs", "sessions", column: "current_session_id"
  add_foreign_key "user_programs", "users"
end
